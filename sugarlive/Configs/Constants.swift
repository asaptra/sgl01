//
//  Constants.swift
//  sugarlive
//
//  Created by msi on 09/09/21.
//

import Foundation
import UIKit
import ObjectMapper

class Constant: NSObject {
    public static let isDebug = true   // true - development / false - production
    
    public static let baseUrlDevelopment = "https://hostdev.sugarlive.co.id/v2"
    public static let baseUrlProduction = ""
    public static let baseUrl = isDebug ? baseUrlDevelopment : baseUrlProduction
    
    public static let apiKey = "AhLN6V5R9zIt4HdXKhtVfICID55VXrwFlXDaKjhH"
    public static let fontFamilyName = "OpenSans"
    
    public static var isLogin = false
    public static let screenSize = UIScreen.main.bounds.size
    public static let currentDevice = UIDevice.current
    
    public static let GoogleClientID = "1094017337065-flhjscmeblv201qb7annh72qc1nl085e.apps.googleusercontent.com"
    
    public static let kTeamID = "ZRY5YFW34W"
    public static let kID = "52TFTR7K45"
    public static let kServiceID = "com.sugarlive.store"//"com.citratalentagemilang.sugarlive.client"
    public static let kRedirectUri = "https://store.sugarlive.co.id/api/auth/apple"//"https://example-app.com/redirect"
    
    public static let ZegoAppID: UInt32 = 3387446559
    public static let ZegoAppSign = "681bf0465b1facd99b7f87676d912eb54632e501f2f419d50fe9edc3c1740ced"
    
    public static let PusherAppID = "733514"
    public static let PusherKeyID = "f7493a9afb57067a6c5c"
    public static let PusherAppSecret = "ea9fd95d46bdf3261c50"
    public static let PusherCluster = "ap1"
    
    //public static let AdminPhoneNumber = "+6287786346464"
    public static let AdminPhoneNumber = "+6285882908928"
    public static let CustServicePhoneNumber = "+6281292680913"
}

class PusherEvents: NSObject {
    // Live Events
    public static let INVITATION = "invitation"
    public static let BATTLE_END = "battleEnd"
    public static let ACCEPT_CHALLANGE = "accept_challenge"
    public static let BATTLE_INFO = "battleInfo"
    public static let GUEST_LEAVE = "guest_leave"
    public static let KICK_MESSAGE = "kickmessage"
    public static let MODERATION = "moderation"
    public static let FOLLOW = "follow"
    public static let JOIN_REQUEST = "join_request"
    public static let CARROT_UPDATE = "carrot_update"
    
    // Watch Events
    public static let STOP = "stop"
    public static let KICK_GUEST = "kick_guest"
    
    // ZEGO Data Channel Live Events
    public static let TURN_OFF_AUDIO_VIDEO = "TURN_OFF_AUDIO_VIDEO"
    public static let CHAT = "chat"
    public static let ROOM_ADMIN = "roomAdmin"
    public static let STICKER = "sticker"
    public static let GIFT = "gift"
    public static let JACKPOT = "jackpot"
    public static let CHRISTMAS_2EARNING = "christmas2Earning"
    public static let DONOR_EARNING = "donorEarning"
    public static let BID_PLACED = "bidPlaced"
    public static let BID_ACCEPTED = "bidAccepted"
    public static let GAME_SUIT_RESULT = "gameSuitResult"
    public static let GAME_SUIT_CLOSE = "gameSuitClose"
    
    // ZEGO Data Channel Watch Events
    public static let MUTE_ROOM = "mute_room"
    public static let BID_RESPONDED = "bidResponded"
    public static let PRIVATE_ROOM = "privateRoom"
    
    //Direct Message
    public static let NEW_MESSAGE = "new-message"
}

enum ScreenSize {
    case ScreenSizeCurrent
    case ScreenSize3p5Inch
    case ScreenSize4Inch
    case ScreenSize4p7Inch
    case ScreenSize5p5Inch
    case ScreenSize7p9Inch
    case ScreenSize9p7Inch
    case ScreenSize12p9Inch
}

enum KeyboardInputType {
    case Default
    case Alphabet
    case Alphanumeric
    case Sentence
    case Numeric
    case Phone
    case Nominal
    case NominalDecimal
}

let headerHTML = "<html><head><link href=' http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'></head><body>%@</body></html><style> body { font-family: 'Open Sans'; font-size: 16; color: 6D6D6D;}; li { font-weight: bold;}; li p { font-weight: normal;} </style>"

private let helpMenus: [[String: Any]] = [["header": "Bagaimana cara mendapatkan Candy dan Carrot?", "menu": [["title": "<p>Untuk mendapatkan candy :</p><ol><li><p>Anda dapat membeli candy dengan mengunjungi halaman profil anda dan mengetuk ikon \"Pengaturan\"->\"My Wallet\", kemudian pilih opsi Top Up untuk menambah candy.</p></li><li><p>Anda juga dapat membeli candy dengan mengetuk gambar kotak hadiah diruangan penyiaran lalu pilih opsi Top Up.</p></li></ol><p>Untuk mendapatkan Carrot :<br />Carrot tidak bisa dibeli. Ketika penonton mengirimkan Anda hadiah, maka hadiah tersebut akan berubah menjadi sejumlah carrot yang sama dan disimpan dalam dompet Anda.</p>", "icon": ""]]],
                                          ["header": "Bagaimana cara naik tingkat?", "menu": [["title": "", "icon": ""]]],
                                          ["header": "Bagaimana cara menghubungi pelayanan pelanggan?", "menu": [["title": "", "icon": ""]]],
                                          ["header": "Saya telah dilarang bersiaran, apa yang harus saya lakukan?", "menu": [["title": "", "icon": ""]]]]
let helpModel: [SettingModel] = Mapper<SettingModel>().mapArray(JSONObject: helpMenus) ?? []

private let ruleMenus: [[String: Any]] = [["header": "Peraturan Umum untuk Penyiar, Agensi, dan Pengguna. ", "menu": [["title": "<p>Sesuai dengan PERATURAN KOMISI PENYIARAN INDONESIA Nomor 02/P/KPI/03/2012 tentang STANDAR PROGRAM SIARAN maka setiap penyiar maupun pengguna di aplikasi SUGARLIVE harus diarahkan agar: </p><ol><li><p>Menjunjung tinggi dan meningkatkan rasa persatuan dan kesatuan Negara Kesatuan Republik Indonesia.</p></li><li><p>Meningkatkan kesadaran dan ketaatan terhadap hukum dan segenap peraturan perundang-undangan yang berlaku di Indonesia.</p></li><li><p>Menghormati dan menjunjung tinggi norma dan nilai agama dan budaya bangsa yang multicultural.</p></li><li><p>Menghormati dan menjunjung tinggi etika profesi yang diakui oleh peraturan perundang-undangan. </p></li><li><p>Menghormati dan menjunjung tinggi prinsip prinsip demokrasi.</p></li><li><p>Menghormati dan menjunjung tinggi hak asasi manusia.</p></li><li><p>Menghormati dan menjunjung tinggi hak dan kepentingan publik.</p></li><li><p>Menghormati dan menjunjung tinggi hak anak-anak dan remaja.</p></li><li><p>Menghormati dan menjunjung tinggi hak orang dan/atau kelompok masyarakat tertentu.</p></li><li><p>Menjunjung tinggi prinsip-prinsip jurnalistik.</p></li></ol><p>http://www.kpi.go.id/download/regulasi/P3SPS_2012_Final.pdf </p><p>PERATURAN KOMISI PENYIARAN INDONESIA Nomor 01/P/KPI/03/2012 tentang PEDOMAN PERILAKU PENYIARAN<br /><br />PERATURAN KOMISI PENYIARAN INDONESIA Nomor 02/P/KPI/03/2012 tentang STANDAR PROGRAM SIARAN</p><style> li { font-weight: bold;} li p { font-weight: normal;}</style>", "icon": ""]]],
                                          ["header": "Bagaimana cara naik tingkat?", "menu": [["title": "", "icon": ""]]],
                                          ["header": "Bagaimana cara menghubungi pelayanan pelanggan?", "menu": [["title": "", "icon": ""]]],
                                          ["header": "Saya telah dilarang bersiaran, apa yang harus saya lakukan?", "menu": [["title": "", "icon": ""]]]]
let ruleModel: [SettingModel] = Mapper<SettingModel>().mapArray(JSONObject: ruleMenus) ?? []

let howTopupHTML = "<p>How to topup</p><ol><li>Anda dapat melakukan isi ulang dengan cara memilih salah satu channel pembayaran yang sudah tersedia di halaman depan.</li><li>Setelah itu pilihlah jumlah Candy yang ingin Anda beli. Setiap channel memiliki harga dan jumlah candy yg berbeda, Pilihlah Channel pembayaran yg terbaik menurut Anda.</li><li>Ikuti setiap langkah dan proses Pembayaran yang harus Anda lakukan sesuai kebijakan setiap channel yang telah dipilih.</li><li>Candy akan otomatis masuk ke Akun Sugarlive Anda jika transaksi telah berhasil.</li></ol><style> p { font-weight: bold; font-size: 16px; color: #2C004D;} li { color: #6D6D6D; font-size: 16px;}</style>"
