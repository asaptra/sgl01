//
//  Data+Extension.swift
//  sugarlive
//
//  Created by msi on 23/09/21.
//

import Foundation

extension Data {
  var hexString: String {
    let hexString = map { String(format: "%02.2hhx", $0) }.joined()
    return hexString
  }
  
  func base64EncodedURLString() -> String {
    return base64EncodedString()
      .replacingOccurrences(of: "=", with: "")
      .replacingOccurrences(of: "+", with: "-")
      .replacingOccurrences(of: "/", with: "_")
  }
}
