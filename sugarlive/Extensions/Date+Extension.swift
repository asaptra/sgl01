//
//  Date+Extension.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import Foundation

extension Date {
  func toString(with format: String, timezone: TimeZone = TimeZone(abbreviation: "UTC")!, locale: Locale = Locale(identifier: "en_US_POSIX")) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    dateFormatter.timeZone = timezone
    dateFormatter.locale = locale
    return dateFormatter.string(from: self)
  }
    
    func localDate() -> Date {
        let nowUTC = Date()
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: nowUTC))
        guard let localDate = Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: nowUTC) else {return Date()}

        return localDate
    }
  
  func timeAgoDisplay() -> String {
    let secondsAgo = Int(Date().timeIntervalSince(self))

    let minute = 60
    let hour = 60 * minute
    let day = 24 * hour
    let week = 30 * day
    let month = 2 * week

    let quotient: Int
    let unit: String
    if secondsAgo < minute {
        return "Just now"
    } else if secondsAgo < hour {
        quotient = secondsAgo / minute
        unit = "min"
    } else if secondsAgo < day {
        quotient = secondsAgo / hour
        unit = "h"
    } else if secondsAgo < week {
        quotient = secondsAgo / day
        unit = "d"
    } else if secondsAgo <= month {
        quotient = secondsAgo / month
        unit = "mo"
    } else {
        return self.toString(with: "dd-MM-yy")
    }
//    return "\(quotient) \(unit)\(quotient == 1 ? "" : "s") ago"
    return "\(quotient) \(unit) ago"
  }
    
    func chatTimeAgo() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))

        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 30 * day
        let month = 2 * week
        let quotient: Int
        
        if secondsAgo < day {
            quotient = secondsAgo / hour
            return self.toString(with: "HH:mm")
        } else if secondsAgo < week {
            quotient = secondsAgo / day
            if quotient == 1 {
                return "Yesterday"
            }
        } else if secondsAgo <= month {
            quotient = secondsAgo / month
            if quotient == 12 {
                return self.toString(with: "dd MMM HH:mm")
            }
        }
        return self.toString(with: "dd MMM yy")
    }

  func toSeconds() -> Int {
    let components = Calendar(identifier: .gregorian).dateComponents(in: TimeZone(abbreviation: "UTC")!, from: self)
    var seconds = 0
    seconds += (components.hour ?? 0) * 3600
    seconds += (components.minute ?? 0) * 60
    seconds += components.second ?? 0
    return seconds
  }
  
  func addDate(day: Int = 0) -> Date {
    return Calendar.current.date(byAdding: .day, value: day, to: self) ?? Date()
  }
}
