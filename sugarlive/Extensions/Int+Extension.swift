//
//  Int+Extension.swift
//  sugarlive
//
//  Created by msi on 06/10/21.
//

import Foundation

extension Int {
  func roundedWithAbbreviations() -> String {
    let num = abs(Double(self))
    let sign = (self < 0) ? "-" : ""
    
    switch num {
    case 1_000_000_000...:
      var formatted = num / 1_000_000_000
      formatted = formatted.reduceScale(to: 1)
      return "\(sign)\(formatted) B"
      
    case 1_000_000...:
      var formatted = num / 1_000_000
      formatted = formatted.reduceScale(to: 1)
      return "\(sign)\(formatted) M"
      
    case 1_000...:
      var formatted = num / 1_000
      formatted = formatted.reduceScale(to: 1)
      return "\(sign)\(formatted) K"
      
    case 0...:
      return "\(self)"
      
    default:
      return "\(sign)\(self)"
    }
  }
  
  func toTimeFormat(units: NSCalendar.Unit) -> String {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = units
    formatter.unitsStyle = .short
    
    return formatter.string(from: TimeInterval(self))!
  }
}
