//
//  UIColor+Extension.swift
//  sugarlive
//
//  Created by msi on 09/09/21.
//

import UIKit

extension UIColor {
  convenience init(hex: String) {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
      cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
      self.init()
      return
    }
    
    let scanner = Scanner(string: cString)
    scanner.scanLocation = 0
    
    var rgbValue: UInt64 = 0
    
    scanner.scanHexInt64(&rgbValue)
    
    let r = (rgbValue & 0xff0000) >> 16
    let g = (rgbValue & 0xff00) >> 8
    let b = rgbValue & 0xff
    
    self.init(
      red: CGFloat(r) / 0xff,
      green: CGFloat(g) / 0xff,
      blue: CGFloat(b) / 0xff, alpha: 1
    )
  }
    
    static let PRIMARY_1 = UIColor(hex: "#2C004D")
    static let PRIMARY_2 = UIColor(hex: "#582BC6")
    static let DARK_TEXT = UIColor(hex: "#444444")
    static let MEDIUM_TEXT = UIColor(hex: "#6D6D6D")
    static let LIGHT_TEXT = UIColor(hex: "#929292")
    static let NEGATIVE_TEXT = UIColor(hex: "#FF3D3D")
    static let BORDER_LIST = UIColor(hex: "#DCDCDC")
    static let GREEN_SUCCESS = UIColor(hex: "#5CC219")
    static let ORANGE_PROCESS = UIColor(hex: "#FFBD09")
    static let RED_FAILED = UIColor(hex: "#EC2020")
    
}
