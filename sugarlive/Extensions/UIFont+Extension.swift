//
//  UIFont+Extension.swift
//  sugarlive
//
//  Created by msi on 09/09/21.
//

import UIKit

extension UIFont {
  static func defaultFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName, size: size) ?? UIFont.systemFont(ofSize: size)
  }
  
  static func defaultBoldFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-Bold", size: size) ?? UIFont.boldSystemFont(ofSize: size)
  }
  
  static func defaultExtraBoldFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-ExtraBold", size: size) ?? UIFont.boldSystemFont(ofSize: size)
  }
  
  static func defaultItalicFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-Italic", size: size) ?? UIFont.italicSystemFont(ofSize: size)
  }
  
  static func defaultLightFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-Light", size: size) ?? UIFont.systemFont(ofSize: size)
  }
  
  static func defaultBoldItalicFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-BoldItalic", size: size) ?? UIFont.boldSystemFont(ofSize: size)
  }
  
  static func defaultExtraBoldItalicFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-ExtraBoldItalic", size: size) ?? UIFont.boldSystemFont(ofSize: size)
  }
  
  static func defaultLightItalicFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-LightItalic", size: size) ?? UIFont.systemFont(ofSize: size)
  }
  
  static func defaultSemiBoldFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-SemiBold", size: size) ?? UIFont.boldSystemFont(ofSize: size)
  }
  
  static func defaultSemiBoldItalicFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-SemiBoldItalic", size: size) ?? UIFont.boldSystemFont(ofSize: size)
  }
  
  static func defaultRegularFont(size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: Constant.fontFamilyName+"-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
  }
  
  static func customFont(fontFamilyName: String, size: CGFloat = 12.0) -> UIFont {
    let size = self.scaledFontForReference(screenSize: .ScreenSize4Inch, size: size)
    return UIFont(name: fontFamilyName, size: size) ?? UIFont.systemFont(ofSize: size)
  }
  
  static func screenWidthFor(screenSize: ScreenSize) -> CGFloat {
    switch screenSize {
    case .ScreenSize4Inch:
      return 320
    case .ScreenSize4p7Inch:
      return 375
    case .ScreenSize5p5Inch:
      return 414
    case .ScreenSize9p7Inch:
      return 768
    case .ScreenSize12p9Inch:
      return 1024
    default:
      return Constant.screenSize.width
    }
  }
  
  static func fontPointSizeMultiplierForReference(screenSize: ScreenSize) -> CGFloat {
//    return Constant.screenSize.width / screenWidthFor(screenSize: screenSize)
      return 1
  }
  
  static func scaledFontForReference(screenSize: ScreenSize, size: CGFloat) -> CGFloat {
    let multiplier = fontPointSizeMultiplierForReference(screenSize: screenSize)
    var finalSize = size
    if (multiplier != 1) {
      finalSize = size * multiplier
    }
    return finalSize
  }
}

