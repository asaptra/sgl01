//
//  UIView+Extension.swift
//  sugarlive
//
//  Created by msi on 09/09/21.
//

import UIKit

extension UIView {
    @IBInspectable
        var cornerRadius: CGFloat {
            get {
                return self.layer.cornerRadius
            } set {
                self.layer.cornerRadius = newValue
            }
        }
    
    static func nib<T: UIView>(withType type: T.Type) -> T {
        return Bundle.main.loadNibNamed(String(describing: type), owner: self, options: nil)?.first as! T
    }
    
    func applyCardView(cornerRadius: CGFloat = 10,
                       shadowOffsetWidth: Int = 1,
                       shadowOffsetHeight: Int = 1,
                       shadowColor: UIColor = UIColor.lightGray,
                       shadowOpacity: Float = 0.5) {
        
        self.layer.cornerRadius = cornerRadius
        //    let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        self.layer.shadowOpacity = shadowOpacity
        //        self.layer.shadowPath = shadowPath.cgPath
        
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func makeSecure() {
        DispatchQueue.main.async {
            let field = UITextField()
            field.isSecureTextEntry = true
            self.addSubview(field)
            field.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            field.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
            self.layer.superlayer?.addSublayer(field.layer)
            field.layer.sublayers?.first?.addSublayer(self.layer)
        }
    }
}
