//
//  ModerationModel.swift
//  sugarlive
//
//  Created by msi on 24/09/21.
//

import Foundation
import ObjectMapper
import Moya

struct BanCategoryModel: Mappable {
  var categoryId: String?
  var name: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    categoryId <- map["category_id"]
    name <- map["name"]
  }
}

struct BanReasonModel: Mappable {
  var reasonId: String?
  var reason: String?
  var type: String?
  var banClass: String?
  var duration: Int = 0
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    reasonId <- map["reason_id"]
    reason <- map["reason"]
    type <- map["type"]
    banClass <- map["class"]
    duration <- map["duration"]
  }
}

struct BanDurationModel: Mappable {
  var durationInHours: Int = 0
  var description: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    durationInHours <- map["durationInHours"]
    description <- map["description"]
  }
}

class BanCategoryResponseModel: SuccessResponseModel {
  var attributes: [BanCategoryModel] = []
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}

class BanReasonResponseModel: SuccessResponseModel {
  var attributes: [BanReasonModel] = []
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}

class BanDurationResponseModel: SuccessResponseModel {
  var attributes: [BanDurationModel] = []
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}

class FeedbackRequestModel: UploadRequestModel {
  var email: String?
  var message: String?
  
  var feedbackFormData: [MultipartFormData] {
    var formData = self.formData
    if let content = email, let emailData = content.data(using: .utf8) {
      formData.append(MultipartFormData(provider: .data(emailData), name: "email"))
    }
    if let content = message, let messageData = content.data(using: .utf8) {
      formData.append(MultipartFormData(provider: .data(messageData), name: "message"))
    }
    return formData
  }
  
  override func mapping(map: Map) {
    email <- map["email"]
    message <- map["message"]
  }
}

struct ModerationRequestModel: Mappable {
  var action: String?
  var userId: String?
  var banDurationInHours: Int = 0
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    action <- map["action"]
    userId <- map["userId"]
    banDurationInHours <- map["banDurationInHours"]
  }
}
