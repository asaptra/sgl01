//
//  BannerModel.swift
//  sugarlive
//
//  Created by msi on 23/09/21.
//

import Foundation
import ObjectMapper

struct BannerModel: Mappable {
  var id: Int?
  var baner: String?
  var urlImage: String?
  var urlLink: String?
  var bannerPosition: NSNumber?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    id <- map["id"]
    baner <- map["baner"]
    urlImage <- map["url_image"]
    urlLink <- map["url_link"]
    bannerPosition <- map["banner_position"]
  }
}

class BannerResponseModel: SuccessResponseModel {
  var attributes: [BannerModel]?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}

class EventResponseModel: SuccessResponseModel {
  var attributes: [EventModel]?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    meta <- map["meta"]
    attributes <- map["attributes"]
  }
}

struct EventModel: Mappable {
  
  var name: String?
  var image: String?
  var link: String?
  var startDate: String?
  var endDate: String?

  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    name <- map["name"]
    image <- map["image"]
    link <- map["link"]
    startDate <- map["start_date"]
    endDate <- map["end_date"]
  }
}

struct EntranceEffectsModel: Mappable {
  var type: String?
  var name: String?
  var entrance_effect_id: String?
  var image: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    type <- map["type"]
    name <- map["name"]
    entrance_effect_id <- map["entrance_effect_id"]
    image <- map["image"]
  }
}

class EntranceEffectResponseModel: SuccessResponseModel {
  var attributes: [EntranceEffectsModel]?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}
