//
//  BattleModel.swift
//  sugarlive
//
//  Created by cleanmac-ada on 20/06/22.
//

import Foundation
import ObjectMapper


struct StartBattleRequestModel: Mappable {
    var multiHostId: String?
    var duration: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        multiHostId <- map["multiHostId"]
        duration <- map["duration"]
    }
}

struct StopBattleRequestModel: Mappable {
    var multiHostId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        multiHostId <- map["multiHostId"]
    }
}

struct BattleTopSpenderModel: Mappable {
    var sugarId: String?
    var userId: String?
    var coverPicture: String?
    var fullname: String?
    var spend: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        sugarId <- map["sugar_id"]
        userId <- map["user_id"]
        coverPicture <- map["cover_picture"]
        fullname <- map["fullname"]
        spend <- map["spend"]
    }
    
}

struct BattleInfoUserModel: Mappable {
    var userId: String?
    var liveId: String?
    var isHost: Bool?
    var position: Int?
    var progress: Double?
    var earnedGifts: String?
    var topSpender: [BattleTopSpenderModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["user_id"]
        liveId <- map["live_id"]
        isHost <- map["is_host"]
        position <- map["position"]
        progress <- map["progress"]
        earnedGifts <- map["earnedGifts"]
        topSpender <- map["topSpender"]
    }
}

struct BattleInfoEventModel: Mappable {
    var battleId: String?
    var startAt: String?
    var endAt: String?
    var remaining: Int?
    var duration: Int?
    var result: [BattleInfoUserModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        battleId <- map["battle_id"]
        startAt <- map["start_at"]
        endAt <- map["end_at"]
        remaining <- map["remaining"]
        duration <- map["duration"]
        result <- map["result"]
    }
}
