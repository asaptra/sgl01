//
//  BidModel.swift
//  sugarlive
//
//  Created by cleanmac-ada on 18/05/22.
//

import Foundation
import ObjectMapper

// MARK: - Event models
struct BidRespondedEventModel: Mappable {
    var receiverId: String?
    var senderId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        receiverId <- map["data.receiver.userId"]
        senderId <- map["data.sender.userId"]
    }
}

struct BidPlacedEventModel: Mappable {
    var receiverId: String?
    var senderId: String?
    var bidId: String?
    var amount: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        receiverId <- map["data.receiver.userId"]
        senderId <- map["data.sender.userId"]
        bidId <- map["data.bid.id"]
        amount <- map["data.amount"]
    }
}

struct BidAcceptedEventModel: Mappable {
    var receiverId: String?
    var senderId: String?
    var bidId: String?
    var sessionId: String?
    var liveId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        receiverId <- map["data.receiver.userId"]
        senderId <- map["data.sender.userId"]
        bidId <- map["data.bid.id"]
        sessionId <- map["data.bid.sessionId"]
        liveId <- map["data.live.id"]
    }
}

struct UserBidResultModel: Mappable {
    var userId: String?
    var fullname: String?
    var answer: String?
    var result: String?
    var url: String?
    var totalAmount: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["userId"]
        fullname <- map["fullname"]
        answer <- map["suit.answer"]
        result <- map["suit.result"]
        url <- map["suit.url"]
        totalAmount <- map["totalAmount"]
    }
}

struct BidResultEventModel: Mappable {
    var receiver: UserBidResultModel?
    var sender: UserBidResultModel?
    var bidId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        receiver <- map["data.receiver"]
        sender <- map["data.sender"]
        bidId <- map["data.bid.id"]
    }
}

// MARK: - Request models
struct BidRespondedRequestModel: Mappable {
    var viewerId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        viewerId <- map["viewerId"]
    }
}

struct BidPlacedRequestModel: Mappable {
    var receiverId: String?
    var amount: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        receiverId <- map["receiverId"]
        amount <- map["amount"]
    }
}

struct BidActionRequestModel: Mappable {
    var bidId: String?
    var action: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        bidId <- map["bidId"]
        action <- map["action"]
    }
}

struct BidAnswerRequestModel: Mappable {
    var user: String?
    var liveId: String?
    var bidId: String?
    var sessionId: String?
    var answer: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        user <- map["user"]
        liveId <- map["liveId"]
        bidId <- map["bidId"]
        sessionId <- map["sessionId"]
        answer <- map["answer"]
    }
}

struct BidCloseRequestModel: Mappable {
    var liveId: String?
    var bidId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        liveId <- map["liveId"]
        bidId <- map["bidId"]
    }
}

// MARK: - Response models
