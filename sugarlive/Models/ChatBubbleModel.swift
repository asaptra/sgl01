//
//  ChatBubbleModel.swift
//  sugarlive
//
//  Created by cleanmac-ada on 03/06/22.
//

import Foundation
import ObjectMapper

class ChatBubbleResponseModel: SuccessResponseModel {
    var attributes: [ChatBubbleModel]?
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        attributes <- map["attributes"]
    }
}
