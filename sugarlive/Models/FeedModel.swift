//
//  FeedModel.swift
//  sugarlive
//
//  Created by msi on 25/09/21.
//

import Foundation
import ObjectMapper
import Moya

class PostFeedMediaRequestModel: UploadRequestModel {
  var feedFormData: [MultipartFormData] {
    return self.formData
  }
}

class FeedMediaResponseModel: SuccessResponseModel {
    var image: String?
    var image_2: String?
    var image_3: String?
    var image_4: String?
  
    override func mapping(map: Map) {
        image <- map["attributes.image"]
        image_2 <- map["attributes.image_2"]
        image_3 <- map["attributes.image_3"]
        image_4 <- map["attributes.image_4"]
    }
}

struct LikeFeedRequestModel: Mappable {
  var isLiked: Int = 0
  var type: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    isLiked <- map["isLiked"]
    type <- map["type"]
  }
}

class FeedModel: UserModel {
  var postId: String?
  var content: String?
  var image: String?
  var image2: String?
  var image3: String?
  var image4: String?
  var isLiked: Bool = false
  var totalLike: Int = 0
  var totalComment: Int = 0
  var createdAt: String?
  var taggedUser: [String] = []
  var comments: [FeedCommentModel]?
  
  override func mapping(map: Map) {
    userId <- map["user_id"]
    sugarId <- map["sugar_id"]
    fullname <- map["fullname"]
    level <- map["level"]
    hometown <- map["hometown"]
    bio <- map["bio"]
    coverPicture <- map["cover_picture"]
    postId <- map["post_id"]
    content <- map["content"]
    image <- map["image"]
    image2 <- map["image_2"]
    image3 <- map["image_3"]
    image4 <- map["image_4"]
    isLiked <- map["is_liked"]
    totalLike <- map["total_like"]
    totalComment <- map["total_comment"]
    createdAt <- map["created_at"]
    taggedUser <- map["tagged_user"]
  }
  
  
}

class FeedsResponseModel: SuccessResponseModel {
  var data: [FeedModel] = []
  var total: Int = 0
  var feed: FeedModel?
  
  override func mapping(map: Map) {
    data <- map["attributes.data"]
    total <- map["attributes.total"]
    feed <- map["attributes"]
  }
}


class FeedCommentModel: UserModel {
  var parentId: String?
  var commentId: String?
  var postId: String?
  var content: String?
  var isLiked: Bool = false
  var totalLike: Int = 0
  var createdAt: String?
  var taggedUser = [UserModel]()
  var replies = [FeedCommentModel]()
  
  override func mapping(map: Map) {
    userId <- map["user_id"]
    sugarId <- map["sugar_id"]
    fullname <- map["fullname"]
    level <- map["level"]
    hometown <- map["hometown"]
    bio <- map["bio"]
    coverPicture <- map["cover_picture"]
    parentId <- map["parent_id"]
    commentId <- map["comment_id"]
    postId <- map["post_id"]
    content <- map["content"]
    isLiked <- map["is_liked"]
    totalLike <- map["is_liked"]
    createdAt <- map["created_at"]
    taggedUser <- map["tagged_user"]
    replies <- map["replies"]
  }
}

class FeedCommentsResponseModel: SuccessResponseModel {
  var data: [FeedCommentModel] = []
  var total: Int = 0
  
  override func mapping(map: Map) {
    data <- map["attributes.data"]
    total <- map["attributes.total"]
  }
}

struct PostFeedRequestModel: Mappable {
    var content: String?
    var image: String?
    var image_2: String?
    var image_3: String?
    var image_4: String?
  
    init?(map: Map) {

    }

    init() {

    }
  
    mutating func mapping(map: Map) {
        content <- map["content"]
        image <- map["image"]
        image_2 <- map["image_2"]
        image_3 <- map["image_3"]
        image_4 <- map["image_4"]
    }
}

struct ReportPostFeedRequestModel: Mappable {
    var postId: String?
    var reasonId: String?
  
    init?(map: Map) {
    }

    init() {
    }
  
    mutating func mapping(map: Map) {
        postId <- map["postId"]
        reasonId <- map["reasonId"]
    }
}
