//
//  GiftModel.swift
//  sugarlive
//
//  Created by msi on 24/09/21.
//

import Foundation
import ObjectMapper

struct GiftModel: Mappable {
    var giftId: String?
    var gift: String?
    var price: Int = 0
    var thumbnail: String?
    var image: String?
    var version: String?
    var dataSequence: String?
    var specialEvent: Bool = false
    var label: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        giftId <- map["gift_id"]
        gift <- map["gift"]
        price <- map["price"]
        thumbnail <- map["thumbnail"]
        image <- map["image"]
        version <- map["version"]
        dataSequence <- map["data_sequence"]
        specialEvent <- map["special_event"]
        label <- map["label"]
    }
}

class GiftResponseModel: SuccessResponseModel {
    var attributes: [GiftModel] = []
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        attributes <- map["attributes.gifts"]
    }
}

struct SendGiftRequestModel: Mappable {
    var giftID: String?
    var receiverID: String?
    var count: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        giftID <- map["giftID"]
        receiverID <- map["receiverID"]
        count <- map["count"]
    }
}

struct JackpotEventModel: Mappable {
    var fullname: String?
    var picture: String?
    var win: Int?
    var gift: String?
    var prize: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        fullname <- map["data.fullname"]
        picture <- map["data.picture"]
        win <- map["data.win"]
        gift <- map["data.gift"]
        prize <- map["data.prize"]
    }
    
}

struct GiftSenderEventModel: Mappable {
    var userId: String?
    var fullname: String?
    var candy: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["userId"]
        fullname <- map["fullname"]
        candy <- map["candy"]
    }
}

struct GiftReceiverEventModel: Mappable {
    var userId: String?
    var fullname: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["userId"]
        fullname <- map["fullname"]
    }
}

struct GiftEventModel: Mappable {
    var name: String?
    var image: String?
    var category: GiftCategory?
    var thumbnail: String?
    var duration: Int?
    var price: Int?
    var count: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        name <- map["name"]
        image <- map["image"]
        category <- map["category"]
        thumbnail <- map["thumbnail"]
        duration <- map["duration"]
        price <- map["price"]
        count <- map["count"]
    }
}

struct GiftDetailEventModel: Mappable, Equatable {
    var level: LevelModel?
    var chat: String?
    var carrot: Int?
    var receiver: GiftReceiverEventModel?
    var sender: GiftSenderEventModel?
    var gift: GiftEventModel?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        level <- map["data.level"]
        chat <- map["data.chat"]
        carrot <- map["data.carrot"]
        receiver <- map["data.receiver"]
        sender <- map["data.sender"]
        gift <- map["data.gift"]
    }
    
    static func == (lhs: GiftDetailEventModel, rhs: GiftDetailEventModel) -> Bool {
        return (lhs.receiver?.userId == rhs.receiver?.userId)
            && (lhs.sender?.userId == rhs.sender?.userId)
            && (lhs.gift?.name == rhs.gift?.name)
    }
}

class ComboGiftModel {
    var detailModel: GiftDetailEventModel!
    var timer: Timer?
    var multiplier = 1
    
    init(detailModel: GiftDetailEventModel, timer: Timer) {
        self.detailModel = detailModel
        self.timer = timer
        self.multiplier = detailModel.gift?.count ?? 1
    }
    
    func increaseMultiplier() {
        multiplier += detailModel.gift?.count ?? 1
    }
}
