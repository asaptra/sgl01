//
//  InAppPurchaseModel.swift
//  sugarlive
//
//  Created by dodets on 27/10/21.
//

import Foundation
import ObjectMapper

struct InAppPurchaseModel: Mappable {
  
  var itemID: String?
  var coin: Int?
  var price: Int?

  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    itemID <- map["item_id"]
    coin <- map["coin"]
    price <- map["price"]
  }
}

struct InAppPurchaseResponseModel: Mappable {
  var attributes: [InAppPurchaseModel]?
  var id: String?
  var type: String?
  
  init?(map: Map) {
  }
  
  mutating func mapping(map: Map) {
    attributes <- map["attributes"]
    id <- map["id"]
    type <- map["type"]
  }
}
