//
//  LevelModel.swift
//  sugarlive
//
//  Created by msi on 24/09/21.
//

import Foundation
import ObjectMapper

class LevelCategoryResponseModel: SuccessResponseModel {
  var attributes: [LevelModel] = []
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}

class LevelProgressResponseModel: SuccessResponseModel {
  var attributes: LevelProgressModel?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}
