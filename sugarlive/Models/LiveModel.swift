//
//  LiveModel.swift
//  sugarlive
//
//  Created by msi on 24/09/21.
//

import Foundation
import ObjectMapper

struct LiveCategoryModel: Mappable {
    var categoryId: Int?
    var liveCategory: String?
    var color: String?
    var description: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        categoryId <- map["id"]
        liveCategory <- map["live_category"]
        color <- map["color"]
        description <- map["description"]
    }
}

struct CategoryModel: Mappable {
    var color: String?
    var title: String?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        color <- map["color"]
        title <- map["title"]
    }
}

class LiveCategoryResponseModel: SuccessResponseModel {
    var attributes: [LiveCategoryModel] = []
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        attributes <- map["attributes"]
    }
}

struct LiveHistoryModel: Mappable {
    var duration: String?
    var amount: String?
    var durationText: String?
    var date: String?
    var dateText: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        duration <- map["duration"]
        amount <- map["amount"]
        durationText <- map["durationText"]
        date <- map["date"]
        dateText <- map["dateText"]
    }
    
    var missingDurationText: String {
        return durationText == "" ? "0 min" : durationText!.replacingOccurrences(of: "Hours", with: "h").replacingOccurrences(of: "Hour", with: "h").replacingOccurrences(of: "Minutes", with: "min").replacingOccurrences(of: "Minute", with: "min")
    }
}

struct LiveHistoryGroupModel: Mappable {
    
    var duration: String?
    var durationText: String?
    var startDate: String?
    var endDate: String?
    var dateText: String?
    var amount: String?
    var details: [LiveHistoryModel] = []
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        duration <- map["duration"]
        durationText <- map["durationText"]
        startDate <- map["startDate"]
        endDate <- map["endDate"]
        dateText <- map["dateText"]
        amount <- map["amount"]
        details <- map["details"]
    }
    
    var missingDurationText: String {
        return durationText == "" ? "0 min" : durationText!.replacingOccurrences(of: "Hours", with: "h").replacingOccurrences(of: "Hour", with: "h").replacingOccurrences(of: "Minutes", with: "min").replacingOccurrences(of: "Minute", with: "min")
    }
}

class LiveHistoryResponseModel: SuccessResponseModel {
    var attributes: [LiveHistoryGroupModel] = []
    
    override func mapping(map: Map) {
        meta <- map["meta"]
        id <- map["id"]
        type <- map["type"]
        attributes <- map["attributes"]
    }
}

struct WatchAndStopWatchLiveRequestModel: Mappable {
    var liveId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        liveId <- map["liveId"]
    }
}

struct WatchLiveDetailModel: Mappable {
    var liveId: String?
    var userId: String?
    var multiHostId: String?
    var hashtags: [String]?
    var category: CategoryModel?
    var title: String?
    var viewerCount: Int?
    var isPrivate: Int?
    var privateType: Int?
    var multiHostCount: Int?
    var roomAdminUserId: Int?
    var members: [UserModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        liveId <- map["live_id"]
        userId <- map["user_id"]
        multiHostId <- map["multi_host_id"]
        hashtags <- map["hashtags"]
        category <- map["category"]
        title <- map["title"]
        viewerCount <- map["viewer_count"]
        isPrivate <- map["private"]
        privateType <- map["private_type"]
        multiHostCount <- map["multi_host_count"]
        roomAdminUserId <- map["room_admin_user_id"]
        members <- map["members"]
    }
}

struct WatchLiveAttributesModel: Mappable {
    var detail: WatchLiveDetailModel?
    var profile: UserModel?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        detail <- map["detail"]
        profile <- map["profile.data.attributes"]
    }
}

class WatchLiveResponseModel: SuccessResponseModel {
    var attributes: WatchLiveAttributesModel?
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        attributes <- map["attributes"]
    }
}

class GetLiveDetailResponseModel: SuccessResponseModel {
    var attributes: WatchLiveDetailModel?
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        attributes <- map["attributes"]
    }
}

struct StartLiveRequestModel: Mappable {
    var title: String?
    var category: String?
    var hashtag: String?
    var multiHost: Bool?
    var multiHostCount: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        title <- map["title"]
        category <- map["category"]
        hashtag <- map["hashtag"]
        multiHost <- map["multiHost"]
        multiHostCount <- map["multiHostCount"]
    }
}

class StartLiveResponseModel: SuccessResponseModel {
    var multiHostId: String?
    var multiHostCount: Int?
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        multiHostId <- map["attributes.multiHostId"]
        multiHostCount <- map["attributes.multiHostCount"]
    }
}

struct StopLiveRequestModel: Mappable {
    var duration: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        duration <- map["duration"]
    }
}

class StopLiveResponseModel: SuccessResponseModel {
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
    }
}

struct LiveChatResponseModel: Mappable {
    var userId: String?
    var fullName: String?
    var levelColor: String?
    var level: Int?
    var levelTextColor: String?
    var message: String?
    var senderChatBubble: String?
    var receiverUserId: String?
    
    init?(map: Map) {}
    
    init(userId: String, fullName: String, levelColor: String, level: Int, levelTextColor: String, message: String) {
        self.userId = userId
        self.fullName = fullName
        self.levelColor = levelColor
        self.level = level
        self.levelTextColor = levelTextColor
        self.message = message
    }
    
    mutating func mapping(map: Map) {
        userId <- map["data.user_id"]
        fullName <- map["data.fullname"]
        levelColor <- map["data.level.color"]
        level <- map["data.level.level"]
        levelTextColor <- map["data.level.color_text"]
        message <- map["data.message"]
        senderChatBubble <- map["data.sender_chat_bubble"]
        receiverUserId <- map["data.receiver.user_id"]
    }
}

struct LiveStickerResponseModel: Mappable {
    var userId: String?
    var fullName: String?
    var level: LevelModel?
    var sticker: StickerModel?
    var receiverUserId: String?
    var receiverFullName: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["data.user_id"]
        fullName <- map["data.fullname"]
        level <- map["data.level"]
        sticker <- map["data.sticker"]
        receiverUserId <- map["data.receiver.user_id"]
        receiverFullName <- map["data.receiver.fullname"]
    }
}

struct LiveChatRequestModel: Mappable {
    var liveId: String?
    var message: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        liveId <- map["liveId"]
        message <- map["message"]
    }
}

struct LiveReportRequestModel: Mappable {
    var liveId: String?
    var reason: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        liveId <- map["liveId"]
        reason <- map["reason"]
    }
}

struct BattleOpponentsRequestModel: Mappable {
    var page: Int = 1
    var perPage: Int = 50
    var keyword: String = ""
    
    init() {}
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        page <- map["page"]
        perPage <- map["perPage"]
        keyword <- map["keyword"]
    }
}

struct BattleOpponentsResponseModel: Mappable {
    var attributes: [UserModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        attributes <- map["attributes"]
    }
}

struct ChangeLiveModeRequestModel: Mappable {
    var multiHostCount: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        multiHostCount <- map["multiHostCount"]
    }
}

struct LiveTurnRequestModel: Mappable {
    var turnOffAudio: Int?
    var turnOffVideo: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        turnOffAudio <- map["turnOffAudio"]
        turnOffVideo <- map["turnOffVideo"]
    }
}

struct LiveSummaryResponseModel: Mappable {
    var userId: String?
    var fullName: String?
    var sugarId: String?
    var coverPicture: String?
    var date: String?
    var duration: String?
    var viewer: Int?
    var newFans: Int?
    var loves: Int?
    var carrots: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["attributes.user_id"]
        fullName <- map["attributes.fullname"]
        sugarId <- map["attributes.sugar_id"]
        coverPicture <- map["attributes.cover_picture"]
        date <- map["attributes.date"]
        duration <- map["attributes.duration"]
        viewer <- map["attributes.viewer"]
        newFans <- map["attributes.new_fans"]
        loves <- map["attributes.loves"]
        carrots <- map["attributes.carrots"]
    }
}

struct AudioVideoEventResponseModel: Mappable {
    var userId: String?
    var turnOffAudio: AudioVideoState?
    var turnOffVideo: AudioVideoState?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["data.userId"]
        turnOffAudio <- (map["data.turnOffAudio"],EnumTransform<AudioVideoState>())
        turnOffVideo <- (map["data.turnOffVideo"],EnumTransform<AudioVideoState>())
    }
}

struct MuteRoomResponseModel: Mappable {
    var mode: String?
    var position: Int?
    var roomId: String?
    var audioState: AudioVideoState?
    var videoState: AudioVideoState?
    var userId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        mode <- map["data.mode"]
        position <- map["data.position"]
        roomId <- map["data.roomId"]
        userId <- map["data.userId"]
        
        var isAudio: Bool?
        var isVideo: Bool?
        
        isAudio <- map["data.streamConfig.isAudio"]
        isVideo <- map["data.streamConfig.isVideo"]
        
        audioState = (isAudio ?? true) ? .Unmute : .Mute
        videoState = (isVideo ?? true) ? .Unmute : .Mute
    }
}

class MuteRoomRequestModel {
    private let event = "mute_room"
    private var mode: String = ""
    private var position: Int = 0
    private var roomId: String = ""
    private var userId: String = ""
    private var audioState: AudioVideoState = .Unmute
    private var videoState: AudioVideoState = .Unmute
    
    init(multihostType: MultiHostType, position: Int, roomId: String, userId: String, audio: AudioVideoState, video: AudioVideoState) {
        self.mode = multihostType == .Solo ? "MULTIGUEST3" : "MULTIGUEST\(multihostType.rawValue)"
        self.position = position
        self.roomId = roomId
        self.userId = userId
        self.audioState = audio
        self.videoState = video
    }
    
    func toJSON() -> String? {
        let streamConfig: [String: Bool] = [
            "isAudio": (audioState == .Unmute) ? true : false,
            "isVideo": (videoState == .Unmute) ? true : false
        ]
        
        let data: [String: Any] = [
            "mode": mode,
            "position": 1,
            "roomId": roomId,
            "streamConfig": streamConfig,
            "userId": userId
        ]
        
        let eventData: [String: Any] = [
            "event": event,
            "data": data
        ]
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: eventData, options: []) {
            let text = String(data: jsonData, encoding: .ascii)
            return text
        }
        return nil
    }
}

struct MultihostJoinRequestResponseModel: Mappable {
    var request: [UserMultihostJoinModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        request <- map["attributes.request"]
    }
}

struct AskJoinMultihostRequestModel: Mappable {
    var hostUserId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        hostUserId <- map["hostUserId"]
    }
}

struct ViewerLiveJoinNotificationModel {
    var userId: String
    var name: String
    
    init(userId: String, name: String) {
        self.userId = userId
        self.name = name
    }
}

struct LiveNotificationModel {
    var message: String
}

struct SetLivePrivateRequestModel: Mappable {
    var privateType: String?
    var requirement: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        privateType <- map["privateType"]
        requirement <- map["requirement"]
    }
}

struct LiveValidateRequestModel: Mappable {
    var liveId: String?
    var requirement: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        liveId <- map["liveId"]
        requirement <- map["requirement"]
    }
}

struct WatchLiveErrorDetailResponseModel: Mappable {
    var message: String?
    var privateType: String?
    var requirement: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        message <- map["message"]
        privateType <- map["privateType"]
        requirement <- map["requirement"]
    }
}

struct LiveShareLinkModel: Mappable {
    var link: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        link <- map["link"]
    }
}

class LiveShareLinkResponseModel: SuccessResponseModel {
    var attributes: LiveShareLinkModel?
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        attributes <- map["attributes"]
    }
}

struct LiveFilterModel {
    var polishIntensity: Int32
    var sharpenIntensity: Int32
    var whitenIntensity: Int32
}
