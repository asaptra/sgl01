//
//  ModalActionModel.swift
//  sugarlive
//
//  Created by cleanmac-ada on 27/02/22.
//

import Foundation

struct ModalActionModel {
    var title: String
    var type: ModalActionType
    
    init(title: String, type: ModalActionType) {
        self.title = title
        self.type = type
    }
}
