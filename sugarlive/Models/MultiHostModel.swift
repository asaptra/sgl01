//
//  MultiHostModel.swift
//  sugarlive
//
//  Created by cleanmac on 06/12/21.
//

import Foundation
import ObjectMapper

struct InviteMultiHostRequestModel: Mappable {
    var userId: String?
    var multiHostId: String?
    var position: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["userId"]
        multiHostId <- map["multiHostId"]
        position <- map["position"]
    }
}

struct JoinMultiHostRequestModel: Mappable {
    var hostId: String?
    var position: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        hostId <- map["hostId"]
        position <- map["position"]
    }
}

struct UserIdRequestModel: Mappable {
    var userId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        userId <- map["userId"]
    }
}

struct RequestJoinMultiHostRequestModel: Mappable {
    var hostUserId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        hostUserId <- map["hostUserId"]
    }
}

struct ApproveCancelJoinMultiHostRequestModel: Mappable {
    var requestId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        requestId <- map["requestId"]
    }
}

struct GuestInvitationModel: Mappable {
    var coverPicture: String?
    var fullName: String?
    var level: String?
    var multiHostCount: Int?
    var multiHostId: String?
    var position: Int?
    var userId: String?
    var body: String?
    var title: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        coverPicture <- map["additional_data.cover_picture"]
        fullName <- map["additional_data.fullname"]
        level <- map["additional_data.level"]
        multiHostCount <- map["additional_data.multiHostCount"]
        multiHostId <- map["additional_data.multiHostId"]
        position <- map["additional_data.position"]
        userId <- map["additional_data.userId"]
        body <- map["body"]
        title <- map["title"]
    }
}
