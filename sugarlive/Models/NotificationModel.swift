//
//  NotificationModel.swift
//  sugarlive
//
//  Created by msi on 03/10/21.
//

import Foundation
import ObjectMapper

class NotificationBadgeResponseModel: SuccessResponseModel {
  var notification: Int = 0
  
  override func mapping(map: Map) {
    notification <- map["attributes.notification"]
  }
}

class NotificationResponseModel: SuccessResponseModel {
  var attributes: [NotificationModel]?
  
  override func mapping(map: Map) {
    attributes <- map["attributes"]
  }
}

struct NotificationModel: Mappable {
  
  var id: String?
  var title: String?
  var content: String?
  var type: String?
  var additionalData: NotificationAdditionalModel?
  var createdAt: String?
  var read: Bool = false
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    id <- map["_id"]
    title <- map["title"]
    content <- map["content"]
    type <- map["type"]
    additionalData <- map["additionalData"]
    createdAt <- map["createdAt"]
    read <- map["read"]
  }
}

struct NotificationAdditionalModel: Mappable {
  
  var coverPicture: String?
  var postId: String?
  var followerUserId: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    coverPicture <- map["coverPicture"]
    postId <- map["postId"]
    followerUserId <- map["followerUserId"]
  }
}
