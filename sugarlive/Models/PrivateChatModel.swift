//
//  PrivateChatModel.swift
//  sugarlive
//
//  Created by dodets on 10/12/21.
//

import Foundation
import ObjectMapper

struct PrivateChatModel: Mappable {
    var conversationId: String?
    var totalUnread: String?
    var lastMessage: MessageChatModel?
    var receiverUserId: String?
    var receiverFullName: String?
    var receiverCoverPicture: String?
    
    init?(map: Map) {}
    
    init(conversationId: String, totalUnread: String, lastMessage: MessageChatModel, receiverUserId: String, receiverFullName: String, receiverCoverPicture: String ) {
        self.conversationId = conversationId
        self.totalUnread = totalUnread
        self.lastMessage = lastMessage
        self.receiverUserId = receiverUserId
        self.receiverFullName = receiverFullName
        self.receiverCoverPicture = receiverCoverPicture
    }
    
    mutating func mapping(map: Map) {
        conversationId <- map["conversationId"]
        totalUnread <- map["totalUnread"]
        lastMessage <- map["lastMessage"]
        receiverUserId <- map["receiverInfo.receiverUserId"]
        receiverFullName <- map["receiverInfo.fullname"]
        receiverCoverPicture <- map["receiverInfo.cover_picture"]
    }
}

class PrivateChatResponseModel: SuccessResponseModel {
  var attributes: [PrivateChatModel]?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}

class MessageChatResponseModel: SuccessResponseModel {
  var attributes: [MessageChatModel]?
    var paging: MetaModel?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
    paging <- map["meta"]
  }
}

class MessageBadgeResponseModel: SuccessResponseModel {
  var unreadMessage: Int = 0
  
  override func mapping(map: Map) {
      unreadMessage <- map["attributes.totalUnread"]
  }
}

struct MessageChatModel: Mappable {
    var message: String?
    var image: String?
    var caption: String?
    var type: String?
    var createdAt: String?
    var senderInfo: ReceiverChatModel?
    
    init?(map: Map) {}
    
    init(message: String, image: String, caption: String, type: String, createdAt: String, senderInfo: ReceiverChatModel) {
        self.message = message
        self.image = image
        self.caption = caption
        self.type = type
        self.createdAt = createdAt
        self.senderInfo = senderInfo
    }
    
    mutating func mapping(map: Map) {
        message <- map["message"]
        image <- map["image"]
        caption <- map["caption"]
        type <- map["type"]
        createdAt <- map["createdAt"]
        senderInfo <- map["senderInfo"]
    }
}

struct ReceiverChatModel: Mappable {
    var receiverUserId: String?
    var fullname: String?
    var coverPicture: String?
    
    init?(map: Map) {}
    
    init(receiverUserId: String, fullname: String, coverPicture: String) {
        self.receiverUserId = receiverUserId
        self.fullname = fullname
        self.coverPicture = coverPicture
    }
    
    mutating func mapping(map: Map) {
        receiverUserId <- map["receiverUserId"]
        coverPicture <- map["coverPicture"]
        fullname <- map["fullname"]
    }
}

struct InitChatRequestModel: Mappable {
    var receiverUserId: String?
  
    init?(map: Map) {

    }
    
    init() {
        
    }
  
    mutating func mapping(map: Map) {
        receiverUserId <- map["receiverUserId"]
    }
}

class InitChatResponseModel: SuccessResponseModel {
  var conversationId: String?
  
  override func mapping(map: Map) {
      conversationId <- map["attributes.conversationId"]
  }
}

struct SendChatRequestModel: Mappable {
    var receiverUserId: String?
    var message: String?
    init?(map: Map) {

    }
    
    init() {
        
    }
  
    mutating func mapping(map: Map) {
        receiverUserId <- map["receiverUserId"]
        message <- map["message"]
    }
}
