//
//  ProductModel.swift
//  sugarlive
//
//  Created by msi on 12/10/21.
//

import ObjectMapper

struct ProductModel: Mappable {
  
  var id: String?
  var type: String?
  var name: String?
  var price: Double = 0
  var priceDiscount: Double = 0
  var isDiscount: Bool = false
  var thumb: String?
  
  init?(map: Map) {
  }
    
    init(){}
  
  mutating func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    name <- map["name"]
    price <- map["price"]
    priceDiscount <- map["price_discount"]
    isDiscount <- map["is_discount"]
    thumb <- map["thumb"]
  }
}

class ProductResponseModel: SuccessResponseModel {
    var attributes: [ProductModel]?
    var paging: MetaModel?
  
    override func mapping(map: Map) {
        attributes <- map["attributes"]
        paging <- map["meta"]
    }

    func products(type: String) -> [ProductModel] {
        return attributes?.filter({ $0.type == type }) ?? []
    }
}

struct PurchaseRequestModel: Mappable {
  var id: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    id <- map["id"]
  }
}

struct PurchaseCandyRequestModel: Mappable {
    var userID: String?
    var product: String?
    var provider: String?
    var token: String?
    var sgid: String?
    
    init?(map: Map) {

    }
  
    mutating func mapping(map: Map) {
        userID <- map["user_id"]
        product <- map["product"]
        provider <- map["provider"]
        token <- map["token"]
        sgid <- map["sgid"]
    }
}
