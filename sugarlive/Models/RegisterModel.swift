//
//  RegisterModel.swift
//  sugarlive
//
//  Created by msi on 21/09/21.
//

import UIKit
import ObjectMapper

struct RegisterModel {
  var request: RegisterRequestModel?
  var response: RegisterResponseModel?
}

struct RegisterRequestModel: Mappable {
  var phone: String?
  var fullname: String?
  var password: String?
  var deviceToken: String?
  var firebaseToken: String?
  var device: String?
  var operatingSystem: String?
  var preferredNetwork: String?
  var googleIdToken: String?
  var fbToken: String?
  var appleToken: String?
  
  init?(map: Map) {
    
  }
  
  init() {
    
  }
  
  mutating func mapping(map: Map) {
    phone <- map["phone"]
    fullname <- map["fullname"]
    password <- map["password"]
    deviceToken <- map["deviceToken"]
    firebaseToken <- map["firebaseToken"]
    device <- map["device"]
    operatingSystem <- map["operatingSystem"]
    preferredNetwork <- map["preferredNetwork"]
    googleIdToken <- map["googleIdToken"]
    fbToken <- map["fbToken"]
    appleToken <- map["token"]
  }
}

class RegisterResponseModel: SuccessResponseModel {
  var attributes: UserTokenModel?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}

struct PusherRequestModel: Mappable {
  var userId: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    userId <- map["userId"]
  }
}

struct RefreshTokenRequestModel: Mappable {
  var newToken: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    newToken <- map["newToken"]
  }
}

struct ForgotPasswordRequestModel: Mappable {
  var phone: String?
  var code: String?
  var password: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    phone <- map["phone"]
    code <- map["code"]
    password <- map["password"]
  }
}

struct OTPRequestModel: Mappable {
  var code: String?
  var phone: String?
  var userId: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    code <- map["code"]
    phone <- map["phone"]
    userId <- map["userId"]
  }
}

struct AppleTokenRequestModel: Mappable {
  var grantType: String?
  var code: String?
  var redirectUri: String?
  var clientId: String?
  var clientSecret: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    grantType <- map["grant_type"]
    code <- map["code"]
    redirectUri <- map["redirect_uri"]
    clientId <- map["client_id"]
    clientSecret <- map["client_secret"]
  }
  
  
}
