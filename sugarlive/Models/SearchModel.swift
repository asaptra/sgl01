//
//  SearchModel.swift
//  sugarlive
//
//  Created by msi on 24/09/21.
//

import Foundation
import ObjectMapper

struct SearchRequestModel: Mappable {
  var search: String = ""
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    search <- map["keyword"]
  }
}

struct SearchMemberModel: Mappable {
  var userId: String?
  var sugarId: String?
  var fullname: String?
  var hometown:String?
  var bio:String?
  var coverPicture:String?
  var userLevel: LevelModel?
  
  init?(map: Map) {
  }
  
  mutating func mapping(map: Map) {
    userId <- map["user_id"]
    sugarId <- map["sugar_id"]
    fullname <- map["fullname"]
    hometown <- map["hometown"]
    bio <- map["bio"]
    coverPicture <- map["cover_picture"]
    userLevel <- map["level"]
  }
}

struct SearchMemberResponseModel: Mappable {
  var list: [SearchMemberModel]?
  
  init?(map: Map) {
  }
  
  mutating func mapping(map: Map) {
    list <- map["data"]["attributes"]
  }
}
