//
//  ServiceModel.swift
//  sugarlive
//
//  Created by msi on 22/09/21.
//

import Foundation
import ObjectMapper
import Moya

struct ErrorResponseModel: Mappable {
  var code: String?
  var title: String?
  var status: Int?
  var detail: Any?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    code <- map["code"]
    title <- map["title"]
    status <- map["status"]
    detail <- map["detail"]
  }
  
  var messages: [String] {
    if let d = detail as? [String: [String]] {
      var arr = [String]()
      _ = d.values.map({ arr.append(contentsOf: $0) })
      return arr
    } else if let d = detail as? String {
      return [d]
    } else {
      return []
    }
  }
}

struct MetaModel: Mappable {
  var page: Int?
  var perPage: Int?
  var total: Int?
  var totalPages: Int?
 
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    page <- map["page"]
    perPage <- map["per_page"]
    total <- map["total"]
    totalPages <- map["total_pages"]
  }
}

class SuccessResponseModel: Mappable {
  var id: String?
  var type: String?
  var meta: MetaModel?
  
  required init?(map: Map) {
    
  }
  
  init() {
    
  }
  
  func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    meta <- map["meta"]
  }
}

class PagingRequestModel: Mappable {
  var page: Int = 1
  var perPage: Int = 50
  
  required init?(map: Map) {
    
  }
  
  init() {
    
  }
  
  func mapping(map: Map) {
    page <- map["page"]
    perPage <- map["perPage"]
  }
}

class UploadRequestModel: Mappable {
  var imageURL: URL?
  var imageURLs: [URL]?
  
  var image: UIImage?
  var images: [UIImage]?
  
  var imageData: Data?
  var imagesData: [Data]?
  
  var formData: [MultipartFormData] {
    var formData = [MultipartFormData]()
    if let url = imageURL {
      formData.append(MultipartFormData(provider: .file(url), name: "image", fileName: url.lastPathComponent, mimeType: url.absoluteString.mimeType()))
    }
    if let img = image, let data = img.jpegData(compressionQuality: 0.5) {
      formData.append(MultipartFormData(provider: .data(data), name: "image", fileName: "image", mimeType: "image/jpeg"))
    }
    
    if let data = imageData {
      formData.append(MultipartFormData(provider: .data(data), name: "image", fileName: "image", mimeType: "image/jpeg"))
    }
    var i = 0
//    for url in imageURLs ?? [] {
//      formData.append(MultipartFormData(provider: .file(url), name: "image\(i > 0 ? "_\(i)": "")", fileName: url.lastPathComponent, mimeType: url.absoluteString.mimeType()))
//      i += 1
//    }
    for img in images ?? [] {
      if let data = img.jpegData(compressionQuality: 0.5) {
        formData.append(MultipartFormData(provider: .data(data), name: "image\(i > 0 ? "_\(i)": "")", fileName: "image", mimeType: "image/jpeg"))
        i += 1
      }
    }
      
      var x = 0
      if (imageURLs?.count ?? 0) != 0 {
          x = (imageURLs?.count ?? 0) + 1
      }
      
      for data in imagesData ?? [] {
          if x == 1 {
              x = 2
          }
        formData.append(MultipartFormData(provider: .data(data), name: "image", fileName: "image\(x > 0 ? "_\(x)": "")", mimeType: "image/jpeg"))
        x += 1
      }
    return formData
  }
  
  required init?(map: Map) {
    
  }
  
  init() {
    
  }
  
  func mapping(map: Map) {
    imageURL <- map["imageURL"]
  }
}
