//
//  SettingModel.swift
//  sugarlive
//
//  Created by msi on 13/09/21.
//

import UIKit
import ObjectMapper

class SettingModel: Mappable {
  
  var settingGroup: String = ""
  var settingMenu: [SettingMenuModel] = []
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    settingGroup    <- map["header"]
    settingMenu     <- map["menu"]
  }
}

class SettingMenuModel: Mappable {

  var title: String = ""
  var icon: String = ""
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    title     <- map["title"]
    icon      <- map["icon"]
  }
}

struct VersionModel: Mappable {
  var iosVersion: String?
  var androidVersion: String?
  var huaweiVersion: String?
  var isMaintenance: Bool = false
  var isForceUpdate: Bool = false
  var iosForceUpdate: Bool = false
  var huaweiForceUpdate: Bool = false
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    iosVersion <- map["ios_version"]
    androidVersion <- map["android_version"]
    huaweiVersion <- map["huawei_version"]
    isMaintenance <- map["is_maintenance"]
    isForceUpdate <- map["is_force_update"]
    iosForceUpdate <- map["ios_force_update"]
    huaweiForceUpdate <- map["huawei_force_update"]
  }
}

class VersionResponseModel: SuccessResponseModel {
  var attributes: VersionModel?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}

struct ImageModel: Mappable {
  var imageId: Int?
  var imageName: String?
  var imageUrl: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    imageId <- map["id"]
    imageName <- map["name"]
    imageUrl <- map["image_url"]
  }
}

struct SplashScreenModel: Mappable {
  var current: ImageModel?
  var upcoming: ImageModel?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    current <- map["current"]
    upcoming <- map["upcoming"]
  }
}

class SplashScreenResponseModel: SuccessResponseModel {
  var attributes: SplashScreenModel?
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    attributes <- map["attributes"]
  }
}
