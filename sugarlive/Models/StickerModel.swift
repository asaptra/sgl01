//
//  StickerModel.swift
//  sugarlive
//
//  Created by cleanmac-ada on 02/05/22.
//

import Foundation
import ObjectMapper

struct StickerModel: Mappable {
    var packageId: String?
    var stickerId: String?
    var stickerUrl: String?
    var name: String?
    var sticker: String?
    var price: Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        packageId <- map["package_id"]
        stickerId <- map["sticker_id"]
        stickerUrl <- map["sticker_url"]
        name <- map["name"]
        sticker <- map["sticker"]
        price <- map["price"]
    }
}

struct StickerPackageModel: Mappable {
    var packageId: String?
    var name: String?
    var icon: String?
    var stickers: [StickerModel]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        packageId <- map["package_id"]
        name <- map["name"]
        icon <- map["icon"]
        stickers <- map["stickers"]
    }
}

class StickerListResponseModel: SuccessResponseModel {
    var attributes: [StickerPackageModel]?
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        attributes <- map["attributes"]
    }
}

struct SendStickerRequestModel: Mappable {
    var liveId: String?
    var stickerId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        liveId <- map["liveId"]
        stickerId <- map["stickerId"]
    }
}
