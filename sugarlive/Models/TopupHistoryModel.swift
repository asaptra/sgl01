//
//  TopupHistoryModel.swift
//  sugarlive
//
//  Created by dodets on 16/10/21.
//

import Foundation
import ObjectMapper

struct TopupHistoryModel: Mappable {
  
  var id: Int?
  var reference: String?
  var link: String?
  var datetime: String?
  var amount: Int?

  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    reference <- map["reference"]
    id <- map["id"]
    datetime <- map["datetime"]
    amount <- map["amount"]
  }
}

struct TopupHistoryResponseModel: Mappable {
  var attributes: [TopupHistoryModel]?
  var id: String?
  var type: String?
  var meta: MetaModel?
  
  init?(map: Map) {
  }
  
  mutating func mapping(map: Map) {
    attributes <- map["attributes"]
    id <- map["id"]
    type <- map["type"]
    meta <- map["meta"]
  }
}
