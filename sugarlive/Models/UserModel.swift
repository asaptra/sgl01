//
//  UserModel.swift
//  sugarlive
//
//  Created by msi on 24/09/21.
//

import Foundation
import ObjectMapper
import SwiftKeychainWrapper

let kAccessToken = "AccessToken"

enum SocialMedia: String {
  case Facebook = "facebook"
  case Twitter = "twitter"
  case Instagram = "instagram"
  case Line = "line"
  case None = "None"
}

class UserLiveResponseModel: SuccessResponseModel {
  var attributes: [UserModel] = []
  
  override func mapping(map: Map) {
    id <- map["id"]
    type <- map["type"]
    meta <- map["meta"]
    attributes <- map["attributes"]
  }
}

//TOP SPENDER
class TopSpenderModel: Mappable {
    var userId: String?
    var sugarId: String?
    var fullname: String?
    var coverPicture: String?
    var levelColor: String?
    var level: Int?
    var levelColorText: String?
    var spend: String?
  
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        userId <- map["user_id"]
        sugarId <- map["sugar_id"]
        fullname <- map["fullname"]
        coverPicture <- map["cover_picture"]
        levelColor <- map["level.color"]
        level <- map["level.level"]
        levelColorText <- map["level.color_text"]
        spend <- map["spend"]
    }
}

class TopSpenderResponseModel: SuccessResponseModel {
  var daily = [TopSpenderModel]()
  var monthly = [TopSpenderModel]()
  var total = [TopSpenderModel]()
    
  override func mapping(map: Map) {
    daily <- map["attributes.daily"]
    monthly <- map["attributes.monthly"]
    total <- map["attributes.total"]
  }
}

struct UserMultihostJoinModel: Mappable {
    var requestId: String?
    var coverPicture: String?
    var userId: String?
    var fullname: String?
    var sugarId: String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        requestId <- map["request_id"]
        coverPicture <- map["cover_picture"]
        userId <- map["user_id"]
        fullname <- map["fullname"]
        sugarId <- map["sugar_id"]
    }
}

class UserResponseModel: SuccessResponseModel {
  var attributes: UserModel?
  
  override func mapping(map: Map) {
    attributes <- map["attributes"]
  }
}

class UserModel: Mappable {
  var liveId: String?
  var userId: String?
  var userLiveId: String?
  var sugarId: String?
  var fullname: String?
  var age: Int = 0
  var bio: String?
  var hometown: String?
  var level: LevelModel?
  var coverPicture: String?
  var liveDetail: LiveDetailModel?
  var verified: Bool = false
  var startStreaming: String?
  var carrot: Double = 0
  var totalCarrot: Double = 0
  var view: Double = 0
  var multiHostId: String?
  var multiHostCount: Int = 0
  var position: Int = 0
  var isHost: Bool = false
  var multiHost: MultiHostModel?
//  var avatarFrame: AvatarFrameModel?
  
  var phone: String?
  var gender: String?
  var birthday: String?
  var isAdmin: Bool = false
  var coin: Int = 0
  var avatarFrames: AvatarFrameModel?
  var entranceEffects: EntranceEffectModel?
  var badges: BadgesModel?
  var chatBubbles: ChatBubblesModel?
  var followerCount: Int = 0
  var followingCount: Int = 0
  var isFollowing: Bool = false
  var isBlock: Bool = false
  var userPictures: [UserPictureModel] = []
  var userPicture: UserPictureModel?
  var userPictureUrl: String?
  var socialMedia: SocialMediaModel?
  var socialMedias: [SocialMediaModel] = []
  var isFollow: Bool = false
    
  var turnOffAudio: AudioVideoState?
  var turnOffVideo: AudioVideoState?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    liveId <- map["live_id"]
    userId <- map["user_id"]
    userLiveId <- map["userId"]
    sugarId <- map["sugar_id"]
    fullname <- map["fullname"]
    age <- map["age"]
    bio <- map["bio"]
    hometown <- map["hometown"]
    level <- map["level"]
    coverPicture <- map["cover_picture"]
    liveDetail <- map["live_detail"]
    verified <- map["verified"]
    startStreaming <- map["start_streaming"]
    totalCarrot <- map["total_carrot"]
    view <- map["view"]
    multiHostId <- map["multi_host_id"]
    multiHostCount <- map["multi_host_count"]
    position <- map["position"]
//    isHost <- map["isHost"]
    multiHost <- map["multiHost"]
//    avatarFrame <- map["avatarFrame"]
    
    phone <- map["phone"]
    gender <- map["gender"]
    birthday <- map["birthday"]
    isAdmin <- map["isAdmin"]
    coin <- map["coin"]
    avatarFrames <- map["avatarFrames"]
    entranceEffects <- map["entranceEffects"]
    badges <- map["badges"]
    chatBubbles <- map["chatBubbles"]
    followerCount <- map["followerCount"]
    followingCount <- map["followingCount"]
    isFollowing <- map["isFollowing"]
    isBlock <- map["isBlock"]
    userPictures <- map["userPictures"]
    userPicture <- map["userPicture"]
    userPictureUrl <- map["userPicture"]
    socialMedia <- map["social_media"]
    isFollow <- map["is_follow"]
      
    turnOffAudio <- (map["turnOffAudio"],EnumTransform<AudioVideoState>())
    turnOffVideo <- (map["turnOffVideo"],EnumTransform<AudioVideoState>())
  }
  
  func hideAllSocialMedia() {
    var tSocialMedia = [SocialMediaModel]()
    for socmed in socialMedias {
      var socmed = socmed
      socmed.showOnLive = false
      tSocialMedia.append(socmed)
    }
    socialMedias = tSocialMedia
  }
  
  var socialMediaAsRequest: [String: Any] {
    var result = [String: Any]()
    for socmed in socialMedias {
      result[socmed.socialMedia.rawValue] = ["username": socmed.userName ?? "",
                                             "showOnLive": NSNumber(value: socmed.showOnLive).intValue]
    }
    return result
  }
}

struct ChatBubbleModel: Mappable {
  var chatBubbleId: String?
  var name: String?
  var mainImage: String?
  var bottomLeftImage: String?
  var bottomMidImage: String?
  var bottomRightImage: String?
  var midLeftImage: String?
  var midMidImage: String?
  var midRightImage: String?
  var topLeftImage: String?
  var topMidImage: String?
  var topRightImage: String?
  var showName:String?
  var isDefault:String?
  
  init?(map: Map) {
  }
    
    init(){}
  
  mutating func mapping(map: Map) {
    chatBubbleId <- map["chatBubbleId"]
    name <- map["name"]
    mainImage <- map["mainImage"]
    bottomLeftImage <- map["bottomLeftImage"]
    bottomMidImage <- map["bottomMidImage"]
    bottomRightImage <- map["bottomRightImage"]
    midLeftImage <- map["midLeftImage"]
    midMidImage <- map["midMidImage"]
    midRightImage <- map["midRightImage"]
    topLeftImage <- map["topLeftImage"]
    topMidImage <- map["topMidImage"]
    topRightImage <- map["topRightImage"]
    showName <- map["showName"]
    isDefault <- map["isDefault"]
  }
}

struct ChatBubblesModel: Mappable {
  var defaultChatBubble: ChatBubbleModel?
  var list: [ChatBubbleModel]?
  
  init?(map: Map) {
  }
  
  mutating func mapping(map: Map) {
    defaultChatBubble <- map["default"]
    list <- map["list"]
  }
}

struct AvatarModel: Mappable {
  var name: String?
  var avatarFrameId: String?
  var profile: String?
  var avatarIcon:String?
  var liveBackground:String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    name <- map["name"]
    avatarFrameId <- map["avatarFrameId"]
    profile <- map["profile"]
    avatarIcon <- map["avatarIcon"]
    liveBackground <- map["liveBackground"]
  }
}

struct AvatarFrameModel: Mappable {
  var defaultFrame: AvatarModel?
  var list: [AvatarModel]?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    defaultFrame <- map["default"]
    list <- map["list"]
  }
}

//struct AvatarFrameResponseModel: Mappable {
//  var defaultFrame: AvatarModel?
//  var list: [AvatarModel]?
//  
//  init?(map: Map) {
//    
//  }
//  
//  mutating func mapping(map: Map) {
//    defaultFrame <- map["default"]
//    list <- map["list"]
//  }
//}

struct BadgeModel: Mappable {
  var badgeId: String?
  var name: String?
  var type: String?
  var url:String?
  var textColor:String?
  var backgroundColor:String?
  var active:Bool?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    badgeId <- map["badge_id"]
    name <- map["name"]
    type <- map["type"]
    url <- map["url"]
    textColor <- map["textColor"]
    backgroundColor <- map["backgroundColor"]
    active <- map["active"]
  }
}

struct BadgesModel: Mappable {
  var active: [BadgeModel]?
  var list: [BadgeModel]?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    active <- map["active"]
    list <- map["list"]
  }
}

struct LevelModel: Mappable {
  var color: String?
  var level: Int?
  var levelStep: String?
  var colorText: String?
  var badgeColor: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    color <- map["color"]
    level <- map["level"]
    levelStep <- map["level"]
    colorText <- map["color_text"]
    if map.currentKey == "color_background" {
      color <- map["color_background"]
    }
      badgeColor <- map["color_background"]
  }
}

struct LevelProgressModel: Mappable {
  var currentLevel: Int?
  var currentExp: Int?
  var nextLevel: Int?
  var nextExp: Int?
  var progress: Int?
  
  init?(map: Map) {
    
  }
    
    init() {
      
    }
  
  mutating func mapping(map: Map) {
      currentLevel <- map["currentLevel"]
      currentExp <- map["currentExp"]
      nextLevel <- map["nextLevel"]
      nextExp <- map["nextExp"]
      progress <- map["progress"]
  }
}

struct LiveDetailModel: Mappable {
  var title: String?
  var category: String?
  var categoryColor: String?
  var viewerCount: Int = 0
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    title <- map["title"]
    category <- map["category"]
    categoryColor <- map["category_color"]
    viewerCount <- map["viewer_count"]
  }
}

struct MultiHostModel: Mappable {
  var multuHostid: String?
  var multiHostCount: Int = 0
  var members: [UserModel] = []
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    multuHostid <- map["id"]
    multiHostCount <- map["multiHostCount"]
    members <- map["members"]
  }
}

struct UserRequestModel: Mappable {
  var userId: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    userId <- map["userID"]
  }
}

struct UpdateProfileRequestModel: Mappable {
  var name: String?
  var gender: Int = 0
  var birthDate: String?
  var homeTown: String?
  var bio: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    name <- map["name"]
    gender <- map["gender"]
    birthDate <- map["birthDate"]
    homeTown <- map["homeTown"]
    bio <- map["bio"]
  }
}

struct UpdatePasswordRequestModel: Mappable {
  var currentPassword: String?
  var newPassword: String?
  var confirmPassword: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    currentPassword <- map["currentPassword"]
    newPassword <- map["newPassword"]
    confirmPassword <- map["confirmPassword"]
  }
}

struct ChangePhoneRequestModel: Mappable {
  var phone: String?
  var code: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    phone <- map["phone"]
    code <- map["code"]
  }
}

struct ProfileDefaultRequestModel: Mappable {
  var avatarFrameId: String?
  var entranceEffectId: String?
  var badgeIds: [String]?
  var chatBubbleId: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    avatarFrameId <- map["avatarFrameId"]
    entranceEffectId <- map["entranceEffectId"]
    badgeIds <- map["badgeIds"]
    chatBubbleId <- map["chatBubbleId"]
  }
}

struct UserTokenModel: Mappable {
  var token: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    token <- map["token"]
  }
  
  func cacheToken() {
    if let token = token {
      KeychainWrapper.standard.set(token, forKey: kAccessToken)
      Constant.isLogin = true
    }
  }
}

struct EntranceModel: Mappable {
  var entranceEffectId: String?
  var name: String?
  var image: String?
  var showName: Int64?
  var type: String?
  var isDefault: Bool?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    entranceEffectId <- map["entranceEffectId"]
    name <- map["name"]
    image <- map["image"]
    showName <- map["showName"]
    type <- map["type"]
    isDefault <- map["isDefault"]
  }
}

struct EntranceQueueModel {
    var entrance: EntranceModel
    var fullname: String
}

struct EntranceEffectModel: Mappable {
  var defaultEffect: EntranceModel?
  var list: [EntranceModel]?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    defaultEffect <- map["default"]
    list <- map["list"]
  }
}

struct UserPictureModel: Mappable {
  var id: String?
  var picture_url: String?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    id <- map["id"]
    picture_url <- map["picture.url"]
  }
}

struct SocialMediaRequestModel: Mappable {
  var userName: String?
  var isDisplay: Bool = false
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    userName <- map["username"]
    isDisplay <- map["is_display"]
  }
}

class SocialMediaResponseModel: SuccessResponseModel {
  var attributes: [SocialMediaModel]?
  
  override func mapping(map: Map) {
    attributes <- map["attributes.data"]
  }
}

struct SocialMediaModel: Mappable {
  
  var socialMedia: SocialMedia = .None
  var userName: String?
  var showOnLive: Bool = false
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    socialMedia <- map["social_media"]
    userName <- map["username"]
    showOnLive <- map["show_on_live"]
  }
}
