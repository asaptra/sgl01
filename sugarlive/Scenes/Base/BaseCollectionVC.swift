//
//  BaseCollectionVC.swift
//  sugarlive
//
//  Created by msi on 13/09/21.
//

import UIKit

class BaseCollectionVC: BaseVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var doneButton: UIButton!
    
    var collectionRefreshControl: UIRefreshControl?
    
    var cells = [String]() {
        didSet{
            for cell in cells {
                collectionView.register(UINib(nibName: cell, bundle: nil), forCellWithReuseIdentifier: cell)
            }
        }
    }
    
    var headers = [String] () {
        didSet {
            for header in headers {
                collectionView.register(UINib(nibName: header, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: header)
            }
        }
    }
    
    @IBOutlet weak var collectionContainerView: UIView!
    @IBOutlet weak var emptyContainerView: UIView!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    func configureCollectionRefreshControl() {
        collectionRefreshControl = UIRefreshControl()
        collectionRefreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        collectionView.refreshControl = collectionRefreshControl
    }
    
    @objc func refreshHandler() {}
    
}
