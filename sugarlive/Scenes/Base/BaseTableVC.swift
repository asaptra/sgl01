//
//  BaseTableVC.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit
import ObjectMapper

private let menus: [[String: Any]] = [["header": "PROFILE", "menu": [["title": "Edit profile", "icon": "ic_profile_setting_purple"],
                                                                     ["title": "Level", "icon": "ic_arrow_setting_green"],
                                                                     ["title": "Live history", "icon": "ic_clock_setting_green"]]],
                                      ["header": "SHOPPING", "menu": [["title": "Store", "icon": "ic_cart_setting_yellow"]]],
                                      ["header": "GENERAL", "menu": [["title": "About Us", "icon": "ic_info_setting_purple"],
                                                                     ["title": "Feedback", "icon": "ic_feedback_setting_blue"],
                                                                     ["title": "Help Center", "icon": "ic_book_setting_red"],
                                                                     ["title": "Deactivate account", "icon": "ic_deactivate_setting_red"],
                                                                     ["title": "Blocked user", "icon": "ic_block_setting_red"],
                                                                     ["title": "Logout", "icon": "ic_logout_setting_red"]]]]
private let menusLoginViaPhone: [[String: Any]] = [["header": "PROFILE", "menu": [["title": "Edit profile", "icon": "ic_profile_setting_purple"],
                                                                     ["title": "Level", "icon": "ic_arrow_setting_green"],
                                                                     ["title": "Live history", "icon": "ic_clock_setting_green"]]],
                                      ["header": "SHOPPING", "menu": [["title": "Store", "icon": "ic_cart_setting_yellow"]]],
                                      ["header": "ACCOUNT", "menu": [["title": "Change password", "icon": "ic_lock_setting_yellow"],
                                                                     ["title": "Change phone number", "icon": "ic_phone_setting_yellow"]]],
                                      ["header": "GENERAL", "menu": [["title": "About Us", "icon": "ic_info_setting_purple"],
                                                                     ["title": "Feedback", "icon": "ic_feedback_setting_blue"],
                                                                     ["title": "Help Center", "icon": "ic_book_setting_red"],
                                                                     ["title": "Deactivate account", "icon": "ic_deactivate_setting_red"],
                                                                     ["title": "Blocked user", "icon": "ic_block_setting_red"],
                                                                     ["title": "Logout", "icon": "ic_logout_setting_red"]]]]
let isUserLoginViaPhone = UserDefaults.standard.bool(forKey: "phoneLogin")
let menuModel: [SettingModel] = Mapper<SettingModel>().mapArray(JSONObject: isUserLoginViaPhone ? menusLoginViaPhone : menus) ?? []

enum TableType {
  case Wallet
  case Topup
  case Setting
  case Blocked
  case Rule
  case Help
  case Store
  case HowToTopup
  
  case SocialMedia
  case EditProfile
  case ChangePassword
  case ForgotPassword
  case ChangePhone
  case EnterPassword
  case Feedback
  
  case Search
  case Following
  case Follower
  case Default
  
  func title() -> String {
    switch self {
    case .Wallet:
      return "My Wallet"
    case .Topup:
      return "Topup History"
    case .Setting:
      return "Setting"
    case .Blocked:
      return "Blocked Users"
    case .Store:
      return "Store"
    case .SocialMedia:
      return "Add Social Media"
    case .EditProfile:
      return "Edit Profile"
    case .ChangePassword:
      return "Change Password"
    case .ForgotPassword:
      return "Forgot Password"
    case .ChangePhone:
      return "Change Phone Number"
    case .EnterPassword:
      return "Change Phone Number"
    case .Feedback:
      return "Feedback"
    case .Help:
      return "Help"
    case .Rule:
      return "House Rules"
    case .Search:
      return "Search"
    case .Following:
      return "Following"
    case .Follower:
      return "Followers"
    case .HowToTopup:
      return "How To Topup"
    default:
      return ""
    }
  }
  
  func actionButton() -> String {
    switch self {
    case .Feedback:
      return "Send"
    case .ForgotPassword, .ChangePassword, .ChangePhone, .EnterPassword:
      return "Done"
    default:
      return "Save"
    }
  }
  
  func sections() -> [String] {
    switch self {
    case .Wallet:
      return ["Candies", "Carrots", "What is Carrot ?", "What is Candy ?"]
    case .Setting:
      return menuModel.map{ $0.settingGroup }
    case .EditProfile:
      return ["Profile Picture", "Background Cover", "Basic Info"]
    case .Store:
      return ["Choose store item"]
    case .HowToTopup:
      return ["How to topup"]
    case .Help:
      return helpModel.map{ $0.settingGroup }
    case .Rule:
      return ruleModel.map{ $0.settingGroup }
    default:
      return [""]
    }
  }
  
  func rows(_ section: Int) -> [String] {
    switch self {
    case .Setting:
      return menuModel[section].settingMenu.map{ $0.title }
    case .SocialMedia:
      return ["facebook", "twitter", "instagram", "line"]
    case .EditProfile:
      switch section {
      case 2:
        return ["NAME", "GENDER", "BIRTHDAY", "LOCATION", "BIO"]
      default:
        return [""]
      }
    case .ChangePassword:
      return ["Old Password", "New Password", "Confirm new password"]
    case .ForgotPassword:
      return ["New Password", "Confirm new password"]
    case .Feedback:
      return ["EMAIL", "FEEDBACK", "UPLOAD SCREENSHOT"]
    case .Help:
      return helpModel[section].settingMenu.map{ $0.title }
    case .Store:
      return ["Candy", "Avatar Frame", "Entrance Effect", "Badge", "Chat Bubble"]
    case .Rule:
      return ["", ""]
    case .HowToTopup:
      return [""]
    default:
      return [""]
    }
  }
  
  func numOfSections() -> Int {
    return sections().count
  }
  
  func numOfRow(section: Int) -> Int {
    return rows(section).count
  }
}

class BaseTableVC: BaseVC {
  
  @IBOutlet weak var tableView: UITableView!
  
  var cells = [String]() {
    didSet{
      for cell in cells {
        tableView.register(UINib(nibName: cell, bundle: nil), forCellReuseIdentifier: cell)
      }
    }
  }
  
  var headers = [String] () {
    didSet {
      for header in headers {
        tableView.register(UINib(nibName: header, bundle: nil), forHeaderFooterViewReuseIdentifier: header)
      }
    }
  }
  
  @IBOutlet weak var tableContainerView: UIView!
  @IBOutlet weak var emptyContainerView: UIView!
  @IBOutlet weak var emptyImage: UIImageView!
  @IBOutlet weak var emptyLabel: UILabel!
  @IBOutlet weak var doneButton: UIButton!
  
  func createBasicTableHeader(title: String) -> UIView {
    let v = UIView(frame: .zero)
    v.backgroundColor = .white
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = .defaultBoldFont(size: 13)
    label.textColor = .PRIMARY_1
    label.text = title
    v.addSubview(label)
    label.snp.makeConstraints { (maker) in
      maker.centerY.equalTo(v.snp_centerYWithinMargins)
      maker.leading.equalTo(v.snp_leadingMargin).offset(10)
      maker.trailing.equalTo(v.snp_trailingMargin).inset(10)
    }
    return v
  }
  
}
