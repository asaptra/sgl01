//
//  BaseVC.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit
import SnapKit
import ObjectMapper
import AlamofireImage
import Localize
import SwiftKeychainWrapper
import Fusuma

let kAnauthorizedResponse = "AnauthorizedResponse"
let kUpdateProfileResponse = "UpdateProfileResponse"
let kPostFeedResponse = "PostFeedResponse"
let kDeleteFeedResponse = "DeleteFeedResponse"
let kUpdateFeedResponse = "UpdateFeedResponse"
let kPostCommentResponse = "PostCommentResponse"
let kUpdateReadNotificationResponse = "UpdateReadNotificationResponse"
let kLiveViewerValidateRoomRequirement = "LiveViewerValidateRoomRequirement"

class BaseVC: UIViewController {
    
    func setupLayout(){}
    func setupBottomView(){}
    func setupNav(){}
    
    var tag: Int?
    
    lazy var popupAlertVC: PopupAlertVC = {
        let destinationVC = PopupAlertVC(nibName: "PopupAlertVC", bundle: nil)
        destinationVC.modalTransitionStyle = .crossDissolve
        destinationVC.modalPresentationStyle = .overFullScreen
        return destinationVC
    }()
    
    lazy var popupSheetVC: PopSheetVC = {
        let destinationVC = PopSheetVC(nibName: "PopSheetVC", bundle: nil)
        destinationVC.modalTransitionStyle = .coverVertical
        destinationVC.modalPresentationStyle = .overFullScreen
        return destinationVC
    }()
    
    lazy var popupProfileSheetVC: PopProfileSheetVC = {
        let destinationVC = PopProfileSheetVC(nibName: "PopProfileSheetVC", bundle: nil)
        destinationVC.modalTransitionStyle = .coverVertical
        destinationVC.modalPresentationStyle = .overFullScreen
        return destinationVC
    }()
    
    lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        //    datePicker.minimumDate = Date()
        datePicker.date = Date()
        //    datePicker.maximumDate = Date()
        return datePicker
    }()
    
    lazy var inputToolbar: UIToolbar = {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelInputAction(_:)));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneInputAction(_:)));
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        return toolbar
    }()
    
    lazy var photoPickerVC: FusumaViewController = {
        let fusuma = FusumaViewController()
        fusuma.availableModes = [.camera, .library]
        fusumaCropImage = false
        fusuma.allowMultipleSelection = false
        return fusuma
    }()
    
    private let reportReason = ["Misleading Information", "Dangerous organization and indivuals", "Illegal activities and regulated goods", "Violated and graphic content", "Animal cruelty", "Dangerous acts", "Hate speech", "Harassment or bullying", "Pornography and nudity", "Minor safety", "Spam", "Intellectual property infringement", "Other"]
    private let banSpesificReason = ["Promotion of criminal activities", "Sale or use of weapons", "Drugs and controlled substances", "Frauds and scams"]
    private let kickSpesificReason = ["Smoking", "Using drugs", "Drinking alcohol", "Sleep"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configBackButton()
        setupLayout()
        setupBottomView()
        setupNav()
        
        registerNotification()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ic_back_white")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ic_back_white")
    }
    
    //  MARK: Setup
    private func configBackButton() {
        var navigationItem: UINavigationItem?
        if let mainTabVC = self.tabBarController as? MainTabVC {
            navigationItem = mainTabVC.navigationItem
        } else {
            navigationItem = self.navigationItem
        }
        navigationItem?.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        navigationItem?.leftItemsSupplementBackButton = true
    }
    
    private func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleAnauthorized(_:)), name: NSNotification.Name(rawValue: kAnauthorizedResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateProfile(_:)), name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handlePostFeed(_:)), name: NSNotification.Name(rawValue: kPostFeedResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleDeleteFeed(_:)), name: NSNotification.Name(rawValue: kDeleteFeedResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed(_:)), name: NSNotification.Name(rawValue: kUpdateFeedResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handlePostComment(_:)), name: NSNotification.Name(rawValue: kPostCommentResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleReadNotification(_:)), name: NSNotification.Name(rawValue: kUpdateReadNotificationResponse), object: nil)
    }
    
    //  MARK: Service
    func getMyProfile(handler: @escaping (() -> Void)) {
        provider.request(.MyProfile, successModel: UserResponseModel.self) { (result) in
            AuthManager.shared.currentUser = result.attributes
            handler()
        } failure: { (error) in
            self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
        }
    }
    
    func getBanCategory() {
        provider.request(.BanCategory, successModel: BanCategoryResponseModel.self) { (result) in
            
        } failure: { (error) in
            self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
        }
    }
    
    func getBanReason(category: BanCategoryModel) {
        if let categoryId = category.categoryId {
            provider.request(.BanReason(identifier: categoryId), successModel: BanReasonResponseModel.self) { (result) in
                
            } failure: { (error) in
                self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
            }
        }
    }
    
    func getBanDuration() {
        provider.request(.BanDuration, successModel: BanDurationResponseModel.self) { (result) in
            
        } failure: { (error) in
            self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
        }
    }
    
    func doDeactivateAccount() {
        provider.request(.DeactivateProfile, successModel: SuccessResponseModel.self) { (result) in
            self.routeToWarningModal(message: "Your account success deactivated.") {
                self.doLogout()
            }
        } failure: { (error) in
            self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
        }
    }
    
    //  MARK: Route
    func routeToLoginVia() {
        let destinationVC = LoginViaVC.init(nibName: "LoginViaVC", bundle: nil)
        destinationVC.modalPresentationStyle = .overFullScreen
        destinationVC.modalTransitionStyle = .crossDissolve
        destinationVC.termTapped = {(sender) in
            self.routeToWebVC(type: .TermOfUse)
        }
        self.present(destinationVC, animated: true, completion: nil)
    }
    
    func routeToRegister(registerType: RegisterType = .Registered) {
        let destinationVC = RegisterVC.init(nibName: "RegisterVC", bundle: nil)
        destinationVC.registerType = registerType
        destinationVC.modalPresentationStyle = .fullScreen
        destinationVC.modalTransitionStyle = .crossDissolve
        self.present(destinationVC, animated: true, completion: nil)
    }
    
    func routeToOTP(otpType: OTPVC.OTPType) {
        let destinationVC = OTPVC(nibName: "OTPVC", bundle: nil)
        destinationVC.otpType = otpType
        getRootVC()?.pushViewController(destinationVC, animated: true)
    }
    
    func routeToTableActionVC(type: TableType) {
        let destinationVC = TableWithActionVC(nibName: "TableWithActionVC", bundle: nil)
        destinationVC.tableType = type
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func routeToTableVC(type: TableType) {
        let destinationVC = TableWithoutAction(nibName: "TableWithoutAction", bundle: nil)
        destinationVC.tableType = type
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func routeToChatListVC() {
        let destinationVC = ChatListVC(nibName: "ChatListVC", bundle: nil)
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func routeToSearchVC() {
        let destinationVC = SearchVC(nibName: "SearchVC", bundle: nil)
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func routeToNotificationVC() {
        let destinationVC = NotificationVC(nibName: "NotificationVC", bundle: nil)
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func routeToLive(userModel: UserModel, type: LiveType, category: CategoryModel? = nil, title: String, hashtags: [String]? = nil, filterModel: LiveFilterModel? = nil) {
        let destinationVC = LiveVC(nibName: "LiveVC", bundle: nil)
        destinationVC.type = type
        destinationVC.userModel = userModel
        destinationVC.category = category
        destinationVC.hashtags = hashtags
        destinationVC.filterModel = filterModel
        destinationVC.liveTitle = title
        navigationController?.pushViewController(destinationVC, animated: false)
    }
    
    func routeToMultiguestLive(userModel: UserModel, type: LiveType, multihostType: MultiHostType, category: CategoryModel? = nil, hashtags: [String]? = nil, filterModel: LiveFilterModel? = nil) {
        let destinationVC = MultiguestLiveVC(nibName: "MultiguestLiveVC", bundle: nil)
        destinationVC.type = type
        destinationVC.multihostType = multihostType
        destinationVC.userModel = userModel
        destinationVC.category = category
        destinationVC.hashtags = hashtags
        destinationVC.filterModel = filterModel
        navigationController?.pushViewController(destinationVC, animated: false)
    }
    
    func routeToLiveAsViewer(userModel: UserModel, detailModel: WatchLiveDetailModel) {
        let destinationVC = LiveVC(nibName: "LiveVC", bundle: nil)
        destinationVC.type = (AuthManager.shared.currentUser?.isAdmin ?? false) ? .Admin : .Viewer
        destinationVC.userModel = userModel
        destinationVC.detailModel = detailModel
        navigationController?.pushViewController(destinationVC, animated: false)
    }
    
    func routeToMultiguestLiveAsViewer(userModel: UserModel, multihostType: MultiHostType, detailModel: WatchLiveDetailModel) {
        let destinationVC = MultiguestLiveVC(nibName: "MultiguestLiveVC", bundle: nil)
        destinationVC.type = (AuthManager.shared.currentUser?.isAdmin ?? false) ? .Admin : .Viewer
        destinationVC.multihostType = multihostType
        destinationVC.userModel = userModel
        destinationVC.detailModel = detailModel
        navigationController?.pushViewController(destinationVC, animated: false)
    }
    
    func routeToWebVC(type: WebVC.PageType) {
        let destinationVC = WebVC(nibName: "WebVC", bundle: nil)
        destinationVC.pageType = type
        if self.navigationController == nil {
            self.getRootVC()?.pushViewController(destinationVC, animated: true)
        } else {
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
    }
    
    func routeToProfileManagerVC(type: ProfileManagerType, userModel: UserModel) {
        let destinationVC = ProfileManagerVC(nibName: "ProfileManagerVC", bundle: nil)
        destinationVC.profileManagerType = type
        destinationVC.userModel = userModel
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func routeToTopupVC(type: TopupVC.TopupType, paymentMethod: TopupVC.PaymentMethod? = nil) {
        let destinationVC = TopupVC(nibName: "TopupVC", bundle: nil)
        destinationVC.topupType = type
        destinationVC.paymentMethod = paymentMethod
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func routeToUnblockModal() {
        popupAlertVC.cancelStr = "Unblock"
        popupAlertVC.actionStr = "Cancel"
        popupAlertVC.messageStr = "Are you sure to unblock this user ?"
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true) {
                
            }
        }
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true, completion: nil)
        }
        self.present(popupAlertVC, animated: true, completion: nil)
    }
    
    func routeToLogoutModal() {
        popupAlertVC.cancelStr = "Log out"
        popupAlertVC.actionStr = "No"
        popupAlertVC.messageStr = "Log out from Sugarlive?"
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true) {
                self.doLogout()
            }
        }
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true, completion: nil)
        }
        self.present(popupAlertVC, animated: true, completion: nil)
    }
    
    func routeToDeactivateConfirmationModal() {
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = "Deactivate"
        popupAlertVC.messageStr = "Are you sure you want to deactivate this account?\nYou can activate by signing in later."
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true) {
                //        self.routeToDeactivateModal()
                self.doDeactivateAccount()
            }
        }
        self.present(popupAlertVC, animated: true, completion: nil)
    }
    
    func routeToDeactivateModal() {
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = "OK"
        popupAlertVC.messageStr = "Please enter password to deactivate account"
        let passwordField = RegisterFieldView.nib(withType: RegisterFieldView.self)
        passwordField.textField.placeholder = "Password"
        passwordField.textField.minLength = 8
        passwordField.textField.maxLength = 15
        passwordField.textField.font = .defaultFont(size: 12)
        passwordField.textField.isSecureTextEntry = true
        passwordField.leftIcon.superview?.isHidden = true
        passwordField.rightIcon.image = UIImage(named: "ic_eye_black")
        passwordField.containerView.layer.cornerRadius = 9
        passwordField.containerView.layer.masksToBounds = true
        passwordField.rightTapped = {(sender) in
            passwordField.textField.isSecureTextEntry = !passwordField.textField.isSecureTextEntry
        }
        popupAlertVC.additionalViews = [passwordField]
        popupAlertVC.cancelTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.additionalViews = []
            self.popupAlertVC.dismiss(animated: true) {
                self.doDeactivateAccount()
            }
        }
        self.present(popupAlertVC, animated: true, completion: nil)
    }
    
    func routeToReportModal() {
        popupAlertVC.alertType = .Selector
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = ""
        popupAlertVC.messageStr = "Choose main reason"
        var additionals = [UIButton]()
        var index = 0
        for str in reportReason {
            index += 1
            let button = UIButton(type: .custom)
            button.tag = index
            button.titleLabel?.numberOfLines = 0
            button.setAttributedTitle(str.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 13),
                                                                    NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                      for: .normal)
            button.contentEdgeInsets = .init(top: 22, left: 22, bottom: 22, right: 22)
            button.addTarget(self, action: #selector(selectorAction(_:)), for: .touchUpInside)
            additionals.append(button)
        }
        
        popupAlertVC.additionalViews = additionals
        popupAlertVC.cancelTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true) {
                
            }
        }
        self.present(popupAlertVC, animated: true, completion: nil)
    }
    
    func routeToSpesificReportModal() {
        popupAlertVC.alertType = .Selector
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = ""
        popupAlertVC.messageStr = "Choose specific reason"
        var additionals = [UIButton]()
        var index = 0
        for str in reportReason {
            index += 1
            let button = UIButton(type: .custom)
            button.tag = index
            button.titleLabel?.numberOfLines = 0
            button.setAttributedTitle(str.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 13),
                                                                    NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                      for: .normal)
            button.contentEdgeInsets = .init(top: 22, left: 22, bottom: 22, right: 22)
            additionals.append(button)
        }
        
        popupAlertVC.additionalViews = additionals
        popupAlertVC.cancelTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true) {
                
            }
        }
        self.present(popupAlertVC, animated: true, completion: nil)
    }
    
    func routeToSendReportModal() {
        popupAlertVC.alertType = .Selected
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = "Kick"
        popupAlertVC.messageStr = ""
        
        let mainField = RegisterFieldView.nib(withType: RegisterFieldView.self)
        mainField.textField.placeholder = "Main reason"
        mainField.leftIcon.superview?.isHidden = true
        mainField.rightIcon.superview?.isHidden = true
        mainField.containerView.layer.cornerRadius = 9
        mainField.containerView.layer.masksToBounds = true
        
        let specificField = RegisterFieldView.nib(withType: RegisterFieldView.self)
        specificField.textField.placeholder = "Specific reason"
        specificField.leftIcon.superview?.isHidden = true
        specificField.rightIcon.superview?.isHidden = true
        specificField.containerView.layer.cornerRadius = 9
        specificField.containerView.layer.masksToBounds = true
        
        popupAlertVC.additionalViews = [mainField, specificField]
        popupAlertVC.cancelTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true) {
                
            }
        }
        self.present(popupAlertVC, animated: true, completion: nil)
    }
    
    func routeToWarningModal(message: String, handler: @escaping (() -> Void) = {}) {
        popupAlertVC.alertType = .Alert
        popupAlertVC.actionStr = "OK"
        popupAlertVC.messageStr = message
        popupAlertVC.hideCancelButton = true
        
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true) {
                handler()
            }
        }
        
        self.present(popupAlertVC, animated: true, completion: nil)
    }
    
    func routeToInformationModal(type: PopupAlertVC.Successor, message: String, handler: @escaping (() -> Void) = {}) {
        let alert = PopupAlertVC(nibName: "PopupAlertVC", bundle: nil)
        alert.modalTransitionStyle = .crossDissolve
        alert.modalPresentationStyle = .overFullScreen
        alert.alertType = .Successor(type: type)
        alert.messageStr = message
        alert.hideCancelButton = true
        
        alert.actionTapped = { [unowned alert] (sender) in
            alert.dismiss(animated: true) {
                handler()
            }
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    func routeToCheckoutSheet(product: ProductModel, handler: @escaping ((Bool) -> Void)) {
        let price = Int(product.price - product.priceDiscount)
        let roundPrice = price.roundedWithAbbreviations()
        let coin = AuthManager.shared.currentUser?.coin
        let roundCoin = coin?.roundedWithAbbreviations()
        let isInsufficient = (coin ?? 0) < price
        popupSheetVC.titleStr = "Checkout"
        popupSheetVC.productImage = product.thumb ?? ""
        popupSheetVC.productStr = product.name ?? ""
        popupSheetVC.costStr = "\(roundPrice) candy"
        popupSheetVC.productType = product.type ?? ""
        popupSheetVC.statusStr = "You have \(roundCoin ?? "0") Candy"
        popupSheetVC.isInsufficient = isInsufficient
        popupSheetVC.descStr = "This transaction cannot be reversible. Make sure you have the right item to buy."
        popupSheetVC.actionStr = "Buy with \(roundPrice) Candy"
        popupSheetVC.actionSelectedStr = "Insufficient candy. Top up now."
        popupSheetVC.additionalViews = []
        popupSheetVC.actionTapped = {(sender) in
            self.popupSheetVC.dismiss(animated: true) {
                handler(!isInsufficient)
            }
        }
        self.present(popupSheetVC, animated: true, completion: nil)
    }
    
    func routeToSocmedSheet() {
        popupSheetVC.titleStr = "Username Social Media"
        let fb = SocMedView.nib(withType: SocMedView.self)
        fb.titleLabel.text = "username"
        fb.actionTapped = {(sender) in
            
        }
        let tw = SocMedView.nib(withType: SocMedView.self)
        tw.titleLabel.text = "@username"
        tw.actionTapped = {(sender) in
            
        }
        let ig = SocMedView.nib(withType: SocMedView.self)
        ig.titleLabel.text = "@username"
        ig.actionTapped = {(sender) in
            
        }
        let ln = SocMedView.nib(withType: SocMedView.self)
        ln.titleLabel.text = "@username"
        ln.actionTapped = {(sender) in
            
        }
        popupSheetVC.additionalViews = [fb, tw, ig, ln]
        popupSheetVC.actionTapped = {(sender) in
            self.popupSheetVC.dismiss(animated: true) {
                
            }
        }
        self.present(popupSheetVC, animated: true, completion: nil)
    }
    
    func routeToAdminActionSheet() {
        popupSheetVC.titleStr = "Choose action"
        let kick = SocMedView.nib(withType: SocMedView.self)
        kick.iconImageView.superview?.isHidden = true
        kick.titleLabel.text = "Kick user"
        kick.actionTapped = {(sender) in
            
        }
        let ban = SocMedView.nib(withType: SocMedView.self)
        ban.iconImageView.superview?.isHidden = true
        ban.titleLabel.text = "Ban user"
        ban.actionTapped = {(sender) in
            
        }
        popupSheetVC.additionalViews = [kick, ban]
        popupSheetVC.actionTapped = {(sender) in
            self.popupSheetVC.dismiss(animated: true) {
                
            }
        }
        self.present(popupSheetVC, animated: true, completion: nil)
    }
    
    func routeToProfileSheet() {
        //    popupProfileSheetVC.nameStr = ""
        //    popupProfileSheetVC.idStr = ""
        //    popupProfileSheetVC.locationStr = ""
        //    popupProfileSheetVC.levelStr = ""
        //    popupProfileSheetVC.ageStr = ""
        //    popupProfileSheetVC.viewerStr = ""
        //    popupProfileSheetVC.fansStr = ""
        //    popupProfileSheetVC.likeStr = ""
        //    popupProfileSheetVC.carrotStr = ""
        
        popupProfileSheetVC.infoTapped = {(sender) in
            self.popupProfileSheetVC.dismiss(animated: true) {
                
            }
        }
        popupProfileSheetVC.profileTapped = {(sender) in
            self.popupProfileSheetVC.dismiss(animated: true) {
                
            }
        }
        popupProfileSheetVC.followTapped = {(sender) in
            self.popupProfileSheetVC.dismiss(animated: true) {
                
            }
        }
        popupProfileSheetVC.chatTapped = {(sender) in
            self.popupProfileSheetVC.dismiss(animated: true) {
                
            }
        }
        self.present(popupProfileSheetVC, animated: true, completion: nil)
    }
    
    func routeToPictureActionSheet(galleryHandler: (() -> Void)?, cameraHandler: (() -> Void)?) {
        popupSheetVC.showTitle = false
        let gallery = SocMedView.nib(withType: SocMedView.self)
        gallery.iconImageView.superview?.isHidden = true
        gallery.titleLabel.text = "Open Gallery"
        gallery.titleLabel.textAlignment = .center
        gallery.actionTapped = {(sender) in
            self.popupSheetVC.dismiss(animated: true) {
                self.photoPickerVC.availableModes = [.camera, .library]
                self.present(self.photoPickerVC, animated: true, completion: nil)
            }
        }
        let camera = SocMedView.nib(withType: SocMedView.self)
        camera.iconImageView.superview?.isHidden = true
        camera.titleLabel.text = "Open Camera"
        camera.titleLabel.textAlignment = .center
        camera.actionTapped = {(sender) in
            self.popupSheetVC.dismiss(animated: true) {
                self.photoPickerVC.availableModes = [.camera, .library]
                self.present(self.photoPickerVC, animated: true, completion: nil)
            }
        }
        popupSheetVC.additionalViews = [gallery, camera]
        popupSheetVC.actionTapped = {(sender) in
            self.popupSheetVC.dismiss(animated: true) {
                
            }
        }
        self.present(popupSheetVC, animated: true, completion: nil)
    }
    
    func routeToHome() {
        getRootVC()?.popToRootViewController(animated: true)
    }
    
    // MARK: Action
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backToRootAction(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func selectorAction(_ sender: Any) {
        self.popupAlertVC.dismiss(animated: true) {
            self.routeToSendReportModal()
        }
    }
    
    @IBAction func searchAction(_ sender: Any) {
        routeToSearchVC()
    }
    
    @IBAction func chatAction(_ sender: Any) {
        if !Constant.isLogin {
            self.routeToLoginVia()
            return
        }
        routeToChatListVC()
    }
    
    @IBAction func notifAction(_ sender: Any) {
        if !Constant.isLogin {
            self.routeToLoginVia()
            return
        }
        routeToNotificationVC()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func doneInputAction(_ sender: Any) {}
    @IBAction func cancelInputAction(_ sender: Any) {}
    
    @IBAction func handleAnauthorized(_ sender: Any) {
        doLogout()
    }
    
    @IBAction func handleUpdateProfile(_ sender: Any) {}
    @IBAction func handlePostFeed(_ sender: Any) {}
    @IBAction func handleDeleteFeed(_ sender: Any) {}
    @IBAction func handleUpdateFeed(_ sender: Any) {}
    @IBAction func handlePostComment(_ sender: Any) {}
    @IBAction func handleReadNotification(_ sender: Any) {}
    
    // MARK: Custom
    func setIdleTimerStatus(_ status: Bool) {
        UIApplication.shared.isIdleTimerDisabled = status
    }
    
    func italicTitle(title: String, fontSize: CGFloat = 14, color: UIColor = UIColor.white) -> NSAttributedString {
        return title.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultItalicFont(size: fontSize),
                                               NSAttributedString.Key.foregroundColor: color])
    }
    
    func navLabel(title: String) -> UILabel {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.font = .defaultBoldFont(size: 16)
        label.textColor = .white
        label.text = title
        return label
    }
    
    func doLogout() {
        UserDefaults.standard.set(false, forKey: "relogin")
        UserDefaults.standard.set(false, forKey: "phoneLogin")
        UserDefaults.standard.set(false, forKey: "phoneLoginPass")
        KeychainWrapper.standard.removeObject(forKey: kAccessToken)
        Constant.isLogin = false
        if let mainTabVC = self.navigationController?.viewControllers.first as? MainTabVC {
            mainTabVC.selectedIndex = 0
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func getRootVC() -> UINavigationController? {
        return (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)
    }
    
    func dismissAllModal(completion: @escaping (()->Void) = { } ) {
        getRootVC()?.dismiss(animated: true, completion: {
            completion()
        })
    }
    
    func openURL(string: String) {
        if let urlString = string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let url = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(url){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                else {
                    print("can't open \(string)")
                }
            }
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}
