//
//  BaseTextField.swift
//  sugarlive
//
//  Created by msi on 23/09/21.
//

import UIKit

class BaseTextField: UITextField {
  
  enum TextFieldHandlerType {
    case Valid
    case Min
    case Prefix
    case Begin
    case Changed
    case End
  }
  
  var minLength: Int?
  var maxLength: Int?
  
  var containString: [String]?
  
  var keyboardInputType: KeyboardInputType = .Default {
    didSet {
      setupKeyboard()
    }
  }
  var textFieldHandler: ((_ sender: TextFieldHandlerType) -> Void)?
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  private func setupLayout() {
    self.delegate = self
    self.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
  }
  
  private func setupKeyboard() {
    switch keyboardInputType {
    case .Numeric, .Nominal, .NominalDecimal:
      self.keyboardType = .numberPad
    case .Phone:
      self.keyboardType = .phonePad
    default:
      self.keyboardType = .default
    }
  }
  
//  MARK: Custom
  private func doValidate(newLength: Int, str: String = "", tf: BaseTextField) {
    if newLength < tf.minLength ?? 0 {
      if tf.textFieldHandler != nil {
        tf.textFieldHandler?(.Min)
      }
    } else {
      if tf.containString != nil {
        var contain = false
        for c in tf.containString ?? [] {
          if str.hasPrefix(c) {
            contain = true
            break
          }
        }
        if !contain {
          tf.textFieldHandler?(.Prefix)
        } else {
          tf.textFieldHandler?(.Valid)
        }
      } else {
        tf.textFieldHandler?(.Valid)
      }
    }
  }
}

extension BaseTextField: UITextFieldDelegate {
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let numSet = "0123456789"
    let phoneSet = "+0123456789"
    var charSet: CharacterSet?
    var validLength = true
    
    if let tf = textField as? BaseTextField {
      let newLength = (textField.text?.count ?? 0) + string.count - range.length
      validLength = tf.maxLength != nil ? (newLength <= tf.maxLength ?? 0) : true
//      let str = (textField.text ?? "") + string
      
//      doValidate(newLength: newLength, str: str, tf: tf)
      
      switch (tf.keyboardInputType) {
      case .Alphabet:
        charSet = CharacterSet.letters.inverted;
        break;
      case .Alphanumeric:
        var set = CharacterSet.letters
        set.formUnion(CharacterSet(charactersIn: numSet))
        charSet = set.inverted
        break
      case .Sentence:
        var set = CharacterSet.letters
        set.formUnion(CharacterSet.whitespaces)
        charSet = set.inverted
        break
      case .Numeric:
        charSet = CharacterSet(charactersIn: numSet).inverted
        break;
      case .Phone:
        charSet = CharacterSet(charactersIn: phoneSet).inverted
        break;
      case .Nominal:
        charSet = CharacterSet.decimalDigits.inverted
        break
      default:
        if tf.maxLength != nil {
          return validLength
        }
        break
      }
    }
    
    if ((charSet) != nil) {
      return (string.rangeOfCharacter(from: charSet!) == nil) && validLength
    }
    return true
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    if let tf = textField as? BaseTextField {
      tf.textFieldHandler?(.Begin)
    }
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    if let tf = textField as? BaseTextField {
      tf.textFieldHandler?(.End)
      doValidate(newLength: tf.text?.count ?? 0, str: tf.text ?? "", tf: tf)
    }
  }
  
  @IBAction func textFieldDidChanged(_ textField: UITextField) {
    if let tf = textField as? BaseTextField {
      tf.textFieldHandler?(.Changed)
      doValidate(newLength: tf.text?.count ?? 0, str: tf.text ?? "", tf: tf)
    }
  }
}
