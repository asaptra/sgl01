//
//  BaseTextView.swift
//  sugarlive
//
//  Created by msi on 30/09/21.
//

import UIKit

class BaseTextView: UITextView {
  
  var textViewDidBeginHandler: ((_ sender: Any) -> Void)?
  var textViewDidChangeHandler: ((_ sender: Any) -> Void)?
  var textViewDidEndHandler: ((_ sender: Any) -> Void)?
  
  var placeholder: String = "" {
    didSet {
      setupLayout()
    }
  }
  
  var textStr: String? {
    didSet {
      if textStr != nil {
        setupLayout()
        self.text = textStr
        self.textColor = .black
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  private func setupLayout() {
    self.delegate = self
    self.text = placeholder
    if #available(iOS 13.0, *) {
      self.textColor = .placeholderText
    } else {
      self.textColor = .init(hex: "C7C7CD")
    }
  }
}

extension BaseTextView: UITextViewDelegate {
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.text == placeholder {
      textView.text = ""
      textView.textColor = .black
    }
    if let handler = textViewDidBeginHandler {
      handler(textView)
    }
  }
  
  func textViewDidChange(_ textView: UITextView) {
    if let handler = textViewDidChangeHandler {
      handler(textView)
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text == "" {
      self.text = placeholder
      if #available(iOS 13.0, *) {
        self.textColor = .placeholderText
      } else {
        self.textColor = .init(hex: "C7C7CD")
      }
    }
    if let handler = textViewDidEndHandler {
      handler(textView)
    }
  }
}
