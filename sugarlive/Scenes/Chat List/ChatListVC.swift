//
//  ChatListVC.swift
//  sugarlive
//
//  Created by msi on 18/09/21.
//

import UIKit

class ChatListVC: BaseTableVC, UITextFieldDelegate {
    @IBOutlet weak var searchTxt: BaseTextField!
    @IBOutlet weak var closeSearchView: UIView!
    
    private var listPrivateChat = [PrivateChatModel]()
    private var defaultListPrivateChat = [PrivateChatModel]()
    private var searchMemberResponseModel: SearchMemberResponseModel?
    var searchKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        closeSearchView.isHidden = true
        getListChat()
    }
  
    override func setupNav() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: "Chat"))
        self.navigationItem.leftItemsSupplementBackButton = true
    }
  
    override func setupLayout() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        cells = ["ChatListCell"]
        
        searchTxt.font = .defaultRegularFont()
        searchTxt.textColor = .darkText
        searchTxt.cornerRadius = 10
        searchTxt.delegate = self
        let padding: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 0))
        searchTxt.leftView = padding
        searchTxt.leftViewMode = .always
        
        searchTxt.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        
        let closeSearchTap = UITapGestureRecognizer(target: self, action: #selector(closeSearch))
        closeSearchView.addGestureRecognizer(closeSearchTap)
    }
    
    @objc func textFieldDidChanged(_ textField: UITextField) {
        searchKey = textField.text ?? ""
        if searchKey.count >= 3 {
            let filteredQuestion = listPrivateChat.filter { (item : PrivateChatModel) -> Bool in
                let stringMatch = item.receiverFullName?.lowercased().range(of: searchKey.lowercased())
                return stringMatch != nil ? true : false
            }
            listPrivateChat = filteredQuestion
            self.closeSearchView.isHidden = false
        } else if searchKey.count == 0 {
            closeSearch()
        } else {
            listPrivateChat = defaultListPrivateChat
        }
        tableView.reloadData()
    }
    
    //  MARK: Action
    @objc func closeSearch(){
        closeSearchView.isHidden = true
        searchTxt.text = nil
        listPrivateChat = defaultListPrivateChat
        tableView.reloadData()
    }
    
    //  MARK: Service
    private func getListChat() {
        provider.request(.PrivateChat, successModel: PrivateChatResponseModel.self) { (result) in
            self.listPrivateChat = result.attributes ?? [PrivateChatModel]()
            self.defaultListPrivateChat = self.listPrivateChat
            self.tableView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
}

extension ChatListVC: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPrivateChat.count
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
//  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//    switch editingStyle {
//    case .delete:
//      
//      break
//    default:
//      break
//    }
//  }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell", for: indexPath) as! ChatListCell
        let data = listPrivateChat[indexPath.row]
        cell.nameLabel.text = data.receiverFullName
        cell.chatLabel.text = data.lastMessage?.message
        cell.timeLabel.text = data.lastMessage?.createdAt?.toDate(with: "yyyy-MM-dd HH:mm:ss").chatTimeAgo()

        if data.totalUnread == "0" {
            cell.chatLabel.textColor = .LIGHT_TEXT
            cell.chatLabel.font = .defaultRegularFont(size: 12)
        } else {
            cell.chatLabel.font = .defaultBoldFont(size: 14)
            cell.chatLabel.textColor = .PRIMARY_2
        }
        
        if let imageUrl = data.receiverCoverPicture, let url = URL(string: imageUrl) {
            cell.profileImageView?.af.setImage(withURL: url)
        }

        return cell
    }
}

extension ChatListVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 70
  }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = DirectChatVC(nibName: "DirectChatVC", bundle: nil)
        let data = listPrivateChat[indexPath.row]
        vc.conversationID = data.conversationId ?? ""
        vc.receiverUserId = data.receiverUserId ?? ""
        vc.receiverUserName = data.receiverFullName ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
