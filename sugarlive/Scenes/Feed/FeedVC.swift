//
//  FeedVC.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit
import Popover
import BadgeHub
import PopItUp

class FeedVC: BaseTableVC {
  
  enum FeedType {
    case FeedList
    case FeedDetail
  }
  
  enum LikeType: String {
    case Post = "post"
    case Comment = "comment"
  }
  
  var feedType: FeedType = .FeedList
  var feed: FeedModel?
  var isCommentTapped: Bool = false
  private var badgeNotif: BadgeHub?
  private var badgeChat: BadgeHub?
  
  private let popTip: Popover = {
    let options = [
      .type(.down),
      .cornerRadius(4),
      .animationIn(0.3),
      .blackOverlayColor(UIColor.clear),
      .arrowSize(CGSize.zero),
      .borderColor(UIColor.lightGray)
    ] as [PopoverOption]
    return Popover(options: options, showHandler: nil, dismissHandler: nil)
  }()
  private var pageModel = PagingRequestModel()
  private var feeds: FeedsResponseModel?
  private var selectedFeedIndexpath: IndexPath = IndexPath(row: 0, section: 0)
  
  private var refreshControl = UIRefreshControl()
  
  lazy var postInputView = CommentInputView.nib(withType: CommentInputView.self)
  lazy var profileView = FeedProfileView.nib(withType: FeedProfileView.self)
  lazy var popTipView = PopTipView.nib(withType: PopTipView.self)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    NotificationCenter.default.addObserver(self, selector: #selector(selectedDeletePost(_:)), name: NSNotification.Name("deleteFeed"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(selectedReportPost(_:)), name: NSNotification.Name("reportFeed"), object: nil)
  }
  
  @objc func selectedDeletePost(_ notification: NSNotification) {
    if let feedData = notification.object as? FeedModel {
      deletePost(selectedFeed: feedData)
    }
  }
  
  @objc func selectedReportPost(_ notification: NSNotification) {
    if let feedData = notification.object as? FeedModel {
      let sWidth = UIScreen.main.bounds.size.width - 40
      let sHeight = UIScreen.main.bounds.size.height - 60
      let destinationVC = ReportFeedVC()
      destinationVC.selectedID = feedData.postId ?? ""
      self.presentPopup(destinationVC, animated: true, constraints: [.width(sWidth), .height(sHeight)])
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if Constant.isLogin {
      getNotificationBadge()
      getChatBadge()
    }
    setupNav()
    navigationController?.navigationBar.barTintColor = feedType == .FeedList ? .PRIMARY_1 : .white
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    tableView.reloadData()
    
    if isCommentTapped {
      postInputView.inputField.becomeFirstResponder()
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    if feedType == .FeedDetail {
      if #available(iOS 13.0, *) {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .PRIMARY_1
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
      }
    }
  }
  
  override func setupNav() {
    var buttonItems = [UIBarButtonItem]()
    if feedType == .FeedList {
      let searchButon = UIButton()
      searchButon.setImage(UIImage(named: "ic_search_white"), for: .normal)
      searchButon.setTitle("", for: .normal)
      searchButon.addTarget(self, action: #selector(searchAction(_:)), for: .touchUpInside)
      let searchBarItem = UIBarButtonItem.init(customView: searchButon)
      
      let chatButon = UIButton()
      chatButon.setImage(UIImage(named: "ic_chat_white"), for: .normal)
      chatButon.setTitle("", for: .normal)
      chatButon.addTarget(self, action: #selector(chatAction(_:)), for: .touchUpInside)
      let chatBarItem = UIBarButtonItem.init(customView: chatButon)
      badgeChat = BadgeHub(barButtonItem: chatBarItem)
      badgeChat?.moveCircleBy(x: 19, y: 0)
      badgeChat?.setCircleBorderColor(.PRIMARY_1, borderWidth: 2)
      badgeChat?.scaleCircleSize(by: 0.9)
      
      let notifButon = UIButton()
      notifButon.setImage(UIImage(named: "ic_notif_white"), for: .normal)
      notifButon.setTitle("", for: .normal)
      notifButon.addTarget(self, action: #selector(notifAction(_:)), for: .touchUpInside)
      let notifBarItem = UIBarButtonItem.init(customView: notifButon)
      badgeNotif = BadgeHub(barButtonItem: notifBarItem)
      badgeNotif?.moveCircleBy(x: 19, y: 0)
      badgeNotif?.setCircleBorderColor(.PRIMARY_1, borderWidth: 2)
      badgeNotif?.scaleCircleSize(by: 0.9)
      
      searchBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true
      searchBarItem.customView?.heightAnchor.constraint(equalToConstant: 23).isActive = true
      chatBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true
      chatBarItem.customView?.heightAnchor.constraint(equalToConstant: 23).isActive = true
      notifBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true
      notifBarItem.customView?.heightAnchor.constraint(equalToConstant: 23).isActive = true
      
      buttonItems = [notifBarItem, chatBarItem, searchBarItem]
    } else {
      if #available(iOS 13.0, *) {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .white
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
      }
      navigationController?.navigationBar.tintColor = .DARK_TEXT
      navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ic_back_gray")
      navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ic_back_gray")
    }
    if let mainTabVC = self.tabBarController as? MainTabVC {
      mainTabVC.navigationItem.rightBarButtonItems = buttonItems
      if feedType == .FeedList {
        mainTabVC.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: UIImageView(image: UIImage(named: "ic_nav_logo")))
      }
    }
  }
  
  override func setupLayout() {
    tableView.dataSource = self
    tableView.delegate = self
    cells = ["FeedTableCell", "FeedCommentCell"]
    
    refreshControl.addTarget(self, action: #selector(getFeeds), for: .valueChanged)
    tableView.refreshControl = refreshControl
    
    setupPostView()
    if feedType == .FeedList {
      postInputView.inputField.placeholder = "Write a New Post"
      postInputView.inputField.textViewDidBeginHandler = {(sender) in
        self.view.endEditing(true)
        if self.feedType == .FeedList {
          self.routeToPostFeed()
        }
      }
      postInputView.sendButton.isUserInteractionEnabled = false
      tableView.contentInset = UIEdgeInsets(top: 65, left: 0, bottom: 0, right: 0)
      getFeeds()
    } else {
      postInputView.inputField.placeholder = "Write a comment"
      postInputView.sendButton.isUserInteractionEnabled = true
      postInputView.sendTapped = {(sender) in
        self.postComment()
      }
      setupProfileNav()
      tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
      getComments()
    }
  }
  
  private func setupPostView() {
    let postView = UIView()
    postView.backgroundColor = .white
    postView.translatesAutoresizingMaskIntoConstraints = false
    postView.addSubview(postInputView)
    self.view.addSubview(postView)
    let edge = feedType == .FeedList ? 12 : 0
    postView.snp.makeConstraints { (maker) in
      if feedType == .FeedList {
        maker.top.equalTo(self.tableView.snp.top)
      } else {
        maker.bottom.equalTo(self.tableView.snp.bottom)
      }
      maker.leading.equalTo(self.view.snp.leading)
      maker.trailing.equalTo(self.view.snp.trailing)
    }
    postInputView.snp.makeConstraints { (maker) in
      maker.top.equalTo(postView.snp.top).offset(edge)
      maker.bottom.equalTo(postView.snp.bottom).inset(edge)
      maker.leading.equalTo(postView.snp.leading).offset(edge)
      maker.trailing.equalTo(postView.snp.trailing).inset(edge)
      maker.height.equalTo(40)
    }
    postInputView.inputField.textViewDidChangeHandler = {(sender) in
      if let textview = sender as? BaseTextView {
        var height = textview.text.height(withConstrainedWidth: Constant.screenSize.width - 74, font: .defaultSemiBoldFont(size: 14)) + 32
        height = height < 40 ? 40 : (height > 150 ? 150 : height)
        self.postInputView.snp.updateConstraints { (maker) in
          maker.height.equalTo(height)
        }
      }
    }
  }
  
  private func setupProfileNav() {
    let titleView = UIView(frame: .init(x: 0, y: 0, width: Constant.screenSize.width - 30, height: 40))
    profileView.frame = .init(x: 0, y: 0, width: Constant.screenSize.width - 60, height: 40)
    if let urlString = feed?.coverPicture, let url = URL(string: urlString) {
      profileView.profileImageView.af.setImage(withURL: url)
    } else {
      profileView.profileImageView.image = UIImage(named: "bg_profile_picture")
    }
    profileView.nameLabel.text = feed?.fullname
    profileView.timeLabel.text = feed?.createdAt?.toDate(with: "yyyy-MM-dd HH:mm:ss").timeAgoDisplay()
    profileView.actionButton.addTarget(self, action: #selector(showPopTip(_:)), for: .touchUpInside)
    titleView.addSubview(profileView)
    profileView.profileImageView.layer.cornerRadius = (profileView.frame.width*0.1)/2
    self.navigationItem.titleView = titleView
  }
  
//  MARK: Service
  private func getNotificationBadge() {
      provider.request(.UnreadNotifications, successModel: NotificationBadgeResponseModel.self) { (result) in
          self.badgeNotif?.setCount(result.notification)
      } failure: { (error) in
          print(error.messages.joined(separator: "\n"))
      }
  }
  
  private func getChatBadge() {
      provider.request(.UnreadMessages, successModel: MessageBadgeResponseModel.self) { (result) in
          self.badgeChat?.setCount(result.unreadMessage)
      } failure: { (error) in
          print(error.messages.joined(separator: "\n"))
      }
  }
  
  @objc private func getFeeds() {
    self.feeds = nil
    provider.request(.Feeds(request: pageModel), successModel: FeedsResponseModel.self) { (result) in
      self.feeds = result
      self.tableView.reloadData()
      self.refreshControl.endRefreshing()
    } failure: { (error) in
      self.refreshControl.endRefreshing()
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func getComments() {
    if let id = feed?.postId {
      provider.request(.FeedComments(identifier: id, request: pageModel), successModel: FeedCommentsResponseModel.self) { (result) in
        self.feed?.comments = result.data
        self.tableView.reloadData()
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
  private func doLikePost(selectedFeed: FeedModel) {
    guard let request = LikeFeedRequestModel(JSON: ["isLiked": NSNumber(value: !selectedFeed.isLiked).intValue,
                                                    "type": LikeType.Post.rawValue]) else { return }
    if let id = selectedFeed.postId {
      provider.request(.LikeFeed(identifier: id, request: request), successModel: SuccessResponseModel.self) { (result) in
        print("like post success")
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
  private func doLikeComment(selectedComment: FeedCommentModel) {
    guard let request = LikeFeedRequestModel(JSON: ["isLiked": NSNumber(value: !selectedComment.isLiked).intValue,
                                                    "type": LikeType.Comment.rawValue]) else { return }
    if let id = selectedComment.commentId {
      provider.request(.LikeFeed(identifier: id, request: request), successModel: SuccessResponseModel.self) { (result) in
        print("like comment success")
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
  private func postComment() {
    if let id = feed?.postId {
      guard let postCommentRequestModel = PostFeedRequestModel(JSON: ["content": postInputView.inputField.text ?? ""]) else { return }
      provider.request(.PostComment(identifier: id, request: postCommentRequestModel), successModel: SuccessResponseModel.self) { (result) in
        self.postInputView.inputField.placeholder = "Write a comment"
        self.view.endEditing(true)
        self.getComments()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kPostCommentResponse), object: "add")
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
  private func deletePost(selectedFeed: FeedModel?) {
    popTip.dismiss()
    if let id = selectedFeed?.postId {
      provider.request(.DeleteFeed(identifier: id), successModel: SuccessResponseModel.self) { (result) in
        if self.feedType == .FeedList {
          if let index = self.feeds?.data.firstIndex(where: { $0.postId == selectedFeed?.postId }) {
            self.tableView.beginUpdates()
            self.feeds?.data.remove(at: index)
            self.tableView.deleteSections([index], with: .fade)
            self.tableView.endUpdates()
          }
        } else {
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: kDeleteFeedResponse), object: nil)
          self.navigationController?.popViewController(animated: true)
        }
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
//  MARK: Route
  func routeToFeedDetail(selectedFeed: FeedModel, isCommentTapped: Bool = false) {
    let destinationVC = FeedVC(nibName: "FeedVC", bundle: nil)
    destinationVC.feedType = .FeedDetail
    destinationVC.feed = selectedFeed
    destinationVC.isCommentTapped = isCommentTapped
    self.navigationController?.pushViewController(destinationVC, animated: true)
  }
  
  func routeToPostFeed(selectedFeed: FeedModel? = nil) {
    let destinationVC = PostFeedVC(nibName: "PostFeedVC", bundle: nil)
    destinationVC.feed = selectedFeed
    self.navigationController?.pushViewController(destinationVC, animated: true)
  }
  
  func routeToAccountProfile(userId: String) {
    let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
    vc.profileType = .Other
    vc.userId = userId
    vc.navigationItem.titleView = navLabel(title: "Profile Details")
    navigationController?.pushViewController(vc, animated: true)
  }
  
//  MARK: Action
  override func handlePostFeed(_ sender: Any) {
    pageModel = PagingRequestModel()
    getFeeds()
  }
  
  override func handleDeleteFeed(_ sender: Any) {
    if self.feedType == .FeedList {
      let index = selectedFeedIndexpath.section
      self.tableView.beginUpdates()
      self.feeds?.data.remove(at: index)
      self.tableView.deleteSections([index], with: .fade)
      self.tableView.endUpdates()
    }
  }
  
  @IBAction func showPopTip(_ sender: Any) {
    //poptip from detail feed
    let sWidth = UIScreen.main.bounds.size.width - 40
    let sHeight = UIScreen.main.bounds.size.height - 60
    if feed?.sugarId == AuthManager.shared.currentUser?.sugarId {
      popTipView.editButton.isHidden = false
      popTipView.editString = "Edit"
      popTipView.destructionString = "Delete"
      popTipView.editTapped = {(sender) in
        self.popTip.dismiss()
        self.routeToPostFeed(selectedFeed: self.feed)
      }
      popTipView.deleteTapped = {(sender) in
        self.popTip.dismiss()
        self.deletePost(selectedFeed: self.feed)
      }
    } else {
      //report feed from feed detail page
      popTipView.editButton.isHidden = true
      popTipView.destructionString = "Report"
      popTipView.deleteTapped = {(sender) in
        self.popTip.dismiss()
        let destinationVC = ReportFeedVC()
        destinationVC.selectedID = self.feed?.postId ?? ""
        self.presentPopup(destinationVC, animated: true, constraints: [.width(sWidth), .height(sHeight)])
      }
    }
    popTipView.frame = .init(x: 0, y: 0, width: 150, height: popTipView.editButton.isHidden ? 40 : 80)
    if let button = sender as? UIButton {
      popTip.show(popTipView, fromView: button )
    }
  }
  
//  MARK: Custom
  override func handlePostComment(_ sender: Any) {
    if feedType == .FeedList {
      if let cell = tableView.cellForRow(at: selectedFeedIndexpath) as? FeedTableCell {
        if let feed = feeds?.data[selectedFeedIndexpath.section] {
          feed.totalComment +=  1
          cell.bind(likeCount: feed.totalLike, commentCount: feed.totalComment)
        }
      }
    }
  }
  
  override func handleUpdateFeed(_ sender: Any) {
    if let notification = sender as? NSNotification, let updatedFeed = notification.object as? FeedModel {
      self.tableView.beginUpdates()
      if feedType == .FeedList {
        feeds?.data[selectedFeedIndexpath.section] = updatedFeed
        DispatchQueue.main.async {
          self.tableView.reloadSections([self.selectedFeedIndexpath.section], with: .automatic)
        }
      } else {
        feed = updatedFeed
        tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
      }
      self.tableView.endUpdates()
    }
  }
}

extension FeedVC: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return feedType == .FeedList ? feeds?.data.count ?? 0 : 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return feedType == .FeedList ? 1 : (feed?.comments?.count ?? 0) + 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.row == 0 {
      let feedCell = tableView.dequeueReusableCell(withIdentifier: "FeedTableCell", for: indexPath) as! FeedTableCell
      feedCell.isHideAction = feedType == .FeedDetail
      var feedBinding: FeedModel?
      if feedType == .FeedList {
        if let feed = feeds?.data[indexPath.section] {
          feedBinding = feed
        }
      } else {
        if let feed = feed {
          feedBinding = feed
        }
      }
      if let feed = feedBinding {
        feedCell.bind(feed: feed)
        feedCell.likeTapped = {(sender) in
          self.doLikePost(selectedFeed: feed)
          feed.isLiked = !feed.isLiked
          feed.totalLike = feed.totalLike + (feed.isLiked ? 1 : -1)
          feedCell.bind(likeCount: feed.totalLike, commentCount: feed.totalComment)
        }
        feedCell.commentTapped = { _ in
          self.routeToFeedDetail(selectedFeed: feed, isCommentTapped: true)
        }
        feedCell.profileTapped = { (sender) in
          self.routeToAccountProfile(userId: feed.userId ?? "")
        }
      }
      feedCell.popTipView.editTapped = {(sender) in
        feedCell.popTip.dismiss()
        self.selectedFeedIndexpath = indexPath
        self.routeToPostFeed(selectedFeed: feedBinding)
      }
      feedCell.popTipView.deleteTapped = {(sender) in
        feedCell.popTip.dismiss()
        self.selectedFeedIndexpath = indexPath
        if feedBinding?.sugarId == AuthManager.shared.currentUser?.sugarId {
          self.deletePost(selectedFeed: feedBinding)
        }
      }
      return feedCell
    } else {
      let feedCommentCell = tableView.dequeueReusableCell(withIdentifier: "FeedCommentCell", for: indexPath) as! FeedCommentCell
      feedCommentCell.isHideAction = feedType == .FeedList
      feedCommentCell.selectionStyle = .none
      if let comment = feed?.comments?[indexPath.row - 1] {
        feedCommentCell.bind(comment: comment)
        feedCommentCell.likeTapped = {(sender) in
          self.doLikeComment(selectedComment: comment)
          comment.isLiked = !comment.isLiked
          comment.totalLike = comment.totalLike + (comment.isLiked ? 1 : -1)
          feedCommentCell.bind(likeCount: comment.totalLike, isLiked: comment.isLiked)
        }
      }
      return feedCommentCell
    }
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = UIView()
    headerView.backgroundColor = .init(hex: "F6F6F6")
    return headerView
  }
}

extension FeedVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return section == 0 ? 1 : 20
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0.0001
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if feedType == .FeedList && indexPath.row == 0{
      if feedType == .FeedList {
        selectedFeedIndexpath = indexPath
        if let feed = feeds?.data[selectedFeedIndexpath.section] {
          routeToFeedDetail(selectedFeed: feed)
        }
      }
    }
  }
}
