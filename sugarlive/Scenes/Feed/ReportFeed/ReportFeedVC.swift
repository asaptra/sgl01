//
//  ReportFeedVC.swift
//  sugarlive
//
//  Created by dodets on 04/06/22.
//

import UIKit
import PopItUp

class ReportFeedVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var closeView: UIView!
    
    private var banCategories: [BanCategoryModel] = []
    private var banReasons: [BanReasonModel] = []
    private var reportPostFeedRequest = ReportPostFeedRequestModel()
    var isReason = false
    var selectedID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewContainer.cornerRadius = 16
        titleLbl.font = .defaultBoldFont(size: 12)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "BanSelectionCell", bundle: nil), forCellReuseIdentifier: "BanSelectionCell")
        getBanCategory()
        closeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissPopTip)))
    }
    
    @objc func dismissPopTip() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override  func getBanCategory() {
        provider.request(.BanCategory, successModel: BanCategoryResponseModel.self) { (result) in
            self.banCategories = result.attributes
            self.tableView.reloadData()
        } failure: { (error) in
            self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
        }
    }
    
    override func getBanReason(category: BanCategoryModel) {
        if let categoryId = category.categoryId {
            provider.request(.BanReason(identifier: categoryId), successModel: BanReasonResponseModel.self) { (result) in
                self.banReasons = result.attributes
                self.isReason = true
                self.tableView.reloadData()
            } failure: { (error) in
                self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
            }
        }
    }
    
    func reportFeed(reason: BanReasonModel) {
        reportPostFeedRequest.reasonId = reason.reasonId
        reportPostFeedRequest.postId = selectedID
        provider.request(.ReportPostFeed(request: reportPostFeedRequest), successModel: SuccessResponseModel.self) { (result) in
            self.routeToWarningModal(message: "Report successfully sent") {
                self.dismiss(animated: true, completion: nil)
            }
        } failure: { (error) in
            self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
        }
    }
    
    
}

// MARK: - Table view delegate and data source
extension ReportFeedVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        isReason ? self.banReasons.count : self.banCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BanSelectionCell") as! BanSelectionCell
        if !isReason {
            cell.setTitle(self.banCategories[indexPath.row].name ?? "")
        } else {
            cell.setTitle(self.banReasons[indexPath.row].reason ?? "")
        }
        cell.selectionLabel.textAlignment = .left
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isReason {
            let data = self.banReasons[indexPath.row]
            reportFeed(reason: data)
        } else {
            let data = self.banCategories[indexPath.row]
            getBanReason(category: data)
        }
    }
}
