//
//  HomeVC.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit
import FSPagerView
import BadgeHub

class HomeVC: BaseCollectionVC {
    
    enum HomeType {
        case LiveNow
        case Recomended
        case NewComer
        case Event
        case Followed
        
        func title() -> String {
            switch self {
            case .LiveNow:
                return "Live Now"
            case .Recomended:
                return "Recommended"
            case .NewComer:
                return "Newcomers"
            case .Event:
                return "Events"
            case .Followed:
                return "Followed"
            }
        }
    }
    
    lazy var bannerView: FSPagerView = {
        let pagerView = FSPagerView()
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        pagerView.isInfinite = true
        pagerView.isScrollEnabled = true
        pagerView.automaticSlidingInterval = 3.0
        pagerView.translatesAutoresizingMaskIntoConstraints = false
        return pagerView
    }()
    
    var homeType: HomeType = .LiveNow
    var pageModel = PagingRequestModel()
    
    private var bannerResponseModel: BannerResponseModel?
    private var liveUserResponseModel: UserLiveResponseModel?
    private var recomendedUserResponseModel: UserLiveResponseModel?
    private var newUserResponseModel: UserLiveResponseModel?
    private var onlineResponseModel: UserLiveResponseModel?
    private var offlineResponseModel: UserLiveResponseModel?
    private var eventResponseModel: EventResponseModel?
    
    private var badgeNotif: BadgeHub?
    private var badgeChat: BadgeHub?
    
    private var isCapturing: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(viewerValidationRequest(_:)), name: NSNotification.Name(kLiveViewerValidateRoomRequirement), object: nil)
//        if #available(iOS 11.0, *) {
//            NotificationCenter.default.addObserver(forName: UIScreen.capturedDidChangeNotification, object: nil, queue: .main, using: { _ in
//                self.isCapturing.toggle()
//            })
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        setupNav()
        if Constant.isLogin {
            getNotificationBadge()
            getChatBadge()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if homeType == .LiveNow {
            getLiveUser()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(kLiveViewerValidateRoomRequirement), object: nil)
    }
    
    //  MARK: Setup
    override func setupLayout() {
        setupCollection()
        
        switch homeType {
        case .LiveNow:
            bannerView.dataSource = self
            bannerView.delegate = self
            getBanner()
            getLiveUser()
            break
        case .Recomended:
            getRecomendedUser()
            break
        case .NewComer:
            getNewUser()
            break
        case .Event:
            getEvent()
            break
        case .Followed:
            getOnlineFollowing()
            getOfflineFollowing()
            break
        }
    }
    
    override func setupNav() {
        let searchButon = UIButton()
        searchButon.setImage(UIImage(named: "ic_search_white"), for: .normal)
        searchButon.setTitle("", for: .normal)
        searchButon.addTarget(self, action: #selector(searchAction(_:)), for: .touchUpInside)
        let searchBarItem = UIBarButtonItem.init(customView: searchButon)
        
        let chatButon = UIButton()
        chatButon.setImage(UIImage(named: "ic_chat_white"), for: .normal)
        chatButon.setTitle("", for: .normal)
        chatButon.addTarget(self, action: #selector(chatAction(_:)), for: .touchUpInside)
        let chatBarItem = UIBarButtonItem.init(customView: chatButon)
        badgeChat = BadgeHub(barButtonItem: chatBarItem)
        badgeChat?.moveCircleBy(x: 19, y: 0)
        badgeChat?.setCircleBorderColor(.PRIMARY_1, borderWidth: 2)
        badgeChat?.scaleCircleSize(by: 0.9)
        
        let notifButon = UIButton()
        notifButon.setImage(UIImage(named: "ic_notif_white"), for: .normal)
        notifButon.setTitle("", for: .normal)
        notifButon.addTarget(self, action: #selector(notifAction(_:)), for: .touchUpInside)
        let notifBarItem = UIBarButtonItem.init(customView: notifButon)
        badgeNotif = BadgeHub(barButtonItem: notifBarItem)
        badgeNotif?.moveCircleBy(x: 19, y: 0)
        badgeNotif?.setCircleBorderColor(.PRIMARY_1, borderWidth: 2)
        badgeNotif?.scaleCircleSize(by: 0.9)
        
        searchBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBarItem.customView?.heightAnchor.constraint(equalToConstant: 23).isActive = true
        chatBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true
        chatBarItem.customView?.heightAnchor.constraint(equalToConstant: 23).isActive = true
        notifBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true
        notifBarItem.customView?.heightAnchor.constraint(equalToConstant: 23).isActive = true
        
        if let mainTabVC = self.tabBarController as? MainTabVC {
            mainTabVC.navigationItem.rightBarButtonItems = [notifBarItem, chatBarItem, searchBarItem]
            mainTabVC.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: UIImageView(image: UIImage(named: "ic_nav_logo")))
        }
    }
    
    private func setupCollection() {
        collectionView.dataSource = self
        collectionView.delegate = self
        
        cells = ["StreamerCollectionCell", "PostImageCollectionCell"]
        headers = ["StreamerCollectionHeaderView"]
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        configureCollectionRefreshControl()
    }
    
    override func refreshHandler() {
        switch homeType {
        case .LiveNow:
            getLiveUser()
            break
        case .Recomended:
            getRecomendedUser()
            break
        case .NewComer:
            getNewUser()
            break
        case .Event:
            getEvent()
            break
        case .Followed:
            getOnlineFollowing()
            getOfflineFollowing()
            break
        }
        collectionRefreshControl?.endRefreshing()
    }
    
    override func handleReadNotification(_ sender: Any) {
        getNotificationBadge()
    }
    
    // MARK: Custom Functions
    private func showLiveEndModal(user: UserModel, endType: LiveEndType = .Default) {
        let vc = LiveEndVC(for: user, endType: endType)
        present(vc, animated: true, completion: nil)
    }
    
    private func showRequestRoomPasswordModal(userModel: UserModel) {
        let vc = PopupTextFieldVC(title: "Please enter password to watch", actionTitle: "Enter", placeholder: "Room password", keyboardType: .numberPad, isSecureTextEntry: true)
        vc.submitTapped = { [unowned userModel, unowned self] text in
            self.validateLiveRoom(userModel: userModel, requirement: text)
        }
        present(vc, animated: true)
    }
    
    private func showRequestCandyRoomModal(userModel: UserModel, requirement: String) {
        popupAlertVC.alertType = .Alert
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = "Pay \(requirement)"
        popupAlertVC.messageStr = "You will be charged for this session"
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = { [unowned popupAlertVC] (sender) in
            popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = { [unowned self, unowned popupAlertVC] (sender) in
            popupAlertVC.dismiss(animated: true) {
                self.validateLiveRoom(userModel: userModel, requirement: requirement)
            }
        }
        present(popupAlertVC, animated: true, completion: nil)
    }
    
    @objc private func viewerValidationRequest(_ notification: NSNotification) {
        if let userModel = notification.object as? UserModel {
            watchLive(userModel: userModel) { [unowned userModel] (user, detail) in
                userModel.socialMedia = user.socialMedia
                var multihostType: MultiHostType = .Solo
                switch detail.multiHostCount {
                case 2, 3:
                    multihostType = .Solo
                case 4:
                    multihostType = .FourMultiHost
                case 6:
                    multihostType = .SixMultiHost
                case 9:
                    multihostType = .GroupCall
                default:
                    multihostType = .Solo
                }
                if multihostType == .Solo {
                    self.routeToLiveAsViewer(userModel: userModel, detailModel: detail)
                } else {
                    self.routeToMultiguestLiveAsViewer(userModel: userModel, multihostType: multihostType, detailModel: detail)
                }
            }
        }
    }
    
    //  MARK: Service
    private func getBanner() {
        provider.request(.Banner, successModel: BannerResponseModel.self) { (result) in
            self.bannerResponseModel = result
            self.bannerView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getLiveUser() {
        provider.request(.LiveUser, successModel: UserLiveResponseModel.self, withProgressHud: false) { (result) in
            self.liveUserResponseModel = result
            self.collectionView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getRecomendedUser() {
        provider.request(.RecomendedMember, successModel: UserLiveResponseModel.self) { (result) in
            self.recomendedUserResponseModel = result
            self.collectionView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getNewUser() {
        provider.request(.NewMember(request: pageModel), successModel: UserLiveResponseModel.self) { (result) in
            self.newUserResponseModel = result
            self.collectionView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getEvent() {
        provider.request(.Event(request: pageModel), successModel: EventResponseModel.self) { (result) in
            self.eventResponseModel = result
            self.collectionView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getOnlineFollowing() {
        if let currentUser = AuthManager.shared.currentUser, let userId = currentUser.userId {
            provider.request(.OnLiveFollowing(identifier: userId), successModel: UserLiveResponseModel.self) { (result) in
                self.onlineResponseModel = result
                self.collectionView.reloadData()
            } failure: { (error) in
                print(error.messages.joined(separator: "\n"))
            }
        }
    }
    
    private func getOfflineFollowing() {
        if let currentUser = AuthManager.shared.currentUser, let userId = currentUser.userId {
            provider.request(.OfflineFollowing(identifier: userId), successModel: UserLiveResponseModel.self) { (result) in
                self.offlineResponseModel = result
                self.collectionView.reloadData()
            } failure: { (error) in
                print(error.messages.joined(separator: "\n"))
            }
        }
    }
    
    private func getNotificationBadge() {
        provider.request(.UnreadNotifications, successModel: NotificationBadgeResponseModel.self) { (result) in
            self.badgeNotif?.setCount(result.notification)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getChatBadge() {
        provider.request(.UnreadMessages, successModel: MessageBadgeResponseModel.self) { (result) in
            self.badgeChat?.setCount(result.unreadMessage)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func watchLive(userModel: UserModel, completion: @escaping (UserModel, WatchLiveDetailModel) -> Void) {
        guard let request = WatchAndStopWatchLiveRequestModel(JSON: ["liveId": userModel.userId ?? ""]) else { return }
        provider.request(.WatchLive(request: request), successModel: WatchLiveResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.returnOtherProfile(userId: userModel.userId ?? "") { model in
                if let model = model {
                    completion(model, (result.attributes?.detail)!)
                }
            }
        } failure: { [weak self] (error) in
            guard let self = self else { return }
            let detail = error.detail as? String
            if detail == "You're blocked from this live session" {
                self.showLiveEndModal(user: userModel, endType: .BannedSession)
            } else {
                guard let validateErrorModel = WatchLiveErrorDetailResponseModel(JSON: error.detail as? [String: Any] ?? [:]) else {
                    self.showLiveEndModal(user: userModel)
                    return
                }
                
                if validateErrorModel.privateType == PrivateRoomType.Password.rawValue {
                    self.showRequestRoomPasswordModal(userModel: userModel)
                } else if validateErrorModel.privateType == PrivateRoomType.PayPerSession.rawValue {
                    self.showRequestCandyRoomModal(userModel: userModel, requirement: validateErrorModel.requirement ?? "")
                } else {
                    self.showLiveEndModal(user: userModel)
                }
            }
            
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func returnOtherProfile(userId: String, completion: @escaping (UserModel?) -> Void) {
        provider.request(.UserProfile(identifier: userId), successModel: UserResponseModel.self) { (result) in
            if let userModel = result.attributes {
                completion(userModel)
            } else {
                completion(nil)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
            completion(nil)
        }
    }
    
    private func validateLiveRoom(userModel: UserModel, requirement: String) {
        guard let request = LiveValidateRequestModel(JSON: ["liveId": userModel.liveId ?? "", "requirement": requirement]) else { return }
        provider.request(.LiveValidate(request: request), successModel: SuccessResponseModel.self, success: { [weak self, unowned userModel] result in
            guard let self = self else { return }
            self.watchLiveAfterValidating(userModel: userModel)
        }, failure: { [weak self] error in
            guard let self = self else { return }
            let detail = error.detail as? String
            if detail == "Live Ended" {
                self.showLiveEndModal(user: userModel)
            }
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    private func watchLiveAfterValidating(userModel: UserModel) {
        guard let request = WatchAndStopWatchLiveRequestModel(JSON: ["liveId": userModel.userId ?? ""]) else { return }
        provider.request(.WatchLive(request: request), successModel: WatchLiveResponseModel.self) { [weak self, unowned userModel] (result) in
            guard let self = self, let detail = result.attributes?.detail, let user = result.attributes?.profile else { return }
            userModel.socialMedia = user.socialMedia
            var multihostType: MultiHostType = .Solo
            switch detail.multiHostCount {
            case 2, 3:
                multihostType = .Solo
            case 4:
                multihostType = .FourMultiHost
            case 6:
                multihostType = .SixMultiHost
            case 9:
                multihostType = .GroupCall
            default:
                multihostType = .Solo
            }
            if multihostType == .Solo {
                self.routeToLiveAsViewer(userModel: userModel, detailModel: detail)
            } else {
                self.routeToMultiguestLiveAsViewer(userModel: userModel, multihostType: multihostType, detailModel: detail)
            }
        } failure: { [weak self] (error) in
            guard let self = self else { return }
            let detail = error.detail as? String
            if detail == "You're blocked from this live session" {
                self.showLiveEndModal(user: userModel, endType: .BannedSession)
            } else {
                self.showLiveEndModal(user: userModel)
            }
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    //  MARK: Action
    private func showCaptureIsProhibitedAlert() {
        let alert = UIAlertController(title: "Screen record is prohibited", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        alert.addAction(action)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true)
    }
}

// MARK: - Collection view data source
extension HomeVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        switch homeType {
        case .LiveNow:
            return 4
        case .Followed:
            return 2
        default:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch homeType {
        case .LiveNow:
            let totalUser = liveUserResponseModel?.attributes.count ?? 0
            if [0, 1].contains(section) {
                return 1
            } else if [2].contains(section) {
                return totalUser > 2 ? 2 : totalUser
            } else {
                return totalUser > 2 ? totalUser - 2 : 0
            }
        case .Recomended:
            return recomendedUserResponseModel?.attributes.count ?? 0
        case .NewComer:
            return newUserResponseModel?.attributes.count ?? 0
        case .Event:
            return eventResponseModel?.attributes?.count ?? 0
        case .Followed:
            return section == 0 ? (onlineResponseModel?.attributes.count ?? 0) : (offlineResponseModel?.attributes.count ?? 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let streamerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StreamerCollectionCell", for: indexPath) as! StreamerCollectionCell
        var userModel: UserModel?
        var isTop = false
        
        switch homeType {
        case .LiveNow:
            switch indexPath.section {
            case 0:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
                cell.contentView.addSubview(bannerView)
                bannerView.snp.makeConstraints { (maker) in
                    maker.edges.equalTo(cell.contentView)
                }
                return cell
            case 1:
                return collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            case 3:
                userModel = liveUserResponseModel?.attributes[indexPath.row+2]
                break
            default:
                userModel = liveUserResponseModel?.attributes[indexPath.row]
                isTop = true
                break
            }
        case .Recomended:
            userModel = recomendedUserResponseModel?.attributes[indexPath.row]
            break
        case .NewComer:
            userModel = newUserResponseModel?.attributes[indexPath.row]
            break
        case .Event:
            let eventCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostImageCollectionCell", for: indexPath) as! PostImageCollectionCell
            if let event = eventResponseModel?.attributes?[indexPath.row] {
                if let urlString = event.image, let url = URL(string: urlString) {
                    eventCell.bind(url: url)
                }
            }
            return eventCell
        case .Followed:
            if indexPath.section == 0 {
                userModel = onlineResponseModel?.attributes[indexPath.row]
            } else {
                userModel = offlineResponseModel?.attributes[indexPath.row]
            }
        }
        
        if let model = userModel {
            streamerCell.bind(user: model, isTop)
        }
        
        return streamerCell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "StreamerCollectionHeaderView", for: indexPath) as! StreamerCollectionHeaderView
        if homeType == .Followed && kind == UICollectionView.elementKindSectionHeader {
            headerView.titleLabel.text = indexPath.section == 0 ? "Online" : "Offline"
        }
        return headerView
    }
    
    func routeToOtherProfileVC(userID: String) {
        let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
        vc.profileType = .Other
        vc.userId = userID
        vc.navigationItem.titleView = navLabel(title: "Profile Details")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - Collection view delegate
extension HomeVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if !Constant.isLogin {
            routeToLoginVia()
        } else {
            switch homeType {
            case .Event:
                if let event = eventResponseModel?.attributes?[indexPath.row] {
                    if let urlString = event.link {
                        routeToWebVC(type: .Custom(name: event.name ?? "", url: urlString))
                    }
                }
            case .LiveNow:
                if indexPath.section == 2 {
//                    getLiveUser()
                    if let userModel = liveUserResponseModel?.attributes[indexPath.row] {
                        watchLive(userModel: userModel) { (user, detail) in
                            user.startStreaming = userModel.startStreaming ?? ""
                            var multihostType: MultiHostType = .Solo
                            switch detail.multiHostCount {
                            case 2, 3:
                                multihostType = .Solo
                            case 4:
                                multihostType = .FourMultiHost
                            case 6:
                                multihostType = .SixMultiHost
                            case 9:
                                multihostType = .GroupCall
                            default:
                                multihostType = .Solo
                            }
                            if multihostType == .Solo {
                                self.routeToLiveAsViewer(userModel: user, detailModel: detail)
                            } else {
                                self.routeToMultiguestLiveAsViewer(userModel: user, multihostType: multihostType, detailModel: detail)
                            }
                        }
                    }
                } else if indexPath.section == 3 {
                    if let userModel = liveUserResponseModel?.attributes[indexPath.row + 2] {
                        watchLive(userModel: userModel) { (user, detail) in
                            user.startStreaming = userModel.startStreaming ?? ""
                            var multihostType: MultiHostType = .Solo
                            switch detail.multiHostCount {
                            case 2, 3:
                                multihostType = .Solo
                            case 4:
                                multihostType = .FourMultiHost
                            case 6:
                                multihostType = .SixMultiHost
                            case 9:
                                multihostType = .GroupCall
                            default:
                                multihostType = .Solo
                            }
                            if multihostType == .Solo {
                                self.routeToLiveAsViewer(userModel: user, detailModel: detail)
                            } else {
                                self.routeToMultiguestLiveAsViewer(userModel: user, multihostType: multihostType, detailModel: detail)
                            }
                        }
                    }
                }
//                else {
//                    showCaptureIsProhibitedAlert()
//                }
            case .Recomended:
                if let userModel = recomendedUserResponseModel?.attributes[indexPath.row] {
                    routeToOtherProfileVC(userID: userModel.userId ?? "")
                }
            case .NewComer:
                if let userModel = newUserResponseModel?.attributes[indexPath.row] {
                    routeToOtherProfileVC(userID: userModel.userId ?? "")
                }
            case .Followed:
                if indexPath.section == 0 {
                    let userModel = onlineResponseModel?.attributes[indexPath.row]
                    routeToOtherProfileVC(userID: userModel?.userId ?? "")
                } else {
                    let userModel = offlineResponseModel?.attributes[indexPath.row]
                    routeToOtherProfileVC(userID: userModel?.userId ?? "")
                }
            }
        }
    }
}

// MARK: - Collection view delegate flow layout
extension HomeVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = collectionView.frame.width
        if homeType == .Followed {
            return .init(width: width, height: 60)
        }
        return .init(width: width, height: 6)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = (Constant.screenSize.width - 20)
        var height = width
        switch homeType {
        case .LiveNow:
            if indexPath.section == 0 {
                width = width + 20
                height = (111 / 400) * width
            } else if indexPath.section == 1 {
                height = (60 / 380) * width
            } else {
                width = (width - 6) / 2
                if indexPath.section == 2 {
                    height = (292 / 185) * width
                } else {
                    height = width
                }
            }
            break
        case .Event:
            height = (115 / 400) * width
            break
        default:
            width = (width - 6) / 2
            height = width
            break
        }
        return .init(width: width, height: height)
    }
}

// MARK: - FSPagerView data source
extension HomeVC: FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return bannerResponseModel?.attributes?.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let bannerObject = bannerResponseModel?.attributes?[index]
        if let imageUrl = bannerObject?.urlImage, let url = URL(string: imageUrl) {
            cell.imageView?.af.setImage(withURL: url)
        } else {
            cell.imageView?.image = nil
        }
        return cell
    }
}

// MARK: - FSPagerView delegate
extension HomeVC: FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        let bannerObject = bannerResponseModel?.attributes?[index]
        if bannerObject?.baner != "Staff" {
            if let linkUrl = bannerObject?.urlLink {
                if (linkUrl.contains("whatsapp")) {
                    openURL(string: (bannerObject?.urlLink)!)
                } else {
                    routeToWebVC(type: .Custom(name: bannerObject?.baner ?? "", url: linkUrl))
                }
            }
        }
    }
}
