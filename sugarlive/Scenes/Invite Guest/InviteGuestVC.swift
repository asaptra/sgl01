//
//  InviteGuestVC.swift
//  sugarlive
//
//  Created by cleanmac on 11/12/21.
//

import UIKit
import ZegoExpressEngine

let kUpdateGuestList = "UpdateGuestList"
let kStartVideoCallInvitationCountdown = "StartVideoCallInvitationCountdown"

class InviteGuestVC: BaseTableVC {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var searchTextField: UITextField!
    
    // MARK: - Variables
    private var currentUser = AuthManager.shared.currentUser
    private var viewers: [UserModel] = []
    private var viewersContainer: [UserModel] = []
    private var position: Int?
    private var forBattle: Bool = false
    
    private var refreshControl = UIRefreshControl()
    
    var zegoUsers: [ZegoUser] = []

    // MARK: - Overriden Functions
    init(position: Int) {
        super.init(nibName: "InviteGuestVC", bundle: nil)
        self.position = position
    }
    
    init(battle: Bool = true) {
        super.init(nibName: "InviteGuestVC", bundle: nil)
        forBattle = battle
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getGuests()
        NotificationCenter.default.addObserver(self, selector: #selector(getGuests), name: NSNotification.Name(kUpdateGuestList), object: nil)
    }
    
    override func setupNav() {
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_back_white"), style: .plain, target: self, action: #selector(dismissAction))
        let navTitle = UIBarButtonItem(customView: navLabel(title: forBattle ? "Ask to Battle" : "Invite"))
        navigationItem.leftBarButtonItems = [backButton, navTitle]
        navigationController?.navigationBar.barTintColor = .PRIMARY_1
        navigationController?.navigationBar.tintColor = .white
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(kUpdateGuestList), object: nil)
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.refreshControl = refreshControl
        
        cells = ["InviteGuestCell", "NoGuestCell"]
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    // MARK: - Custom Functions
    private func showErrorModal(message: String) {
        popupAlertVC.actionStr = "Retry"
        popupAlertVC.hideCancelButton = true
        popupAlertVC.messageStr = message
        popupAlertVC.actionTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true) {
                self.getGuests()
            }
        }
        present(popupAlertVC, animated: true, completion: nil)
    }
    
    // MARK: - Action
    @objc private func refreshAction() {
        getGuests()
        refreshControl.endRefreshing()
    }
    
    @objc private func dismissAction() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text
        if viewers.isEmpty {
            return
        } else {
            if text?.count ?? 0 >= 1 {
                let filtered = viewers.filter { (item: UserModel) -> Bool in
                    let stringMatch = item.fullname?.lowercased().range(of: text?.lowercased() ?? "")
                    return stringMatch != nil ? true : false
                }
                viewersContainer = filtered
            } else {
                viewersContainer = viewers
            }
            tableView.reloadData()
        }
    }
    
    // MARK: - API Calls
    @objc private func getGuests() {
        provider.request(.GetBattleOpponents(request: BattleOpponentsRequestModel()), successModel: BattleOpponentsResponseModel.self, withProgressHud: false) { [weak self] (result) in
            guard let self = self else { return }
            if let viewersList = result.attributes {
                self.viewers = viewersList.filter { $0.isAdmin == false }
            }
            self.viewersContainer = self.viewers
            self.tableView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func sendInvitation(userId: String) {
        if let request = InviteMultiHostRequestModel(JSON: ["userId": userId, "multiHostId": currentUser?.userId ?? "", "position": position ?? 1]) {
            provider.request(.InviteMultiHost(request: request), successModel: SuccessResponseModel.self, withProgressHud: false) { [weak self] (result) in
                guard let self = self else { return }
                if !self.forBattle {
                    NotificationCenter.default.post(name: NSNotification.Name(kStartVideoCallInvitationCountdown), object: self.position)
                }
                self.dismissAction()
            } failure: { [weak self] (error) in
                guard let self = self else { return }
                print(error.messages.joined(separator: "\n"))
                self.showErrorModal(message: error.messages.first ?? "")
            }
        }
    }
}

// MARK: - Table View delegate and data source
extension InviteGuestVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewers.isEmpty ? 1 : 0
        case 1:
            return viewersContainer.count
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoGuestCell", for: indexPath) as! NoGuestCell
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InviteGuestCell", for: indexPath) as! InviteGuestCell
            if indexPath.section == 1 {
                cell.setContents(userModel: viewersContainer[indexPath.row])
            } else {
                cell.guestNameLabel.text = zegoUsers[indexPath.row].userName
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 100
        case 1:
            return 50
        default:
            return 50
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            tableView.deselectRow(at: indexPath, animated: true)
            if let userId = viewersContainer[indexPath.row].userId {
                sendInvitation(userId: userId)
            }
        } else if indexPath.section == 2 {
            sendInvitation(userId: zegoUsers[indexPath.row].userID)
        }
    }
}
