//
//  LevelVC.swift
//  sugarlive
//
//  Created by msi on 15/10/21.
//

import UIKit

class LevelVC: BaseVC {
  
    @IBOutlet weak var tableView: UITableView!
    
    var levelCategories = [LevelModel]()
    var levelProgress: LevelProgressModel?
    
    @IBOutlet weak var levelTitleLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var whatLevelLabel: UILabel!
    @IBOutlet weak var upgradeLevelLabel: UILabel!
    @IBOutlet weak var userLevelLabel: UILabel!
    @IBOutlet weak var userLevelBadgeLabel: UILabel!
    @IBOutlet weak var level1: UILabel!
    @IBOutlet weak var badge1: UIButton!
    @IBOutlet weak var level2: UILabel!
    @IBOutlet weak var badge2: UIButton!
    @IBOutlet weak var level3: UILabel!
    @IBOutlet weak var badge3: UIButton!
    @IBOutlet weak var level4: UILabel!
    @IBOutlet weak var badge4: UIButton!
    @IBOutlet weak var level5: UILabel!
    @IBOutlet weak var badge5: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
      self.setupTable()
      self.getLevelCategory()
      self.getLevelProgress()
  }
    
    //Service
    func getLevelCategory() {
        provider.request(.LevelCategory, successModel: LevelCategoryResponseModel.self) { (result) in
            self.levelCategories = result.attributes
            self.tableView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func getLevelProgress() {
        provider.request(.LevelProgress, successModel: LevelProgressResponseModel.self) { (result) in
            self.levelProgress = result.attributes ?? LevelProgressModel()
            self.tableView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func setupTable() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 16)
        tableView.register(UINib(nibName: "LevelCategoryCell", bundle: nil), forCellReuseIdentifier: "LevelCategoryCell")
        tableView.register(UINib(nibName: "LevelCell", bundle: nil), forCellReuseIdentifier: "LevelCell")
    }

  override func setupNav() {
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: "Level"))
    self.navigationItem.leftItemsSupplementBackButton = true
  }
  
    override func setupLayout() {
        let font = UIFont.defaultBoldFont(size: 10)
        let badgeFont = UIFont.defaultBoldFont(size: 8)
        userLevelLabel.font = font
        userLevelLabel.textColor = .white
        userLevelLabel.text = "User Level"
        userLevelBadgeLabel.font = font
        userLevelBadgeLabel.textColor = .white
        userLevelBadgeLabel.text = "User Level Badge"

        level1.font = font
        level1.text = "0-100"
        badge1.setAttributedTitle("Level 1".toAttributedString(with: [NSAttributedString.Key.font: badgeFont, NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        badge1.layer.cornerRadius = 10
        badge1.layer.masksToBounds = true
        badge1.setBackgroundImage(UIImage(color: .init(hex: "999999")), for: .normal)
    }
}
// MARK: - Table view delegate and data source
    
extension LevelVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return levelCategories.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LevelCategoryCell", for: indexPath) as! LevelCategoryCell
            cell.setupUI()
            cell.levelLabel.text = "\(levelProgress?.currentLevel ?? 0)"
            cell.pointLabel.text = "\(levelProgress?.currentExp ?? 0) / \(levelProgress?.nextExp ?? 0)"
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LevelCell", for: indexPath) as! LevelCell
            let data = levelCategories[indexPath.row] as LevelModel
            cell.userLevel.text = data.levelStep
            let badgeColor = data.badgeColor?.replacingOccurrences(of: "#", with: "")
            cell.levelBadge.setBackgroundImage(UIImage(color: .init(hex: badgeColor ?? "")), for: .normal)
            return cell
        }
    }
}

extension LevelVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
