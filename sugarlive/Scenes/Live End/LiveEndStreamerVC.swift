//
//  LiveEndStreamerVC.swift
//  sugarlive
//
//  Created by cleanmac on 17/11/21.
//

import UIKit

class LiveEndStreamerVC: UIViewController {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var viewerCountLabel: UILabel!
    @IBOutlet weak var fansCountLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var carrotCountLabel: UILabel!
    
    @IBOutlet weak var homeButton: UIButton!
    
    // MARK: - Variables
    var userModel: UserModel?
    var streamDuration: TimeInterval = 0
    
    var delegate: LiveEndVCDelegate?

    // MARK: - Overriden Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        getLiveSummary()
    }
    
    // MARK: - UI Setups
    private func setupUI(_ model: LiveSummaryResponseModel) {
        if let urlString = userModel?.userPictureUrl, let url = URL(string: urlString) {
            backgroundImage.af.setImage(withURL: url)
            coverImage.af.setImage(withURL: url)
        }
        
        durationLabel.text = streamDuration.stringFromTimeInterval()
        nameLabel.text = userModel?.fullname ?? ""
        
        homeButton.titleLabel?.font = .defaultSemiBoldFont(size: 16)
        homeButton.setAttributedTitle("Home".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        
        viewerCountLabel.text = "\(model.viewer ?? 0)"
        fansCountLabel.text = "\(model.newFans ?? 0)"
        likeCountLabel.text = "\(model.loves ?? 0)"
        carrotCountLabel.text = "\(model.carrots ?? 0)"
    }
    
    // MARK: API Calls
    private func getLiveSummary() {
        provider.request(.LiveSummary(identifier: userModel?.userId ?? ""), successModel: LiveSummaryResponseModel.self, withProgressHud: false) { (result) in
            self.setupUI(result)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    // MARK: - Actions
    @IBAction func homeAction(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.didDismissModal()
        }
    }
    
}
