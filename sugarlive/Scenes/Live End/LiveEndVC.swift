//
//  LiveEndVC.swift
//  sugarlive
//
//  Created by cleanmac on 17/11/21.
//

import UIKit
import SnapKit
import AlamofireImage

protocol LiveEndVCDelegate: AnyObject {
    func didDismissModal()
}

class LiveEndVC: UIViewController {
    
    // MARK: - UI Components
    private var backgroundImage: UIImageView!
    private var blurVisualEffectView: UIVisualEffectView!
    private var closeButton: UIButton!
    private var iconImage: UIImageView!
    private var nameLabel: UILabel!
    private var liveEndLabel: UILabel!
    
    // MARK: - Variables
    private var userModel: UserModel?
    private var endType: LiveEndType?
    private var endMessage: String?
    
    weak var delegate: LiveEndVCDelegate?
    
    // MARK: - Overriden Functions
    init(for user: UserModel, endType: LiveEndType = .Default) {
        super.init(nibName: nil, bundle: nil)
        userModel = user
        self.endType = endType
        setupUI()
    }
    
    init(for user: UserModel, endMessage: String) {
        super.init(nibName: nil, bundle: nil)
        userModel = user
        self.endMessage = endMessage
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        iconImage.layer.cornerRadius = 50
    }

    // MARK: - UI Setups
    private func setupUI() {
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        
        backgroundImage = UIImageView()
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.contentMode = .scaleAspectFill
        if let urlString = userModel?.coverPicture, let url = URL(string: urlString) {
            backgroundImage.af.setImage(withURL: url)
        }
        view.addSubview(backgroundImage)
        backgroundImage.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        let blurEffect = UIBlurEffect.init(style: .light)
        blurVisualEffectView = UIVisualEffectView(effect: blurEffect)
        view.addSubview(blurVisualEffectView)
        blurVisualEffectView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        closeButton = UIButton()
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.setImage(UIImage(named: "ic_close_white"), for: .normal)
        closeButton.addTarget(self, action: #selector(dismissModal), for: .touchUpInside)
        view.addSubview(closeButton)
        closeButton.snp.makeConstraints { constraint in
            constraint.height.equalTo(20)
            constraint.width.equalTo(20)
            constraint.trailing.equalToSuperview().offset(-16)
            if #available(iOS 11.0, *) {
                constraint.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            } else {
                constraint.top.equalToSuperview().offset(10)
            }
        }
        
        iconImage = UIImageView()
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        iconImage.clipsToBounds = true
        iconImage.contentMode = .scaleAspectFill
        if let urlString = userModel?.coverPicture, let url = URL(string: urlString) {
            iconImage.af.setImage(withURL: url)
        }
        view.addSubview(iconImage)
        iconImage.snp.makeConstraints { constraint in
            constraint.height.equalTo(100)
            constraint.width.equalTo(100)
            constraint.centerX.equalToSuperview()
            if #available(iOS 11.0, *) {
                constraint.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin).offset(100)
            } else {
                constraint.top.equalToSuperview().offset(100)
            }
        }
        
        nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.font = .defaultSemiBoldFont(size: 16)
        nameLabel.textColor = .white
        nameLabel.text = userModel?.fullname ?? ""
        view.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { constraint in
            constraint.centerX.equalToSuperview()
            constraint.top.equalTo(iconImage.snp.bottom).offset(20)
        }

        liveEndLabel = UILabel()
        liveEndLabel.translatesAutoresizingMaskIntoConstraints = false
        liveEndLabel.font = .defaultSemiBoldFont(size: 16)
        liveEndLabel.textColor = .white
        liveEndLabel.numberOfLines = 0
        liveEndLabel.textAlignment = .center
        if let endType = endType {
            switch endType {
            case .Default:
                liveEndLabel.text = "Live Ended"
            case .HostKicked:
                liveEndLabel.text = "Host Kick"
            case .AdminKicked:
                liveEndLabel.text = "Admin Kick"
            case .BannedSession:
                liveEndLabel.text = "You're blocked from this live session"
            }
        } else if let endMessage = endMessage {
            liveEndLabel.text = endMessage
        }
        view.addSubview(liveEndLabel)
        liveEndLabel.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.top.equalTo(nameLabel.snp.bottom).offset(16)
        }
    }
    
    // MARK: - Custom Functions
    
    // MARK: - Actions
    @objc private func dismissModal() {
        dismiss(animated: true) {
            self.delegate?.didDismissModal()
        }
    }
    
}
