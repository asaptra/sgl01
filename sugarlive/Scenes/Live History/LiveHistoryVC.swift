//
//  LiveHistoryVC.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class LiveHistoryVC: BaseTableVC {
  
  enum HistoryType {
    case Weekly
    case Monthly
    case All
    
    var title: String {
      switch self {
      case .Weekly:
        return "weekly"
      case .Monthly:
        return "monthly"
      default:
        return "all"
      }
    }
    
  }
  
  @IBOutlet weak var weeklyButton: UIButton!
  @IBOutlet weak var monthlyButton: UIButton!
  
  private var selectedIndexes = [IndexPath]()
  private let selected = [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                          NSAttributedString.Key.foregroundColor: UIColor.PRIMARY_1]
  private let normal = [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12),
                        NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]
  private var historyType: HistoryType = .Weekly {
    didSet {
      switch historyType {
      case .Weekly:
        weeklyButton.isSelected = true
        monthlyButton.isSelected = false
      case .Monthly:
        weeklyButton.isSelected = false
        monthlyButton.isSelected = true
      default:
        break
      }
      pageModel = PagingRequestModel()
      getLiveHistory()
    }
  }
  private var pageModel = PagingRequestModel()
  private var histories = [LiveHistoryGroupModel]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    getLiveHistory()
  }
  
  override func setupNav() {
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: "Live History"))
    self.navigationItem.leftItemsSupplementBackButton = true
  }
  
  override func setupLayout() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.tableFooterView = UIView()
    cells = ["LiveHeaderCell", "LiveDetailCell"]
    
    weeklyButton.setAttributedTitle("Weekly".toAttributedString(with: selected), for: .selected)
    weeklyButton.setAttributedTitle("Weekly".toAttributedString(with: normal), for: .normal)
    monthlyButton.setAttributedTitle("Monthly".toAttributedString(with: selected), for: .selected)
    monthlyButton.setAttributedTitle("Monthly".toAttributedString(with: normal), for: .normal)
    weeklyAction(weeklyButton as Any)
  }
  
//  MARK: Service
  private func getLiveHistory() {
    provider.request(.LiveHistory(identifier: historyType.title, request: pageModel), successModel: LiveHistoryResponseModel.self) { (result) in
      self.histories = result.attributes
      self.tableView.reloadData()
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
//  MARK: Action
  @IBAction func weeklyAction(_ sender: Any) {
    historyType = .Weekly
    tableView.reloadData()
  }
  
  @IBAction func monthlyAction(_ sender: Any) {
    historyType = .Monthly
    tableView.reloadData()
  }
}

extension LiveHistoryVC: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return historyType == .Weekly ? histories.count : 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if historyType == .Weekly {
      if selectedIndexes.map({ $0.section }).contains(section) {
        let week = histories[section]
        return week.details.count + 1
      } else {
        return 1
      }
    } else {
      return histories.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if historyType == .Weekly {
      if indexPath.row == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveHeaderCell", for: indexPath) as! LiveHeaderCell
        cell.backgroundColor = .white
        cell.indicatorButton.isSelected = selectedIndexes.contains(indexPath)
        let week = histories[indexPath.section]
        cell.dateLabel.text = week.dateText
        cell.durationDetailLabel.text = week.missingDurationText
          cell.carrotDetailLabel.text = "\(week.amount ?? "0")".thousandFormatter()
        cell.carrotDetailLabel.textAlignment = .left
        return cell
      } else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveDetailCell", for: indexPath) as! LiveDetailCell
        cell.backgroundColor = (indexPath.row%2) == 0 ? .white : .init(hex: "F5F5F5")
        cell.selectionStyle = .none
        let week = histories[indexPath.section]
        let detail = week.details[indexPath.row - 1]
        cell.dateLabel.text = detail.dateText
          cell.carrotLabel.text = "\(detail.amount ?? "0")".thousandFormatter()
        cell.regTimeLabel.text = detail.missingDurationText
        return cell
      }
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: "LiveHeaderCell", for: indexPath) as! LiveHeaderCell
      cell.indicatorButton.superview?.isHidden = true
//      cell.bottomLabel.superview?.isHidden = false
      let month = histories[indexPath.row]
      cell.dateLabel.text = month.dateText
      cell.durationDetailLabel.text = month.missingDurationText
        cell.carrotDetailLabel.text = "\(month.amount ?? "0")".thousandFormatter()
      cell.carrotDetailLabel.textAlignment = .right
      return cell
    }
  }
}

extension LiveHistoryVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if historyType == .Weekly {
      if indexPath.row == 0 {
        if selectedIndexes.contains(indexPath) {
          selectedIndexes.removeAll{ $0 == indexPath }
        } else {
          selectedIndexes.append(indexPath)
        }
        tableView.reloadData()
      }
    } else {
      tableView.deselectRow(at: indexPath, animated: true)
    }
  }
}
