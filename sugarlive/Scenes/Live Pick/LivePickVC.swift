//
//  LivePickVC.swift
//  sugarlive
//
//  Created by msi on 06/10/21.
//

import UIKit
import ZegoExpressEngine

class LivePickVC: BaseVC {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var cameraBackgroundView: UIView!
    @IBOutlet weak var imageProfileView: UIImageView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var categoryHashtagCollectionView: UICollectionView!
    @IBOutlet weak var configButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var liveButton: UIButton!
    @IBOutlet weak var multiguestTypesStackView: UIStackView!
    @IBOutlet weak var fourMultiguestButton: UIButton!
    @IBOutlet weak var sixMultiguestButton: UIButton!
    @IBOutlet weak var multiguestButton: UIButton!
    @IBOutlet weak var groupcallButton: UIButton!
    
    @IBOutlet weak var filterCloseButton: UIButton!
    @IBOutlet weak var filterContainerView: UIView!
    @IBOutlet weak var polishSlider: UISlider!
    @IBOutlet weak var sharpenSlider: UISlider!
    @IBOutlet weak var whitenSlider: UISlider!
    
    // MARK: - Variables
    private unowned let currentUser = AuthManager.shared.currentUser
    
    private var hashtags: [String] = [] {
        didSet {
            updateButtonState()
        }
    }
    private var category: LiveCategoryModel? {
        didSet {
            updateButtonState()
        }
    }
    private var liveTitle: String? {
        didSet {
            updateButtonState()
        }
    }
    
    private var isButtonLocked: Bool = true
    private var isAllFieldsFilled: Bool = true
    
    private var multiHostType: MultiHostType = .Solo {
        didSet {
            setLiveButtonState()
        }
    }
    
    private var beautyParams = ZegoEffectsBeautyParam()
    private var polishIntensity = 10
    private var sharpenIntensity = 10
    private var whitenIntensity = 10
    
    // MARK: - Overriden Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    // MARK: - UI Setups
    override func setupLayout() {
        imageProfileView.layer.cornerRadius = imageProfileView.frame.size.width/2
        imageProfileView.layer.masksToBounds = true
        if let urlString = currentUser?.userPictureUrl, let url = URL(string: urlString) {
            imageProfileView.af.setImage(withURL: url)
        } else {
            imageProfileView.image = UIImage(named: "ic_default_profile")
        }
        
        inputField.font = .defaultFont(size: 12)
        inputField.textColor = .white
        inputField.attributedPlaceholder = "Add title to this room".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.white])
        inputField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        
        startButton.layer.cornerRadius = startButton.frame.size.height/2
        startButton.layer.masksToBounds = true
        startButton.layer.borderColor = UIColor.init(hex: "A29EFF").cgColor
        startButton.layer.borderWidth = 2
        startButton.setAttributedTitle("START".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 16), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        setupStartButtonState()
        
        liveButton.layer.cornerRadius = liveButton.frame.size.height/2
        liveButton.layer.masksToBounds = true
        liveButton.layer.borderColor = UIColor.white.cgColor
        liveButton.layer.borderWidth = 2
        liveButton.setImage(UIImage(named: "ic_pick_live")?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        fourMultiguestButton.backgroundColor = .lightGray
        fourMultiguestButton.setAttributedTitle("4 Guest".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 16), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        
        sixMultiguestButton.backgroundColor = .lightGray
        sixMultiguestButton.setAttributedTitle("6 Guest".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 16), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        
        multiguestButton.layer.cornerRadius = multiguestButton.frame.size.height/2
        multiguestButton.layer.masksToBounds = true
        multiguestButton.layer.borderColor = UIColor.white.cgColor
        multiguestButton.layer.borderWidth = 2
        multiguestButton.setImage(UIImage(named: "ic_pick_multiguest")?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        groupcallButton.layer.cornerRadius = groupcallButton.frame.size.height/2
        groupcallButton.layer.masksToBounds = true
        groupcallButton.layer.borderColor = UIColor.white.cgColor
        groupcallButton.layer.borderWidth = 2
        groupcallButton.setImage(UIImage(named: "ic_pick_groupcall")?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        filterContainerView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        polishSlider.addTarget(self, action: #selector(sliderAction(_:)), for: .valueChanged)
        sharpenSlider.addTarget(self, action: #selector(sliderAction(_:)), for: .valueChanged)
        whitenSlider.addTarget(self, action: #selector(sliderAction(_:)), for: .valueChanged)
        
        setLiveButtonState()
        
        setupCategoryCollectionView()
        
        createZegoEngine()
        startZegoPreviewLayer()
    }
    
    private func setupCategoryCollectionView() {
        categoryHashtagCollectionView.backgroundColor = .clear
        categoryHashtagCollectionView.delegate = self
        categoryHashtagCollectionView.dataSource = self
        categoryHashtagCollectionView.register(UINib(nibName: "LiveCategoryCollectionCell", bundle: nil), forCellWithReuseIdentifier: "LiveCategoryCollectionCell")
    }
    
    private func setupStartButtonState() {
        //startButton.backgroundColor = isButtonLocked ? UIColor.init(hex: "6964D8").withAlphaComponent(0.3) : UIColor.init(hex: "6964D8")
        startButton.backgroundColor = UIColor.init(hex: "6964D8")
    }
    
    // MARK: - Custom Functions
    private func setLiveButtonState() {
        if multiHostType == .Solo {
            liveButton.backgroundColor = .white
            liveButton.imageView?.tintColor = UIColor(hex: "#6964D8")
            liveButton.layer.borderColor =  UIColor(hex: "#6964D8").cgColor
            multiguestButton.backgroundColor = .clear
            multiguestButton.imageView?.tintColor = .white
            multiguestButton.layer.borderColor = UIColor.white.cgColor
            groupcallButton.backgroundColor = .clear
            groupcallButton.imageView?.tintColor = .white
            groupcallButton.layer.borderColor = UIColor.white.cgColor
        } else if multiHostType == .FourMultiHost || multiHostType == .SixMultiHost {
            liveButton.backgroundColor = .clear
            liveButton.imageView?.tintColor = .white
            liveButton.layer.borderColor =  UIColor.white.cgColor
            multiguestButton.backgroundColor = .white
            multiguestButton.imageView?.tintColor = UIColor(hex: "#6964D8")
            multiguestButton.layer.borderColor =  UIColor(hex: "#6964D8").cgColor
            groupcallButton.backgroundColor = .clear
            groupcallButton.imageView?.tintColor = .white
            groupcallButton.layer.borderColor = UIColor.white.cgColor
        } else if multiHostType == .GroupCall {
            liveButton.backgroundColor = .clear
            liveButton.imageView?.tintColor = .white
            liveButton.layer.borderColor =  UIColor.white.cgColor
            multiguestButton.backgroundColor = .clear
            multiguestButton.imageView?.tintColor = .white
            multiguestButton.layer.borderColor = UIColor.white.cgColor
            groupcallButton.backgroundColor = .white
            groupcallButton.imageView?.tintColor = UIColor(hex: "#6964D8")
            groupcallButton.layer.borderColor =  UIColor(hex: "#6964D8").cgColor
        }
    }
    
    private func getValidationMessage() -> String {
        if liveTitle == nil {
            return "Please fill the live title"
        } else if category == nil {
            return "Please choose live category"
        } else if liveTitle?.count ?? 0 < 3 {
            return "Live title should be at least 3 characters"
        } else {
            return "Please fill the required fields"
        }
    }
    
    private func showCategoriesModal() {
        inputField.endEditing(true)
        let vc = LiveCategoriesModalVC()
        vc.delegate = self
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    private func showHashtagPopup() {
        let vc = PopupTagFieldVC(title: "Add Tag", placeholder: "type here without #")
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    private func showValidationModal(message: String) {
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = "Retry"
        popupAlertVC.messageStr = message
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = {(sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        present(popupAlertVC, animated: true, completion: nil)
    }
    
    private func updateButtonState() {
        isAllFieldsFilled = !((category != nil) && (liveTitle != nil) && (liveTitle?.count ?? 0 > 3))
        isButtonLocked = !((liveTitle != nil) && (liveTitle?.count ?? 0 > 3))
    }
    
    @objc private func textFieldDidChanged(_ sender: UITextField) {
        if sender == inputField {
            liveTitle = sender.text
        }
    }
    
    //  MARK: - Action
    @IBAction func refreshAction(_ sender: Any) {
        
    }
    
    @IBAction func configAction(_ sender: Any) {
        filterCloseButton.isHidden = false
        filterContainerView.isHidden = false
    }
    
    @IBAction func startAction(_ sender: Any) {
        if !isAllFieldsFilled {
            startLive()
        } else {
            showValidationModal(message: getValidationMessage())
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        destroyZegoEngine()
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func liveAction(_ sender: Any) {
        multiHostType = .Solo
        startButton.isHidden = false
        multiguestTypesStackView.isHidden = true
    }
    
    @IBAction func multiguestAction(_ sender: Any) {
        multiHostType = .FourMultiHost
        startButton.isHidden = true
        multiguestTypesStackView.isHidden = false
    }
    
    @IBAction func groupcallAction(_ sender: Any) {
        multiHostType = .GroupCall
        startButton.isHidden = false
        multiguestTypesStackView.isHidden = true
    }
    
    @IBAction func fourMultiguestAction(_ sender: Any) {
        multiHostType = .FourMultiHost
        startButton.isHidden = false
        multiguestTypesStackView.isHidden = true
    }
    
    @IBAction func sixMultiguestAction(_ sender: Any) {
        multiHostType = .SixMultiHost
        startButton.isHidden = false
        multiguestTypesStackView.isHidden = true
    }
    
    @IBAction func filterCloseAction(_ sender: Any) {
        filterCloseButton.isHidden = true
        filterContainerView.isHidden = true
    }
    
    @objc private func sliderAction(_ sender: UISlider) {
        let value = sender.value
        if sender == polishSlider {
            polishIntensity = Int(value)
        } else if sender == sharpenSlider {
            sharpenIntensity = Int(value)
        } else if sender == whitenSlider {
            whitenIntensity = Int(value)
        }
        
        beautyParams.smoothIntensity = Int32(polishIntensity)
        beautyParams.sharpenIntensity = Int32(sharpenIntensity)
        beautyParams.whitenIntensity =  Int32(whitenIntensity)
        ZegoExpressEngine.shared().setEffectsBeautyParam(beautyParams)
    }
    
    // MARK: - API Calls
    private func startLive() {
        var hashtagsRequest = ""
        if !hashtags.isEmpty {
            for hashtag in hashtags {
                hashtagsRequest += hashtag + ", "
            }
        }
        
        let request: StartLiveRequestModel?
        
        if hashtagsRequest.isEmpty {
            request = StartLiveRequestModel(JSON: [
                "title": inputField.text ?? "",
                "category": category?.liveCategory ?? "",
                "multiHost": true,
                "multiHostCount": multiHostType.rawValue
            ])
        } else {
            request = StartLiveRequestModel(JSON: [
                "title": inputField.text ?? "",
                "category": category?.liveCategory ?? "",
                "hashtag": hashtagsRequest,
                "multiHost": true,
                "multiHostCount": multiHostType.rawValue
            ])
        }
        
        provider.request(.StartLive(request: request!), successModel: StartLiveResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.destroyZegoEngine()
            let categoryModel = CategoryModel(JSON: ["color": self.category?.color ?? "", "title": self.category?.liveCategory ?? ""])
            let filterModel = LiveFilterModel(polishIntensity: Int32(self.polishIntensity), sharpenIntensity: Int32(self.sharpenIntensity), whitenIntensity: Int32(self.whitenIntensity))
            self.currentUser?.multiHostId = result.multiHostId ?? ""
            if self.multiHostType == .Solo {
                self.routeToLive(userModel: self.currentUser!, type: .Host, category: categoryModel, title: self.inputField.text ?? "", hashtags: self.hashtags, filterModel: filterModel)
            } else {
                self.routeToMultiguestLive(userModel: self.currentUser!, type: .Host, multihostType: self.multiHostType, category: categoryModel, hashtags: self.hashtags, filterModel: filterModel)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
}

// MARK: - Zego Engine Configurations
extension LivePickVC {
    private func createZegoEngine() {
        ZegoExpressEngine.createEngine(withAppID: Constant.ZegoAppID, appSign: Constant.ZegoAppSign, isTestEnv: true, scenario: ZegoScenario.general, eventHandler: self)
    }
    
    private func destroyZegoEngine() {
        ZegoExpressEngine.destroy(nil)
    }
    
    private func startZegoPreviewLayer() {
        ZegoExpressEngine.shared().startEffectsEnv()
        ZegoExpressEngine.shared().enableEffectsBeauty(true)
        
        beautyParams.smoothIntensity = Int32(polishIntensity)
        beautyParams.sharpenIntensity = Int32(sharpenIntensity)
        beautyParams.whitenIntensity =  Int32(whitenIntensity)
        ZegoExpressEngine.shared().setEffectsBeautyParam(beautyParams)
        
        let canvas = ZegoCanvas(view: cameraBackgroundView)
        canvas.viewMode = .aspectFill
        ZegoExpressEngine.shared().startPreview(canvas)
    }
    
    private func stopZegoPreviewLayer() {
        ZegoExpressEngine.shared().stopPreview()
    }
}

// MARK: - Zego Event Handler protocol
extension LivePickVC: ZegoEventHandler {}

// MARK: - Collection View data source
extension LivePickVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        (section == 0 || section == 1) ? 1 : hashtags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveCategoryCollectionCell", for: indexPath) as! LiveCategoryCollectionCell
        if indexPath.section == 0 {
            cell.setTitle(title: category?.liveCategory ?? "Choose Category", color: category?.color ?? "#000000")
        } else if indexPath.section == 1 {
            cell.setTitle(title: "Add tag")
        } else {
            cell.setTitle(hashtag: hashtags[indexPath.row])
        }
        return cell
    }
}

// MARK: - Collection View delegates
extension LivePickVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if indexPath.section == 0 {
            showCategoriesModal()
        } else if indexPath.section == 1 {
            showHashtagPopup()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size: CGSize = {
            if indexPath.section == 0 {
                return (category?.liveCategory ?? "Choose Category").size(withAttributes: nil)
            } else if indexPath.section == 1 {
                return "Add tag".size(withAttributes: nil)
            } else {
                return (hashtags[indexPath.row]).size(withAttributes: nil)
            }
        }()
        let height = collectionView.frame.height
        return .init(width: (size.width + 25), height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 || section == 1 {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        }
        return .zero
    }
}

// MARK: - Live Categories Modal delegate
extension LivePickVC: LiveCategoriesModalVCDelegate {
    func didSelectItem(_ item: LiveCategoryModel) {
        category = item
        categoryHashtagCollectionView.reloadData()
    }
}

// MARK: - Popup Text Field delegate
extension LivePickVC: PopupTagFieldDelegate {
    func setTextFieldValue(_ value: String) {
        if !value.isEmpty {
            hashtags.append(value)
            categoryHashtagCollectionView.reloadData()
        }
    }
}

// MARK: - Transitioning delegate
extension LivePickVC: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        SlideOverPresentationController(presentedViewController: presented, presenting: presenting)
    }
}
