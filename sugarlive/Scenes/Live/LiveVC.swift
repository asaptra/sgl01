//
//  LiveVC.swift
//  sugarlive
//
//  Created by msi on 07/10/21.
//

import UIKit
import SnapKit
import PusherSwift
import ZegoExpressEngine
import AlamofireImage
import SDWebImage
import SnapshotSafeView

class LiveVC: BaseVC {
    
    // MARK: - IBOutlets and UI Components
    //@IBOutlet weak var streamView: UIView!
    
    var snapshotContainerView: ScreenshotProtectController<UIView>!
    var streamView: UIView!
    
    lazy var hostBattleView: HostBattleView = {
        let view = HostBattleView.nib(withType: HostBattleView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var profileTimerView: ProfileTimerLiveView = {
        let view = ProfileTimerLiveView.nib(withType: ProfileTimerLiveView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var categoryView: CategoryLiveView = {
        let view = CategoryLiveView.nib(withType: CategoryLiveView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var socMedView: LiveSocMedView = {
        let view = LiveSocMedView.nib(withType: LiveSocMedView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var firstVideoCallView: VideoCallView = {
        let view = VideoCallView.nib(withType: VideoCallView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var secondVideoCallView: VideoCallView = {
        let view = VideoCallView.nib(withType: VideoCallView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var chatTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private lazy var chatView: ChatLiveView = {
        let view = ChatLiveView.nib(withType: ChatLiveView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var suitAnswerView: SuitAnswerView = {
        let view = SuitAnswerView.nib(withType: SuitAnswerView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var suitResultView: SuitResultView = {
        let view = SuitResultView.nib(withType: SuitResultView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var suitResultAnimationImageView: SDAnimatedImageView = {
        let view = SDAnimatedImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var comboGiftTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isScrollEnabled = false
        return tableView
    }()
    
    lazy var comboButtonImage: SDAnimatedImageView = {
        let view = SDAnimatedImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var screenshootView: ScreenshootView = {
        let view = ScreenshootView.nib(withType: ScreenshootView.self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Variables
    var isFrontCamera: Bool = true
    
    unowned var currentUser = AuthManager.shared.currentUser
    
    var type: LiveType!
    var userModel: UserModel!
    var detailModel: WatchLiveDetailModel!
    var liveTitle: String?
    var category: CategoryModel?
    var hashtags: [String]?
    
    private var streamDuration: TimeInterval!
    private var streamTimer: Timer?
    
    private var chats: [Any] = []
    private var chatsReversed: [Any] = []
    
    private var modalActions: [ModalActionModel] = []
    private var reportActions: [ModalActionModel] = []
    private var videoCallActions: [ModalActionModel] = []
    private var hostStreamActions: [ModalActionModel] = []
    private var battleDurationActions: [ModalActionModel] = [ModalActionModel(title: "2 minutes", type: .Normal),
                                                             ModalActionModel(title: "5 minutes", type: .Normal),
                                                             ModalActionModel(title: "10 minutes", type: .Normal)]
    
    private var pusher: Pusher?
    private var channel: PusherChannel?
    private var channelViewer: PusherChannel?
    private var channelPresence: PusherPresenceChannel?
    
    var zegoUsers: [ZegoUser] = []
    var guestModels: [UserModel] = []
    
    private let videoCallViewWidth: CGFloat = 110
    private let videoCallViewHeight: CGFloat = 150
    private var chatTableViewHeight: CGFloat?
    
    var filterModel: LiveFilterModel?
    
    var flyingBannerModels: [Any] = []
    var isflyingBannerQueueActive: Bool = false
    
    var giftModels: [GiftDetailEventModel] = []
    var isGiftQueueActive: Bool = false
    
    var bidAcceptedModel: BidAcceptedEventModel? {
        didSet {
            userModel.liveId = bidAcceptedModel?.liveId
        }
    }
    var bidResultModel: BidResultEventModel? {
        didSet {
            suitResultView.isHidden = bidResultModel == nil
            suitResultAnimationImageView.isHidden = bidResultModel == nil
            suitResultView.setNames(host: bidResultModel?.receiver?.fullname ?? "", viewer: bidResultModel?.sender?.fullname ?? "")
            suitResultView.setImages(host: bidResultModel?.receiver?.answer ?? "", viewer: bidResultModel?.sender?.answer ?? "")
        }
    }
    
    private let comboTimeout: TimeInterval = 5
    var comboGiftModels: [ComboGiftModel] = [] {
        didSet {
            comboGiftTableView.isHidden = comboGiftModels.isEmpty
        }
    }
    var comboSenderModel: GiftDetailEventModel? {
        didSet {
            comboButtonImage.isHidden = comboSenderModel == nil
        }
    }
    var sentGiftId: String?
    var comboGiftTimer: Timer?
    var comboButtonTimer: Timer?
    
    var hostBattleModel: UserModel? {
        didSet {
            if type == .Host {
                chatView.versusButton.isHidden = hostBattleModel != nil
            }
        }
    }
    var guestBattleModel: UserModel?
    
    private var isCapturing: Bool = false
    
    // MARK: - Overriden Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupPusherConnection()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setIdleTimerStatus(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        setIdleTimerStatus(false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        if chatTableViewHeight == nil {
//            chatTableViewHeight = abs(chatView.frame.minY - secondVideoCallView.frame.maxY) - 100
//            chatTableView.snp.updateConstraints { constraint in
//                constraint.height.equalTo(chatTableViewHeight!)
//            }
//        }
    }
    
    deinit {
        destroyZegoEngine()
        removeNotificationObserver()
        disconnectPusherConnection()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        hideKeyboardWhenTappedAround()
        
        streamView = UIView()
        streamView.backgroundColor = .PRIMARY_1
        
        snapshotContainerView = ScreenshotProtectController(content: streamView)
        snapshotContainerView.container.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(snapshotContainerView.container)
        snapshotContainerView.container.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        if !(currentUser?.isAdmin ?? true) {
            if currentUser?.userId != userModel.userId {
                snapshotContainerView.setupContentAsHiddenInScreenshotMode()
            }
        }
        
        view.addSubview(firstVideoCallView)
        firstVideoCallView.snp.makeConstraints { constraint in
            if #available(iOS 11.0, *) {
                constraint.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin).offset(100)
            } else {
                constraint.top.equalTo(view.snp.topMargin).offset(100)
            }
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.height.equalTo(videoCallViewHeight)
            constraint.width.equalTo(videoCallViewWidth)
        }
        firstVideoCallView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(videoCallAction)))
        firstVideoCallView.isHidden = type != .Host
        firstVideoCallView.closeTapped = { [unowned self] model, type in
            if type == .Streaming {
                self.kickMultiguest(guestModel: model)
            } else {
                self.stopInvitedVideoCall(position: model.position)
            }
        }
        
        view.addSubview(secondVideoCallView)
        secondVideoCallView.snp.makeConstraints { constraint in
            constraint.top.equalTo(firstVideoCallView.snp.bottom).offset(8)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.height.equalTo(videoCallViewHeight)
            constraint.width.equalTo(videoCallViewWidth)
        }
        secondVideoCallView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(videoCallAction)))
        secondVideoCallView.isHidden = type != .Host
        secondVideoCallView.closeTapped = { [unowned self] model, type in
            if type == .Streaming {
                self.kickMultiguest(guestModel: model)
            } else {
                self.stopInvitedVideoCall(position: model.position)
            }
        }
        
        view.addSubview(hostBattleView)
        hostBattleView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        hostBattleView.setBattleTitle(type == .Host ? (liveTitle ?? "") : (detailModel.title ?? ""))
        hostBattleView.isHidden = true
        
        view.addSubview(profileTimerView)
        profileTimerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            if #available(iOS 11.0, *) {
                constraint.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin)
            } else {
                constraint.top.equalTo(view.snp.topMargin)
            }
            constraint.height.equalTo(32)
        }
        view.addSubview(categoryView)
        categoryView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.top.equalTo(profileTimerView.snp.bottom).offset(10)
            constraint.height.equalTo(20)
        }
        view.addSubview(socMedView)
        socMedView.snp.makeConstraints { constraint in
            constraint.height.equalTo(25)
            constraint.width.equalTo(115)
            constraint.leading.equalToSuperview().offset(16)
            constraint.top.equalTo(categoryView.snp.bottom).offset(10)
        }
        view.addSubview(chatView)
        chatView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            if #available(iOS 11.0, *) {
                constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin).offset(-16)
            } else {
                constraint.bottom.equalTo(view.snp.bottomMargin).offset(-16)
            }
            constraint.height.equalTo(50)
        }
        view.addSubview(chatTableView)
        chatTableView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.bottom.equalTo(chatView.snp.top)
            constraint.height.equalTo(155)
        }
        
        profileTimerView.closeButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        profileTimerView.setupContents(userModel: userModel, detailModel: self.type != .Host ? self.detailModel : nil)
        profileTimerView.profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTimerViewAction)))
        profileTimerView.userSelectedHandler = { [unowned self] userModel in
            self.chatTableView.selectRow(at: nil, animated: true, scrollPosition: .top)
            if let userId = userModel.userId {
                self.getOtherProfile(userId: userId)
            }
        }

        view.addSubview(suitAnswerView)
        suitAnswerView.snp.makeConstraints { constraint in
            constraint.bottom.equalTo(chatTableView.snp.top).offset(-16)
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.height.equalTo(150)
        }
        suitAnswerView.isHidden = true
        suitAnswerView.answerHandler = { [unowned self] answer in
            if self.bidAcceptedModel != nil {
                self.answerGame(user: self.type, answer: answer)
            }
        }
        
        view.addSubview(suitResultView)
        suitResultView.snp.makeConstraints { constraint in
            constraint.bottom.equalTo(chatTableView.snp.top).offset(-16)
            constraint.leading.equalToSuperview().offset(32)
            constraint.trailing.equalToSuperview().offset(-32)
            constraint.height.equalTo(150)
        }
        suitResultView.isHidden = true
        
        view.addSubview(suitResultAnimationImageView)
        suitResultAnimationImageView.snp.makeConstraints { constraint in
            constraint.height.equalTo(200)
            constraint.width.equalTo(200)
            constraint.centerX.equalToSuperview()
            constraint.bottom.equalTo(suitResultView.snp.top).offset(-16)
        }
        
        view.addSubview(comboGiftTableView)
        comboGiftTableView.snp.makeConstraints { constraint in
            constraint.height.equalTo(ComboBannerCell.DEFAULT_HEIGHT * 3)
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.bottom.equalTo(chatTableView.snp.top).offset(-8)
        }
        comboGiftModels.removeAll()
        
        view.addSubview(comboButtonImage)
        comboButtonImage.snp.makeConstraints { constraint in
            constraint.height.equalTo(110)
            constraint.width.equalTo(110)
            constraint.trailing.equalTo(-16)
            constraint.bottom.equalTo(chatView.snp.top).offset(-16)
        }
        let comboTap = UITapGestureRecognizer(target: self, action: #selector(comboAction))
        comboButtonImage.isUserInteractionEnabled = true
        comboButtonImage.addGestureRecognizer(comboTap)
        comboButtonImage.isHidden = true
        
        categoryView.carrotContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showTopSpenderModal)))
        
        chatView.setupButtons(type, .Solo)
        chatView.setChatFieldDelegate(self)
        
        if type != .Host {
            categoryView.setupCollectionViewData(category: detailModel.category!, hashtags: detailModel.hashtags ?? [])
        }
        else {
            categoryView.setupCollectionViewData(category: category!, hashtags: hashtags!)
        }
        
        categoryView.setCarrotAmount(userModel?.totalCarrot ?? 0)
        
        if let socMedModel = userModel.socialMedia {
            socMedView.setupContents(socMedModel)
        } else {
            socMedView.isHidden = true
        }
        
        if type == .Viewer, let currentEntranceEffect = currentUser?.entranceEffects?.defaultEffect {
            flyingBannerModels.append(EntranceQueueModel(entrance: currentEntranceEffect, fullname: currentUser?.fullname ?? ""))
            if !isflyingBannerQueueActive {
                isflyingBannerQueueActive = true
                showFlyingBannerQueue()
            }
        }
        
        streamDuration = type != .Host ? calculateTimeIntervalDifference() : 0
        setupStreamTimer()
        
        setupChatTableView()
        setupComboGiftTableView()
        setupZegoEngine()
        setupNotificationObserver()
        setupButtonTargets()
        getChatBubbles()
    }
    
    private func setupChatTableView() {
        chatTableView.delegate = self
        chatTableView.dataSource = self
        chatTableView.rowHeight = UITableView.automaticDimension
        chatTableView.separatorStyle = .none
        chatTableView.register(UINib(nibName: "ChatLiveCell", bundle: nil), forCellReuseIdentifier: "ChatLiveCell")
        chatTableView.register(UINib(nibName: "LiveActivityCell", bundle: nil), forCellReuseIdentifier: "LiveActivityCell")
        chatTableView.register(UINib(nibName: "TnCLiveCell", bundle: nil), forCellReuseIdentifier: "TnCLiveCell")
        chatTableView.register(UINib(nibName: "ChatNotificationCell", bundle: nil), forCellReuseIdentifier: "ChatNotificationCell")
        chatTableView.register(UINib(nibName: "GiftNotificationCell", bundle: nil), forCellReuseIdentifier: "GiftNotificationCell")
        chatTableView.transform = CGAffineTransform(scaleX: 1, y: -1)
    }
    
    private func setupComboGiftTableView() {
        comboGiftTableView.delegate = self
        comboGiftTableView.dataSource = self
        comboGiftTableView.separatorStyle = .none
        comboGiftTableView.register(UINib(nibName: "ComboBannerCell", bundle: nil), forCellReuseIdentifier: "ComboBannerCell")
        comboGiftTableView.transform = CGAffineTransform(scaleX: 1, y: -1)
    }
    
    private func setupButtonTargets() {
        chatView.rotateButton.addTarget(self, action: #selector(switchCamera), for: .touchUpInside)
        chatView.versusButton.addTarget(self, action: #selector(showInviteBattleModal), for: .touchUpInside)
        chatView.giftButton.addTarget(self, action: #selector(showGiftModal), for: .touchUpInside)
        chatView.lockButton.addTarget(self, action: #selector(showCreatePrivateRoomModal), for: .touchUpInside)
        chatView.shareButton.addTarget(self, action: #selector(getShareLink), for: .touchUpInside)
        chatView.chatIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showStickerModal)))
    }
    
    private func setupZegoEngine() {
        createZegoEngine()
        
        liveLogIn()
        
        if type == .Host {
            startZegoPreviewLayer()
            startPublishingStream()
        } else {
            if let members = detailModel.members {
                if members.count <= 1 {
                    startPlayingStream()
                } else {
                    if detailModel.multiHostCount == 3 {
                        startPlayingStream()
                        guestModels = members
                        guestModels.removeAll(where: { $0.position == 0 })
                        startPlayingVideoCallsForViewer()
                    } else {
                        startBattle(host: (detailModel.members?.first)!, guest: (detailModel.members?.last)!)
                    }
                }
            }
        }
    }
    
    private func setupStreamTimer() {
        streamTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            self.streamDuration += 1
            self.profileTimerView.timerLabel.text = self.streamDuration.stringFromTimeInterval()
        }
    }
    
    // MARK: - Custom Functions
    private func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(showOtherFullProfileModal(_:)), name: NSNotification.Name(rawValue: kProfileModalProfileAction), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showChatModal(_:)), name: NSNotification.Name(rawValue: kProfileModalChatAction), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showReportModal(_:)), name: NSNotification.Name(rawValue: kProfileModalReportAction), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showTopupModal), name: NSNotification.Name(rawValue: kShowTopupModalAction), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showBattleDurationModal), name: NSNotification.Name(rawValue: kStartBattleAction), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(startInvitationCountdown(_:)), name: NSNotification.Name(rawValue: kStartVideoCallInvitationCountdown), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
//        if #available(iOS 11.0, *) {
//            NotificationCenter.default.addObserver(self, selector: #selector(userDidCaptureHandler(_:)), name: UIScreen.capturedDidChangeNotification, object: nil)
//        }
    }
    
    private func removeNotificationObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kProfileModalProfileAction), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kProfileModalChatAction), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kProfileModalReportAction), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kShowTopupModalAction), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kStartBattleAction), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kStartVideoCallInvitationCountdown), object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.userDidTakeScreenshotNotification, object: nil)
//        if #available(iOS 11.0, *) {
//            NotificationCenter.default.removeObserver(self, name: UIScreen.capturedDidChangeNotification, object: nil)
//        }
    }
    
    private func setupPusherConnection() {
        let authBuilder = PusherRequestBuilder(userId: currentUser?.userId ?? "")
        let options = PusherClientOptions(authMethod: AuthMethod.authRequestBuilder(authRequestBuilder: authBuilder),
                                          host: .cluster(Constant.PusherCluster))
        
        pusher = Pusher(key: Constant.PusherKeyID, options: options)
        
        channel = pusher?.subscribe(channelName: userModel?.userId ?? "")
        channelViewer = pusher?.subscribe(channelName: currentUser?.userId ?? "")
        channelPresence = pusher?.subscribeToPresenceChannel(channelName: "presence-\(userModel?.userId ?? "")", onMemberAdded: { [weak self] member in
            guard let self = self else { return }
            let userDictionary = member.userInfo as! [String: Any]
            if let model = UserModel(JSON: userDictionary) {
                self.returnOtherProfile(userId: model.userId ?? "" , completion: { userModel in
                    if let userModel = userModel {
                        if !userModel.isAdmin {
                            self.profileTimerView.addUserToList(model: userModel)
                            self.appendChatModel(ViewerLiveJoinNotificationModel(userId: userModel.userId ?? "", name: userModel.fullname ?? ""))
                            
                            if let defaultEnterance = userModel.entranceEffects?.defaultEffect {
                                self.flyingBannerModels.append(EntranceQueueModel(entrance: defaultEnterance, fullname: userModel.fullname ?? ""))
                                if !self.isflyingBannerQueueActive {
                                    self.isflyingBannerQueueActive = true
                                    self.showFlyingBannerQueue()
                                }
                            }
                        }
                    }
                })
            }
            NotificationCenter.default.post(name: NSNotification.Name(kUpdateGuestList), object: nil)
        }, onMemberRemoved: { [weak self] member in
            guard let self = self else { return }
            let userDictionary = member.userInfo as! [String: Any]
            if let model = UserModel(JSON: userDictionary) {
                if !model.isAdmin {
                    self.profileTimerView.removeUserFromList(model: model)
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(kUpdateGuestList), object: nil)
        })
        
        let _ = channelPresence?.bind(eventName: "pusher:subscription_succeeded", callback: { [weak self] data in
            guard let self = self else { return }
            let object = data as! [String: Any]
            let presence = object["presence"] as! [String: Any]
            let userIds = presence["ids"] as! [String]
            for id in userIds {
                if id == self.userModel.userId {
                    continue
                } else {
                    self.returnOtherProfile(userId: id, completion: { userModel in
                        if let userModel = userModel {
                            if !userModel.isAdmin {
                                self.profileTimerView.addUserToList(model: userModel)
                            }
                        }
                    })
                }
            }
        })
        
        let _ = channelViewer?.bind(eventName: PusherEvents.INVITATION, callback: { [weak self] data in
            guard let self = self else { return }
            let dataJSON = data as! [String: Any]
            if let dataModel = GuestInvitationModel(JSON: dataJSON) {
                self.showInvitationPopup(model: dataModel)
            }
        })
        let _ = channel?.bind(eventName: PusherEvents.FOLLOW, callback: { [weak self] data in
            guard let self = self else { return }
            let dataJSON = data as! [String: Any]
            let model = LiveNotificationModel(message: dataJSON["message"] as! String)
            self.appendChatModel(model)
        })
        let _ = channel?.bind(eventName: PusherEvents.GUEST_LEAVE, callback: { [weak self] data in
            guard let self = self else { return }
            let dataJSON = data as! [String: Any]
            let userId = dataJSON["userId"] as! String
            let position = dataJSON["position"] as! Int
            
            if self.type == .Host && (userId == self.hostBattleModel?.userLiveId || userId == self.guestBattleModel?.userLiveId) {
                self.stopBattle()
            } else {
                self.stopPlayingVideoCall(id: userId, position: position)
            }
        })
        let _ = channel?.bind(eventName: PusherEvents.ACCEPT_CHALLANGE, callback: { [weak self] data in
            guard let self = self else { return }
            let dataJSON = data as! [String: Any]
            let multiHostCount = dataJSON["multi_host_count"] as! Int64
            if let members = dataJSON["members"] as? Array<[String: Any]> {
                if multiHostCount == 2 {
                    if let hostModel = UserModel(JSON: members.first!), let guestModel = UserModel(JSON: members.last!) {
                        self.startBattle(host: hostModel, guest: guestModel)
                    }
                } else {
                    if let userModel = UserModel(JSON: members.last!) {
                        self.guestModels.append(userModel)
                        
                        if userModel.userLiveId ?? "" != self.currentUser?.userId {
                            self.startPlayingVideoCall(id: userModel.userLiveId ?? "", position: userModel.position)
                        }
                    }
                }
            }
        })
        
        let _ = channel?.bind(eventName: PusherEvents.KICK_GUEST, callback: { [weak self] data in
            guard let self = self else { return }
            let dataJSON = data as! [String: Any]
            if let id = dataJSON["userId"] as? String {
                if id == self.currentUser?.userId {
                    self.stopPublishingVideoCall(position: self.currentUser?.position ?? 0)
                }
            }
        })
        let _ = channel?.bind(eventName: PusherEvents.STOP, callback: { [weak self] data in
            guard let self = self else { return }
            if self.type != .Host {
                self.leaveLive()
                self.stopPlayingStream()
                self.showLiveEndModal()
                
                for position in 0..<3 {
                    if self.getVideoCallContainerView(position).streamingType == .Publishing {
                        self.stopPublishingVideoCall(position: position)
                        break
                    }
                }
            }
        })
        let _ = channel?.bind(eventName: PusherEvents.MODERATION, callback: { [weak self] data in
            guard let self = self else { return }
            let dataJSON = data as! [String: Any]
            let message = dataJSON["message"] as! String
            let data = dataJSON["data"] as! [String: Any]
            let userId = data["userId"] as! String
            if self.type != .Host {
                if userId == self.currentUser?.userId {
                    self.leaveLive()
                    self.stopPlayingStream()
                    self.showLiveEndModal(endMessage: message)

                    for position in 0..<3 {
                        if self.getVideoCallContainerView(position).streamingType == .Publishing {
                            self.stopPublishingVideoCall(position: position)
                            break
                        }
                    }
                }
            } else {
                if userId == self.currentUser?.userId {
                    self.stopLive()
                    self.showLiveEndModal(endMessage: message)
                }
            }
        })
        
        let _ = channel?.bind(eventName: PusherEvents.BATTLE_INFO, callback: { [weak self] data in
            guard let self = self else { return }
            let dataJSON = data as! [String: Any]
            if let battleInfoModel = BattleInfoEventModel(JSON: dataJSON) {
                guard battleInfoModel.remaining != 0 else { return }
                self.updateBattleInfo(battleInfoModel)
            }
        })
        let _ = channel?.bind(eventName: PusherEvents.BATTLE_END, callback: { [weak self] data in
            guard let self = self else { return }
            self.endBattle()
        })
        
        pusher?.connect()
    }
    
    private func disconnectPusherConnection() {
        pusher?.unsubscribeAll()
        pusher?.disconnect()
    }
    private func setupActionData(position: Int) {
        let audioTitle = getVideoCallContainerView(position).audioState == .Mute ? "Unmute" : "Mute"
        let videoTitle = getVideoCallContainerView(position).videoState == .Mute ? "Enable Camera" : "Disable Camera"
        
        videoCallActions = [ModalActionModel(title: audioTitle, type: .Normal), ModalActionModel(title: videoTitle, type: .Normal), ModalActionModel(title: "Leave call", type: .Warning)]
    }
    
    private func setupHostStreamActionData(position: Int) {
        let audioTitle = getVideoCallContainerView(position).audioState == .Mute ? "Unmute" : "Mute"
        let videoTitle = getVideoCallContainerView(position).videoState == .Mute ? "Enable Camera" : "Disable Camera"
        
        hostStreamActions = [ModalActionModel(title: audioTitle, type: .Normal), ModalActionModel(title: videoTitle, type: .Normal), ModalActionModel(title: "Kick from this call", type: .Warning)]
    }
    
    private func setupAdminStreamActionData(position: Int) {
        let audioTitle = getVideoCallContainerView(position).audioState == .Mute ? "Unmute" : "Mute"
        let videoTitle = getVideoCallContainerView(position).videoState == .Mute ? "Enable Camera" : "Disable Camera"
        
        hostStreamActions = [ModalActionModel(title: audioTitle, type: .Normal), ModalActionModel(title: videoTitle, type: .Normal), ModalActionModel(title: "View \(getVideoCallContainerView(position).guestModel?.fullname ?? "") profile", type: .Normal)]
    }
    
    func setCarrotAmount(_ amount: Int) {
        userModel.totalCarrot = Double(amount)
        categoryView.setCarrotAmount(userModel.totalCarrot)
    }
    
    @objc private func goBack() {
        if type == .Host {
            if (hostBattleModel != nil) && (hostBattleModel?.userLiveId == currentUser?.userId) {
                showStopBattlePopup()
            } else {
                showStopLivePopup()
            }
        } else {
            leaveLive()
        }
    }
    
    private func backToHome() {
        guard let viewControllers = navigationController?.viewControllers else { return }
        for vc in viewControllers {
            if vc is MainTabVC {
                navigationController?.popToViewController(vc, animated: false)
            }
        }
    }
    
    private func calculateTimeIntervalDifference() -> TimeInterval {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        let startStreamDate = dateFormatter.date(from: userModel.startStreaming ?? "")
        
        let delta = Date().timeIntervalSinceReferenceDate - startStreamDate!.timeIntervalSinceReferenceDate
        return delta
    }
    
    private func storeDataToDisk(data: Data, name: String) {
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(name)
        do {
            try data.write(to: url)
        } catch {
            print("Store data error: \(error.localizedDescription)")
        }
    }
    
    private func retrieveDataFromDisk(name: String) -> Data? {
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(name)
        if let data = NSData(contentsOfFile: url.path) {
            return Data(referencing: data)
        } else {
            return nil
        }
    }
    
    private func downloadDataInBackground(name: String, url: URL, completion: @escaping (Data?) -> Void) {
        if let retrievedData = retrieveDataFromDisk(name: name) {
            completion(retrievedData)
        } else {
            DispatchQueue.global(qos: .background).async { [weak self] in
                do {
                    let data = try Data(contentsOf: url)
                    self?.storeDataToDisk(data: data, name: name)
                    completion(data)
                } catch {
                    completion(nil)
                }
            }
        }
    }
    
    func showFlyingBannerQueue() {
        if let model = flyingBannerModels.first {
            if model is EntranceQueueModel {
                (model as! EntranceQueueModel).entrance.type == "animation" ? showAnimatedEntranceEffectImage(model as! EntranceQueueModel) : showEntranceEffectImage(model as! EntranceQueueModel)
            } else if model is JackpotEventModel {
                showJackpotBanner(model as! JackpotEventModel)
            } else if model is GiftDetailEventModel {
                showGlobalGiftBanner(model as! GiftDetailEventModel)
            }
        }
    }
    
    private func showEntranceEffectImage(_ model: EntranceQueueModel) {
        let imageContainer = UIView()
        imageContainer.translatesAutoresizingMaskIntoConstraints = false
        imageContainer.backgroundColor = .clear
        
        view.addSubview(imageContainer)
        imageContainer.snp.makeConstraints { constraint in
            constraint.size.equalTo(CGSize(width: 300, height: 90))
            constraint.top.equalTo(categoryView.snp.bottom).offset(24)
            constraint.leading.equalTo(view.snp.trailing)
        }
        
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        
        imageContainer.addSubview(image)
        image.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.numberOfLines = 0
        nameLabel.text = (model.entrance.showName ?? 0) == 0 ? "" : "\(model.fullname)\n IS COMING"
        nameLabel.font = .systemFont(ofSize: 11, weight: .bold)
        imageContainer.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { constraint in
            constraint.trailing.equalToSuperview().offset(-8)
            constraint.leading.equalToSuperview().offset(130)
            constraint.centerY.equalToSuperview().offset(10)
        }
        
        view.layoutIfNeeded()
        
        if let url = URL(string: model.entrance.image ?? "") {
            let filter = AspectScaledToFitSizeFilter(size: CGSize(width: 300, height: 90))
            image.af.setImage(withURL: url, filter: filter)
        }
        
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseInOut, animations: {
            imageContainer.snp.remakeConstraints { constraint in
                constraint.size.equalTo(CGSize(width: 300, height: 90))
                constraint.top.equalTo(self.categoryView.snp.bottom).offset(24)
                constraint.centerX.equalToSuperview()
            }
            self.view.layoutIfNeeded()
        }, completion: { _ in
            UIView.animate(withDuration: 1, delay: 3, animations: {
                imageContainer.alpha = 0
            }, completion: { _ in
                nameLabel.removeFromSuperview()
                image.removeFromSuperview()
                imageContainer.removeFromSuperview()
                self.flyingBannerModels.removeFirst()
                if !self.flyingBannerModels.isEmpty {
                    self.showFlyingBannerQueue()
                } else {
                    self.isflyingBannerQueueActive = false
                }
            })
        })
    }
    
    func showAnimatedEntranceEffectImage(_ model: EntranceQueueModel) {
        if let url = URL(string: model.entrance.image ?? "") {
            downloadDataInBackground(name: model.entrance.name ?? "entrance_animation", url: url, completion: { [weak self] data in
                guard let self, let data else { return }
                DispatchQueue.main.async {
                    let imageContainer = UIView()
                    imageContainer.translatesAutoresizingMaskIntoConstraints = false
                    imageContainer.backgroundColor = .clear
                    
                    self.view.addSubview(imageContainer)
                    imageContainer.snp.makeConstraints { constraint in
                        constraint.size.equalTo(CGSize(width: 300, height: 90))
                        constraint.top.equalTo(self.categoryView.snp.bottom).offset(30)
                        constraint.leading.equalTo(self.view.snp.trailing)
                    }
                    
                    let image = SDAnimatedImageView()
                    let imageData = SDAnimatedImage(data: data)
                    image.image = imageData
                    image.translatesAutoresizingMaskIntoConstraints = false
                    image.contentMode = .scaleAspectFit
                    
                    imageContainer.addSubview(image)
                    image.snp.makeConstraints { constraint in
                        constraint.leading.equalToSuperview()
                        constraint.trailing.equalToSuperview()
                        constraint.top.equalToSuperview()
                        constraint.bottom.equalToSuperview()
                    }
                    
                    self.view.layoutIfNeeded()
                    
                    UIView.animate(withDuration: 2, delay: 0, options: .curveEaseInOut, animations: {
                        imageContainer.snp.remakeConstraints { constraint in
                            constraint.size.equalTo(CGSize(width: 300, height: 90))
                            constraint.top.equalTo(self.categoryView.snp.bottom).offset(30)
                            constraint.centerX.equalToSuperview()
                        }
                        self.view.layoutIfNeeded()
                    }, completion: { _ in
                        UIView.animate(withDuration: 1, delay: 3, animations: {
                            imageContainer.alpha = 0
                        }, completion: { _ in
                            image.removeFromSuperview()
                            imageContainer.removeFromSuperview()
                            self.flyingBannerModels.removeFirst()
                            if !self.flyingBannerModels.isEmpty {
                                self.showFlyingBannerQueue()
                            } else {
                                self.isflyingBannerQueueActive = false
                            }
                        })
                    })
                }
            })
        }
    }
    
    func showJackpotBanner(_ model: JackpotEventModel) {
        let jackpotBanner = JackpotBannerView.nib(withType: JackpotBannerView.self)
        jackpotBanner.translatesAutoresizingMaskIntoConstraints = false
        jackpotBanner.setContents(model)
        view.addSubview(jackpotBanner)
        
        jackpotBanner.snp.makeConstraints { constraint in
            constraint.size.equalTo(CGSize(width: JackpotBannerView.DEFAULT_WIDTH, height: JackpotBannerView.DEFAULT_HEIGHT))
            constraint.top.equalTo(categoryView.snp.bottom).offset(24)
            constraint.leading.equalTo(view.snp.trailing)
        }
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseInOut, animations: {
            jackpotBanner.snp.remakeConstraints { constraint in
                constraint.size.equalTo(CGSize(width: JackpotBannerView.DEFAULT_WIDTH, height: JackpotBannerView.DEFAULT_HEIGHT))
                constraint.top.equalTo(self.categoryView.snp.bottom).offset(24)
                constraint.centerX.equalToSuperview()
            }
            self.view.layoutIfNeeded()
        }, completion: { _ in
            UIView.animate(withDuration: 1, delay: 3, animations: {
                jackpotBanner.alpha = 0
            }, completion: { _ in
                jackpotBanner.removeFromSuperview()
                self.flyingBannerModels.removeFirst()
                if !self.flyingBannerModels.isEmpty {
                    self.showFlyingBannerQueue()
                } else {
                    self.isflyingBannerQueueActive = false
                }
            })
        })
    }
    
    func showGlobalGiftBanner(_ model: GiftDetailEventModel) {
        let globalBanner = GlobalGiftBannerView.nib(withType: GlobalGiftBannerView.self)
        globalBanner.translatesAutoresizingMaskIntoConstraints = false
        globalBanner.setContents(model)
        view.addSubview(globalBanner)
        
        globalBanner.snp.makeConstraints { constraint in
            constraint.size.equalTo(CGSize(width: GlobalGiftBannerView.DEFAULT_WIDTH, height: GlobalGiftBannerView.DEFAULT_HEIGHT))
            constraint.top.equalTo(categoryView.snp.bottom).offset(24)
            constraint.leading.equalTo(view.snp.trailing)
        }
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseInOut, animations: {
            globalBanner.snp.remakeConstraints { constraint in
                constraint.size.equalTo(CGSize(width: GlobalGiftBannerView.DEFAULT_WIDTH, height: GlobalGiftBannerView.DEFAULT_HEIGHT))
                constraint.top.equalTo(self.categoryView.snp.bottom).offset(24)
                constraint.centerX.equalToSuperview()
            }
            self.view.layoutIfNeeded()
        }, completion: { _ in
            UIView.animate(withDuration: 1, delay: 3, animations: {
                globalBanner.alpha = 0
            }, completion: { _ in
                globalBanner.removeFromSuperview()
                self.flyingBannerModels.removeFirst()
                if !self.flyingBannerModels.isEmpty {
                    self.showFlyingBannerQueue()
                } else {
                    self.isflyingBannerQueueActive = false
                }
            })
        })
    }
    
    func showComboButton(_ model: GiftDetailEventModel) {
        comboSenderModel = model
        comboButtonImage.image = SDAnimatedImage(named: "ic_combo_button_animated.gif")
        comboButtonTimer = Timer.scheduledTimer(withTimeInterval: comboTimeout, repeats: false) { [weak self] _ in
            guard let self = self else { return }
            self.comboSenderModel = nil
            self.sentGiftId = nil
            self.comboButtonImage.image = nil
        }
    }
    
    func showGiftAnimationQueue() {
        if let model = giftModels.first {
            if let url = URL(string: model.gift?.image ?? "") {
                let giftImage = SDAnimatedImageView()
                giftImage.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(giftImage)
                
                giftImage.snp.makeConstraints { constraint in
                    constraint.leading.equalToSuperview()
                    constraint.trailing.equalToSuperview()
                    if #available(iOS 11.0, *) {
                        constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
                        constraint.top.equalTo(view.safeAreaLayoutGuide.snp.top)
                    } else {
                        constraint.bottom.equalToSuperview()
                        constraint.top.equalToSuperview()
                    }
                }
                
                view.layoutIfNeeded()
                
                downloadDataInBackground(name: model.gift?.name ?? "", url: url, completion: { data in
                    if let data = data {
                        DispatchQueue.main.async {
                            let image = SDAnimatedImage(data: data)
                            giftImage.image = image
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(model.gift?.duration ?? 4), execute: {
                                giftImage.removeFromSuperview()
                                self.giftModels.removeFirst()
                                if !self.giftModels.isEmpty {
                                    self.showGiftAnimationQueue()
                                } else {
                                    self.isGiftQueueActive = false
                                }
                            })
                        }
                    } else {
                        giftImage.removeFromSuperview()
                        self.giftModels.removeFirst()
                        if !self.giftModels.isEmpty {
                            self.showGiftAnimationQueue()
                        } else {
                            self.isGiftQueueActive = false
                        }
                    }
                })
            }
        }
    }
    
    func appendComboGiftModel(_ model: GiftDetailEventModel) {
        let timer = Timer.scheduledTimer(withTimeInterval: comboTimeout, repeats: false) { [weak self] _ in
            guard let self = self else { return }
            self.comboGiftModels.removeAll(where: { $0.detailModel == model })
            self.comboGiftTableView.reloadData()
        }
        comboGiftModels.insert(ComboGiftModel(detailModel: model, timer: timer), at: 0)
        comboGiftTableView.reloadData()
    }
    
    func increaseComboMultiplier(_ model: GiftDetailEventModel) {
        if let comboModel = comboGiftModels.first(where: { $0.detailModel == model }) {
            comboModel.increaseMultiplier()
            comboModel.timer?.invalidate()
            comboModel.timer = Timer.scheduledTimer(withTimeInterval: comboTimeout, repeats: false) { [weak self] _ in
                guard let self = self else { return }
                self.comboGiftModels.removeAll(where: { $0.detailModel == model })
                self.comboGiftTableView.reloadData()
            }
            if let rowIndex = comboGiftModels.firstIndex(where: { $0.detailModel == model }) {
                if rowIndex < 2 {
                    (comboGiftTableView.cellForRow(at: IndexPath(row: rowIndex, section: 0)) as! ComboBannerCell).increaseMultiplier(count: comboModel.multiplier)
                } else {
                    comboGiftModels.move(from: rowIndex, to: 0)
                    comboGiftTableView.reloadData()
                    (comboGiftTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ComboBannerCell).increaseMultiplier(count: comboModel.multiplier)
                }
            }
        }
    }
    
    func showSuitAnimationResult(model: UserBidResultModel) {
        if let url = URL(string: model.url ?? "") {
            downloadDataInBackground(name: model.result ?? "", url: url, completion: { data in
                if let data = data {
                    DispatchQueue.main.async {
                        let image = SDAnimatedImage(data: data)
                        self.suitResultAnimationImageView.image = image
                    }
                }
            })
        }
    }
    
    func startBattle(host: UserModel, guest: UserModel) {
        if type == .Host {
            stopZegoPreviewLayer()
        } else {
            stopPlayingStream()
        }
        
        var battleViewType: HostBattleView.BattleViewType!
        if type == .Viewer || type == .Admin {
            battleViewType = .Viewer
        } else if currentUser?.userId == host.userLiveId {
            battleViewType = .Host
        } else {
            battleViewType = .Guest
        }
        
        hostBattleModel = host
        guestBattleModel = guest
        hostBattleView.setupModels(host: host, guest: guest, type: battleViewType)
    }
    
    func updateBattleInfo(_ model: BattleInfoEventModel) {
        hostBattleView.updateBattleInfo(model)
    }
    
    func endBattle() {
        hostBattleModel = nil
        guestBattleModel = nil
        hostBattleView.endBattle()
        
        if type == .Host {
            changeLiveModeForHostBattle()
            startZegoPreviewLayer()
        } else {
            startPlayingStream()
        }
    }
    
    func appendChatModel(_ model: Any) {
        chats.append(model)
        chatsReversed = chats.reversed()
        chatTableView.reloadData()
    }
    
    func getVideoCallContainerView(_ position: Int) -> VideoCallView {
        switch position {
        case 1:
            return firstVideoCallView
        case 2:
            return secondVideoCallView
        default:
            return firstVideoCallView
        }
    }
    
    func getVideoCallViewPosition(_ view: VideoCallView) -> Int {
        if view == firstVideoCallView {
            return 1
        } else {
            return 2
        }
    }
    
    @objc private func startInvitationCountdown(_ sender: NSNotification) {
        if let position = sender.object as? Int {
            getVideoCallContainerView(position).startInviteCountdown()
        }
    }
    
    @objc private func showOtherFullProfileModal(_ notification: NSNotification) {
        if let userId = notification.object as? String {
            let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
            vc.profileType = .Other
            vc.userId = userId
            vc.isPresentedAsModal = true
            vc.navigationItem.titleView = navLabel(title: "Profile Details")
            
            let navController = UINavigationController(rootViewController: vc)
            navController.modalPresentationStyle = .overCurrentContext
            present(navController, animated: true)
        }
    }
    
    @objc private func showChatModal(_ notification: NSNotification) {
        if let userModel = notification.object as? UserModel {
            let vc = DirectChatVC(nibName: "DirectChatVC", bundle: nil)
            vc.receiverUserId = userModel.userId ?? ""
            vc.receiverUserName = userModel.fullname ?? ""
            vc.isPresentedAsModal = true
            
            let navController = UINavigationController(rootViewController: vc)
            navController.modalPresentationStyle = .overCurrentContext
            present(navController, animated: true)
        }
    }
    
    private func showUserDetailModal(userModel: UserModel) {
        let vc = ProfileModalVC(nibName: "ProfileModalVC", bundle: nil)
        vc.userModel = userModel
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @objc private func showStickerModal() {
        let vc = StickerModalVC(liveId: userModel.userId ?? "")
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @objc private func showGiftModal() {
        var vc: GiftModalVC
        if let host = hostBattleModel, let guest = guestBattleModel {
            let receivers: [UserModel] = [host, guest]
            vc = GiftModalVC(with: receivers)
        } else {
            vc = GiftModalVC(for: userModel.userId ?? "")
        }
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func showBidModal(for receiverId: String) {
        let vc = GameBidModalVC(receiverId: receiverId)
        present(vc, animated: true, completion: nil)
    }
    
    func showBidPlacedModal(model: BidPlacedEventModel) {
        popupAlertVC.alertType = .Alert
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = "Agree"
        popupAlertVC.messageStr = "Your opponent has placed \(model.amount ?? 0) Carrots to play this game. "
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true) {
                self.gameAction(bidId: model.bidId ?? "", action: .Reject)
            }
        }
        popupAlertVC.actionTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true) {
                self.gameAction(bidId: model.bidId ?? "", action: .Accept)
            }
        }
        present(popupAlertVC, animated: true, completion: nil)
    }
    
    func showLiveEndModal(endType: LiveEndType = .Default) {
        let vc = LiveEndVC(for: userModel, endType: endType)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func showLiveEndModal(endMessage: String) {
        let vc = LiveEndVC(for: userModel, endMessage: endMessage)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    private func showLiveStreamerEndModal() {
        let vc = LiveEndStreamerVC(nibName: "LiveEndStreamerVC", bundle: nil)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.userModel = userModel
        vc.delegate = self
        vc.streamDuration = streamDuration
        present(vc, animated: true, completion: {
            self.streamTimer?.invalidate()
            self.liveLogOut()
        })
    }
    
    private func showActionModal(tag: Int, withAdditionalData: Any? = nil) {
        let vc = BottomModalSheetVC(tag: tag, delegate: self)
        if withAdditionalData != nil {
            vc.additionalData = withAdditionalData
        }
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @objc private func showReportModal(_ sender: NSNotification) {
        let userId = sender.object as! String
        if type == .Admin {
            reportActions = [ModalActionModel(title: "Kick user", type: .Normal), ModalActionModel(title: "Ban user", type: .Normal)]
        } else if type == .Host {
            reportActions = [ModalActionModel(title: "Kick user", type: .Normal), ModalActionModel(title: "Report user", type: .Normal)]
        } else if type == .Viewer {
            reportActions = [ModalActionModel(title: "Report user", type: .Normal), ModalActionModel(title: "Block user", type: .Normal)]
        }
        
        let vc = BottomModalSheetVC(tag: 1, delegate: self)
        vc.additionalData = userId
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @objc func showBattleDurationModal() {
        let vc = BottomModalSheetVC(tag: 4, delegate: self)
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    private func showBanModal(userId: String) {
        let vc = BanModalVC(userId: userId)
        present(vc, animated: true)
    }
    
    private func showInviteGuestModal(position: Int) {
        let vc = InviteGuestVC(position: position)
        let navController = UINavigationController(rootViewController: vc)
        navController.modalPresentationStyle = .overFullScreen
        present(navController, animated: true)
    }
    
    @objc private func showInviteBattleModal() {
        changeLiveModeForHostBattle(completion: { [weak self] in
            let vc = InviteGuestVC(battle: true)
            let navController = UINavigationController(rootViewController: vc)
            navController.modalPresentationStyle = .overFullScreen
            self?.present(navController, animated: true)
        })
    }
    
    @objc private func showTopSpenderModal() {
        let vc = TopSpenderVC(userModel: userModel)
        let navController = UINavigationController(rootViewController: vc)
        navController.modalPresentationStyle = .overFullScreen
        present(navController, animated: true)
    }
    
    @objc private func showTopupModal() {
        let vc = TopupVC(nibName: "TopupVC", bundle: nil)
        vc.isPresentedAsModal = true
        vc.topupType = .Topup
        vc.paymentMethod = .Default
        let navVC = UINavigationController(rootViewController: vc)
        navVC.modalPresentationStyle = .overCurrentContext
        present(navVC, animated: true)
    }
    
    @objc private func showCreatePrivateRoomModal() {
        let vc = PrivateRoomVC()
        let navController = UINavigationController(rootViewController: vc)
        navController.modalPresentationStyle = .overFullScreen
        present(navController, animated: true)
    }
    
    private func showSendReportModal(userId: String) {
        let vc = ReportVC(nibName: "ReportVC", bundle: nil)
        vc.userID = userId
        let navController = UINavigationController(rootViewController: vc)
        navController.modalPresentationStyle = .overFullScreen
        present(navController, animated: true)
    }
    
    private func showInvitationPopup(model: GuestInvitationModel) {
        let vc = PopupGuestInviteVC(nibName: "PopupGuestInviteVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.model = model
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    private func showKickViewerPopup(userId: String) {
        popupAlertVC.alertType = .Alert
        popupAlertVC.cancelStr = "Yes"
        popupAlertVC.actionStr = "No"
        popupAlertVC.messageStr = "Kick this user from your liveroom?"
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true) {
                self.kickViewer(userId: userId)
            }
        }
        popupAlertVC.actionTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        present(popupAlertVC, animated: true, completion: nil)
    }
    
    private func showKickViewerSuccessPopup() {
        if let selectedTableViewIndexPath = chatTableView.indexPathForSelectedRow {
            if let chatModel = chatsReversed[selectedTableViewIndexPath.row] as? LiveChatResponseModel {
                if let name = chatModel.fullName {
                    routeToInformationModal(type: .Info, message: "\(name) kicked from this room")
                }
            }
        }
    }
    
    private func showStopBattlePopup() {
        popupAlertVC.alertType = .Alert
        popupAlertVC.cancelStr = "Not Now"
        popupAlertVC.actionStr = "Sure"
        popupAlertVC.messageStr = "Are you sure end this battle?"
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true) {
                self.stopBattle()
            }
        }
        present(popupAlertVC, animated: true, completion: nil)
    }
    
    private func showStopLivePopup() {
        popupAlertVC.alertType = .Alert
        popupAlertVC.cancelStr = "Not Now"
        popupAlertVC.actionStr = "Sure"
        popupAlertVC.messageStr = "Are you sure end this broadcast?"
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = { [unowned self] (sender) in
            self.popupAlertVC.dismiss(animated: true) {
                self.stopLive()
            }
        }
        present(popupAlertVC, animated: true, completion: nil)
    }
    
    private func reportActionHandler(indexPathRow: Int, userId: String) {
        if indexPathRow == 0 {
            switch type {
            case .Admin:
                adminKick(userId: userId)
            case .Host:
                showKickViewerPopup(userId: userId)
            case .Viewer:
                showSendReportModal(userId: userId)
            default:
                break
            }
        } else if indexPathRow == 1 {
            switch type {
            case .Admin:
                showBanModal(userId: userId)
            case .Host:
                showSendReportModal(userId: userId)
            case .Viewer:
                blockUser(userId: userId)
                break
            default:
                break
            }
        }
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            chatView.snp.updateConstraints { constraint in
                if #available(iOS 11.0, *) {
                    constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin).offset(-(keyboardHeight))
                } else {
                    constraint.bottom.equalTo(view.snp.bottomMargin).offset(-(keyboardHeight))
                }
            }
            view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {
        chatView.snp.updateConstraints { constraint in
            if #available(iOS 11.0, *) {
                constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin).offset(-16)
            } else {
                constraint.bottom.equalTo(view.snp.bottomMargin).offset(-16)
            }
        }
        view.layoutIfNeeded()
    }
    
    @objc private func userDidCaptureHandler(_ notification: NSNotification) {
        isCapturing.toggle()
        
        if !(currentUser?.isAdmin ?? true) {
            if isCapturing {
                DispatchQueue.main.async {
                    self.view.addSubview(self.screenshootView)
                    self.screenshootView.snp.makeConstraints { constraint in
                        constraint.trailing.equalToSuperview()
                        constraint.leading.equalToSuperview()
                        constraint.top.equalToSuperview()
                        constraint.bottom.equalToSuperview()
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.screenshootView.removeFromSuperview()
                }
            }
        }
    }
    
    // MARK: - Actions
    @objc private func videoCallAction(_ sender: UIGestureRecognizer) {
        if let view = sender.view as? VideoCallView {
            let position = getVideoCallViewPosition(view)
            
            if type == .Host {
                if view.guestModel == nil {
                    changeLiveModeForVideoCall(position: position)
                } else {
                    setupHostStreamActionData(position: position)
                    showActionModal(tag: 3, withAdditionalData: position)
                }
            } else if type == .Admin {
                setupAdminStreamActionData(position: position)
                showActionModal(tag: 3, withAdditionalData: position)
            } else if type == .Viewer {
                if view.streamingType == .Publishing {
                    setupActionData(position: position)
                    showActionModal(tag: 2, withAdditionalData: position)
                }
            }
        }
    }
    
    @objc private func profileTimerViewAction() {
        if let userId = userModel.userId {
            getOtherProfile(userId: userId)
        }
    }
    
    @objc private func comboAction() {
        if comboSenderModel != nil {
            sendComboGift()
            let transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            comboButtonImage.image = nil
            comboButtonImage.image = SDAnimatedImage(named: "ic_combo_button_animated.gif")
            comboButtonImage.transform = transform
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: {
                self.comboButtonImage.transform = .identity
            })
            comboButtonTimer?.invalidate()
            comboButtonTimer = Timer.scheduledTimer(withTimeInterval: comboTimeout, repeats: false) { [weak self] _ in
                guard let self = self else { return }
                self.comboSenderModel = nil
                self.sentGiftId = nil
                self.comboButtonImage.image = nil
            }
        }
    }
    
    // MARK: - API Calls
    private func getChatBubbles() {
        provider.request(.ChatBubbles, successModel: ChatBubbleResponseModel.self, withProgressHud: false, success: { response in
            if let list = response.attributes {
                ChatBubbleProvider.shared.storeChatBubbles(list)
            }
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    private func getDetailLive() {
        provider.request(.DetailLive(identifier: userModel.userId ?? ""), successModel: GetLiveDetailResponseModel.self, withProgressHud: false) { [weak self] (result) in
            guard let self = self else { return }
            self.detailModel = result.attributes
            if let category = self.detailModel.category, let hashtags = self.detailModel.hashtags {
                self.categoryView.setupCollectionViewData(category: category, hashtags: hashtags)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func sendChat(_ model: LiveChatRequestModel) {
        provider.request(.ChatLive(request: model), successModel: SuccessResponseModel.self, withProgressHud: false) { (result) in
            // Do nothing since the chat reloads itself
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func stopInvitedVideoCall(position: Int) {
        guard let request = StopLiveRequestModel(JSON: ["duration": streamDuration.stringFromTimeInterval()]) else { return }
        provider.request(.StopLive(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.stopPublishingVideoCall(position: position)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }

    }
    
    private func stopLive(isKicked: Bool = false) {
        guard let request = StopLiveRequestModel(JSON: ["duration": streamDuration.stringFromTimeInterval()]) else { return }
        provider.request(.StopLive(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.stopZegoPreviewLayer()
            if !isKicked {
                self.showLiveStreamerEndModal()
            } else {
                self.streamTimer?.invalidate()
                self.liveLogOut()
                //self.navigationController?.popViewController(animated: false)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func leaveLive() {
        guard let request = WatchAndStopWatchLiveRequestModel(JSON: ["liveId": userModel.userId ?? ""]) else { return }
        provider.request(.StopWatchLive(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.streamTimer?.invalidate()
            self.liveLogOut()
            self.navigationController?.popViewController(animated: false)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getOtherProfile(userId: String) {
        provider.request(.UserProfile(identifier: userId), successModel: UserResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            if let userModel = result.attributes {
                self.showUserDetailModal(userModel: userModel)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func returnOtherProfile(userId: String, completion: @escaping (UserModel?) -> Void) {
        provider.request(.UserProfile(identifier: userId), successModel: UserResponseModel.self) { (result) in
            if let userModel = result.attributes {
                completion(userModel)
            } else {
                completion(nil)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
            completion(nil)
        }
    }
    
    private func acceptMultiguestInvitation() {
        guard let request = ApproveCancelJoinMultiHostRequestModel(JSON: ["requestId": currentUser?.userId ?? ""]) else { return }
        provider.request(.ApproveJoinMultiHost(request: request), successModel: SuccessResponseModel.self) { (result) in
            
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func acceptVideoCallInvitation(position: Int) {
        guard let request = JoinMultiHostRequestModel(JSON: ["hostId": userModel.userId ?? ""]) else { return }
        provider.request(.JoinMultiHost(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.startPublishingVideoCall(position: position)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func acceptBattleInvitation(hostId: String) {
        guard let request = JoinMultiHostRequestModel(JSON: ["hostId": hostId, "position": 1]) else { return }
        provider.request(.JoinMultiHost(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func kickViewer(userId: String) {
        guard let request = UserIdRequestModel(JSON: ["userId": userId]) else { return }
        provider.request(.KickViewer(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.showKickViewerSuccessPopup()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func kickMultiguest(guestModel: UserModel) {
        guard let request = UserIdRequestModel(JSON: ["userId": guestModel.userLiveId ?? ""]) else { return }
        provider.request(.KickMultiguest(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.stopPlayingVideoCall(id: guestModel.userLiveId ?? "", position: guestModel.position)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func changeLiveModeForVideoCall(position: Int) {
        guard let request = ChangeLiveModeRequestModel(JSON: ["multiHostCount": 3]) else { return }
        provider.request(.ChangeLiveMode(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.showInviteGuestModal(position: position)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func changeLiveModeForHostBattle(completion: @escaping () -> Void = {}) {
        guard let request = ChangeLiveModeRequestModel(JSON: ["multiHostCount": 2]) else { return }
        provider.request(.ChangeLiveMode(request: request), successModel: SuccessResponseModel.self, withProgressHud: false) { _ in
            completion()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func turnAudioVideo(audio: AudioVideoState, video: AudioVideoState) {
        guard let request = LiveTurnRequestModel(JSON: ["turnOffAudio": audio.rawValue, "turnOffVideo": video.rawValue]) else { return }
        provider.request(.LiveTurn(request: request), successModel: SuccessResponseModel.self) { (result) in
            // FIXME: Implement this if needed
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func blockUser(userId: String) {
        guard let request = UserRequestModel(JSON: ["userID": userId]) else { return }
        provider.request(.BlockUser(request: request), successModel: SuccessResponseModel.self) { (result) in
            // FIXME: Implement this if needed
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func adminKick(userId: String) {
        guard let request = ModerationRequestModel(JSON: ["action": "kick", "userId": userId, "banDurationInHours": 0]) else { return }
        provider.request(.Moderation(request: request), successModel: SuccessResponseModel.self) { (result) in
            // FIXME: Implement if needed
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    func sendComboGift() {
        if let comboSenderModel = comboSenderModel, let sentGiftId = sentGiftId {
            guard let request = SendGiftRequestModel(JSON: ["giftID": sentGiftId, "receiverID": comboSenderModel.receiver?.userId ?? "", "count": comboSenderModel.gift?.count ?? 1]) else { return }
            provider.request(.SendGift(request: request), successModel: SuccessResponseModel.self, withProgressHud: false, success: { _ in
                // Do Nothing
            }, failure: { error in
                print(error.messages.joined(separator: "\n"))
            })
        }
    }
    
    func startBattle(duration: Int) {
        guard let request = StartBattleRequestModel(JSON: ["multiHostId": userModel.multiHostId ?? "", "duration": duration]) else { return }
        provider.request(.StartBattle(request: request), successModel: SuccessResponseModel.self, success: { _ in
            // FIXME: Implement this soon
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    func stopBattle() {
        guard let request = StopBattleRequestModel(JSON: ["multiHostId": userModel.multiHostId ?? ""]) else { return }
        provider.request(.StopBattle(request: request), successModel: SuccessResponseModel.self, success: { [weak self] _ in
            guard let self = self else { return }
            self.endBattle()
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    func playGame(with viewerId: String) {
        guard let request = BidRespondedRequestModel(JSON: ["viewerId": viewerId]) else { return }
        provider.request(.BidResponded(request: request), successModel: SuccessResponseModel.self, success: { response in
            // FIXME: Implement if needed
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    func gameAction(bidId: String, action: BidAction) {
        guard let request = BidActionRequestModel(JSON: ["bidId": bidId, "action": action.rawValue]) else { return }
        provider.request(.BidAction(request: request), successModel: SuccessResponseModel.self, success: { response in
            // FIXME: Implement if needed
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    func answerGame(user: LiveType, answer: SuitAnswer) {
        if let liveId = bidAcceptedModel?.liveId, let bidId = bidAcceptedModel?.bidId, let sessionId = bidAcceptedModel?.sessionId {
            guard let request = BidAnswerRequestModel(JSON: ["user": (user == .Host ? "host" : "viewer"), "liveId": liveId, "bidId": bidId, "sessionId": sessionId, "answer": answer.rawValue]) else { return }
            provider.request(.BidAnswer(request: request), successModel: SuccessResponseModel.self, success: { [weak self] response in
                guard let self = self else { return }
                self.suitAnswerView.isHidden = true
            }, failure: { error in
                print(error.messages.joined(separator: "\n"))
            })
        }
    }
    
    func closeGame() {
        guard let request = BidCloseRequestModel(JSON: ["liveId": userModel?.liveId ?? "", "bidId": bidResultModel?.bidId ?? ""]) else { return }
        provider.request(.BidClose(request: request), successModel: SuccessResponseModel.self, success: { [weak self] response in
            guard let self = self else { return }
            self.bidResultModel = nil
            self.suitResultAnimationImageView.image = nil
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    @objc private func getShareLink() {
        if let userId = userModel.userId, let fullname = userModel.fullname {
            provider.request(.LiveShareLink(identifier: userId), successModel: LiveShareLinkResponseModel.self) { [weak self] (result) in
                guard let self = self else { return }
                if let url = URL(string: result.attributes?.link ?? "") {
                    let items: [Any] = ["Come and see \(fullname)'s live in SugarLive", url]
                    let vc = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    self.present(vc, animated: true)
                }
            } failure: { (error) in
                print(error.messages.joined(separator: "\n"))
            }
        }
    }
}

// MARK: - Table view Delegate and Data Source
extension LiveVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView == chatTableView ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == comboGiftTableView {
            return comboGiftModels.count >= 3 ? 3 : comboGiftModels.count
        } else {
            return section == 1 ? 1 : chatsReversed.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == chatTableView {
            if indexPath.section == 0 {
                if chatsReversed[indexPath.row] is LiveChatResponseModel {
                    let model = chatsReversed[indexPath.row] as! LiveChatResponseModel
                    let chatCell = tableView.dequeueReusableCell(withIdentifier: "ChatLiveCell") as! ChatLiveCell
                    chatCell.selectionStyle = .none
                    if hostBattleModel != nil && guestBattleModel != nil {
                        let battleType: ChatLiveCell.BattleChatType = hostBattleModel?.userLiveId == model.receiverUserId ? .Host : .Guest
                        chatCell.setContents(model: model, battleType: battleType)
                    } else {
                        chatCell.setContents(model: model)
                    }
                    chatCell.transform = CGAffineTransform(scaleX: 1, y: -1)
                    return chatCell
                } else if chatsReversed[indexPath.row] is LiveStickerResponseModel {
                    let chatCell = tableView.dequeueReusableCell(withIdentifier: "ChatLiveCell") as! ChatLiveCell
                    chatCell.selectionStyle = .none
                    chatCell.setContents(sticker: chatsReversed[indexPath.row] as! LiveStickerResponseModel)
                    chatCell.transform = CGAffineTransform(scaleX: 1, y: -1)
                    return chatCell
                } else if chatsReversed[indexPath.row] is ViewerLiveJoinNotificationModel {
                    let notificationCell = tableView.dequeueReusableCell(withIdentifier: "ChatNotificationCell") as! ChatNotificationCell
                    notificationCell.selectionStyle = .none
                    notificationCell.transform = CGAffineTransform(scaleX: 1, y: -1)
                    notificationCell.setContents(chatsReversed[indexPath.row] as! ViewerLiveJoinNotificationModel)
                    return notificationCell
                } else if chatsReversed[indexPath.row] is GiftDetailEventModel {
                    let giftNotificationCell = tableView.dequeueReusableCell(withIdentifier: "GiftNotificationCell") as! GiftNotificationCell
                    giftNotificationCell.selectionStyle = .none
                    giftNotificationCell.transform = CGAffineTransform(scaleX: 1, y: -1)
                    giftNotificationCell.setContents(chatsReversed[indexPath.row] as! GiftDetailEventModel)
                    return giftNotificationCell
                } else if chatsReversed[indexPath.row] is LiveNotificationModel {
                    let notificationCell = tableView.dequeueReusableCell(withIdentifier: "ChatNotificationCell") as! ChatNotificationCell
                    notificationCell.selectionStyle = .none
                    notificationCell.transform = CGAffineTransform(scaleX: 1, y: -1)
                    notificationCell.setContents(chatsReversed[indexPath.row] as! LiveNotificationModel)
                    return notificationCell
                } else {
                    return UITableViewCell()
                }
            } else {
                let tncCell = tableView.dequeueReusableCell(withIdentifier: "TnCLiveCell") as! TnCLiveCell
                tncCell.selectionStyle = .none
                tncCell.transform = CGAffineTransform(scaleX: 1, y: -1)
                return tncCell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ComboBannerCell") as! ComboBannerCell
            cell.setContents(comboGiftModels[indexPath.row])
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == chatTableView {
            return UITableView.automaticDimension
        } else {
            return ComboBannerCell.DEFAULT_HEIGHT
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == chatTableView {
            profileTimerView.userCollectionView.selectItem(at: nil, animated: true, scrollPosition: .left)
            if indexPath.section == 0 {
                if let chatModel = chatsReversed[indexPath.row] as? LiveChatResponseModel {
                    if type == .Host {
                        modalActions = [ModalActionModel(title: "View \(chatModel.fullName ?? "") profile", type: .Normal), ModalActionModel(title: "Play game with \(chatModel.fullName ?? "")", type: .Normal)]
                    } else {
                        modalActions = [ModalActionModel(title: "View \(chatModel.fullName ?? "") profile", type: .Normal)]
                    }
                    showActionModal(tag: 0)
                }
            }
        }
    }
}

// MARK: - Text Field delegate
extension LiveVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let messageText = textField.text {
            guard let request = LiveChatRequestModel(JSON: [
                "liveId": userModel?.userId ?? "",
                "message": messageText,
            ]) else {
                textField.text = nil
                //textField.resignFirstResponder()
                return false
            }
            sendChat(request)
        }
        
        textField.text = nil
        //textField.resignFirstResponder()
        
        return false
    }
}

// MARK: - LiveEndVC delegate
extension LiveVC: LiveEndVCDelegate {
    func didDismissModal() {
        backToHome()
    }
}

// MARK: - PopupGuestInvite delegate
extension LiveVC: PopupGuestInviteDelegate {
    func acceptAction(position: Int) {
        acceptVideoCallInvitation(position: position)
    }
    
    func acceptBattleAction(hostId: String) {
        acceptBattleInvitation(hostId: hostId)
    }
    
    func cancelAction() {
        // FIXME: Implement if needed
    }
}

// MARK: - Bottom Modal Sheet delegate
extension LiveVC: BottomModalSheetDelegate {
    func numberOfItems(in modalSheet: BottomModalSheetVC) -> Int {
        switch modalSheet.tag {
        case 0:
            return modalActions.count
        case 1:
            return reportActions.count
        case 2:
            return videoCallActions.count
        case 3:
            return hostStreamActions.count
        case 4:
            return battleDurationActions.count
        default:
            return 0
        }
    }
    
    func modalSheet(_ modalSheet: BottomModalSheetVC, itemForRowAt indexPath: IndexPath) -> ModalActionModel {
        switch modalSheet.tag {
        case 0:
            return modalActions[indexPath.row]
        case 1:
            return reportActions[indexPath.row]
        case 2:
            return videoCallActions[indexPath.row]
        case 3:
            return hostStreamActions[indexPath.row]
        case 4:
            return battleDurationActions[indexPath.row]
        default:
            return ModalActionModel(title: "", type: .Normal)
        }
    }
    
    func modalSheet(_ modalSheet: BottomModalSheetVC, didSelectRowAt indexPath: IndexPath) {
        if modalSheet.tag == 0 { // Chat action
            if let selectedTableViewIndexPath = chatTableView.indexPathForSelectedRow {
                if let userId = (chatsReversed[selectedTableViewIndexPath.row] as? LiveChatResponseModel)?.userId {
                    if indexPath.row == 0 {
                        getOtherProfile(userId: userId)
                    } else if indexPath.row == 1 {
                        playGame(with: userId)
                    }
                }
            }
        } else if modalSheet.tag == 1 { // Report Action
            if let selectedTableViewIndexPath = chatTableView.indexPathForSelectedRow {
                if let userId = (chatsReversed[selectedTableViewIndexPath.row] as? LiveChatResponseModel)?.userId {
                    reportActionHandler(indexPathRow: indexPath.row, userId: userId)
                }
            } else if let selectedCollectionViewIndexPath = profileTimerView.userCollectionView.indexPathsForSelectedItems?.last {
                if let userId = profileTimerView.users[selectedCollectionViewIndexPath.row].userId {
                    reportActionHandler(indexPathRow: indexPath.row, userId: userId)
                }
            } else if let userId = (modalSheet.additionalData as? String) {
                reportActionHandler(indexPathRow: indexPath.row, userId: userId)
            }
        } else if modalSheet.tag == 2 {
            if let position = (modalSheet.additionalData as? Int) {
                if indexPath.row == 0 {
                    turnAudioVideo(audio: getVideoCallContainerView(position).audioState.getOppositeValue(), video: getVideoCallContainerView(position).videoState)
                } else if indexPath.row == 1 {
                    turnAudioVideo(audio: getVideoCallContainerView(position).audioState, video: getVideoCallContainerView(position).videoState.getOppositeValue())
                } else if indexPath.row == 2 {
                    stopInvitedVideoCall(position: position)
                }
            }
        } else if modalSheet.tag == 3 { // Host stream control actions
            if let position = (modalSheet.additionalData as? Int) {
                if indexPath.row == 0 {
                    let audio = getVideoCallContainerView(position).audioState.getOppositeValue()
                    let video = getVideoCallContainerView(position).videoState
                    let userId = getVideoCallContainerView(position).guestModel?.userLiveId ?? ""
                    toggleGuestStreamState(userId: userId, audio: audio, video: video)
                } else if indexPath.row == 1 {
                    let audio = getVideoCallContainerView(position).audioState
                    let video = getVideoCallContainerView(position).videoState.getOppositeValue()
                    let userId = getVideoCallContainerView(position).guestModel?.userLiveId ?? ""
                    toggleGuestStreamState(userId: userId, audio: audio, video: video)
                } else if indexPath.row == 2 {
                    if type == .Admin {
                        getOtherProfile(userId: getVideoCallContainerView(position).guestModel?.userLiveId ?? "")
                    } else {
                        if let model = guestModels.first(where: { $0.position == position }) {
                            kickMultiguest(guestModel: model)
                        }
                    }
                }
            }
        } else if modalSheet.tag == 4 { // Battle duration action
            var duration = 0
            switch indexPath.row {
            case 0:
                duration = 120
            case 1:
                duration = 300
            case 2:
                duration = 600
            default:
                break
            }
            hostBattleView.startCountdown { [unowned self] in
                self.startBattle(duration: duration)
            }
        }
    }
}

// MARK: - Transitioning delegate
extension LiveVC: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        SlideOverPresentationController(presentedViewController: presented, presenting: presenting)
    }
}
