//
//  LoginViaVC.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit
import AuthenticationServices

class LoginViaVC: BaseStackVC {
  
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var appleIDButton: UIButton!
    @IBOutlet weak var gmailButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var hostLabel: UILabel!
    @IBOutlet weak var termLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var forgotPasswordButton: UIButton!
  
    var termTapped: (_ sender: Any) -> Void = {_ in }
    var round = CGFloat(45 / 2)
    private let buttonAttributes = [NSAttributedString.Key.font: UIFont.defaultSemiBoldFont(size: 12),
                                  NSAttributedString.Key.foregroundColor: UIColor.white]
  
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  
    override func setupLayout() {
        if Constant.currentDevice.type == DeviceModel.iPhone5S {
            appleIDButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
            gmailButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
            facebookButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
            round = CGFloat(35 / 2)
        }
        
        titleLabel.text = "SUGAR"
        titleLabel.textColor = .PRIMARY_2
        titleLabel.font = .customFont(fontFamilyName: "BiondiSansW00-Regular", size: 20)


        appleIDButton.layer.cornerRadius = round
        appleIDButton.backgroundColor = .black
        appleIDButton.setAttributedTitle("Login with Apple ID".toAttributedString(with: buttonAttributes), for: .normal)

        gmailButton.layer.cornerRadius = round
        gmailButton.backgroundColor = .init(hex: "DC4335")
        gmailButton.setAttributedTitle("Login with Google account".toAttributedString(with: buttonAttributes), for: .normal)

        facebookButton.layer.cornerRadius = round
        facebookButton.backgroundColor = .init(hex: "3C5A9A")
        facebookButton.setAttributedTitle("Login with Facebook".toAttributedString(with: buttonAttributes), for: .normal)

        phoneLabel.attributedText = "Or login with phone number".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 10), NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT])

        let attStr = NSMutableAttributedString(attributedString: NSAttributedString(string: "Want to go Live ? ", attributes: [.font: UIFont.defaultRegularFont(size: 12), .foregroundColor: UIColor.DARK_TEXT]))
        attStr.append(NSAttributedString(string: "Be a Host Now !", attributes: [.font: UIFont.defaultBoldFont(size: 12), .foregroundColor: UIColor.DARK_TEXT]))
        hostLabel.attributedText = attStr
//        hostLabel.font = UIFont.defaultRegularFont(size: 12)
//        hostLabel.textColor = .DARK_TEXT
        hostLabel.isUserInteractionEnabled = true
        let hostGesture = UITapGestureRecognizer(target: self, action: #selector(hostAction(_:)))
        hostLabel.addGestureRecognizer(hostGesture)


        let termAttstr = NSMutableAttributedString(attributedString: "Read our Terms of Use".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]))
        termLabel.attributedText = termAttstr
        termLabel.isUserInteractionEnabled = true
        let termGesture = UITapGestureRecognizer(target: self, action: #selector(termAction(_:)))
        termLabel.addGestureRecognizer(termGesture)

        forgotPasswordButton.setAttributedTitle("Forgot Password?".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12),
        NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]),
        for: .normal)

        containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = true
    }
  
//  MARK: Service
  private func doGoogleSignIn() {
    AuthManager.shared.signWithGoogle(vc: self) { (user, error) in
      guard error == nil else {
        print(error?.localizedDescription ?? "")
        return
      }
      guard let user = user else {
        print( "User not found")
        return
      }
      
      AuthManager.shared.googleAuthentication(user: user) { (authentication, error) in
        guard error == nil else {
          print(error?.localizedDescription ?? "")
          return
        }
        guard let authentication = authentication else {
          print("Token not found")
          return
        }
        
        let json: [String: Any] = [
          "googleIdToken": authentication.idToken ?? "",
          "deviceToken": AuthManager.shared.deviceToken,
          "firebaseToken": AuthManager.shared.FCMToken,
          "device": Constant.currentDevice.type.rawValue,
          "operatingSystem": "\(Constant.currentDevice.systemName) \(Constant.currentDevice.systemVersion)",
          "preferredNetwork": getConnectionType()
        ]
        
        guard let requestModel = RegisterRequestModel.init(JSON: json) else { return }
        
        var registerModel = RegisterModel(request: requestModel, response: nil)
        
        provider.request(.GoogleLogin(request: requestModel), successModel: RegisterResponseModel.self) { (result) in
            UserDefaults.standard.set(false, forKey: "phoneLogin")
          registerModel.response = result
          registerModel.response?.attributes?.cacheToken()
          self.dismissAllModal {
            self.getMyProfile {}
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateReadNotificationResponse), object: nil)
          }
        } failure: { (error) in
          print(error.messages.joined(separator: "\n"))
        }
      }
    }
  }
  
  private func doFBSignIn() {
    AuthManager.shared.signWithFB(vc: self) { (result) in
      var fbToken = ""
      switch result {
      case .success(_, _, let token):
        fbToken = token?.tokenString ?? ""
        break
      default:
        return
      }
      
      let json: [String: Any] = [
        "fbToken": fbToken,
        "deviceToken": AuthManager.shared.deviceToken,
        "firebaseToken": AuthManager.shared.FCMToken,
        "device": Constant.currentDevice.type.rawValue,
        "operatingSystem": "\(Constant.currentDevice.systemName) \(Constant.currentDevice.systemVersion)",
        "preferredNetwork": getConnectionType()
      ]
      
      guard let requestModel = RegisterRequestModel.init(JSON: json) else { return }
      
      var registerModel = RegisterModel(request: requestModel, response: nil)
      
      provider.request(.FBLogin(request: requestModel), successModel: RegisterResponseModel.self) { (result) in
          UserDefaults.standard.set(false, forKey: "phoneLogin")
        registerModel.response = result
        registerModel.response?.attributes?.cacheToken()
        self.dismissAllModal {
          self.getMyProfile {}
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateReadNotificationResponse), object: nil)
        }
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
  @available(iOS 13.0, *)
  private func doAppleSignIn() {
    let authManager = AuthManager.shared
    authManager.appleHandler = {(credential, error) in
      if let credential = credential as? ASAuthorizationAppleIDCredential {
        let json: [String: Any] = [
          "token": String(data: (credential.identityToken)!, encoding: .utf8) ?? "",
          "deviceToken": AuthManager.shared.deviceToken,
          "firebaseToken": AuthManager.shared.FCMToken,
          "device": Constant.currentDevice.type.rawValue,
          "operatingSystem": "\(Constant.currentDevice.systemName) \(Constant.currentDevice.systemVersion)",
          "preferredNetwork": getConnectionType()
        ]
        
        guard let requestModel = RegisterRequestModel.init(JSON: json) else { return }
        
        var registerModel = RegisterModel(request: requestModel, response: nil)
        
        provider.request(.AppleLogin(request: requestModel), successModel: RegisterResponseModel.self) { (result) in
            UserDefaults.standard.set(false, forKey: "phoneLogin")
          registerModel.response = result
          registerModel.response?.attributes?.cacheToken()
          self.dismissAllModal {
            self.getMyProfile {}
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateReadNotificationResponse), object: nil)
          }
        } failure: { (error) in
          print(error.messages.joined(separator: "\n"))
        }
      }
    }
    AuthManager.shared.signWithApple()
  }
  
//  MARK: Action
  @IBAction func closeAction(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func appleIDAction(_ sender: Any) {
    if #available(iOS 13.0, *) {
      doAppleSignIn()
    } else {
      self.dismissAllModal {
        self.routeToWebVC(type: .Custom(name: "Apple Sign in", url: AuthManager.startSigninWithAppleJS()))
      }
    }
  }
  
  @IBAction func gmailAction(_ sender: Any) {
    doGoogleSignIn()
  }
  
  @IBAction func facebookAction(_ sender: Any) {
    doFBSignIn()
  }
  
  @IBAction func phoneAction(_ sender: Any) {
    routeToRegister(registerType: .Phone)
  }
  
  @IBAction func hostAction(_ sender: Any) {
    routeToRegister(registerType: .Host)
  }
  
  @IBAction func termAction(_ sender: Any) {
    dismiss(animated: true) {
      self.termTapped(sender)
    }
  }
  
  @IBAction func forgotPasswordAction(_ sender: Any) {
    self.routeToRegister(registerType: .ForgotPassword)
  }
  
  //  MARK: Custom
  
}
