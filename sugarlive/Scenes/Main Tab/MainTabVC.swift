//
//  MainTabVC.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit
import Tabman
import Pageboy
import Localize

class MainTabVC: UITabBarController {
  
  private let tabScene: TabmanViewController = {
    let tabScene = TabmanViewController()
    tabScene.tabBarItem.title = "Home".localized
    tabScene.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.defaultSemiBoldFont(size: 10)], for: .normal)
    tabScene.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 10)], for: .selected)
    tabScene.tabBarItem.image = UIImage(named: "ic_home_gray")
    tabScene.tabBarItem.selectedImage = UIImage(named: "ic_home_purple")
    tabScene.tabBarItem.imageInsets = .init(top: 0, left: 2, bottom: 0, right: 2)
    return tabScene
  }()
  
  private let tmBar: TMBarView<TMHorizontalBarLayout, TMLabelBarButton, TMBarIndicator.None> = {
    let tmBar = TMBarView<TMHorizontalBarLayout, TMLabelBarButton, TMBarIndicator.None>()//TMBar.ButtonBar()
    tmBar.layout.transitionStyle = .snap
    tmBar.layout.alignment = .leading
    tmBar.buttons.customize { (button) in
      button.font = .defaultRegularFont(size: 12)
      button.selectedFont = .defaultBoldFont(size: 12)
      button.tintColor = .MEDIUM_TEXT
      button.selectedTintColor = .PRIMARY_1
      button.contentInset = .init(top: 15, left: 20, bottom: 15, right: 20)
    }
    tmBar.indicator.tintColor = .clear
    tmBar.indicator.isHidden = false
    return tmBar
  }()
  
  private let liveScene: HomeVC = {
    let liveScene = HomeVC(nibName: "HomeVC", bundle: nil)
    liveScene.homeType = .LiveNow
    liveScene.tag = 1
    return liveScene
  }()
  
  private let recomendedScene: HomeVC = {
    let recomendedScene = HomeVC(nibName: "HomeVC", bundle: nil)
    recomendedScene.homeType = .Recomended
    recomendedScene.tag = 1
    return recomendedScene
  }()
  
  private let comerScene: HomeVC = {
    let comerScene = HomeVC(nibName: "HomeVC", bundle: nil)
    comerScene.homeType = .NewComer
    comerScene.tag = 1
    return comerScene
  }()
  
  private let eventScene: HomeVC = {
    let eventScene = HomeVC(nibName: "HomeVC", bundle: nil)
    eventScene.homeType = .Event
    eventScene.tag = 1
    return eventScene
  }()
  
  private let followedScene: HomeVC = {
    let followedScene = HomeVC(nibName: "HomeVC", bundle: nil)
    followedScene.homeType = .Followed
    followedScene.tabBarItem.title = "Followed".localized
    followedScene.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.defaultSemiBoldFont(size: 10)], for: .normal)
    followedScene.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 10)], for: .selected)
    followedScene.tabBarItem.image = UIImage(named: "ic_star_gray")
    followedScene.tabBarItem.selectedImage = UIImage(named: "ic_star_purple")
    followedScene.tabBarItem.imageInsets = .init(top: 0, left: 2, bottom: 0, right: 2)
    followedScene.tag = 2
    return followedScene
  }()
  
  private let feedScene: FeedVC = {
    let feedScene = FeedVC(nibName: "FeedVC", bundle: nil)
    feedScene.tabBarItem.title = "Feed".localized
    feedScene.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.defaultSemiBoldFont(size: 10)], for: .normal)
    feedScene.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 10)], for: .selected)
    feedScene.tabBarItem.image = UIImage(named: "ic_feed_gray")
    feedScene.tabBarItem.selectedImage = UIImage(named: "ic_feed_purple")
    feedScene.tabBarItem.imageInsets = .init(top: 0, left: 2, bottom: 0, right: 2)
    feedScene.tag = 4
    return feedScene
  }()
  
  private let profileScene: ProfileVC = {
    let profileScene = ProfileVC(nibName: "ProfileVC", bundle: nil)
    profileScene.tabBarItem.title = "Profile".localized
    profileScene.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.defaultSemiBoldFont(size: 10)], for: .normal)
    profileScene.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 10)], for: .selected)
    profileScene.tabBarItem.image = UIImage(named: "ic_profile_gray")
    profileScene.tabBarItem.selectedImage = UIImage(named: "ic_profile_purple")
    profileScene.tabBarItem.imageInsets = .init(top: 0, left: 2, bottom: 0, right: 2)
    profileScene.tag = 5
    return profileScene
  }()
  
  private let streamVC: BaseVC = {
    let streamVC = BaseVC()
    streamVC.tag = 0
    streamVC.tabBarItem.image = UIImage(named: "ic_play_purple")
    streamVC.tabBarItem.selectedImage = UIImage(named: "ic_play_purple")
    streamVC.tabBarItem.imageInsets = .init(top: -14, left: 8, bottom: 0, right: 8)
    streamVC.tag = 3
    return streamVC
  }()
  
  var homeControllers: [HomeVC] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    delegate = self
    
    setupNav()
    
    configTabs()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.navigationController?.navigationBar.barTintColor = .PRIMARY_1
  }
  
  func setupNav() {
    hidesBottomBarWhenPushed = true
  }
  
  func configTabs() {
    homeControllers = [liveScene, recomendedScene, comerScene, eventScene]
    
    tabScene.dataSource = self
    tabScene.delegate = self
    tabScene.addBar(tmBar, dataSource: self, at: .top)
    tmBar.delegate = self
    tabScene.delegate = self
    
    viewControllers = [tabScene, followedScene, streamVC, feedScene, profileScene]
    tabBar.unselectedItemTintColor = .init(hex: "848484")
    tabBar.tintColor = .PRIMARY_2
    
  }
  
//  MARK: Route
  private func routeToLivePick() {
    let destinationVC = LivePickVC(nibName: "LivePickVC", bundle: nil)
    navigationController?.pushViewController(destinationVC, animated: false)
  }
  
  private func showNotHostModal() {
    let popUp = PopupAlertVC(nibName: "PopupAlertVC", bundle: nil)
    popUp.modalTransitionStyle = .crossDissolve
    popUp.modalPresentationStyle = .overFullScreen
    popUp.alertType = .Alert
    popUp.actionStr = "Contact admin"
    popUp.cancelStr = "Cancel"
    let attStr = NSMutableAttributedString(string: "Join as Host\n", attributes: [.font: UIFont.defaultBoldFont(size: 18), .foregroundColor: UIColor.PRIMARY_1])
    attStr.append(NSAttributedString(string: "\nPlease contact our admin to confirm your trial as a host.\n", attributes: [.font: UIFont.defaultRegularFont(size: 14)]))
    popUp.useAttributedText = true
    popUp.attributedTextMessage = attStr
    popUp.hideCancelButton = false
    
    popUp.actionTapped = { [unowned self] (sender) in
      popUp.dismiss(animated: true)
      self.openAdminURL()
    }
    
    popUp.cancelTapped = {(sender) in
      popUp.dismiss(animated: true)
    }
    
    present(popUp, animated: true, completion: nil)
  }
  
  func openAdminURL() {
    let waUrl = "https://api.whatsapp.com/send?phone=\(Constant.AdminPhoneNumber)"
    if let urlString = waUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
      if let url = URL(string: urlString) {
        if UIApplication.shared.canOpenURL(url){
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
      }
    }
  }
}

extension MainTabVC: UITabBarControllerDelegate {
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    if let vc = viewController as? BaseVC {
      if !Constant.isLogin && (vc.tag ?? 0) > 1 {
        vc.routeToLoginVia()
        return false
      }
      if vc.tag == 3 {
        if let verified = AuthManager.shared.currentUser?.verified {
          if verified {
            routeToLivePick()
          } else {
            showNotHostModal()
          }
        } else {
          showNotHostModal()
        }
      }
      return vc.tag != 3
    } else {
      let vc = viewController as? TabmanViewController
      vc?.reloadData()
      return true
    }
  }
}

extension MainTabVC: PageboyViewControllerDataSource {
  func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
    return homeControllers.count
  }
  
  func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
    return homeControllers[index]
  }
  
  func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
    return nil
  }
  
  
}
extension MainTabVC: PageboyViewControllerDelegate {
  func pageboyViewController(_ pageboyViewController: PageboyViewController, didReloadWith currentViewController: UIViewController, currentPageIndex: PageboyViewController.PageIndex) {
    
  }
  
  func pageboyViewController(_ pageboyViewController: PageboyViewController, didScrollTo position: CGPoint, direction: PageboyViewController.NavigationDirection, animated: Bool) {
    
  }
  
  func pageboyViewController(_ pageboyViewController: PageboyViewController, didScrollToPageAt index: PageboyViewController.PageIndex, direction: PageboyViewController.NavigationDirection, animated: Bool) {
    for i in 0..<homeControllers.count {
      tmBar.buttons.for(item: tmBar.items?[i] as! TMBarItemable)?.selectionState = i == index ? .selected : .unselected
    }
    tmBar.update(for: CGFloat(index), capacity: 4, direction: .reverse, animation: TMAnimation(isEnabled: true, duration: 0.5))
  }
  
  func pageboyViewController(_ pageboyViewController: PageboyViewController, willScrollToPageAt index: PageboyViewController.PageIndex, direction: PageboyViewController.NavigationDirection, animated: Bool) {
  }
}

extension MainTabVC: TMBarDataSource {
  func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
    return TMBarItem(title: homeControllers[index].homeType.title())
  }
}

extension MainTabVC: TMBarDelegate {
  func bar(_ bar: TMBar, didRequestScrollTo index: Int) {
    tabScene.scrollToPage(.at(index: index), animated: true)
    for i in 0..<homeControllers.count {
      tmBar.buttons.for(item: bar.items?[i] as! TMBarItemable)?.selectionState = i == index ? .selected : .unselected
    }
  }
}
