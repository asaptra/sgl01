//
//  BanModalVC.swift
//  sugarlive
//
//  Created by cleanmac-ada on 26/04/22.
//

import UIKit

class BanModalVC: UIViewController {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var durationContainerView: UIView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var durationButton: UIButton!
    @IBOutlet weak var durationTableView: UITableView!
    @IBOutlet weak var durationTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    // MARK: - Variables
    private let cellHeight: CGFloat = 45
    private var durations: [BanDurationModel] = []
    private var isShowingDurationSelection: Bool = false {
        didSet {
            durationTableView.isHidden = !isShowingDurationSelection
        }
    }
    
    private var userId: String!
    private var selectedDuration: BanDurationModel? {
        didSet {
            durationLabel.text = selectedDuration?.description
        }
    }

    // MARK: - Overriden Functions
    init(userId: String) {
        super.init(nibName: "BanModalVC", bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        self.userId = userId
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        durationTableViewHeightConstraint.constant = cellHeight * CGFloat(durations.count)
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        getBanDuration()
        
        durationContainerView.layer.borderWidth = 1
        durationContainerView.layer.borderColor = UIColor(hex: "#D8D8D8").cgColor
        
        durationTableView.layer.borderWidth = 1
        durationTableView.layer.borderColor = UIColor(hex: "#D8D8D8").cgColor
        durationTableView.delegate = self
        durationTableView.dataSource = self
        durationTableView.isScrollEnabled = false
        durationTableView.separatorStyle = .none
        durationTableView.register(UINib(nibName: "BanSelectionCell", bundle: nil), forCellReuseIdentifier: "BanSelectionCell")
    }
    
    // MARK: - Custom Functions
    
    // MARK: - Actions
    @IBAction func selectionAction(_ sender: Any) {
        if !durations.isEmpty {
            isShowingDurationSelection.toggle()
        }
    }
    
    @IBAction func proceedAction(_ sender: Any) {
        if !durations.isEmpty && selectedDuration != nil {
            banUser()
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    // MARK: - API Calls
    private func getBanDuration() {
        provider.request(.BanDuration, successModel: BanDurationResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.durations = result.attributes
            self.selectedDuration = self.durations.first
            self.durationTableView.reloadData()
        } failure: { [weak self] (error) in
            guard let self = self else { return }
            self.dismiss(animated: true)
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func banUser() {
        guard let request = ModerationRequestModel(JSON: ["action": "ban", "userId": userId ?? "", "banDurationInHours": selectedDuration?.durationInHours ?? 0]) else { return }
        provider.request(.Moderation(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.dismiss(animated: true)
        } failure: { [weak self] (error) in
            guard let self = self else { return }
            self.dismiss(animated: true)
            print(error.messages.joined(separator: "\n"))
        }
    }
}

// MARK: - Table view delegate and data source
extension BanModalVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        durations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BanSelectionCell") as! BanSelectionCell
        cell.setTitle(durations[indexPath.row].description ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedDuration = durations[indexPath.row]
        isShowingDurationSelection.toggle()
    }
}
