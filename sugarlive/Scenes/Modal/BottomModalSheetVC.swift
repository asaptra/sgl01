//
//  BottomModalSheetVC.swift
//  sugarlive
//
//  Created by cleanmac on 27/11/21.
//

import UIKit
import SnapKit

protocol BottomModalSheetDelegate {
    func modalSheet(_ modalSheet: BottomModalSheetVC, itemForRowAt indexPath: IndexPath) -> ModalActionModel
    func modalSheet(_ modalSheet: BottomModalSheetVC, didSelectRowAt indexPath: IndexPath)
    func numberOfItems(in modalSheet: BottomModalSheetVC) -> Int
}

class BottomModalSheetVC: UIViewController {
    
    // MARK: - UI Components
    private var containerView: UIView!
    private var titleLabel: UILabel!
    private var closeButton: UIButton!
    private var tableView: UITableView!
    
    // MARK: - Variables
    var tag: Int!
    
    var additionalData: Any?
    
    private var delegate: BottomModalSheetDelegate?

    // MARK: - Overriden Functions
    init(title: String = "Choose Action", tag: Int, delegate: BottomModalSheetDelegate) {
        super.init(nibName: nil, bundle: nil)
        self.tag = tag
        self.delegate = delegate
        setupUI(title: title)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UI Setups
    private func setupUI(title: String) {
        view.backgroundColor = .clear
        
        containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .white
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 10
        if #available(iOS 11.0, *) {
            containerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        view.addSubview(containerView)
        containerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            if #available(iOS 11.0, *) {
                constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            } else {
                constraint.bottom.equalToSuperview()
            }
        }
        
        if #available(iOS 11.0, *) {
            let bottomSafeAreaView = UIView()
            bottomSafeAreaView.translatesAutoresizingMaskIntoConstraints = false
            bottomSafeAreaView.backgroundColor = .white
            view.addSubview(bottomSafeAreaView)
            bottomSafeAreaView.snp.makeConstraints { constraint in
                constraint.leading.equalToSuperview()
                constraint.trailing.equalToSuperview()
                constraint.bottom.equalToSuperview()
                constraint.top.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            }
        }
        
        closeButton = UIButton()
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.setImage(UIImage(named: "ic_close_black"), for: .normal)
        closeButton.addTarget(self, action: #selector(dismissAction), for: .touchUpInside)
        containerView.addSubview(closeButton)
        closeButton.snp.makeConstraints { constraint in
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.height.equalTo(50)
            constraint.width.equalTo(50)
        }
        
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = .defaultBoldFont(size: 14)
        titleLabel.text = title
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { constraint in
            constraint.centerY.equalTo(closeButton.snp.centerY)
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalTo(closeButton.snp.leading).offset(-16)
        }
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(tableView)
        tableView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalTo(closeButton.snp.bottom)
            constraint.bottom.equalToSuperview().offset(-16)
            constraint.height.equalTo((delegate?.numberOfItems(in: self) ?? 0) * 50)
        }
    }
    
    // MARK: - Custom Functions
    
    // MARK: - Actions
    @objc private func dismissAction () {
        dismiss(animated: true)
    }
}

// MARK: - Table View delegate and data source
extension BottomModalSheetVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        delegate?.numberOfItems(in: self) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = delegate?.modalSheet(self, itemForRowAt: indexPath).title
        cell.textLabel?.font = .defaultRegularFont(size: 14)
        cell.textLabel?.textColor = delegate?.modalSheet(self, itemForRowAt: indexPath).type.getActionColor()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: {
            self.delegate?.modalSheet(self, didSelectRowAt: indexPath)
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        50
    }
}
