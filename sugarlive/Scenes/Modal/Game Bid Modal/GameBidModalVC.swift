//
//  GameBidModalVC.swift
//  sugarlive
//
//  Created by cleanmac-ada on 14/05/22.
//

import UIKit

class GameBidModalVC: UIViewController {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var tenSelectionStackView: UIStackView!
    @IBOutlet weak var tenRadioButtonImage: UIImageView!
    
    @IBOutlet weak var hundredSelectionStackView: UIStackView!
    @IBOutlet weak var hundredRadioButtonImage: UIImageView!
    
    @IBOutlet weak var otherSelectionStackView: UIStackView!
    @IBOutlet weak var otherRadioButtonImage: UIImageView!
    @IBOutlet weak var otherInputTextField: UITextField!
    
    // MARK: - Variables
    private var selectedBid: BidAmount! {
        didSet {
            switch selectedBid {
            case .TenThousand:
                tenRadioButtonImage.image = UIImage(named: "ic_radio_button_active")
                hundredRadioButtonImage.image = UIImage(named: "ic_radio_button_inactive")
                otherRadioButtonImage.image = UIImage(named: "ic_radio_button_inactive")
            case .HundredThousand:
                tenRadioButtonImage.image = UIImage(named: "ic_radio_button_inactive")
                hundredRadioButtonImage.image = UIImage(named: "ic_radio_button_active")
                otherRadioButtonImage.image = UIImage(named: "ic_radio_button_inactive")
            default:
                tenRadioButtonImage.image = UIImage(named: "ic_radio_button_inactive")
                hundredRadioButtonImage.image = UIImage(named: "ic_radio_button_inactive")
                otherRadioButtonImage.image = UIImage(named: "ic_radio_button_active")
            }
        }
    }
    private var receiverId: String!

    // MARK: - Overriden Functions
    init(receiverId: String) {
        super.init(nibName: "GameBidModalVC", bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        self.receiverId = receiverId
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        otherInputTextField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        otherInputTextField.delegate = self
        
        tenSelectionStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(radioButtonAction(_:))))
        hundredSelectionStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(radioButtonAction(_:))))
        otherSelectionStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(radioButtonAction(_:))))
        
        selectedBid = .TenThousand
    }
    
    // MARK: - Actions
    @objc private func textFieldDidChanged(_ sender: UITextField) {
        if let text = sender.text {
            selectedBid = .Other(Int(text) ?? 0)
        }
    }
    
    @objc private func radioButtonAction(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        switch view {
        case tenSelectionStackView:
            selectedBid = .TenThousand
            self.view.endEditing(true)
        case hundredSelectionStackView:
            selectedBid = .HundredThousand
            self.view.endEditing(true)
        case otherSelectionStackView:
            selectedBid = .Other(0)
            otherInputTextField.becomeFirstResponder()
        default:
            break
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.submitBid()
        })
    }
    
    // MARK: - API Calls
    private func submitBid() {
        guard let request = BidPlacedRequestModel(JSON: ["receiverId": receiverId ?? "", "amount": selectedBid.value]) else { return }
        provider.request(.BidPlaced(request: request), successModel: SuccessResponseModel.self, success: { response in
            // FIXME: Implement if needed
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
}

// MARK: - Text Field delegate
extension GameBidModalVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedBid = .Other(0)
    }
}
