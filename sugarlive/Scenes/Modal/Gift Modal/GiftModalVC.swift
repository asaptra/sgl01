//
//  GiftModalVC.swift
//  sugarlive
//
//  Created by cleanmac on 06/12/21.
//

import UIKit

let kShowTopupModalAction = "ShowTopupModalAction"

class GiftModalVC: UIViewController {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var topUpImageView: UIImageView!
    @IBOutlet weak var howToTopUpImageView: UIImageView!
    
    @IBOutlet weak var giftReceiversCollectionView: UICollectionView!
    
    @IBOutlet weak var giftCollectionView: UICollectionView!
    
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var countContainerView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countSelectionView: UIView!
    
    @IBOutlet weak var sendGiftButton: UIButton!
    
    // MARK: - Variables
    private var currentUser = AuthManager.shared.currentUser
    private var gifts: [GiftModel] = []
    private var selectedGift: GiftModel?
    private var count: Int = 1 {
        didSet {
            countLabel.text = "\(count)"
        }
    }
    
    private var receivers: [UserModel] = []
    private var selectedReceiver: UserModel?
    private var receiverID: String!

    // MARK: - Overriden Functions
    init(for receiverID: String) {
        super.init(nibName: "GiftModalVC", bundle: nil)
        modalPresentationStyle = .custom
        self.receiverID = receiverID
    }
    
    init(with receivers: [UserModel]) {
        super.init(nibName: "GiftModalVC", bundle: nil)
        modalPresentationStyle = .custom
        self.receivers = receivers
        self.receivers.removeAll(where: { $0.userLiveId == currentUser?.userId })
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getGifts()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        topUpImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(topupAction)))
        howToTopUpImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(routeToWhatsapp)))
        
        sendGiftButton.roundCorners(corners: [.topRight, .bottomRight], radius: 10)
        countContainerView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 10)
        
        countContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCountModal)))
        
        balanceLabel.text = "\(currentUser?.coin ?? 0)".thousandFormatter()
        
        giftCollectionView.delegate = self
        giftCollectionView.dataSource = self
        giftCollectionView.register(UINib(nibName: "GiftCell", bundle: nil), forCellWithReuseIdentifier: "GiftCell")
        
        if !receivers.isEmpty {
            giftReceiversCollectionView.isHidden = false
            giftReceiversCollectionView.delegate = self
            giftReceiversCollectionView.dataSource = self
            giftReceiversCollectionView.register(UINib(nibName: "GiftReceiverCell", bundle: nil), forCellWithReuseIdentifier: "GiftReceiverCell")
            giftReceiversCollectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
            selectedReceiver = receivers.first
        }
    }
    
    // MARK: - Custom Functions
    @objc private func routeToWhatsapp() {
        let urlWA = "https://api.whatsapp.com/send?phone=\(Constant.CustServicePhoneNumber)"
        if let urlString = urlWA.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let url = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(url){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                else {
                    print("can't open \(urlWA)")
                }
            }
        }
    }
    
    // MARK: - Actions
    @objc private func showCountModal() {
        countSelectionView.isHidden = false
    }
    
    @objc private func topupAction() {
        dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(kShowTopupModalAction), object: nil)
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func sendGiftAction(_ sender: Any) {
        sendGift()
    }
    
    @IBAction func countAction(_ sender: UIButton) {
        count = sender.tag
        countSelectionView.isHidden = true
    }
    
    // MARK: - API Calls
    private func getGifts() {
        provider.request(.Gift, successModel: GiftResponseModel.self, success: { [weak self] response in
            guard let self = self else { return }
            self.gifts = response.attributes
            self.giftCollectionView.reloadData()
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    private func sendGift() {
        if let selectedGift = selectedGift {
            let receiverId = receiverID != nil ? receiverID : (selectedReceiver?.userLiveId ?? "")
            guard let request = SendGiftRequestModel(JSON: ["giftID": selectedGift.giftId ?? "", "receiverID": receiverId ?? "", "count": count]) else { return }
            provider.request(.SendGift(request: request), successModel: SuccessResponseModel.self, success: { [weak self] response in
                guard let self = self else { return }
                self.currentUser?.coin -= (selectedGift.price * self.count)
                
                let navController = self.presentingViewController as! UINavigationController
                let presentingVC = navController.viewControllers.last
                if presentingVC is LiveVC {
                    (presentingVC as! LiveVC).sentGiftId = selectedGift.giftId ?? ""
                } else if presentingVC is MultiguestLiveVC {
                    (presentingVC as! MultiguestLiveVC).sentGiftId = selectedGift.giftId ?? ""
                }
                self.dismiss(animated: true)
            }, failure: { error in
                print(error.messages.joined(separator: "\n"))
            })
        }
    }
}

// MARK: - Collection view delegate and data source
extension GiftModalVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == giftCollectionView {
            return gifts.count
        } else {
            return receivers.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == giftCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCell", for: indexPath) as! GiftCell
            cell.setContents(gifts[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftReceiverCell", for: indexPath) as! GiftReceiverCell
            cell.setContents(imageURL: receivers[indexPath.row].coverPicture ?? "", position: receivers[indexPath.row].position)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == giftCollectionView {
            let size = (collectionView.frame.height / 2) - 5
            return CGSize(width: size, height: size)
        } else {
            return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == giftCollectionView {
            selectedGift = gifts[indexPath.row]
        } else {
            selectedReceiver = receivers[indexPath.row]
        }
    }
}
