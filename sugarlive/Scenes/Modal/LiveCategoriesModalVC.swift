//
//  LiveCategoriesModal.swift
//  sugarlive
//
//  Created by cleanmac on 14/11/21.
//

import UIKit
import SnapKit

protocol LiveCategoriesModalVCDelegate {
    func didSelectItem(_ item: LiveCategoryModel)
}

class LiveCategoriesModalVC: UIViewController {
    
    // MARK: - UI Components
    private var containerView: UIView!
    private var titleLbl: UILabel!
    private var closeBtn: UIButton!
    private var collectionView: UICollectionView!
    
    // MARK: - Variables
    var delegate: LiveCategoriesModalVCDelegate?
    
    private let screenSize = UIScreen.main.bounds.size
    private let modalHeight: CGFloat = 180
    
    private var categories: [LiveCategoryModel] = []

    // MARK: - Overriden Functions
    init() {
        super.init(nibName: nil, bundle: nil)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        view.backgroundColor = .clear
        
        containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 10
        if #available(iOS 11.0, *) {
            containerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        view.addSubview(containerView)
        containerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.height.equalTo(self.modalHeight)
            if #available(iOS 11.0, *) {
                constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            } else {
                constraint.bottom.equalToSuperview()
            }
        }
        
        if #available(iOS 11.0, *) {
            let bottomSafeAreaView = UIView()
            bottomSafeAreaView.translatesAutoresizingMaskIntoConstraints = false
            bottomSafeAreaView.backgroundColor = .white
            view.addSubview(bottomSafeAreaView)
            bottomSafeAreaView.snp.makeConstraints { constraint in
                constraint.leading.equalToSuperview()
                constraint.trailing.equalToSuperview()
                constraint.bottom.equalToSuperview()
                constraint.top.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            }
        }
        
        closeBtn = UIButton()
        closeBtn.translatesAutoresizingMaskIntoConstraints = false
        closeBtn.setImage(UIImage(named: "ic_close_black"), for: .normal)
        closeBtn.addTarget(self, action: #selector(dismissModal), for: .touchUpInside)
        containerView.addSubview(closeBtn)
        closeBtn.snp.makeConstraints { constraint in
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.top.equalToSuperview().offset(16)
            constraint.height.equalTo(13)
            constraint.width.equalTo(13)
        }
        
        titleLbl = UILabel()
        titleLbl.translatesAutoresizingMaskIntoConstraints = false
        titleLbl.text = "Choose Category"
        titleLbl.font = UIFont.defaultBoldFont(size: 14)
        containerView.addSubview(titleLbl)
        titleLbl.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalTo(closeBtn.snp.leading).offset(-16)
            constraint.top.equalToSuperview().offset(14)
        }
        
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .vertical
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
//        if #available(iOS 11.0, *) {
//            collectionView.contentInsetAdjustmentBehavior = .automatic
//        }
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "LiveCategoryCollectionCell", bundle: nil), forCellWithReuseIdentifier: "LiveCategoryCollectionCell")
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(collectionView)
        collectionView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.bottom.equalToSuperview().offset(-36)
            constraint.top.equalTo(titleLbl.snp.bottom).offset(16)
        }
        
        getCategories()
    }
    
    // MARK: - Custom Functions
    
    // MARK: - Actions
    @objc private func dismissModal() {
        dismiss(animated: true)
    }
    
    // MARK: - API Calls
    private func getCategories() {
        provider.request(.LiveCategory, successModel: LiveCategoryResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.categories = result.attributes
            self.collectionView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }

}

// MARK: - Collection View data source
extension LiveCategoriesModalVC: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveCategoryCollectionCell", for: indexPath) as! LiveCategoryCollectionCell
        cell.setTitle(title: categories[indexPath.row].liveCategory ?? "", color: UIColor.black.withAlphaComponent(0.1), titleColor: .black)
        return cell
    }
}

// MARK: - Collection View delegates
extension LiveCategoriesModalVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        delegate?.didSelectItem(categories[indexPath.row])
        dismissModal()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size: CGSize = (categories[indexPath.row].liveCategory ?? "").size(withAttributes: nil)
        return .init(width: (size.width + 25), height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
}
