//
//  ModalSheetVC.swift
//  sugarlive
//
//  Created by cleanmac on 21/11/21.
//

import UIKit
import SnapKit

protocol ModalSheetDelegate {
    func modalSheet(_ modalSheet: ModalSheetVC, itemForRowAt indexPath: IndexPath) -> String
    func modalSheet(_ modalSheet: ModalSheetVC, didSelectRowAt indexPath: IndexPath)
    func numberOfItems(in modalSheet: ModalSheetVC) -> Int
}

class ModalSheetVC: UIViewController {
    
    // MARK: - UI Components
    private var containerView: UIView!
    private var titleLabel: UILabel!
    private var closeButton: UIButton!
    private var itemsTableView: UITableView!
    
    // MARK: - Variables
    private var delegate: ModalSheetDelegate?

    // MARK: - Overriden Functions
    init(title: String, delegate: ModalSheetDelegate) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
        setupUI(title: title)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        itemsTableView.flashScrollIndicators()
    }
    
    // MARK: - UI Setups
    private func setupUI(title: String) {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        
        containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 10
        view.addSubview(containerView)
        containerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(21)
            constraint.trailing.equalToSuperview().offset(-21)
            constraint.centerY.equalToSuperview()
        }
        
        closeButton = UIButton()
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.setImage(UIImage(named: "ic_close_black"), for: .normal)
        closeButton.addTarget(self, action: #selector(dismissModal), for: .touchUpInside)
        containerView.addSubview(closeButton)
        closeButton.snp.makeConstraints { constraint in
            constraint.height.equalTo(50)
            constraint.width.equalTo(50)
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
        }
        
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = .defaultRegularFont(size: 14)
        titleLabel.textColor = .black
        titleLabel.text = title
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(18)
            constraint.trailing.equalTo(closeButton.snp.leading).offset(-18)
            constraint.centerY.equalTo(closeButton.snp.centerY)
        }
        
        itemsTableView = UITableView()
        itemsTableView.delegate = self
        itemsTableView.dataSource = self
        itemsTableView.separatorStyle = .none
        itemsTableView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(itemsTableView)
        itemsTableView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalTo(closeButton.snp.bottom)
            constraint.bottom.equalToSuperview().offset(-16)
            constraint.height.equalTo(UIScreen.main.bounds.size.height / 2.5)
        }
    }
    
    // MARK: - Custom Functions
    
    // MARK: - Actions
    @objc private func dismissModal() {
        dismiss(animated: true)
    }

}

// MARK: - Table View delegate and data source
extension ModalSheetVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        delegate?.numberOfItems(in: self) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = delegate?.modalSheet(self, itemForRowAt: indexPath) ?? ""
        cell.textLabel?.font = .defaultRegularFont(size: 14)
        cell.textLabel?.textColor = UIColor(hex: "#414141")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        dismiss(animated: true) {
            self.delegate?.modalSheet(self, didSelectRowAt: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        45
    }
}
