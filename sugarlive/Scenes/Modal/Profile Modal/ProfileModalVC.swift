//
//  ProfileModalVC.swift
//  sugarlive
//
//  Created by cleanmac on 24/11/21.
//

import UIKit
import AlamofireImage
import SnapKit

let kProfileModalProfileAction = "LiveRouteToProfileScreen"
let kProfileModalReportAction = "ProfileModalReportAction"
let kProfileModalChatAction = "ProfileModalChatAction"

class ProfileModalVC: UIViewController {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var topButtonContainerView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var avatarFrameImageView: UIImageView!
    
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var followerCountLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var carrotCountLabel: UILabel!
    
    @IBOutlet weak var badgesStackView: UIStackView!
    
    @IBOutlet weak var levelContainerView: UIView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var ageContainerView: UIView!
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var topFansStackView: UIStackView!
    
    @IBOutlet weak var bottomButtonContainerView: UIView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    
    // MARK: - Variables
    var userModel: UserModel?
    
    private var isFollowing: Bool? {
        didSet {
            isFollowing ?? true ? followButton.setTitle("  Unfollow", for: .normal) : followButton.setTitle("   Follow", for: .normal)
        }
    }

    // MARK: - Overriden Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        topButtonContainerView.clipsToBounds = false
        topButtonContainerView.applyCardView(cornerRadius: 0, shadowOffsetWidth: 0, shadowOffsetHeight: 2, shadowColor: .black, shadowOpacity: 0.1)

        bottomButtonContainerView.clipsToBounds = false
        bottomButtonContainerView.applyCardView(cornerRadius: 0, shadowOffsetWidth: 0, shadowOffsetHeight: -2, shadowColor: .black, shadowOpacity: 0.1)
        
        if let urlString = userModel?.userPictureUrl, let url = URL(string: urlString) {
            userImageView.af.setImage(withURL: url)
        } else {
            userImageView.image = UIImage(named: "ic_default_profile")
        }
        
        if let urlString = userModel?.userPictureUrl, let url = URL(string: urlString) {
            userImageView.af.setImage(withURL: url)
        } else {
            userImageView.image = UIImage(named: "ic_default_profile")
        }
        
        if let avatarFrame = userModel?.avatarFrames?.defaultFrame?.avatarIcon, let avatarFrameUrl = URL(string: avatarFrame) {
            avatarFrameImageView.af.setImage(withURL: avatarFrameUrl)
        } else {
            avatarFrameImageView.isHidden = true
        }
        
        nameLabel.text = userModel?.fullname ?? ""
        idLabel.text = "ID : \(userModel?.sugarId ?? "")"
        locationLabel.text = userModel?.hometown ?? ""
        followerCountLabel.text = "\(Int(userModel?.followerCount ?? 0))"
        followingCountLabel.text = "\(userModel?.followingCount ?? 0)"
        likeCountLabel.text = "\(userModel?.view ?? 0)"
        carrotCountLabel.text = "\(Int(userModel?.totalCarrot ?? 0))"
        levelLabel.text = "Lv.\(userModel?.level?.level ?? 0)"
        let userAge = "\(userModel?.age ?? 0)"
        ageLabel.text = "\((userModel?.gender ?? "") == "Male" ? "♂" : "♀") \(userAge == "0" ? "" : userAge)"
        
        levelContainerView.backgroundColor = UIColor(hex: userModel?.level?.color ?? "#67A4DE")
        
        isFollowing = userModel?.isFollowing
        
        if userModel?.userId == AuthManager.shared.currentUser?.userId {
            bottomButtonContainerView.isHidden = true
            reportButton.isHidden = true
        }
        
        if let badges = userModel?.badges?.active {
            badgesStackView.isHidden = false
            for badge in badges {
                if let urlString = badge.url, let url = URL(string: urlString) {
                    let badgeImage = UIImageView()
                    badgeImage.translatesAutoresizingMaskIntoConstraints = false
                    badgeImage.af.setImage(withURL: url)
                    badgesStackView.addArrangedSubview(badgeImage)
                    badgeImage.snp.makeConstraints { constraint in
                        constraint.height.equalTo(badgesStackView.frame.height)
                        constraint.width.equalTo(badgesStackView.frame.height)
                    }
                } else {
                    continue
                }
            }
        }
        
        getTopSpender()
    }
    
    private func setupTopFans(_ fans: [TopSpenderModel]) {
        topFansStackView.isHidden = false
        if fans.count > 3 {
            for index in 0...2 {
                if let url = URL(string: fans[index].coverPicture ?? "") {
                    addTopFansToList(url, index: index)
                } else {
                    continue
                }
            }
        } else {
            for (index, fan) in fans.enumerated() {
                if let url = URL(string: fan.coverPicture ?? "") {
                    addTopFansToList(url, index: index)
                } else {
                    continue
                }
            }
        }
    }
    
    private func addTopFansToList(_ url: URL, index: Int) {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .clear
        containerView.snp.makeConstraints { constraint in
            constraint.height.equalTo(45)
            constraint.width.equalTo(30)
        }
        
        let frameImage = UIImageView()
        frameImage.translatesAutoresizingMaskIntoConstraints = false
        switch index {
        case 0:
            frameImage.image = UIImage(named: "ic_top_fans_frame_gold")
        case 1:
            frameImage.image = UIImage(named: "ic_top_fans_frame_silver")
        case 2:
            frameImage.image = UIImage(named: "ic_top_fans_frame_bronze")
        default:
            break
        }
        containerView.addSubview(frameImage)
        frameImage.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        let fansImage = UIImageView()
        fansImage.translatesAutoresizingMaskIntoConstraints = false
        fansImage.af.setImage(withURL: url)
        fansImage.layer.cornerRadius = 15
        fansImage.clipsToBounds = true
        containerView.addSubview(fansImage)
        fansImage.snp.makeConstraints { constraint in
            constraint.height.equalTo(25)
            constraint.width.equalTo(25)
            constraint.centerX.equalToSuperview()
            constraint.centerY.equalToSuperview()
        }
        
        topFansStackView.addArrangedSubview(containerView)
    }
    
    // MARK: - Custom Functions
    
    // MARK: - Actions
    @IBAction func reportAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(kProfileModalReportAction), object: self.userModel?.userId)
        })
    }
    
    @IBAction func profileAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(kProfileModalProfileAction), object: self.userModel?.userId)
        })
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func followAction(_ sender: Any) {
        isFollowing ?? true ? doUnfollow() : doFollow()
    }
    
    @IBAction func chatAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(kProfileModalChatAction), object: self.userModel)
        })
    }
    
    // MARK: - API Calls
    private func doFollow() {
        guard let id = userModel?.userId else { return }
        guard let request = UserRequestModel(JSON: ["userID": id]) else { return }
        provider.request(.Follow(request: request), successModel: SuccessResponseModel.self) { [weak self](result) in
            guard let self = self else { return }
            self.isFollowing?.toggle()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func doUnfollow() {
        guard let id = userModel?.userId else { return }
        guard let request = UserRequestModel(JSON: ["userID": id]) else { return }
        provider.request(.Unfollow(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.isFollowing?.toggle()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getTopSpender() {
        guard let userId = userModel?.userId else { return }
        provider.request(.TopSpender(identifier: userId), successModel: TopSpenderResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            if result.total.count > 0 {
                self.setupTopFans(result.total)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
}
