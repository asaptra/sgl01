//
//  SocMedModalVC.swift
//  sugarlive
//
//  Created by cleanmac on 30/10/21.
//

import UIKit
import SnapKit

class SocMedModalVC: UIViewController {
    
    // MARK: - UI Components
    private var containerView: UIView!
    private var titleLbl: UILabel!
    private var closeBtn: UIButton!
    
    private var facebookContainerView: UIView!
    private var facebookIconImg: UIImageView!
    private var facebookTitleLbl: UILabel!
    
    private var twitterContainerView: UIView!
    private var twitterIconImg: UIImageView!
    private var twitterTitleLbl: UILabel!
    
    private var instagramContainerView: UIView!
    private var instagramIconImg: UIImageView!
    private var instagramTitleLbl: UILabel!
    
    private var lineContainerView: UIView!
    private var lineIconImg: UIImageView!
    private var lineTitleLbl: UILabel!
    
    // MARK: - Variables
    private let screenSize = UIScreen.main.bounds.size
    private let modalHeight: CGFloat = 350
    private let itemHeight: CGFloat = 53
    
    private var socialMedias: [SocialMediaModel]!
    private var facebookModel: SocialMediaModel?
    private var twitterModel: SocialMediaModel?
    private var instagramModel: SocialMediaModel?
    private var lineModel: SocialMediaModel?
    
    // MARK: - Overriden Functions
    init(for socialMedias: [SocialMediaModel] ) {
        super.init(nibName: nil, bundle: nil)
        self.socialMedias = socialMedias
        checkSocMedContents()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        view.backgroundColor = .clear
        
        containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = .white
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 10
        if #available(iOS 11.0, *) {
            containerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        view.addSubview(containerView)
        containerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            if #available(iOS 11.0, *) {
                constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            } else {
                constraint.bottom.equalToSuperview()
            }
        }
        
        if #available(iOS 11.0, *) {
            let bottomSafeAreaView = UIView()
            bottomSafeAreaView.translatesAutoresizingMaskIntoConstraints = false
            bottomSafeAreaView.backgroundColor = .white
            view.addSubview(bottomSafeAreaView)
            bottomSafeAreaView.snp.makeConstraints { constraint in
                constraint.leading.equalToSuperview()
                constraint.trailing.equalToSuperview()
                constraint.bottom.equalToSuperview()
                constraint.top.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            }
        }

        closeBtn = UIButton()
        closeBtn.translatesAutoresizingMaskIntoConstraints = false
        closeBtn.setImage(UIImage(named: "ic_close_black"), for: .normal)
        closeBtn.addTarget(self, action: #selector(dismissModal), for: .touchUpInside)
        containerView.addSubview(closeBtn)
        closeBtn.snp.makeConstraints { constraint in
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.height.equalTo(50)
            constraint.width.equalTo(50)
        }
        
        titleLbl = UILabel()
        titleLbl.translatesAutoresizingMaskIntoConstraints = false
        titleLbl.text = "Social Media"
        titleLbl.font = UIFont.defaultBoldFont(size: 14)
        containerView.addSubview(titleLbl)
        titleLbl.snp.makeConstraints { constraint in
            constraint.centerX.equalToSuperview()
            constraint.centerY.equalTo(closeBtn.snp.centerY)
        }
        
        facebookContainerView = UIView()
        facebookContainerView.translatesAutoresizingMaskIntoConstraints = true
        containerView.addSubview(facebookContainerView)
        facebookContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalTo(self.titleLbl.snp.bottom).offset(14)
            constraint.height.equalTo(self.itemHeight)
        }
        
        facebookIconImg = UIImageView(image: UIImage(named: "ic_facebook"))
        facebookIconImg.translatesAutoresizingMaskIntoConstraints = false
        facebookContainerView.addSubview(facebookIconImg)
        facebookIconImg.snp.makeConstraints { constraint in
            constraint.centerY.equalToSuperview()
            constraint.leading.equalToSuperview().offset(14)
            constraint.height.equalTo(26)
            constraint.width.equalTo(26)
        }
        
        facebookTitleLbl = UILabel()
        facebookTitleLbl.translatesAutoresizingMaskIntoConstraints = false
        facebookTitleLbl.text = (facebookModel?.userName ?? "").isEmpty ? "-" : facebookModel?.userName
        facebookTitleLbl.font = .defaultRegularFont(size: 12)
        facebookContainerView.addSubview(facebookTitleLbl)
        facebookTitleLbl.snp.makeConstraints { constraint in
            constraint.centerY.equalToSuperview()
            constraint.leading.equalTo(self.facebookIconImg.snp.trailing).offset(13)
        }
        
        twitterContainerView = UIView()
        twitterContainerView.translatesAutoresizingMaskIntoConstraints = true
        containerView.addSubview(twitterContainerView)
        twitterContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalTo(self.facebookContainerView.snp.bottom)
            constraint.height.equalTo(self.itemHeight)
        }
        
        twitterIconImg = UIImageView(image: UIImage(named: "ic_twitter"))
        twitterIconImg.translatesAutoresizingMaskIntoConstraints = false
        twitterContainerView.addSubview(twitterIconImg)
        twitterIconImg.snp.makeConstraints { constraint in
            constraint.centerY.equalToSuperview()
            constraint.leading.equalToSuperview().offset(14)
            constraint.height.equalTo(26)
            constraint.width.equalTo(26)
        }
        
        twitterTitleLbl = UILabel()
        twitterTitleLbl.translatesAutoresizingMaskIntoConstraints = false
        twitterTitleLbl.text = (twitterModel?.userName ?? "").isEmpty ? "-" : "@\(twitterModel?.userName ?? "")"
        twitterTitleLbl.font = .defaultRegularFont(size: 12)
        twitterContainerView.addSubview(twitterTitleLbl)
        twitterTitleLbl.snp.makeConstraints { constraint in
            constraint.centerY.equalToSuperview()
            constraint.leading.equalTo(self.twitterIconImg.snp.trailing).offset(13)
        }
        
        instagramContainerView = UIView()
        instagramContainerView.translatesAutoresizingMaskIntoConstraints = true
        containerView.addSubview(instagramContainerView)
        instagramContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalTo(self.twitterContainerView.snp.bottom)
            constraint.height.equalTo(self.itemHeight)
        }
        
        instagramIconImg = UIImageView(image: UIImage(named: "ic_instagram"))
        instagramIconImg.translatesAutoresizingMaskIntoConstraints = false
        instagramContainerView.addSubview(instagramIconImg)
        instagramIconImg.snp.makeConstraints { constraint in
            constraint.centerY.equalToSuperview()
            constraint.leading.equalToSuperview().offset(14)
            constraint.height.equalTo(26)
            constraint.width.equalTo(26)
        }
        
        instagramTitleLbl = UILabel()
        instagramTitleLbl.translatesAutoresizingMaskIntoConstraints = false
        instagramTitleLbl.text = (instagramModel?.userName ?? "").isEmpty ? "-" : "@\(instagramModel?.userName ?? "")"
        instagramTitleLbl.font = .defaultRegularFont(size: 12)
        instagramContainerView.addSubview(instagramTitleLbl)
        instagramTitleLbl.snp.makeConstraints { constraint in
            constraint.centerY.equalToSuperview()
            constraint.leading.equalTo(self.instagramIconImg.snp.trailing).offset(13)
        }
        
        lineContainerView = UIView()
        lineContainerView.translatesAutoresizingMaskIntoConstraints = true
        containerView.addSubview(lineContainerView)
        lineContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalTo(self.instagramContainerView.snp.bottom)
            constraint.height.equalTo(self.itemHeight)
            constraint.bottom.equalToSuperview().offset(-16)
        }
        
        lineIconImg = UIImageView(image: UIImage(named: "ic_line"))
        lineIconImg.translatesAutoresizingMaskIntoConstraints = false
        lineContainerView.addSubview(lineIconImg)
        lineIconImg.snp.makeConstraints { constraint in
            constraint.centerY.equalToSuperview()
            constraint.leading.equalToSuperview().offset(14)
            constraint.height.equalTo(26)
            constraint.width.equalTo(26)
        }
        
        lineTitleLbl = UILabel()
        lineTitleLbl.translatesAutoresizingMaskIntoConstraints = false
        lineTitleLbl.text = (lineModel?.userName ?? "").isEmpty ? "-" : "@\(lineModel?.userName ?? "")"
        lineTitleLbl.font = .defaultRegularFont(size: 12)
        lineContainerView.addSubview(lineTitleLbl)
        lineTitleLbl.snp.makeConstraints { constraint in
            constraint.centerY.equalToSuperview()
            constraint.leading.equalTo(self.lineIconImg.snp.trailing).offset(13)
        }
        
    }
    
    // MARK: - Custom Functions
    private func checkSocMedContents() {
        for socialMedia in socialMedias {
            if socialMedia.socialMedia == .Facebook {
                self.facebookModel = socialMedia
            } else if socialMedia.socialMedia == .Twitter {
                self.twitterModel = socialMedia
            } else if socialMedia.socialMedia == .Instagram {
                self.instagramModel = socialMedia
            } else if socialMedia.socialMedia == .Line {
                self.lineModel = socialMedia
            }
        }
    }
    
    // MARK: - Actions
    @objc private func dismissModal() {
        dismiss(animated: true)
    }

}
