//
//  StickerModalVC.swift
//  sugarlive
//
//  Created by cleanmac-ada on 30/04/22.
//

import UIKit

class StickerModalVC: UIViewController {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var onboardingContainerView: UIView!
    @IBOutlet weak var onboardingCloseButton: UIButton!
    
    @IBOutlet weak var stickerTableView: UITableView!
    @IBOutlet weak var keyboardButton: UIButton!
    
    @IBOutlet weak var totalCandyLabel: UILabel!
    
    @IBOutlet weak var sendStackView: UIStackView!
    @IBOutlet weak var priceContainerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var sendContainerView: UIView!
    
    // MARK: - Variables
    private var onboardingSeconds: Int = 10
    private var onboardingTimer: Timer?
    
    private var liveId: String = ""
    private var stickerPackages: [StickerPackageModel] = []
    private var stickerList: [StickerModel] = []
    
    private var selectedPackage: StickerPackageModel?
    private var selectedSticker: StickerModel? {
        didSet {
            priceLabel.text = "\(selectedSticker?.price ?? 0)/use"
        }
    }
    
    // MARK: - Overriden Functions
    init(liveId: String) {
        super.init(nibName: "StickerModalVC", bundle: nil)
        modalPresentationStyle = .custom
        self.liveId = liveId
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getStickers()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        totalCandyLabel.text = "\(AuthManager.shared.currentUser?.coin ?? 0)"
        
        priceContainerView.layer.borderWidth = 1
        priceContainerView.layer.borderColor = UIColor(hex: "#6C63FF").cgColor
        priceContainerView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 10)
        
        sendContainerView.layer.borderWidth = 1
        sendContainerView.layer.borderColor = UIColor(hex: "#6C63FF").cgColor
        sendContainerView.roundCorners(corners: [.topRight, .bottomRight], radius: 10)
        
        sendStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendAction)))
        
        stickerTableView.delegate = self
        stickerTableView.dataSource = self
        stickerTableView.register(UINib(nibName: "StickerListCell", bundle: nil), forCellReuseIdentifier: "StickerListCell")
        
        let relogin = UserDefaults.standard.bool(forKey: "relogin")
        if !relogin {
            UserDefaults.standard.set(true, forKey: "relogin")
            startOnboardingTimer()
        } else {
            onboardingContainerView.isHidden = true
        }
    }
    
    // MARK: - Custom Functions
    @objc private func startOnboardingTimer() {
        onboardingTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            if self.onboardingSeconds <= 1 {
                self.onboardingTimer?.invalidate()
                self.onboardingContainerView.isHidden = true
            } else {
                self.onboardingSeconds -= 1
                self.onboardingCloseButton.setTitle("Got it (\(self.onboardingSeconds))", for: .normal)
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func onboardingCloseAction(_ sender: Any) {
        onboardingTimer?.invalidate()
        onboardingContainerView.isHidden = true
    }
    
    @IBAction func keyboardAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @objc private func sendAction() {
        sendSticker()
    }
    
    // MARK: - API Calls
    private func getStickers() {
        provider.request(.StickerList, successModel: StickerListResponseModel.self, success: { [weak self] response in
            guard let self = self else { return }
            if let packages = response.attributes, let list = response.attributes?.first?.stickers {
                self.stickerPackages = packages
                self.stickerList = list
                self.stickerTableView.reloadData()
                let cell = self.stickerTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! StickerListCell
                cell.listCollectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
            }
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
    
    private func sendSticker() {
        guard let request = SendStickerRequestModel(JSON: ["liveId": liveId, "stickerId": selectedSticker?.stickerId ?? ""]) else { return }
        provider.request(.SendSticker(request: request), successModel: SuccessResponseModel.self, success: { [weak self] response in
            guard let self = self else { return }
            self.dismiss(animated: true)
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }

}

// MARK: - Table view delegate and data source
extension StickerModalVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StickerListCell", for: indexPath) as! StickerListCell
        indexPath.section == 0 ? cell.setContents(packages: stickerPackages) : cell.setContents(stickers: stickerList)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        indexPath.section == 0 ? tableView.frame.height * 0.4 : tableView.frame.height * 0.6
    }
    
}

// MARK: - Sticker list cell delegate
extension StickerModalVC: StickerListCellDelegate {
    func didSelectItem(_ package: StickerPackageModel) {
        selectedPackage = package
        stickerList = package.stickers ?? []
        stickerTableView.reloadData()
    }
    
    func didSelectItem(_ sticker: StickerModel) {
        selectedSticker = sticker
    }
}
