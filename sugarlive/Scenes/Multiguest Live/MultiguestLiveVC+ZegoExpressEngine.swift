//
//  MultiguestLiveVC+ZegoExpressEngine.swift
//  sugarlive
//
//  Created by Marchell Llehcram on 16/02/22.
//

import Foundation
import ZegoExpressEngine

extension MultiguestLiveVC: ZegoEventHandler {

    // MARK: - Zego Engine Configurations
    func createZegoEngine() {
        ZegoExpressEngine.createEngine(withAppID: Constant.ZegoAppID, appSign: Constant.ZegoAppSign, isTestEnv: true, scenario: ZegoScenario.general, eventHandler: self)
        
        if let filterModel = filterModel {
            ZegoExpressEngine.shared().startEffectsEnv()
            ZegoExpressEngine.shared().enableEffectsBeauty(true)
            
            let beautyParams = ZegoEffectsBeautyParam()
            beautyParams.smoothIntensity = Int32(filterModel.polishIntensity)
            beautyParams.sharpenIntensity = Int32(filterModel.sharpenIntensity)
            beautyParams.whitenIntensity =  Int32(filterModel.whitenIntensity)
            ZegoExpressEngine.shared().setEffectsBeautyParam(beautyParams)
        }
    }

    func destroyZegoEngine() {
        ZegoExpressEngine.destroy(nil)
    }

    func liveLogIn() {
        let user = ZegoUser(userID: currentUser?.userId ?? "")
        let config = ZegoRoomConfig()
        config.isUserStatusNotify = true
        ZegoExpressEngine.shared().loginRoom(userModel.userId ?? "", user: user, config: config)
    }

    func liveLogOut() {
        ZegoExpressEngine.shared().logoutRoom(userModel.userId ?? "")
    }
    
    func stopPlayingStream() {
        ZegoExpressEngine.shared().stopPlayingStream(userModel.userId ?? "")
    }
    
    func startPlayingGuestStream(id: String, position: Int) {
        let guestModel = guestModels.first(where: { $0.userLiveId == id })
        getLiveContainerView(position).guestModel = guestModel
        
        let canvas = ZegoCanvas(view: getLiveContainerView(position).liveCanvasView)
        canvas.viewMode = .aspectFill
        let playerConfig = ZegoPlayerConfig()
        playerConfig.resourceMode = .default
        ZegoExpressEngine.shared().startPlayingStream(id, canvas: canvas, config: playerConfig)
        
        if let audioState = guestModel?.turnOffAudio {
            getLiveContainerView(position).audioState = audioState
        }

        if let videoState = guestModel?.turnOffVideo {
            getLiveContainerView(position).videoState = videoState
        }
    }
    
    func stopPlayingGuestStream(id: String, position: Int) {
        ZegoExpressEngine.shared().stopPlayingStream(id)
        getLiveContainerView(position).guestModel = nil
        guestModels.removeAll(where: { $0.userLiveId == id})
    }

    func startPublishingStream() {
        ZegoExpressEngine.shared().startPublishingStream(currentUser?.userId ?? "")
        if multihostType == .GroupCall {
            ZegoExpressEngine.shared().mutePublishStreamVideo(true)
            getLiveContainerView(0).videoState = .Mute
        }
    }

    func stopPublishingStream() {
        ZegoExpressEngine.shared().stopPublishingStream()
    }

    func startZegoPreviewLayer(position: Int) {
        getLiveContainerView(position).guestModel = currentUser
        getLiveContainerView(position).videoState = .Unmute
        getLiveContainerView(position).streamingType = .Publishing
        let canvas = ZegoCanvas(view: getLiveContainerView(position).liveCanvasView)
        canvas.viewMode = .aspectFill
        ZegoExpressEngine.shared().startPreview(canvas)
    }

    func stopZegoPreviewLayer(position: Int) {
        getLiveContainerView(position).guestModel = nil
        getLiveContainerView(position).videoState = .Mute
        getLiveContainerView(position).streamingType = .Streaming
        ZegoExpressEngine.shared().stopPreview()
    }
    
    func startPlayingStreamsForViewer() {
        for userModel in guestModels {
            returnOtherProfile(userId: userModel.userLiveId ?? "", completion: { [weak self] model in
                guard let self = self else { return }
                userModel.userPictureUrl = model?.userPictureUrl ?? ""
                self.startPlayingGuestStream(id: userModel.userLiveId ?? "", position: userModel.position)
            })
        }
    }
    
    func startPublishingInvitedStream(position: Int) {
        currentUser?.position = position
        
        getLiveContainerView(position).streamingType = .Publishing
        getLiveContainerView(position).guestModel = currentUser
        turnAudioVideo(audio: .Unmute, video: .Mute)
        toggleStreamVideo(position: position, state: .Mute)
        toggleStreamAudio(position: position, state: .Unmute)
        ZegoExpressEngine.shared().startPublishingStream(currentUser?.userId ?? "")
    }
    
    func stopPublishingInvitedStream(position: Int) {
        currentUser?.position = 0
        
        getLiveContainerView(position).streamingType = nil
        getLiveContainerView(position).guestModel = nil
        ZegoExpressEngine.shared().stopPreview()
        ZegoExpressEngine.shared().stopPublishingStream()
    }

    func toggleStreamVideo(position: Int, state: AudioVideoState) {
        getLiveContainerView(position).videoState = state
        
        if getLiveContainerView(position).streamingType == .Publishing {
            if state == .Unmute {
                let canvas = ZegoCanvas(view: getLiveContainerView(position).liveCanvasView)
                canvas.viewMode = .aspectFill
                ZegoExpressEngine.shared().startPreview(canvas)
                ZegoExpressEngine.shared().mutePublishStreamVideo(false)
            } else {
                ZegoExpressEngine.shared().stopPreview()
                ZegoExpressEngine.shared().mutePublishStreamVideo(true)
            }
        } else if getLiveContainerView(position).streamingType == .Streaming {
            ZegoExpressEngine.shared().mutePlayStreamVideo(state == .Mute, streamID: getLiveContainerView(position).guestModel?.userLiveId ?? "")
        }
    }
    
    func toggleStreamAudio(position: Int, state: AudioVideoState) {
        getLiveContainerView(position).audioState = state
        
        if getLiveContainerView(position).streamingType == .Publishing {
            ZegoExpressEngine.shared().mutePublishStreamAudio(state == .Mute)
        } else if getLiveContainerView(position).streamingType == .Streaming {
            ZegoExpressEngine.shared().mutePlayStreamAudio(state == .Mute, streamID: getLiveContainerView(position).guestModel?.userLiveId ?? "")
        }
    }
    
    func toggleGuestStreamState(userId: String, audio: AudioVideoState, video: AudioVideoState) {
        if let data = MuteRoomRequestModel(multihostType: multihostType, position: 1, roomId: userModel.userId ?? "", userId: userId, audio: audio, video: video).toJSON() {
            ZegoExpressEngine.shared().sendBroadcastMessage(data, roomID: userModel.userId ?? "", callback: nil)
        }
    }
    
    @objc func switchCamera() {
        isFrontCamera.toggle()
        ZegoExpressEngine.shared().useFrontCamera(isFrontCamera)
    }

    // MARK: - Zego Event Handler Protocol
    func onRoomUserUpdate(_ updateType: ZegoUpdateType, userList: [ZegoUser], roomID: String) {
        zegoUsers = userList
    }

    func onIMRecvBroadcastMessage(_ messageList: [ZegoBroadcastMessageInfo], roomID: String) {
        let count = messageList.count
        let message = messageList[count-1].message
        
        if message.contains("\"event\":\"\(PusherEvents.CHAT)\"") {
            if let chatModel = LiveChatResponseModel(JSONString: message) {
                appendChatModel(chatModel)
            }
        } else if message.contains("\"event\":\"\(PusherEvents.STICKER)\"") {
            if let stickerModel = LiveStickerResponseModel(JSONString: message) {
                appendChatModel(stickerModel)
            }
        } else if message.contains("\"event\":\"\(PusherEvents.TURN_OFF_AUDIO_VIDEO)\"") {
            if let audioVideoModel = AudioVideoEventResponseModel(JSONString: message) {
                if let position = guestModels.first(where: { $0.userLiveId == audioVideoModel.userId })?.position {
                    toggleStreamVideo(position: position, state: audioVideoModel.turnOffVideo ?? .Mute)
                    toggleStreamAudio(position: position, state: audioVideoModel.turnOffAudio ?? .Mute)
                }
            }
        } else if message.contains("\"event\":\"\(PusherEvents.MUTE_ROOM)\"") {
            if let muteRoomModel = MuteRoomResponseModel(JSONString: message) {
                if muteRoomModel.userId == currentUser?.userId {
                    turnAudioVideo(audio: muteRoomModel.audioState ?? .Unmute, video: muteRoomModel.videoState ?? .Unmute)
                }
            }
        } else if message.contains("\"event\":\"\(PusherEvents.PRIVATE_ROOM)\"") {
            if type == .Viewer {
                leaveLive()
                NotificationCenter.default.post(name: NSNotification.Name(kLiveViewerValidateRoomRequirement), object: userModel)
            }
        } else if message.contains("\"event\":\"\(PusherEvents.BID_RESPONDED)\"") {
            if let bidRespondedModel = BidRespondedEventModel(JSONString: message) {
                if type == .Viewer && bidRespondedModel.senderId == currentUser?.userId {
                    showBidModal(for: bidRespondedModel.receiverId ?? "")
                }
            }
        } else if message.contains("\"event\":\"\(PusherEvents.BID_PLACED)\"") {
            if let bidPlacedModel = BidPlacedEventModel(JSONString: message) {
                if type == .Host && bidPlacedModel.receiverId == currentUser?.userId {
                    showBidPlacedModal(model: bidPlacedModel)
                }
            }
        } else if message.contains("\"event\":\"\(PusherEvents.BID_ACCEPTED)\"") {
            if let bidAcceptedModel = BidAcceptedEventModel(JSONString: message) {
                self.bidAcceptedModel = bidAcceptedModel
                if (bidAcceptedModel.senderId == currentUser?.userId && type == .Viewer) || (bidAcceptedModel.receiverId == currentUser?.userId && type == .Host) {
                    suitAnswerView.isHidden = false
                }
            }
        } else if message.contains("\"event\":\"\(PusherEvents.GAME_SUIT_RESULT)\"") {
            if let bidResultModel = BidResultEventModel(JSONString: message) {
                self.bidResultModel = bidResultModel
                if bidResultModel.receiver?.userId == currentUser?.userId {
                    showSuitAnimationResult(model: bidResultModel.receiver!)
                    let _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { _ in
                        self.closeGame()
                    })
                    currentUser?.totalCarrot = Double(bidResultModel.receiver?.totalAmount ?? 0)
                } else {
                    showSuitAnimationResult(model: bidResultModel.sender!)
                    let _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { _ in
                        self.bidResultModel = nil
                        self.suitResultAnimationImageView.image = nil
                    })
                }
                let totalAmount = Double(bidResultModel.receiver?.totalAmount ?? 0)
                categoryView.setCarrotAmount(totalAmount)
                returnOtherProfile(userId: currentUser?.userId ?? "", completion: { [weak currentUser] user in
                    currentUser?.coin = user?.coin ?? 0
                })
            }
        } else if message.contains("\"event\":\"\(PusherEvents.GIFT)\"") {
            if let giftModel = GiftDetailEventModel(JSONString: message) {
                if giftModel.gift?.category != .Combo {
                    appendChatModel(giftModel)
                    giftModels.append(giftModel)
                    if !isGiftQueueActive {
                        isGiftQueueActive = true
                        showGiftAnimationQueue()
                    }
                    
                    if giftModel.gift?.category == .GlobalAnimation {
                        flyingBannerModels.append(giftModel)
                        if !isflyingBannerQueueActive {
                            isflyingBannerQueueActive = true
                            showFlyingBannerQueue()
                        }
                    }
                } else {
                    if (comboGiftModels.firstIndex(where: { $0.detailModel == giftModel }) != nil) {
                        increaseComboMultiplier(giftModel)
                    } else {
                        appendComboGiftModel(giftModel)
                        if giftModel.sender?.userId == currentUser?.userId {
                            showComboButton(giftModel)
                        }
                    }
                }
                
                if giftModel.receiver?.userId == userModel.userId {
                    setCarrotAmount(giftModel.carrot ?? 0)
                }
            }
        } else if message.contains("\"event\":\"\(PusherEvents.JACKPOT)\"") {
            if let jackpotModel = JackpotEventModel(JSONString: message) {
                flyingBannerModels.append(jackpotModel)
                if !isflyingBannerQueueActive {
                    isflyingBannerQueueActive = true
                    showFlyingBannerQueue()
                }
            }
        } else {
            print("MESSAGE: \(message)")
        }
    }

    func onRemoteCameraStateUpdate(_ state: ZegoRemoteDeviceState, streamID: String) {
        if let guestModel = guestModels.first(where: { $0.userLiveId == streamID }) {
            getLiveContainerView(guestModel.position).videoState = (state == .open) ? .Unmute : .Mute
        }
    }
}
