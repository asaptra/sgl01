//
//  MultihostRequestVC.swift
//  sugarlive
//
//  Created by cleanmac-ada on 09/03/22.
//

import UIKit

class MultihostRequestVC: UIViewController {

    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var closeButton: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var joinButton: UIButton!
    
    // MARK: - Variables
    private unowned var userModel: UserModel!
    private var type: LiveType!
    
    private var requests: [UserMultihostJoinModel] = []
    private var isRequestingToJoin: Bool = false {
        didSet {
            setButtonState()
        }
    }
    
    // MARK: - Overriden Functions
    init(userModel: UserModel, type: LiveType) {
        super.init(nibName: "MultihostRequestVC", bundle: nil)
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
        self.userModel = userModel
        self.type = type
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        joinButton.titleLabel?.font = .defaultSemiBoldFont(size: 16)
        joinButton.isHidden = type == .Host
        
        isRequestingToJoin = false
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "MultihostRequestCell", bundle: nil), forCellReuseIdentifier: "MultihostRequestCell")
        
        getRequests()
    }
    
    // MARK: - Custom Functions
    private func setButtonState() {
        if isRequestingToJoin {
            joinButton.backgroundColor = .red
            joinButton.setAttributedTitle("Cancel Request".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        } else {
            joinButton.backgroundColor = UIColor(hex: "#4A097A")
            joinButton.setAttributedTitle("Ask to Join".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        }
    }
    
    // MARK: - Actions
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func joinAction(_ sender: Any) {
        if isRequestingToJoin {
            cancelRequest()
        } else {
            askToJoin()
        }
    }
    
    // MARK: - API Calls
    private func getRequests() {
        if type == .Host {
            provider.request(.MultihostJoinRequests, successModel: MultihostJoinRequestResponseModel.self, withProgressHud: false) { [weak self] (result) in
                guard let self = self else { return }
                self.requests.removeAll()
                self.requests = result.request ?? []
                
                for model in self.requests {
                    if model.userId == AuthManager.shared.currentUser?.userId {
                        self.isRequestingToJoin = true
                    }
                }
                
                self.tableView.reloadData()
            } failure: { (error) in
                print(error.messages.joined(separator: "\n"))
            }
        } else {
            provider.request(.MultihostJoinRequestsViewer(identifier: userModel?.userId ?? ""), successModel: MultihostJoinRequestResponseModel.self, withProgressHud: false) { [weak self] (result) in
                guard let self = self else { return }
                self.requests.removeAll()
                self.requests = result.request ?? []
                
                for model in self.requests {
                    if model.userId == AuthManager.shared.currentUser?.userId {
                        self.isRequestingToJoin = true
                    }
                }
                
                self.tableView.reloadData()
            } failure: { (error) in
                print(error.messages.joined(separator: "\n"))
            }
        }
    }
    
    private func askToJoin() {
        guard let request = AskJoinMultihostRequestModel(JSON: ["hostUserId": userModel.userId ?? ""]) else { return }
        provider.request(.AskJoinMultihost(request: request), successModel: SuccessResponseModel.self, withProgressHud: false) { [weak self] (result) in
            guard let self = self else { return }
            self.getRequests()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func approveRequest(model: UserMultihostJoinModel) {
        guard let request = ApproveCancelJoinMultiHostRequestModel(JSON: ["requestId": model.requestId ?? ""]) else { return }
        provider.request(.ApproveJoinMultiHost(request: request), successModel: SuccessResponseModel.self, withProgressHud: false) { [weak self] (result) in
            guard let self = self else { return }
            self.dismiss(animated: true)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func cancelRequest() {
        guard let model = requests.first(where: { $0.userId == AuthManager.shared.currentUser?.userId }), let request = ApproveCancelJoinMultiHostRequestModel(JSON: ["requestId": model.requestId ?? ""]) else { return }
        provider.request(.CancelJoinMultiHost(request: request), successModel: SuccessResponseModel.self, withProgressHud: false) { [weak self] (result) in
            guard let self = self else { return }
            self.getRequests()
            self.isRequestingToJoin = false
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }

}

// MARK: - Table View delegate and data source
extension MultihostRequestVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        requests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MultihostRequestCell", for: indexPath) as! MultihostRequestCell
        cell.setupContents(order: (indexPath.row + 1), model: requests[indexPath.row], isHost: type == .Host)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if type == .Host {
            approveRequest(model: requests[indexPath.row])
        }
    }
}
