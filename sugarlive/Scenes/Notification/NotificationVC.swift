//
//  NotificationVC.swift
//  sugarlive
//
//  Created by msi on 20/09/21.
//

import UIKit
import SVPullToRefresh

class NotificationVC: BaseTableVC {
    
    var pageModel = PagingRequestModel()
    var page = 1
    
    private var notificationResponseModel: NotificationResponseModel?
    var notifications : [NotificationModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupNav() {
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ic_back_white")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: "Notifications"))
        self.navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.barTintColor = .PRIMARY_1
        navigationController?.navigationBar.tintColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNav()
        getNotification(page:page)
    }
    
    override func setupLayout() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        cells = ["ProfileCell"]
        
        tableView.addPullToRefresh {
            self.refreshTable(self.tableView ?? UITableView())
            self.tableView.pullToRefreshView.stopAnimating()
        }
        tableView.addInfiniteScrolling {
            self.getNotification(page: self.page)
            self.tableView.infiniteScrollingView.stopAnimating()
        }
    }
    
    @IBAction func refreshTable(_ sender: Any) {
        page = 1
        getNotification(page: page)
    }
    
    //  MARK: Service
    private func getNotification(page: Int) {
        pageModel.page = page
        pageModel.perPage = 15
        provider.request(.Notifications(request: pageModel), successModel: NotificationResponseModel.self, withProgressHud: page == 1) { (result) in
            if page == 1 {
                self.notifications.removeAll()
            }
            self.notificationResponseModel = result
            self.notificationResponseModel?.attributes?.forEach{self.notifications.append($0)}
            self.page += 1
            
            if result.attributes?.count == 0 {
                self.tableView.tableFooterView = nil
            } else {
                self.tableView.reloadData()
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
            self.tableView.tableFooterView?.isHidden = true
        }
    }
    
    private func setReadNotification(indexPath: IndexPath) {
        let notification = notifications[indexPath.row]
        provider.request(.ReadNotification(identifier: notification.id ?? ""), successModel: SuccessResponseModel.self) { (reult) in
            var notif = notification
            notif.read = true
            self.notifications[indexPath.row] = notif
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getFeed(postId: String, handler: @escaping ((FeedModel) -> Void)) {
        provider.request(.GetCurrentFeed(identifier: postId), successModel: FeedsResponseModel.self) { (result) in
            if let feed = result.feed {
                handler(feed)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
            self.routeToWarningModal(message: "Feed not found or has been deleted")
        }
    }
    
    // MARK: Route
    private func routeToFeedDetail(selectedFeed: FeedModel) {
        let destinationVC = FeedVC(nibName: "FeedVC", bundle: nil)
        destinationVC.feedType = .FeedDetail
        destinationVC.feed = selectedFeed
        navigationController?.pushViewController(destinationVC, animated: true)
    }
}

extension NotificationVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        cell.profileCellType = .Notification
        if self.notifications.count > 0 {
            let notification = self.notifications[indexPath.row]
            cell.bind(notification: notification)
        }
        
        return cell
    }
}

extension NotificationVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        setReadNotification(indexPath: indexPath)
        if let notification = notificationResponseModel?.attributes?[indexPath.row] {
            if notification.type == "personal" {
                if let postId = notification.additionalData?.postId {
                    getFeed(postId: postId) { feed in
                        self.routeToFeedDetail(selectedFeed: feed)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))

            tableView.tableFooterView = spinner
        }
    }
}
