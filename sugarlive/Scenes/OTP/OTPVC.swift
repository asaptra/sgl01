//
//  OTPVC.swift
//  sugarlive
//
//  Created by msi on 04/10/21.
//

import UIKit

class OTPVC: BaseVC {
    enum OTPType {
        case Register
        case Forgot
        case ChangePhone
    }
  
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputField: BaseTextField!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!

    var otpType: OTPType = .Register
    private var timer: Timer?
    private var registerModel = AuthManager.shared.registerModel
    private var changePhoneModel = AuthManager.shared.otpRequestModel
    private var counter = 30
  
    override func viewDidLoad() {
        super.viewDidLoad()
        getResendOTP()
        startTimer()
    }
  
    override func setupNav() {
        var str = ""
        switch otpType {
        case .Forgot:
            str = "Forgot password"
            break
        case .ChangePhone:
            str = "Change phone number"
            break
        case .Register:
            str = "Register"
            break
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: str))
        self.navigationItem.leftItemsSupplementBackButton = true
    }
  
  override func setupLayout() {
    messageLabel.font = .defaultRegularFont(size: 12)
    messageLabel.textColor = .init(hex: "2C004D")
    phoneNumberLabel.font = .defaultBoldFont(size: 20)
    phoneNumberLabel.textColor = .init(hex: "444444")
    inputContainerView.backgroundColor = .init(hex: "F2F2F2")
    inputContainerView.layer.cornerRadius = 4
    inputContainerView.layer.masksToBounds = true
    inputField.font = .defaultRegularFont(size: 12)
    inputField.keyboardInputType = .Numeric
    inputField.minLength = 4
    inputField.maxLength = 4
    inputField.textFieldHandler = {(sender) in
      switch sender {
      case .End:
        self.doneButton.isEnabled = false
        break
      case .Valid:
        self.doneButton.isEnabled = true
        break
      default:
        break
      }
    }
    counterLabel.font = .defaultRegularFont(size: 12)
    counterLabel.textColor = .init(hex: "929292")
    resendButton.backgroundColor = .clear
    resendButton.setAttributedTitle("Resend Verification Code".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14),
                                                                                         NSAttributedString.Key.foregroundColor: UIColor.init(hex: "2C004D")]),
                                    for: .normal)
    resendButton.setAttributedTitle("Resend Verification Code".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14),
                                                                                         NSAttributedString.Key.foregroundColor: UIColor.init(hex: "2C004D").withAlphaComponent(0.3)]),
                                    for: .disabled)
    resendButton.isEnabled = false
    doneButton.setBackgroundImage(UIImage(color: UIColor.init(hex: "2C004D").withAlphaComponent(0.3)), for: .disabled)
    doneButton.setBackgroundImage(UIImage(color: UIColor.init(hex: "2C004D")), for: .normal)
    doneButton.isEnabled = false
    doneButton.setAttributedTitle("Done".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14),
                                                                   NSAttributedString.Key.foregroundColor: UIColor.white]),
                                  for: .normal)
    doneButton.layer.cornerRadius = 4
    doneButton.layer.masksToBounds = true
    
    messageLabel.text = "Verification code has been sent to your number"
    phoneNumberLabel.text = registerModel?.request?.phone
    inputField.placeholder = "Insert verification code here"
    counterLabel.text = "Please enter the number we sent you via SMS within \(counter)s"
  }
  
//  MARK: Service
  private func getResendOTP() {
    if let request = registerModel?.request {
      let response = registerModel?.response
      var identifier = otpType == .Forgot ? request.phone : response?.id
        if request.phone == nil && otpType == .ChangePhone {
            identifier = changePhoneModel?.phone
        }
      provider.request(.ResendOTP(identifier: identifier ?? ""), successModel: SuccessResponseModel.self) { (result) in
        self.startTimer()
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
        self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
      }
    }
  }
  
  private func postVerifyOTP() {
    if !validateInput() {
      routeToWarningModal(message: "OTP is too short (4 characters)")
      return
    }
    
    switch otpType {
    case .Register:
      if let response = registerModel?.response, let id = response.id {
        if let request = OTPRequestModel(JSON: ["code": inputField.text ?? "", "userId": id] as [String: Any]) {
          provider.request(.VerifyOTP(request: request), successModel: RegisterResponseModel.self) { (result) in
            self.registerModel?.response = result
            self.registerModel?.response?.attributes?.cacheToken()
            self.getMyProfile {}
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
          } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
            self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
          }
        }
      }
      break
    case .ChangePhone:
      let authManager = AuthManager.shared
      if let request = ChangePhoneRequestModel(JSON: ["phone": authManager.otpRequestModel?.phone ?? "", "code": inputField.text ?? ""]) {
        provider.request(.ChangePhone(request: request), successModel: SuccessResponseModel.self) { (result) in
          self.getMyProfile {}
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
          self.routeToInformationModal(type: .Success, message: "Phone number successfully changed") {
            self.navigationController?.popToRootViewController(animated: true)
          }
        } failure: { (error) in
          self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
        }
      }
      break
    case .Forgot:
      let request = OTPRequestModel(JSON: ["code": inputField.text ?? "", "userId": ""] as [String: Any])
      AuthManager.shared.otpRequestModel = request
      self.routeToTableActionVC(type: .ForgotPassword)
//      self.routeToRegister(registerType: .NewPassword)
      break
    }
  }
  
//  MARK: Action
  @IBAction func resendAction(_ sender: Any) {
    getResendOTP()
  }
  
  @IBAction func doneAction(_ sender: Any) {
    postVerifyOTP()
  }
  
//  MARK: Custom
  private func validateInput() -> Bool {
    var valid = true
    if inputField.text?.count != 4 {
      valid = false
    }
    return valid
  }
  
  private func startTimer() {
    timer?.invalidate()
    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerDidRun), userInfo: nil, repeats: true)
    resendButton.isEnabled = false
  }
  
  @objc private func timerDidRun() {
    counter -= 1
    counterLabel.text = "Please enter the number we sent you via SMS within \(counter)s"
    
    if counter == 0 {
      counter = 30
      timer?.invalidate()
      resendButton.isEnabled = true
    }
  }
}
