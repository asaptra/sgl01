//
//  PopProfileSheetVC.swift
//  sugarlive
//
//  Created by msi on 17/09/21.
//

import UIKit

class PopProfileSheetVC: BaseVC {
  
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var infoButton: UIButton!
  @IBOutlet weak var profileButton: UIButton!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var idLabel: UILabel!
  @IBOutlet weak var locationLabel: UILabel!
  @IBOutlet weak var levelButton: UIButton!
  @IBOutlet weak var genderButton: UIButton!
  @IBOutlet weak var badgeImageView: UIImageView!
  @IBOutlet weak var topFansLabel: UILabel!
  @IBOutlet weak var topFansStack: UIStackView!
  @IBOutlet weak var followerButton: UIButton!
  @IBOutlet weak var followingButton: UIButton!
  @IBOutlet weak var likeButton: UIButton!
  @IBOutlet weak var carrotButton: UIButton!
  @IBOutlet weak var followButton: UIButton!
  @IBOutlet weak var chatButton: UIButton!
  
  var nameStr = "Sugarlive"
  var idStr = "2928291829"
  var locationStr = "Jakarta Selatan, Indonesia"
  var levelStr = "15"
  var ageStr = "22"
  var viewerStr = "776"
  var fansStr = "252"
  var likeStr = "19921"
  var carrotStr = "83000"
  
  var infoTapped: (_ sender: Any) -> Void = {_ in }
  var profileTapped: (_ sender: Any) -> Void = {_ in }
  var followTapped: (_ sender: Any) -> Void = {_ in }
  var chatTapped: (_ sender: Any) -> Void = {_ in }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupLayout()
  }
  
  override func setupLayout() {
    containerView.layer.cornerRadius = 20
    containerView.layer.masksToBounds = true
    
    nameLabel.font = .defaultSemiBoldFont(size: 20)
    nameLabel.text = nameStr
    idLabel.font = .defaultRegularFont(size: 11)
    idLabel.textColor = .LIGHT_TEXT
    idLabel.text = "ID: \(idStr)"
    locationLabel.font = .defaultRegularFont(size: 11)
    locationLabel.textColor = .LIGHT_TEXT
    locationLabel.text = locationStr
    topFansLabel.font = .defaultRegularFont(size: 11)
    topFansLabel.textColor = .LIGHT_TEXT
    topFansLabel.text = "Top fans"
    
    levelButton.backgroundColor = .init(hex: "67A4DE")
    levelButton.setAttributedTitle("Lv. \(levelStr)".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 11),
                                                                      NSAttributedString.Key.foregroundColor: UIColor.white]),
                                   for: .normal)
    levelButton.layer.cornerRadius = levelButton.frame.height/2
    levelButton.layer.masksToBounds = true
    
    genderButton.backgroundColor = .init(hex: "FF7E39")
    genderButton.setAttributedTitle("  \(ageStr)".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 11),
                                                                   NSAttributedString.Key.foregroundColor: UIColor.white]),
                                    for: .normal)
    genderButton.layer.cornerRadius = genderButton.frame.height/2
    genderButton.layer.masksToBounds = true
    
    followerButton.setAttributedTitle("\(  viewerStr)".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 13),
                                                                    NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                    for: .normal)
    followingButton.setAttributedTitle("\(  fansStr)".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 13),
                                                                     NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                      for: .normal)
    likeButton.setAttributedTitle("\(  likeStr)".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 13),
                                                                NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                      for: .normal)
    carrotButton.setAttributedTitle("\(  carrotStr)".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 13),
                                                                  NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                      for: .normal)
    
    followButton.setAttributedTitle("  Follow".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 15),
                                                                  NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                    for: .normal)
    chatButton.setAttributedTitle("  Chat".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 15),
                                                                NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                    for: .normal)
  }
  
//  MARK: Action
  @IBAction func dismissAction(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func infoAction(_ sender: Any) {
    infoTapped(sender)
  }
  
  @IBAction func profileAction(_ sender: Any) {
    profileTapped(sender)
  }
  
  @IBAction func followAction(_ sender: Any) {
    followTapped(sender)
  }
  
  @IBAction override func chatAction(_ sender: Any) {
    chatTapped(sender)
  }
}
