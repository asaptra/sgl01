//
//  PopSheetVC.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class PopSheetVC: BaseVC {
  
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var closeButton: UIButton!
  @IBOutlet weak var actionButton: UIButton!
  @IBOutlet weak var mainStack: UIStackView!
  
  @IBOutlet weak var productContainerView: UIView!
  @IBOutlet weak var productPreviewImage: UIImageView!
  @IBOutlet weak var productTitleLabel: UILabel!
  @IBOutlet weak var productCostLabel: UILabel!
  
  @IBOutlet weak var descContainerView: UIView!
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var descLabel: UILabel!
  
  @IBOutlet weak var titleContainerView: UIView!
  @IBOutlet weak var actionContainerView: UIView!
  
  var titleStr = ""
  var actionStr = ""
  var actionSelectedStr = ""
  var additionalViews = [UIView]()
  
  var productImage = ""
  var productStr = ""
  var costStr = ""
  var productType = ""
  var isInsufficient = false
  
  var statusStr = ""
  var descStr = ""
  
  var showTitle = true
  
  var actionTapped: (_ sender: Any) -> Void = {_ in }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupLayout()
  }
  
  override func setupLayout() {
    containerView.layer.cornerRadius = 20
    containerView.layer.masksToBounds = true
    
    titleLabel.font = .defaultBoldFont(size: 14)
    titleLabel.text = titleStr
    
    actionButton.layer.cornerRadius = 8
    actionButton.layer.masksToBounds = true
    actionButton.setBackgroundImage(UIImage(color: .PRIMARY_1), for: .normal)
    actionButton.setAttributedTitle(actionStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 18),
                                                                        NSAttributedString.Key.foregroundColor: UIColor.white]),
                                    for: .normal)
    actionButton.setBackgroundImage(UIImage(color: .init(hex: "0FA85C")), for: .selected)
    actionButton.setAttributedTitle(actionSelectedStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 18),
                                                                                NSAttributedString.Key.foregroundColor: UIColor.white]),
                                    for: .selected)
    
    productTitleLabel.font = .defaultBoldFont(size: 14)
    productTitleLabel.text = productStr
    
    if let url = URL(string: productImage) {
      productPreviewImage.af.setImage(withURL: url)
    }
    if productType == "entrance_effect" {
      productPreviewImage.snp.makeConstraints { (maker) in
        maker.width.equalTo((129/43)*57)
      }
    } else {
      productPreviewImage.snp.makeConstraints { (maker) in
        maker.width.equalTo(57)
      }
    }
    
    productCostLabel.font = .defaultRegularFont(size: 14)
    productCostLabel.text = costStr
    
    statusLabel.font = .defaultBoldFont(size: 14)
    if isInsufficient {
      statusLabel.textColor = .init(hex: "FF0000")
      actionButton.isSelected = true
    } else {
      statusLabel.textColor = .init(hex: "4D4D4D")
      actionButton.isSelected = false
    }
    statusLabel.text = statusStr
    descLabel.font = .defaultRegularFont(size: 14)
    descLabel.textColor = .init(hex: "4D4D4D")
    descLabel.text = descStr
    
    if additionalViews.count > 0 {
      productContainerView.isHidden = true
      descContainerView.isHidden = true
      actionContainerView.isHidden = true
      
      for v in mainStack.arrangedSubviews {
        if ![titleContainerView, productContainerView, descContainerView, actionContainerView].contains(v) {
          v.removeFromSuperview()
        }
      }
      
      var i = 0
      for v in additionalViews {
        i += 1
        mainStack.insertArrangedSubview(v, at: i)
      }
    }
    
    titleContainerView.isHidden = !showTitle
  }
  
//  MARK: Action
  @IBAction func dismissAction(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func doAction(_ sender: Any) {
    actionTapped(sender)
  }
}
