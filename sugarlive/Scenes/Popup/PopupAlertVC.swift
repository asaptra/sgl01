//
//  PopupAlertVC.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class PopupAlertVC: BaseVC {
    
    enum Successor {
        case Success
        case Fail
        case Info
    }
    
    enum AlertType: Equatable {
        case Alert
        case Selector
        case Selected
        case Successor(type: Successor)
    }
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var additionalStack: UIStackView!
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet weak var additionalContainerView: UIView!
    @IBOutlet weak var actionContainerView: UIView!
    @IBOutlet weak var selectorActionContainerView: UIView!
    @IBOutlet weak var selectorActionButton: UIButton!
    @IBOutlet weak var selectorCancelButton: UIButton!
    @IBOutlet weak var messageContainerView: UIView!
    
    var cancelTapped: (_ sender: Any) -> Void = {_ in }
    var actionTapped: (_ sender: Any) -> Void = {_ in }
    
    var cancelStr = ""
    var actionStr = ""
    var messageStr = ""
    var additionalViews = [UIView]()
    var alertType: AlertType = .Alert
    var hideCancelButton: Bool = false
    var useAttributedText: Bool = false
    var attributedTextMessage: NSAttributedString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if alertType == .Successor(type: .Success) || alertType == .Successor(type: .Fail) || alertType == .Successor(type: .Info) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.doAction(true)
            }
        }
    }
    
    override func setupLayout() {
        containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = true
        
        if alertType == .Alert {
            messageLabel.textColor = .DARK_TEXT
            messageLabel.textAlignment = .center
            
            cancelButton.setAttributedTitle(cancelStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                                NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]),
                                            for: .normal)
            cancelButton.backgroundColor = .init(hex: "E9E9E9")
            cancelButton.layer.cornerRadius = 9
            cancelButton.layer.masksToBounds = true
            cancelButton.isHidden = hideCancelButton
            
            actionButton.setAttributedTitle(actionStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                                NSAttributedString.Key.foregroundColor: UIColor.white]),
                                            for: .normal)
            actionButton.backgroundColor = .PRIMARY_1
            actionButton.layer.cornerRadius = 9
            actionButton.layer.masksToBounds = true
            
            actionContainerView.isHidden = false
            selectorActionContainerView.isHidden = true
        } else if alertType == .Successor(type: .Success) || alertType == .Successor(type: .Fail) || alertType == .Successor(type: .Info) {
            switch alertType {
            case .Successor(type: .Success):
                imageView.image = UIImage(named: "ic_success_green")
                imageContainerView.isHidden = false
            case .Successor(type: .Info):
                imageContainerView.isHidden = true
            default:
                break
            }
            actionContainerView.isHidden = true
            selectorActionContainerView.isHidden = true
        } else {
            messageContainerView.isHidden = alertType == .Selected
            selectorActionButton.isHidden = alertType == .Selector
            if alertType == .Selector {
                messageLabel.textColor = .black
                messageLabel.textAlignment = .left
                
                selectorCancelButton.setAttributedTitle(cancelStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                                            NSAttributedString.Key.foregroundColor: UIColor.PRIMARY_2]),
                                                        for: .normal)
                selectorCancelButton.backgroundColor = .white
            } else {
                additionalStack.spacing = 20
                selectorActionButton.setAttributedTitle(actionStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                                            NSAttributedString.Key.foregroundColor: UIColor.white]),
                                                        for: .normal)
                selectorActionButton.backgroundColor = .PRIMARY_1
                selectorActionButton.layer.cornerRadius = 9
                selectorActionButton.layer.masksToBounds = true
                
                selectorCancelButton.setAttributedTitle(cancelStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                                            NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]),
                                                        for: .normal)
                selectorCancelButton.backgroundColor = .init(hex: "E9E9E9")
                selectorCancelButton.layer.cornerRadius = 9
                selectorCancelButton.layer.masksToBounds = true
            }
            
            actionContainerView.isHidden = true
            selectorActionContainerView.isHidden = false
        }
        
        messageLabel.font = .defaultRegularFont(size: 14)
        useAttributedText ? (messageLabel.attributedText = attributedTextMessage!) : (messageLabel.text = messageStr)
        
        _ = additionalStack.arrangedSubviews.compactMap{ $0.removeFromSuperview() }
        if additionalViews.count > 0 {
            additionalContainerView.isHidden = false
            _ = additionalViews.compactMap{ additionalStack.addArrangedSubview($0) }
        }
    }
    
    //  MARK: Action
    @IBAction func dismissAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        cancelTapped(sender)
    }
    
    @IBAction func doAction(_ sender: Any) {
        actionTapped(sender)
    }
}
