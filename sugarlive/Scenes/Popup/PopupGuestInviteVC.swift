//
//  PopupGuestInviteVC.swift
//  sugarlive
//
//  Created by cleanmac on 20/12/21.
//

import UIKit

@objc protocol PopupGuestInviteDelegate: AnyObject {
    @objc optional func acceptBattleAction(hostId: String)
    func acceptAction(position: Int)
    func cancelAction()
}

class PopupGuestInviteVC: UIViewController {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userLevelLabel: UILabel!
    @IBOutlet weak var userLevelPillView: UIView!
    
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    // MARK: - Variables
    var model: GuestInvitationModel?
    var delegate: PopupGuestInviteDelegate?
    
    private var isBattleInvitation: Bool = false
    
    // MARK: - Overriden Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        if model?.title == "Battle" {
            isBattleInvitation = true
            titleLabel.text = "Duel request"
        }
        
        if let urlString = model?.coverPicture, let url = URL(string: urlString) {
            userImage.af.setImage(withURL: url)
        } else {
            userImage.image = UIImage(named: "ic_default_profile")
        }
        
        userNameLabel.text = model?.fullName ?? ""
        
        let levelData = model?.level?.convertToDictionary()
        userLevelLabel.text = "Lv. \(levelData?["level"] ?? 0)"
        userLevelPillView.backgroundColor = UIColor(hex: levelData?["color"] as! String)
        userLevelLabel.textColor = UIColor(hex: levelData?["color_text"] as! String)
        
        acceptButton.setAttributedTitle("Accept".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        cancelButton.setAttributedTitle("not now".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.lightGray]), for: .normal)
    }
    
    // MARK: - Custom Functions
    
    // MARK: - Actions
    @IBAction func acceptAction(_ sender: Any) {
        dismiss(animated: true) {
            if self.isBattleInvitation {
                self.delegate?.acceptBattleAction?(hostId: self.model?.userId ?? "")
            } else {
                self.delegate?.acceptAction(position: self.model?.position ?? 1)
            }
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.cancelAction()
        }
    }
    
    // MARK: - API Calls
}
