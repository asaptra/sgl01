//
//  PopupTextFieldVC.swift
//  sugarlive
//
//  Created by cleanmac on 14/11/21.
//

import UIKit
import SnapKit

protocol PopupTagFieldDelegate {
    func setTextFieldValue(_ value: String)
}

class PopupTagFieldVC: UIViewController {
    
    // MARK: - UI Components
    private var containerView: UIView!
    private var titleLabel: UILabel!
    private var inputTextField: UITextField!
    private var actionButton: UIButton!
    
    // MARK: - Variables
    var delegate: PopupTagFieldDelegate?
    
    // MARK: - Overriden Functions
    init(title: String, placeholder: String) {
        super.init(nibName: nil, bundle: nil)
        setupUI(title: title, placeholder: placeholder)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UI Setups
    private func setupUI(title: String, placeholder: String) {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        
        containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.layer.cornerRadius = 20
        containerView.backgroundColor = .white
        view.addSubview(containerView)
        containerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(24)
            constraint.trailing.equalToSuperview().offset(-24)
            constraint.centerY.equalToSuperview()
        }
        
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = title
        titleLabel.font = .defaultBoldFont(size: 16)
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.top.equalToSuperview().offset(16)
        }
        
        actionButton = UIButton()
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.setAttributedTitle("OK".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 16), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        actionButton.backgroundColor = .PRIMARY_1
        actionButton.layer.cornerRadius = 9
        actionButton.layer.masksToBounds = true
        actionButton.addTarget(self, action: #selector(dismissModal), for: .touchUpInside)
        containerView.addSubview(actionButton)
        actionButton.snp.makeConstraints { constraint in
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.top.equalTo(titleLabel.snp.bottom).offset(10)
            constraint.bottom.equalToSuperview().offset(-16)
            constraint.height.equalTo(50)
            constraint.width.equalTo(80)
        }
        
        inputTextField = UITextField()
        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        inputTextField.font = .defaultBoldFont(size: 16)
        inputTextField.placeholder = " " + placeholder
        inputTextField.backgroundColor = .lightGray
        inputTextField.layer.cornerRadius = 9
        containerView.addSubview(inputTextField)
        inputTextField.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalTo(actionButton.snp.leading).offset(-16)
            constraint.top.equalTo(titleLabel.snp.bottom).offset(10)
            constraint.bottom.equalToSuperview().offset(-16)
        }
    }
    
    // MARK: - Custom Functions
    
    // MARK: - Actions
    @objc private func dismissModal() {
        dismiss(animated: true) {
            self.delegate?.setTextFieldValue(self.inputTextField.text ?? "")
        }
    }
}
