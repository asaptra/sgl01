//
//  PopupTextFieldVC.swift
//  sugarlive
//
//  Created by cleanmac-ada on 10/05/22.
//

import UIKit

class PopupTextFieldVC: UIViewController {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    // MARK: - Variables
    private var titleText: String = ""
    private var subtitleText: String?
    private var actionTitleText: String = ""
    private var placeholderText: String = ""
    private var keyboardType: UIKeyboardType!
    private var isSecureTextEntry: Bool!
    private var inputText: String?
    
    var submitTapped: (String) -> Void = { _ in }

    // MARK: - Overriden Functions
    init(title: String, subtitle: String? = nil, actionTitle: String, placeholder: String = "Input here", keyboardType: UIKeyboardType = .default, isSecureTextEntry: Bool = false) {
        super.init(nibName: "PopupTextFieldVC", bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        titleText = title
        subtitleText = subtitle
        actionTitleText = actionTitle
        placeholderText = placeholder
        self.keyboardType = keyboardType
        self.isSecureTextEntry = isSecureTextEntry
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        titleLabel.text = titleText
        
        if subtitleText == nil {
            subtitleLabel.isHidden = true
        } else {
            subtitleLabel.text = subtitleText!
        }
        
        inputTextField.placeholder = placeholderText
        inputTextField.keyboardType = keyboardType
        inputTextField.isSecureTextEntry = isSecureTextEntry
        
        submitButton.setTitle(actionTitleText, for: .normal)
        inputTextField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
    }
    
    // MARK: - Action
    @objc private func textFieldDidChanged(_ sender: UITextField) {
        if let text = sender.text {
            inputText = text
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        dismiss(animated: true) {
            if let inputText = self.inputText {
                self.submitTapped(inputText)
            }
        }
    }
    
}
