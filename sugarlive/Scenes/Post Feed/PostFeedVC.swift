//
//  PostFeedVC.swift
//  sugarlive
//
//  Created by msi on 11/09/21.
//

import UIKit
import Fusuma
import ACProgressHUD_Swift

class PostFeedVC: BaseCollectionVC {
  
  @IBOutlet weak var profileView: FeedProfileView!
  @IBOutlet weak var inputField: BaseTextView!
  @IBOutlet weak var addImageButton: UIButton!
  
  private var feedRequestModel = PostFeedRequestModel()
  private var feedMediaRequestModel = PostFeedMediaRequestModel()
  private var selectedImages = [UIImage]()
  private var imageURLs = [URL]()
  private var userModel: UserModel?
  
  var feed: FeedModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if feed != nil {
      getFeed { (result) in
        self.feed = result
        self.setupFeed()
      }
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupNav()
  }
  
  override func setupLayout() {
    profileView.actionButton.superview?.isHidden = true
    profileView.timeLabel.superview?.isHidden = true
    profileView.profileImageView.layer.cornerRadius = CGFloat(signOf: (Constant.screenSize.width-40)*0.1,magnitudeOf: 1)/2
    
    userModel = AuthManager.shared.currentUser
    profileView.nameLabel.text = userModel?.fullname
    if let urlString = userModel?.userPictureUrl, let url = URL(string: urlString) {
      profileView.profileImageView.af.setImage(withURL: url)
    } else {
      profileView.profileImageView.image = UIImage(named: "ic_default_profile")
    }
    
    inputField.placeholder = "Post Feed"
    inputField.textViewDidEndHandler = {(sender) in
      if let textView = sender as? UITextView {
        self.feedRequestModel.content = textView.text
      }
    }
    inputField.font = .defaultRegularFont(size: 15)
//    inputField.textColor = .DARK_TEXT
    
    addImageButton.setAttributedTitle("  Add photo".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 15),
                                                                              NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]),
                                      for: .normal)
    
    setupCollection()
    
    photoPickerVC.delegate = self
    photoPickerVC.allowMultipleSelection = true
    photoPickerVC.photoSelectionLimit = 4
  }
  
  override func setupNav() {
    let postButton = UIButton(type: .custom)
    postButton.layer.cornerRadius = 5
    postButton.layer.masksToBounds = true
    postButton.backgroundColor = .init(hex: "FFBD09")
    postButton.setAttributedTitle("Post".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14),
                                                                   NSAttributedString.Key.foregroundColor: UIColor.PRIMARY_1]),
                                  for: .normal)
    postButton.addTarget(self, action: #selector(postAction(_:)), for: .touchUpInside)
    let postBarItem = UIBarButtonItem.init(customView: postButton)
    postBarItem.customView?.widthAnchor.constraint(equalToConstant: 74).isActive = true
    postBarItem.customView?.heightAnchor.constraint(equalToConstant: 33).isActive = true
    self.navigationItem.rightBarButtonItem = postBarItem
    
    self.navigationItem.titleView = navLabel(title: feed == nil ? "Create new post" : "Update post")
    
    self.navigationController?.navigationBar.barTintColor = .PRIMARY_1
  }
  
  private func setupCollection() {
    collectionView.dataSource = self
    collectionView.delegate = self
    
    cells = ["PostImageCollectionCell"]
  }
  
    private func setupFeed() {
        inputField.text = feed?.content
        inputField.textColor = .black
        feedRequestModel.content = feed?.content
        if let urlString = feed?.image, let url = URL(string: urlString) {
            imageURLs.append(url)
//            feedRequestModel.image = urlString
        }
        if let urlString = feed?.image2, let url = URL(string: urlString) {
            imageURLs.append(url)
//            feedRequestModel.image_2 = urlString
        }
        if let urlString = feed?.image3, let url = URL(string: urlString) {
            imageURLs.append(url)
//            feedRequestModel.image_3 = urlString
        }
        if let urlString = feed?.image4, let url = URL(string: urlString) {
            imageURLs.append(url)
//            feedRequestModel.image_4 = urlString
        }
        self.collectionView.reloadData()
    }
  
//  MARK: Service
  private func getFeed(handler: @escaping ((FeedModel) -> Void)) {
    if let id = feed?.postId {
      provider.request(.GetCurrentFeed(identifier: id), successModel: FeedsResponseModel.self) { (result) in
        if let feed = result.feed {
          handler(feed)
        }
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
    private func postFeed() {
////    feedRequestModel.images = selectedImages
//      feedRequestModel.imagesData = selectedImages.map({ $0.compress(to: 500) })
//      provider.request(.PostFeed(request: feedRequestModel), successModel: SuccessResponseModel.self) { (result) in
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kPostFeedResponse), object: nil)
//        self.routeToWarningModal(message: "Post feed Successful") {
//          self.navigationController?.popViewController(animated: true)
//        }
//      } failure: { (error) in
//          self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
//      }
        if selectedImages.count > 0 {
            ACProgressHUD.shared.showHUD()
            feedMediaRequestModel.imagesData = selectedImages.map({ $0.compress(to: 500) })
            provider.request(.PostFeedMedia(request: feedMediaRequestModel), successModel: FeedMediaResponseModel.self) { (result) in
                if let urlString = result.image {
                    self.feedRequestModel.image = urlString
                }
                if let urlString = result.image_2 {
                    self.feedRequestModel.image_2 = urlString
                }
                if let urlString = result.image_3 {
                    self.feedRequestModel.image_3 = urlString
                }
                if let urlString = result.image_4 {
                    self.feedRequestModel.image_4 = urlString
                }
                self.postFeedContent()
            } failure: { (error) in
                ACProgressHUD.shared.hideHUD()
                self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
            }
        } else {
            postFeedContent()
        }
    }
    
    private func postFeedContent() {
        provider.request(.PostFeed(request: feedRequestModel), successModel: SuccessResponseModel.self) { (result) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kPostFeedResponse), object: nil)
            self.routeToWarningModal(message: "Post feed Successful") {
                self.navigationController?.popViewController(animated: true)
            }
        } failure: { (error) in
            self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
        }
    }
  
  private func updateFeed() {
//    feedRequestModel.imagesData = selectedImages.map({ $0.compress(to: 500) })
//      feedRequestModel.imageURLs = imageURLs
      
      if imageURLs.count > 0 {
          var idx = 0
          for imageUrl in imageURLs {
              switch idx {
              case 0:
                  feedRequestModel.image = "\(imageUrl)"
              case 1:
                  feedRequestModel.image_2 = "\(imageUrl)"
              case 2:
                  feedRequestModel.image_3 = "\(imageUrl)"
              case 3:
                  feedRequestModel.image_4 = "\(imageUrl)"
              default:
                  break
              }
              idx += 1
          }
      }
      if selectedImages.count > 0 {
//          let totalImage = imageURLs.count + selectedImages.count
          ACProgressHUD.shared.showHUD()
          feedMediaRequestModel.imagesData = selectedImages.map({ $0.compress(to: 500) })
          provider.request(.PostFeedMedia(request: feedMediaRequestModel), successModel: FeedMediaResponseModel.self) { (result) in
              if let urlString = result.image {
                  self.feedRequestModel.image = urlString
              }
              if let urlString = result.image_2 {
                  self.feedRequestModel.image_2 = urlString
              }
              if let urlString = result.image_3 {
                  self.feedRequestModel.image_3 = urlString
              }
              if let urlString = result.image_4 {
                  self.feedRequestModel.image_4 = urlString
              }
              self.updateFeedContent()
          } failure: { (error) in
              ACProgressHUD.shared.hideHUD()
              self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
          }
      } else {
          updateFeedContent()
      }
  }
    
    private func updateFeedContent() {
        if let id = feed?.postId {
          provider.request(.UpdateFeed(identifier: id, request: feedRequestModel), successModel: SuccessResponseModel.self) { (result) in
            self.getFeed { (updatedFeed) in
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateFeedResponse), object: updatedFeed)
              self.routeToWarningModal(message: "Update feed Successful") {
                self.navigationController?.popViewController(animated: true)
              }
            }
          } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
          }
        }
    }
    
  
//  MARK: Action
  @IBAction func postAction(_ sender: Any) {
    self.view.endEditing(true)
    if feed == nil {
      postFeed()
    } else {
      updateFeed()
    }
  }
  
  @IBAction func addPhoto(_ sender: Any) {
    if selectedImages.count < 4 {
      routeToPictureActionSheet(galleryHandler: nil, cameraHandler: nil)
    } else {
      routeToWarningModal(message: "Reached image limit.")
    }
  }
}

extension PostFeedVC: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return feed == nil ? selectedImages.count : imageURLs.count + selectedImages.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostImageCollectionCell", for: indexPath) as! PostImageCollectionCell
    if feed == nil {
      cell.bind(image: selectedImages[indexPath.row])
    } else {
      if indexPath.row < imageURLs.count {
        cell.bind(url: imageURLs[indexPath.row])
      } else {
        cell.bind(image: selectedImages[indexPath.row - imageURLs.count])
      }
    }
    cell.deleteButton.isHidden = false
    cell.deleteTapped = {(sender) in
      if self.feed == nil {
        self.selectedImages.remove(at: indexPath.row)
      } else {
        if indexPath.row < self.imageURLs.count {
          self.imageURLs.remove(at: indexPath.row)
        } else {
          self.selectedImages.remove(at: indexPath.row - self.imageURLs.count)
        }
      }
      collectionView.deleteItems(at: [indexPath])
      collectionView.reloadData()
    }
    return cell
  }
}

extension PostFeedVC: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    collectionView.deselectItem(at: indexPath, animated: true)
  }
}

extension PostFeedVC: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let height = collectionView.frame.height
    return .init(width: height, height: height)
  }
}

extension PostFeedVC: FusumaDelegate {
  func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
    if (feed == nil && selectedImages.count < 4) || (feed != nil && (imageURLs.count + selectedImages.count) < 4) {
      selectedImages.append(image)
      collectionView.reloadData()
    } else {
      routeToWarningModal(message: "Reached image limit.")
    }
  }
  
  func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode, metaData: [ImageMetadata]) {
    print("Image selected")
    selectedImages.removeAll()
    selectedImages = images
    collectionView.reloadData()
  }
  
  func fusumaVideoCompleted(withFileURL fileURL: URL) {
    print("Called just after a video has been selected.")
  }
  
  func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
    
  }
  
  func fusumaCameraRollUnauthorized() {
    print("Camera roll unauthorized")
  }
  
  func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
    
  }
}
