//
//  DirectChatVC.swift
//  sugarlive
//
//  Created by dodets on 11/05/22.
//

import UIKit
import PusherSwift

class DirectChatVC: BaseTableVC, UITextFieldDelegate {
    @IBOutlet weak var chatTxt: UITextField!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var chatFieldContainer: UIView!
    
    private var messageResponseModel: MessageChatResponseModel?
    private var listConversation = [MessageChatModel]()
    private var initChatRequestModel = InitChatRequestModel()
    private var sendChatRequestModel = SendChatRequestModel()
    var isPresentedAsModal: Bool = false
    var conversationID = ""
    var receiverUserId = ""
    var receiverUserName = ""
    var userModel: UserModel?
    var pageModel = PagingRequestModel()
    var page = 1
    
    private var pusher: Pusher?
    private var channel: PusherChannel?
    private var channelPresence: PusherPresenceChannel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.userModel = AuthManager.shared.currentUser
    }
    
    deinit {
        disconnectPusherConnection()
    }
    
    private func disconnectPusherConnection() {
        pusher?.unsubscribeAll()
        pusher?.disconnect()
    }
    
    //pusher
    private func setupPusherConnection() {
        let authBuilder = PusherRequestBuilder(userId: userModel?.userId ?? "")
        let options = PusherClientOptions(authMethod: AuthMethod.authRequestBuilder(authRequestBuilder: authBuilder),
                                          host: .cluster(Constant.PusherCluster))
        
        pusher = Pusher(key: Constant.PusherKeyID, options: options)
        channel = pusher?.subscribe(channelName: userModel?.userId ?? "")
        channelPresence = pusher?.subscribeToPresenceChannel(channelName: "presence-\(conversationID)")
        
        let _ = channelPresence?.bind(eventName: PusherEvents.NEW_MESSAGE, callback: { [weak self] data in
            guard self != nil else { return }
            let dateToday = Date().localDate()
            let dataJSON = data as! [String: Any]
            let senderInfo = dataJSON["senderInfo"] as! [String: Any]
            let receiver = ReceiverChatModel(receiverUserId: senderInfo["userId"] as? String ?? "", fullname: senderInfo["fullname"] as? String ?? "", coverPicture: "")
            let data = MessageChatModel(message: dataJSON["message"] as? String ?? "", image: dataJSON["image"] as? String ?? "", caption: dataJSON["caption"] as? String ?? "", type: dataJSON["type"] as? String ?? "", createdAt: dateToday.toString(with: "yyyy-MM-dd HH:mm:ss"), senderInfo: receiver)
            self?.listConversation.append(data)
            self?.tableView.reloadData()
            self?.scrollToBottom(isLoader: false)
        })
        
        pusher?.connect()
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            chatFieldContainer.snp.updateConstraints { constraint in
                if #available(iOS 11.0, *) {
                    constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin).offset(-(keyboardHeight))
                } else {
                    constraint.bottom.equalTo(view.snp.bottomMargin).offset(-(keyboardHeight))
                }
            }
            view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {
        chatFieldContainer.snp.updateConstraints { constraint in
            if #available(iOS 11.0, *) {
                constraint.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin).offset(0)
            } else {
                constraint.bottom.equalTo(view.snp.bottomMargin).offset(0)
            }
        }
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initChat()
    }
  
    override func setupNav() {
        if isPresentedAsModal {
            let dismissButton = UIBarButtonItem(image: UIImage(named: "ic_back_white"), style: .plain, target: self, action: #selector(dismissNavigationControllerAction))
            let navBarTitle = UIBarButtonItem(customView: navLabel(title: receiverUserName))
            navigationItem.leftBarButtonItems = [dismissButton, navBarTitle]
        } else {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: receiverUserName))
            self.navigationItem.leftItemsSupplementBackButton = true
        }
    }
  
    override func setupLayout() {
        hideKeyboardWhenTappedAround()
        
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        cells = ["ConversationCell", "ConversationRightCell"]
        
        chatTxt.font = .defaultRegularFont()
        chatTxt.textColor = .darkText
        chatTxt.delegate = self
        let padding: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 0))
        chatTxt.leftView = padding
        chatTxt.leftViewMode = .always
        
        chatTxt.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        sendView.backgroundColor = .PRIMARY_1
        sendView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendChat)))
    }
    
    @objc func textFieldDidChanged(_ textField: UITextField) {
        
    }
    
    func scrollToBottom(isLoader: Bool){
        if self.listConversation.count > 0 {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: self.listConversation.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: isLoader)
            }
        }
    }
    
    @objc private func dismissNavigationControllerAction() {
        navigationController?.dismiss(animated: true)
    }
    
    //  MARK: Service
    private func getChat(isLoader: Bool, scrollToBottomAnimate: Bool) {
        pageModel.page = page
        pageModel.perPage = 50
        provider.request(.PrivateChatConversation(identifier: conversationID, request: pageModel), successModel: MessageChatResponseModel.self, withProgressHud: isLoader) { (result) in
//            self.listConversation = result.attributes ?? [MessageChatModel]()
            self.messageResponseModel = result
            self.messageResponseModel?.attributes?.forEach{self.listConversation.append($0)}
            self.page += 1
            self.tableView.reloadData()
            self.scrollToBottom(isLoader: scrollToBottomAnimate)
            if self.page <= self.messageResponseModel?.paging?.totalPages ?? 0 {
                self.getChat(isLoader: true, scrollToBottomAnimate: false)
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func initChat() {
        initChatRequestModel.receiverUserId = receiverUserId
        provider.request(.InitChat(request: initChatRequestModel), successModel: InitChatResponseModel.self) { (result) in
            self.conversationID = result.conversationId ?? ""
            self.setupPusherConnection()
            self.getChat(isLoader: true, scrollToBottomAnimate: false)
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    @objc private func sendChat() {
        sendChatRequestModel.receiverUserId = receiverUserId
        sendChatRequestModel.message = chatTxt.text
        provider.request(.SendChat(request: sendChatRequestModel), successModel: SuccessResponseModel.self, withProgressHud: false) { (result) in
            self.chatTxt.text = nil
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
}

extension DirectChatVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listConversation.count
    }
  
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = listConversation[indexPath.row]
        if data.senderInfo?.receiverUserId == receiverUserId {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell", for: indexPath) as! ConversationCell
            let data = listConversation[indexPath.row]
            cell.chatLbl.text = data.message
            cell.timeLbl.text = data.createdAt?.toDate(with: "yyyy-MM-dd HH:mm:ss").chatTimeAgo()

            if let imageUrl = data.senderInfo?.coverPicture, let url = URL(string: imageUrl) {
                cell.profileView?.af.setImage(withURL: url)
            }

            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationRightCell", for: indexPath) as! ConversationRightCell
            let data = listConversation[indexPath.row]
            cell.chatLbl.text = data.message
            cell.timeLbl.text = data.createdAt?.toDate(with: "yyyy-MM-dd HH:mm:ss").chatTimeAgo()

            if let imageUrl = self.userModel?.userPictures.first?.picture_url, let url = URL(string: imageUrl) {
                cell.profileView?.af.setImage(withURL: url)
            }

            return cell
        }
        
    }
}

extension DirectChatVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
      if indexPath.section == 0 {
          
      } else {
          
      }
  }
}
