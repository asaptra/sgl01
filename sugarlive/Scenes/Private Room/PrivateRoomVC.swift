//
//  PrivateRoomVC.swift
//  sugarlive
//
//  Created by cleanmac-ada on 09/05/22.
//

import UIKit

class PrivateRoomVC: BaseVC {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var passwordRadioButtonStackView: UIStackView!
    @IBOutlet weak var passwordRadioButtonImage: UIImageView!
    
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordValidatorLabel: UILabel!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordValidatorLabel: UILabel!
    
    @IBOutlet weak var payRadioButtonStackView: UIStackView!
    @IBOutlet weak var payRadioButtonImage: UIImageView!
    
    @IBOutlet weak var payStackView: UIStackView!
    @IBOutlet weak var payAmountTextField: UITextField!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    // MARK: - Variables
    private var type: PrivateRoomType = .Password {
        didSet {
            passwordStackView.isHidden = !(type == .Password)
            payStackView.isHidden = type == .Password
            
            passwordRadioButtonImage.image = type == .Password ? UIImage(named: "ic_radio_button_active") : UIImage(named: "ic_radio_button_inactive")
            payRadioButtonImage.image = type == .PayPerSession ? UIImage(named: "ic_radio_button_active") : UIImage(named: "ic_radio_button_inactive")
            
            view.endEditing(true)
        }
    }
    
    private var requirement: String?
    private var inputPassword: String?
    private var inputConfirmPassword: String?
    
    // MARK: - Overriden Functions
    init() {
        super.init(nibName: "PrivateRoomVC", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupNav() {
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_back_white"), style: .plain, target: self, action: #selector(dismissAction))
        let navTitle = UIBarButtonItem(customView: navLabel(title: "Private room"))
        navigationItem.leftBarButtonItems = [backButton, navTitle]
        navigationController?.navigationBar.barTintColor = .PRIMARY_1
        navigationController?.navigationBar.tintColor = .white
    }
    
    override func setupLayout() {
        type = .Password
        
        passwordRadioButtonStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(radioButtonAction(_:))))
        payRadioButtonStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(radioButtonAction(_:))))
        
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        passwordTextField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        confirmPasswordTextField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        payAmountTextField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
    }
    
    // MARK: - Custom Functions
    private func showValidationModal(message: String) {
        popupAlertVC.cancelStr = "Cancel"
        popupAlertVC.actionStr = "Retry"
        popupAlertVC.messageStr = message
        popupAlertVC.additionalViews = []
        popupAlertVC.cancelTapped = { [unowned popupAlertVC] (sender) in
            popupAlertVC.dismiss(animated: true)
        }
        popupAlertVC.actionTapped = { [unowned popupAlertVC] (sender) in
            popupAlertVC.dismiss(animated: true)
        }
        present(popupAlertVC, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    @objc private func textFieldDidChanged(_ sender: UITextField) {
        let value = sender.text
        if sender == passwordTextField {
            if value?.isEmpty ?? true || (value?.count ?? 0) < 6 {
                passwordValidatorLabel.isHidden = false
                inputPassword = nil
            } else {
                passwordValidatorLabel.isHidden = true
                inputPassword = value
            }
        } else if sender == confirmPasswordTextField {
            if value?.isEmpty ?? true || (value?.count ?? 0) < 6 {
                confirmPasswordValidatorLabel.isHidden = false
                inputConfirmPassword = nil
            } else {
                confirmPasswordValidatorLabel.isHidden = true
                inputConfirmPassword = value
            }
        } else if sender == payAmountTextField {
            requirement = value
        }
    }
    
    @objc private func radioButtonAction(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        if view == passwordRadioButtonStackView {
            type = .Password
        } else if view == payRadioButtonStackView {
            type = .PayPerSession
        }
    }
    
    @objc private func dismissAction() {
        navigationController?.dismiss(animated: true)
    }
                                       
    @IBAction func confirmAction(_ sender: Any) {
        if type == .Password {
            if let password = inputPassword, let confirmedPassword = inputConfirmPassword {
                if password != confirmedPassword || (password.isEmpty && confirmedPassword.isEmpty) {
                    showValidationModal(message: "Password didn't match")
                } else {
                    requirement = confirmedPassword
                    setLiveAsPrivate()
                }
            } else {
                showValidationModal(message: "Password didn't match")
            }
            
        } else {
            if let payAmount = requirement {
                if payAmount.isEmpty {
                    showValidationModal(message: "Please fill the required candy amount")
                } else {
                    setLiveAsPrivate()
                }
            }
            
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismissAction()
    }
    
    // MARK: - API Calls
    private func setLiveAsPrivate() {
        guard let request = SetLivePrivateRequestModel(JSON: ["privateType": type.rawValue, "requirement": requirement ?? ""]) else { return }
        provider.request(.SetLivePrivate(request: request), successModel: SuccessResponseModel.self, success: { [weak self] response in
            guard let self = self else { return }
            self.dismissAction()
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
}

// MARK: - Text Field delegate
extension PrivateRoomVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.count <= 6
    }
}
