//
//  ProfileManagerVC.swift
//  sugarlive
//
//  Created by msi on 13/09/21.
//

import UIKit
import AlamofireImage

enum ProfileManagerType: Int {
  case Frame
  case Entrance
  case Badge
  case ChatBubble
  case FrameStore
  case EntranceStore
  case BadgeStore
  case ChatBubbleStore
  case Default
  
  func title() -> String {
    switch self {
    case .Frame, .FrameStore:
      return "Avatar frame"
    case .Entrance, .EntranceStore:
      return "Entrance Effect"
    case .Badge, .BadgeStore:
      return "Badge"
    case .ChatBubble, .ChatBubbleStore:
      return "Chat Bubble"
    default:
      return ""
    }
  }
}

class ProfileManagerVC: BaseCollectionVC {
  var userModel: UserModel?
  private var entranceEffectResponseModel: EntranceEffectResponseModel?
  var profileManagerType: ProfileManagerType = .Default
  lazy var framePreviewView = FramePreviewView.nib(withType: FramePreviewView.self)
  lazy var badgePreviewView = BadgePreviewView.nib(withType: BadgePreviewView.self)
  lazy var previewHeight: CGFloat = {
    if [ProfileManagerType.Frame, ProfileManagerType.FrameStore].contains(profileManagerType) {
      return self.view.frame.width * (200/400)
    } else {
      return 60
    }
  }()
  lazy var isStore: Bool = {
    return [ProfileManagerType.FrameStore, ProfileManagerType.EntranceStore, ProfileManagerType.BadgeStore, ProfileManagerType.ChatBubbleStore].contains(profileManagerType)
  }()
  
  private var selectedIndexes: [IndexPath] = []
  
  private let downloader = ImageDownloader()
  private var products = [ProductModel]()
  private var ownedIds = [String]()
    var pageModel = PagingRequestModel()
    var page = 1
    private var storeResponseModel: ProductResponseModel?
    
  override func viewDidLoad() {
    super.viewDidLoad()

    if let currentUser = AuthManager.shared.currentUser {
      userModel = currentUser
      switch profileManagerType {
      case .FrameStore:
        ownedIds = userModel?.avatarFrames?.list?.map{ $0.avatarFrameId } as? [String] ?? []
        break
      case .EntranceStore:
        ownedIds = userModel?.entranceEffects?.list?.map{ $0.entranceEffectId } as? [String] ?? []
        break
      case .BadgeStore:
        ownedIds = userModel?.badges?.list?.map{ $0.badgeId } as? [String] ?? []
        break
      case .ChatBubbleStore:
        ownedIds = userModel?.chatBubbles?.list?.map{ $0.chatBubbleId } as? [String] ?? []
        break
      default:
        break
      }
    }
    
    if [ProfileManagerType.Frame, ProfileManagerType.FrameStore].contains(profileManagerType) {
      setDefaultAvatarFrame()
    } else if profileManagerType == .Entrance {
      setDefaultEntranceEffect()
    } else if profileManagerType == .Badge {
      setDefaultBadge()
    } else if profileManagerType == .ChatBubble {
      setDefaultChatBubble()
    }
    if [ProfileManagerType.FrameStore, ProfileManagerType.EntranceStore, ProfileManagerType.BadgeStore, ProfileManagerType.ChatBubbleStore].contains(profileManagerType) {
      getProduct()
    }
      
      getChatBubbles()
  }
  
  override func setupNav() {
    self.navigationItem.titleView = navLabel(title: profileManagerType.title())
  }
  
  override func setupLayout() {
    collectionView.dataSource = self
    collectionView.delegate = self
    cells = ["ProfileManagerCollectionCell"]
    headers = ["StreamerCollectionHeaderView"]
    
    doneButton.setAttributedTitle(([ProfileManagerType.FrameStore, ProfileManagerType.EntranceStore, ProfileManagerType.BadgeStore, ProfileManagerType.ChatBubbleStore].contains(profileManagerType) ? "Buy" : "Done").toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 18),
                                                                   NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
    doneButton.setBackgroundImage(UIImage(color: .PRIMARY_1), for: .normal)
    doneButton.setBackgroundImage(UIImage(color: UIColor(hex: "E9E9E9")), for: .selected)
    doneButton.layer.cornerRadius = 10
    doneButton.layer.masksToBounds = true
    doneButton.addTarget(self, action: #selector(submitData), for: .touchUpInside)
    doneButton.isSelected = isStore
    
    switch profileManagerType {
    case .Frame, .FrameStore:
      setupFramePreview()
      collectionView.contentInset = .init(top: previewHeight, left: 0, bottom: 0, right: 0)
      
    case .Badge, .ChatBubble, .BadgeStore, .ChatBubbleStore:
      setupBadgePreview()
      collectionView.contentInset = .init(top: previewHeight, left: 0, bottom: 0, right: 0)
    default:
      break
    }
  }
  
  private func setupFramePreview() {
    framePreviewView.translatesAutoresizingMaskIntoConstraints = false
    self.view.addSubview(framePreviewView)
    framePreviewView.snp.makeConstraints { (maker) in
      maker.top.equalTo(self.collectionView.snp.top)
      maker.leading.equalTo(self.view.snp.leading)
      maker.trailing.equalTo(self.view.snp.trailing)
      maker.height.equalTo(previewHeight)
    }
  }
  
  private func setupBadgePreview() {
    badgePreviewView.translatesAutoresizingMaskIntoConstraints = false
    badgePreviewView.previewType = profileManagerType
    self.view.addSubview(badgePreviewView)
    badgePreviewView.snp.makeConstraints { (maker) in
      maker.top.equalTo(self.collectionView.snp.top)
      maker.leading.equalTo(self.view.snp.leading)
      maker.trailing.equalTo(self.view.snp.trailing)
      maker.height.equalTo(previewHeight)
    }
  }
  
//  MARK: Service
  private func updateUserAvatarFrame(){
    var avatarFrameId = ""
    if selectedIndexes.count > 0 {
      let frameObject = userModel?.avatarFrames?.list?[selectedIndexes.first!.row]
      avatarFrameId = frameObject?.avatarFrameId ?? ""
    }
    guard let request = ProfileDefaultRequestModel(JSON: ["avatarFrameId": avatarFrameId]) else { return }
    provider.request(.SetFrame(request: request), successModel: SuccessResponseModel.self) { (result) in
      self.getMyProfile {}
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
      self.routeToWarningModal(message: "Success change avatar frame.") {
        self.navigationController?.popViewController(animated: true)
      }
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func updateUserEntranceEffect(){
    var entranceEffectId = ""
    if selectedIndexes.count > 0 {
      let entranceObject = userModel?.entranceEffects?.list?[selectedIndexes.first!.row]
      entranceEffectId = entranceObject?.entranceEffectId ?? ""
    }
    guard let request = ProfileDefaultRequestModel(JSON: ["entranceEffectId": entranceEffectId]) else { return }
    provider.request(.SetEntrance(request: request), successModel: SuccessResponseModel.self) { (result) in
      self.getMyProfile {}
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
      self.routeToWarningModal(message: "Success change entrance effect.") {
        self.navigationController?.popViewController(animated: true)
      }
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func updateUserBadge(){
    var badgeIds = [String]()
    if selectedIndexes.count > 0 {
      for index in selectedIndexes {
        let badgeObject = userModel?.badges?.list?[index.row]
        badgeIds.append(badgeObject?.badgeId ?? "")
      }
    }
    guard let request = ProfileDefaultRequestModel(JSON: ["badgeIds": badgeIds]) else { return }
    provider.request(.SetBadge(request: request), successModel: SuccessResponseModel.self) { (result) in
      self.getMyProfile {}
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
      self.routeToWarningModal(message: "Success change badge.") {
        self.navigationController?.popViewController(animated: true)
      }
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func updateUserChatBubble(){
    var chatBubbleId = ""
    if selectedIndexes.count > 0 {
      let bubbleObject = userModel?.chatBubbles?.list?[selectedIndexes.first!.row]
      chatBubbleId = bubbleObject?.chatBubbleId ?? ""
    }
    guard let request = ProfileDefaultRequestModel(JSON: ["chatBubbleId": chatBubbleId]) else { return }
    provider.request(.SetChatBubble(request: request), successModel: SuccessResponseModel.self) { (result) in
      self.getMyProfile {}
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
      self.routeToWarningModal(message: "Success change chat bubble.") {
        self.navigationController?.popViewController(animated: true)
      }
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
    private func getProduct() {
        pageModel.page = page
        pageModel.perPage = 50
        provider.request(.ProductStore(request: pageModel), successModel: ProductResponseModel.self) { (result) in
            self.storeResponseModel = result
            self.storeResponseModel?.attributes?.forEach{self.products.append($0)}
            self.page += 1
        
            switch self.profileManagerType {
            case .FrameStore:
                self.products = self.products.filter({$0.type == "avatar_frame"})
                break
            case .EntranceStore:
                self.products = self.products.filter({$0.type == "entrance_effect"})
                break
            case .BadgeStore:
                self.products = self.products.filter({$0.type == "badge"})
                break
            case .ChatBubbleStore:
                self.products = self.products.filter({$0.type == "chat_bubble"})
                break
            default:
                break
                
            }
            self.collectionView.reloadData()
                if self.page <= self.storeResponseModel?.paging?.totalPages ?? 0 {
            self.getProduct()
            }
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
  
  private func doPurchase() {
    if selectedIndexes.count > 0 {
      let product = products[selectedIndexes.first?.row ?? 0]
      if let request = PurchaseRequestModel(JSON: ["id": product.id ?? ""]) {
        provider.request(.Purchase(request: request), successModel: SuccessResponseModel.self) { (result) in
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
          self.routeToWarningModal(message: "Purchase Successful") {
            self.navigationController?.popToRootViewController(animated: true)
          }
        } failure: { (error) in
          print(error.messages.joined(separator: "\n"))
        }
      }
    }
  }
    
    private func getChatBubbles() {
        provider.request(.ChatBubbles, successModel: ChatBubbleResponseModel.self, withProgressHud: false, success: { response in
            if let list = response.attributes {
                ChatBubbleProvider.shared.storeChatBubbles(list)
                if self.profileManagerType != .ChatBubbleStore {
                    self.setDefaultChatBubble()
                }
            }
        }, failure: { error in
            print(error.messages.joined(separator: "\n"))
        })
    }
  
//  MARK: Action
  @IBAction private func submitData(_ sender: Any){
    switch profileManagerType {
    case .Frame:
      updateUserAvatarFrame()
      break
    case .Entrance:
      updateUserEntranceEffect()
      break
    case .Badge:
      updateUserBadge()
      break
    case .ChatBubble:
      updateUserChatBubble()
      break
    case .FrameStore, .EntranceStore, .BadgeStore, .ChatBubbleStore:
      if doneButton.isSelected { return }
      if selectedIndexes.count > 0 {
        let product = products[selectedIndexes.first?.row ?? 0]
        routeToCheckoutSheet(product: product) { (available) in
          if available {
            self.doPurchase()
          } else {
//            route to topup
          }
        }
      }
      break
    default:
      break
    }
  }
  
//  MARK: Custom
    func updateBadgePreview() {
        badgePreviewView.leftBadgeView.isHidden = !(selectedIndexes.count > 0)
        badgePreviewView.centerBadgeView.isHidden = !(selectedIndexes.count > 1)
        badgePreviewView.rightBadgeView.isHidden = !(selectedIndexes.count > 2)
    
        var idx = 0
        for _ in selectedIndexes {
            if idx == 0 {
                if profileManagerType == .Badge {
                    let badgeObject = userModel?.badges?.list?[selectedIndexes.first?.row ?? 0]
                    if let imageUrl = badgeObject?.url, let url = URL(string: imageUrl) {
                        badgePreviewView.leftBadgeImage.af.setImage(withURL: url)
                    }
                } else if profileManagerType == .BadgeStore {
                    let badgeObject = products[selectedIndexes.first?.row ?? 0]
                    if let imageUrl = badgeObject.thumb, let url = URL(string: imageUrl) {
                        badgePreviewView.leftBadgeImage.af.setImage(withURL: url)
                    }
                }
            }
            
            if idx == 1 && profileManagerType == .Badge{
                let badgeObject = userModel?.badges?.list?[selectedIndexes[1].row]
                if let imageUrl = badgeObject?.url, let url = URL(string: imageUrl) {
                    badgePreviewView.centerBadgeImage.af.setImage(withURL: url)
                }
            }
            
            if idx == 2 && profileManagerType == .Badge {
                let badgeObject = userModel?.badges?.list?[selectedIndexes[2].row]
                if let imageUrl = badgeObject?.url, let url = URL(string: imageUrl) {
                    badgePreviewView.rightBadgeImage.af.setImage(withURL: url)
                }
            }
            idx += 1
        }
    }
  
  func updateChatBubblePreview() {
    if selectedIndexes.count > 0 {
      if profileManagerType == .ChatBubble {
        let bubbleObject = userModel?.chatBubbles?.list?[selectedIndexes.first?.row ?? 0]
          if let bubbleModel = ChatBubbleProvider.shared.getBubble(from: bubbleObject?.chatBubbleId ?? "") {
              setBubbleImage(bubbleModel)
          }
      } else if profileManagerType == .ChatBubbleStore {
          let bubbleObject = products[selectedIndexes.first?.row ?? 0]
          if let bubbleModel = ChatBubbleProvider.shared.getBubble(from: bubbleObject.id ?? "") {
              setBubbleImage(bubbleModel)
          }
      }
    } else {
      self.badgePreviewView.bubbleImage.image = nil
    }
  }
  
  func updateAvatarFramePreview() {
    if selectedIndexes.count > 0 {
      if profileManagerType == .Frame {
        let frameObject = userModel?.avatarFrames?.list?[selectedIndexes.first?.row ?? 0]
        if let imageUrl = frameObject?.avatarIcon, let url = URL(string: imageUrl) {
          framePreviewView.badgeImage.af.setImage(withURL: url)
        }
      } else if profileManagerType == .FrameStore {
        let frameObject = products[selectedIndexes.first?.row ?? 0]
        if let imageUrl = frameObject.thumb, let url = URL(string: imageUrl) {
          framePreviewView.badgeImage.af.setImage(withURL: url)
        }
      }
    } else {
      framePreviewView.badgeImage.image = nil
    }
  }
  
  func setDefaultAvatarFrame() {
    if let urlString = userModel?.userPictureUrl, let url = URL(string: urlString) {
      framePreviewView.profileImage.af.setImage(withURL: url)
    }
    if let urlString = userModel?.coverPicture, let url = URL(string: urlString) {
      framePreviewView.backgroundImage.af.setImage(withURL: url)
    }
    framePreviewView.nameLabel.text = userModel?.fullname
    if let imageUrl = userModel?.avatarFrames?.defaultFrame?.avatarIcon, let url = URL(string: imageUrl) {
      framePreviewView.badgeImage.af.setImage(withURL: url)
      if let index = userModel?.avatarFrames?.list?.firstIndex(where: { (frame) -> Bool in
        return frame.avatarFrameId == userModel?.avatarFrames?.defaultFrame?.avatarFrameId
      }) {
        selectedIndexes = [IndexPath(row: index, section: 0)]
      }
    }
  }
  
  func setDefaultEntranceEffect() {
    if userModel?.entranceEffects?.defaultEffect != nil {
      if let index = userModel?.entranceEffects?.list?.firstIndex(where: { (entrance) -> Bool in
        return entrance.entranceEffectId == userModel?.entranceEffects?.defaultEffect?.entranceEffectId
      }) {
        selectedIndexes = [IndexPath(row: index, section: 0)]
      }
    }
  }
  
  func setDefaultBadge() {
    if userModel?.badges != nil {
      for activeBadge in userModel?.badges?.active ?? [] {
        if let index = userModel?.badges?.list?.firstIndex(where: { (badge) -> Bool in
          return badge.badgeId == activeBadge.badgeId
        }) {
          selectedIndexes.append(IndexPath(row: index, section: 0))
        }
      }
    }
    updateBadgePreview()
  }
  
  func setDefaultChatBubble() {
    if userModel?.chatBubbles?.defaultChatBubble != nil {
      if let index = userModel?.chatBubbles?.list?.firstIndex(where: { (bubble) -> Bool in
        return bubble.chatBubbleId == userModel?.chatBubbles?.defaultChatBubble?.chatBubbleId
      }) {
        selectedIndexes = [IndexPath(row: index, section: 0)]
      }
    }
    updateChatBubblePreview()
  }
  
  private func setBadgePreviewView(url: URL) {
    downloader.download(URLRequest(url: url), completion:  { response in
      if case .success(let image) = response.result {
        let inset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
        let newImage = image.resizableImage(withCapInsets: inset, resizingMode: .stretch)
        self.badgePreviewView.bubbleImage.image = newImage
      } else {
        self.badgePreviewView.bubbleImage.image = nil
      }
    })
  }
    
    private func setBubbleImage(_ model: ChatBubbleModel) {
        guard
            let topLeft = URL(string: model.topLeftImage ?? ""),
            let topMid = URL(string: model.topMidImage ?? ""),
            let topRight = URL(string: model.topRightImage ?? ""),
            let midLeft = URL(string: model.midLeftImage ?? ""),
            let midMid = URL(string: model.midMidImage ?? ""),
            let midRight = URL(string: model.midRightImage ?? ""),
            let bottomLeft = URL(string: model.bottomLeftImage ?? ""),
            let bottomMid = URL(string: model.bottomMidImage ?? ""),
            let bottomRight = URL(string: model.bottomRightImage ?? "")
        else { return }
        
        badgePreviewView.bubbleChatContainer.isHidden = false
        badgePreviewView.badgeContainer.isHidden = true
        badgePreviewView.topLeftBubbleImage.af.setImage(withURL: topLeft)
        badgePreviewView.topMidBubbleImage.af.setImage(withURL: topMid)
        badgePreviewView.topRightBubbleImage.af.setImage(withURL: topRight)
        badgePreviewView.midLeftBubbleImage.af.setImage(withURL: midLeft)
        badgePreviewView.midMidBubbleImage.af.setImage(withURL: midMid)
        badgePreviewView.midRightBubbleImage.af.setImage(withURL: midRight)
        badgePreviewView.bottomLeftBubbleImage.af.setImage(withURL: bottomLeft)
        badgePreviewView.bottomMidBubbleImage.af.setImage(withURL: bottomMid)
        badgePreviewView.bottomRightBubbleImage.af.setImage(withURL: bottomRight)
    }
}

extension ProfileManagerVC: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if profileManagerType == .Entrance {
      return userModel?.entranceEffects?.list?.count ?? 0
    } else if profileManagerType == .Frame {
      return userModel?.avatarFrames?.list?.count ?? 0
    } else if profileManagerType == .Badge {
      return userModel?.badges?.list?.count ?? 0
    } else if profileManagerType == .ChatBubble {
      return userModel?.chatBubbles?.list?.count ?? 0
    }
    return products.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileManagerCollectionCell", for: indexPath) as! ProfileManagerCollectionCell
    cell.selectedButton.isSelected = selectedIndexes.contains(indexPath)
    if [ProfileManagerType.Frame, ProfileManagerType.Entrance, ProfileManagerType.Badge, ProfileManagerType.ChatBubble].contains(profileManagerType) {
      cell.selectedButton.isHidden = false
    } else {
      cell.selectedButton.isHidden = true
      cell.containerView.layer.borderColor = (selectedIndexes.contains(indexPath) ? UIColor.init(hex: "2C004D") : UIColor.clear).cgColor
      cell.containerView.layer.borderWidth = 2
    }
    
    switch profileManagerType {
    case .Entrance:
      let entranceObject = userModel?.entranceEffects?.list?[indexPath.row]
      if let imageUrl = entranceObject?.image, let url = URL(string: imageUrl) {
        cell.imageView?.af.setImage(withURL: url)
      }
      break
    case .Frame:
      let frameObject = userModel?.avatarFrames?.list?[indexPath.row]
      if let imageUrl = frameObject?.avatarIcon, let url = URL(string: imageUrl) {
        cell.imageView?.af.setImage(withURL: url)
      }
      break
    case .Badge:
      let badgeObject = userModel?.badges?.list?[indexPath.row]
      if let imageUrl = badgeObject?.url, let url = URL(string: imageUrl) {
        cell.imageView?.af.setImage(withURL: url)
      }
      break
    case .ChatBubble:
      let badgeObject = userModel?.chatBubbles?.list?[indexPath.row]
      if let imageUrl = badgeObject?.mainImage, let url = URL(string: imageUrl) {
        cell.imageView?.af.setImage(withURL: url)
      }
      break
    default:
      let product = products[indexPath.row]
      if let imageUrl = product.thumb, let url = URL(string: imageUrl) {
        cell.imageView?.af.setImage(withURL: url)
      }
      cell.bind(product: product)

      if let id = product.id {
        cell.ownedButton.isHidden = !ownedIds.contains(id)
        cell.ownedView.isHidden = !ownedIds.contains(id)
      }
      break
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    if [ProfileManagerType.Badge].contains(profileManagerType) {
      let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "StreamerCollectionHeaderView", for: indexPath) as! StreamerCollectionHeaderView
      if kind == UICollectionView.elementKindSectionHeader {
        headerView.titleLabel.font = .defaultBoldFont(size: 14)
        headerView.titleLabel.text = "\(selectedIndexes.count)/3 Badge dipilih"
      }
      return headerView
    } else {
      return UICollectionReusableView()
    }
  }
}

extension ProfileManagerVC: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if isStore {
      let product = products[indexPath.row]
      if let id = product.id, ownedIds.contains(id) {
        return
      }
    }
    
    if selectedIndexes.contains(indexPath) {
      selectedIndexes.removeAll{ $0 == indexPath}
    } else {
      if [ProfileManagerType.Badge].contains(profileManagerType) {
        if selectedIndexes.count == 3 {
          return
        }
      } else {
        selectedIndexes.removeAll()
      }
      selectedIndexes.append(indexPath)
    }
    
    doneButton.isSelected = isStore && selectedIndexes.count == 0
    
    switch profileManagerType {
    case .Frame, .FrameStore:
      updateAvatarFramePreview()
      break
    case .Badge, .BadgeStore:
      updateBadgePreview()
      break
    case .ChatBubble, .ChatBubbleStore:
      updateChatBubblePreview()
      break
    default:
      break
    }
      
    collectionView.reloadData()
  }
}

extension ProfileManagerVC: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    var width = collectionView.frame.size.width - 40
    var height: CGFloat = 0.0
    switch profileManagerType {
    case .Frame, .Badge, .ChatBubble, .FrameStore, .BadgeStore, .ChatBubbleStore:
      width -= 21
      width /= 3
      height = width
      if [ProfileManagerType.FrameStore, ProfileManagerType.BadgeStore, ProfileManagerType.ChatBubbleStore].contains(profileManagerType) {
        height = (127/90) * width
      }
      return .init(width: width, height: height)
    case .Entrance, .EntranceStore:
      height = width*(111/361)
      if profileManagerType == .EntranceStore {
        height = width*(127/295)
      }
      return .init(width: width, height: height)
    default:
      return .zero
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    return [ProfileManagerType.Badge].contains(profileManagerType) ? .init(width: collectionView.frame.width, height: 60) : .zero
  }
}
