//
//  ProfileVC.swift
//  sugarlive
//
//  Created by msi on 11/09/21.
//

import UIKit
import Alamofire
import Popover

class ProfileVC: BaseTableVC {
    
    enum ProfileType {
        case Current
        case Other
    }
    
    var profileType: ProfileType = .Current
    
    private let profileMenus = ["Avatar Frame", "Entrance effect", "Badge", "Chat Bubble", "Top Fans", "My Wallet"]
    private let viewMenus = ["Bio", "Birthday"]
    
    private var userModel: UserModel?
    private var topSpender: [TopSpenderModel]?
    var topFans = ""
    var userId = ""
    var isHost = false
    var isPresentedAsModal = false
    private var refreshControl = UIRefreshControl()
    let popTip: Popover = {
      let options = [
        .type(.down),
        .cornerRadius(4),
        .animationIn(0.3),
        .blackOverlayColor(UIColor.clear),
        .arrowSize(CGSize.zero),
        .borderColor(UIColor.lightGray)
      ] as [PopoverOption]
      return Popover(options: options, showHandler: nil, dismissHandler: nil)
    }()
    lazy var popTipView = PopTipView.nib(withType: PopTipView.self)
    var settingBtn = UIButton(type: .custom)
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(setupProfile), for: .valueChanged)
        tableView.refreshControl = refreshControl

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNav()
        setupProfile()
    }
    
    override func setupLayout() {
        setupTable()
    }
    
    override func setupNav() {
        
        let settingButon = UIButton(type: .custom)
        settingButon.setImage(UIImage(named: profileType == .Current ? "ic_setting_white" : "ic_close_white"), for: .normal)
        settingButon.setTitle("", for: .normal)
        settingButon.addTarget(self, action: profileType == .Current ? #selector(settingAction(_:)) : #selector(dismissAction(_:)), for: .touchUpInside)
        let settingBarItem = UIBarButtonItem.init(customView: settingButon)
        settingBarItem.customView?.widthAnchor.constraint(equalToConstant: 30).isActive = true
        settingBarItem.customView?.heightAnchor.constraint(equalToConstant: 30).isActive = true
        if let mainTabVC = self.tabBarController as? MainTabVC {
            mainTabVC.navigationItem.rightBarButtonItems = []
            mainTabVC.navigationItem.rightBarButtonItem = settingBarItem
            mainTabVC.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: "Profile Details"))
        } else if profileType == .Other {
            
            settingBtn.setImage(UIImage(named: "ic_triple_dot_white"), for: .normal)
            settingBtn.setTitle("", for: .normal)
            settingBtn.addTarget(self, action: #selector(showPopTip), for: .touchUpInside)
            
            let settingBarItem = UIBarButtonItem.init(customView: settingBtn)
            settingBarItem.customView?.widthAnchor.constraint(equalToConstant: 30).isActive = true
            settingBarItem.customView?.heightAnchor.constraint(equalToConstant: 30).isActive = true
            self.navigationItem.rightBarButtonItem = settingBarItem
        }
        
        if isPresentedAsModal {
            let dismissButton = UIBarButtonItem(image: UIImage(named: "ic_back_white"), style: .plain, target: self, action: #selector(dismissNavigationControllerAction))
            navigationItem.leftBarButtonItem = dismissButton
        }
    }
    
    @objc func showPopTip() {
        popTipView.editString = "Block user"
        popTipView.destructionButton.isHidden = true
        popTipView.editTapped = {(sender) in
            self.popTip.dismiss()
            self.doBlock(userID: self.userModel?.userId ?? "")
        }
        popTipView.frame = .init(x: 0, y: 0, width: 150, height: 40)
        popTip.show(popTipView, fromView: settingBtn)
    }
    
    private func setupTable() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.sectionFooterHeight = 0.0
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.01))
        tableView.separatorStyle = .none
        
        cells = ["ProfileDetailCell", "ProfileCell"]
    }
    
    @objc private func setupProfile() {
        switch profileType {
        case .Current:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
//            if AuthManager.shared.currentUser == nil {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
//            } else {
            self.userModel = AuthManager.shared.currentUser
            self.getTopSpender(userId: self.userModel?.userId ?? "")
            self.tableView.reloadData()
//            }
            self.refreshControl.endRefreshing()
            break
        case .Other:
            getOtherProfile()
            break
        }
    }
    
    //  MARK: Service
    
    private func doFollow(userId: String) {
        guard let request = UserRequestModel(JSON: ["userID": userId]) else { return }
        provider.request(.Follow(request: request), successModel: SuccessResponseModel.self) { (result) in
            self.setupProfile()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func doUnfollow(userId: String) {
        guard let request = UserRequestModel(JSON: ["userID": userId]) else { return }
        provider.request(.Unfollow(request: request), successModel: SuccessResponseModel.self) { [weak self] (result) in
            guard let self = self else { return }
            self.setupProfile()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func doBlock(userID: String) {
      guard let request = UserRequestModel(JSON: ["userID": userID]) else { return }
      provider.request(.BlockUser(request: request), successModel: SuccessResponseModel.self) { (result) in
          self.routeToWarningModal(message: "This user has been successfully blocked")
      } failure: { (error) in
          self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
      }
    }
    
    private func getOtherProfile() {
        tableView.isHidden = true
        provider.request(.UserProfile(identifier: userId), successModel: UserResponseModel.self) { (result) in
            self.userModel = result.attributes
            self.isHost = self.userModel?.verified == true
            if self.isHost {
                self.getTopSpender(userId: self.userModel?.userId ?? "")
            }
            self.tableView.isHidden = false
            self.tableView.reloadData()
        } failure: { (error) in
            self.tableView.isHidden = false
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getTopSpender(userId: String) {
        provider.request(.TopSpender(identifier: userId), successModel: TopSpenderResponseModel.self) { (result) in
            self.topSpender = result.total
            self.tableView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getUserSocMed() {
      provider.request(.SocMedAccount(identifier: userModel?.userId ?? ""), successModel: SocialMediaResponseModel.self) { (result) in
        self.userModel?.socialMedias = result.attributes ?? []
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
    
    private func getUserSocMedOthers() {
      provider.request(.SocMedAccount(identifier: userModel?.userId ?? ""), successModel: SocialMediaResponseModel.self) { (result) in
        self.userModel?.socialMedias = result.attributes ?? []
        self.presentSocialMediaModal()
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
    
    //  MARK: Route
    func routeToProfileVC(userID: String) {
        let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
        vc.profileType = .Other
        vc.userId = userID
        vc.navigationItem.titleView = navLabel(title: "Profile Details")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentSocialMediaModal() {
        let vc = SocMedModalVC(for: userModel?.socialMedias ?? [])
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = self
        present(vc, animated: true)
    }
    
    //  MARK: Action
    @IBAction func settingAction(_ sender: Any) {
        routeToTableVC(type: .Setting)
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func dismissNavigationControllerAction() {
        navigationController?.dismiss(animated: true)
    }
    
    //  MARK: Custom
    override func handleUpdateProfile(_ sender: Any) {
        getMyProfile {
            self.userModel = AuthManager.shared.currentUser
            self.tableView.reloadData()
        }
    }
}

extension ProfileVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if profileType == .Current || !isHost {
            return 2
        } else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return profileType == .Current ? profileMenus.count : viewMenus.count
        case 2:
            return topSpender?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let profileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        profileCell.separatorInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let profileDetailCell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailCell", for: indexPath) as! ProfileDetailCell
                profileDetailCell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
                profileDetailCell.selectionStyle = .none
                profileDetailCell.editProfileButton.isHidden = profileType == .Other
                profileDetailCell.editTapped = {(sender) in
                    self.routeToTableActionVC(type: .EditProfile)
                }
                profileDetailCell.followingTapped = {(sender) in
                    let destinationVC = TableWithoutAction(nibName: "TableWithoutAction", bundle: nil)
                    destinationVC.tableType = .Following
                    destinationVC.userModel = self.userModel
                    self.navigationController?.pushViewController(destinationVC, animated: true)
                }
                
                profileDetailCell.followerTapped = {(sender) in
                    let destinationVC = TableWithoutAction(nibName: "TableWithoutAction", bundle: nil)
                    destinationVC.tableType = .Follower
                    destinationVC.userModel = self.userModel
                    self.navigationController?.pushViewController(destinationVC, animated: true)
                }
                if let user = userModel {
                    profileDetailCell.bind(user: user)
                    if profileType != .Current {
                        profileDetailCell.candyContainer.isHidden = true
                        profileDetailCell.followMessageContainer.isHidden = false
                        
                        if user.isFollowing {
                            if let image = UIImage(named: "ic_minus_purple") {
                                profileDetailCell.followBtn.setImage(image, for: .normal)
                            }
                            profileDetailCell.followBtn.setTitle("  Unfollow", for: .normal)
                            profileDetailCell.followUser = {
                                self.doUnfollow(userId: user.userId ?? "")
                            }
                        } else {
                            profileDetailCell.followUser = {
                                self.doFollow(userId: user.userId ?? "")
                            }
                            if let image = UIImage(named: "ic_plus_purple") {
                                profileDetailCell.followBtn.setImage(image, for: .normal)
                            }
                            profileDetailCell.followBtn.setTitle("  Follow", for: .normal)
                        }
                        
                        profileDetailCell.messageUser = {
                            let vc = DirectChatVC(nibName: "DirectChatVC", bundle: nil)
                            vc.receiverUserId = user.userId ?? ""
                            vc.receiverUserName = user.fullname ?? ""
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    } else {
                        profileDetailCell.followMessageContainer.isHidden = true
                    }
                }
                return profileDetailCell
            case 1:
                profileCell.profileCellType = .SocialMedia
                if let username = userModel?.socialMedia?.userName, let type = userModel?.socialMedia?.socialMedia {
                    let button = UIButton(type: .custom)
                    button.isUserInteractionEnabled = false
                    button.titleEdgeInsets = .init(top: 0, left: 20, bottom: 0, right: 0)
                    let str = "\([SocialMedia.Instagram, SocialMedia.Twitter].contains(type) ? "@" : "")\(username)"
                    
                    let inset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
                    let image =  UIImage(named: "ic_\(type.rawValue.lowercased())_account")?.resizableImage(withCapInsets: inset, resizingMode: .stretch)
                    
                    button.setBackgroundImage(image, for: .normal)
                    button.setAttributedTitle(str.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12),
                                                                            NSAttributedString.Key.foregroundColor: UIColor.white]),
                                              for: .normal)
                    profileCell.badgeStackView.addArrangedSubview(button)
                    button.snp.makeConstraints { (maker) in
                        maker.width.equalTo(str.width(withConstrainedHeight: 40, font: UIFont.defaultRegularFont(size: 12)) + 50)
                    }
                }
                profileCell.subtitleLabel.text = "My social media"
            default:
                break
            }
        case 1:
            if profileType == .Current {
                profileCell.profileCellType = .NoBadge
                profileCell.subtitleLabel.text = profileMenus[indexPath.row]
                switch indexPath.row {
                case 0:
                    if userModel?.avatarFrames?.defaultFrame != nil {
                        profileCell.nextButton.isHidden = false
                        profileCell.badgeStackView.superview?.isHidden = false
                        let imageView = UIImageView()
                        if let imageUrl = userModel?.avatarFrames?.defaultFrame?.avatarIcon, let url = URL(string: imageUrl) {
                            profileCell.badgeStackView.addArrangedSubview(imageView)
                            imageView.snp.makeConstraints { (maker) in
                                maker.height.width.equalTo(33)
                            }
                            imageView.af.setImage(withURL: url)
                        }
                    }
                    break
                case 1:
                    if userModel?.entranceEffects?.defaultEffect != nil {
                        profileCell.nextButton.isHidden = false
                        profileCell.badgeStackView.superview?.isHidden = false
                        let imageView = UIImageView()
                        if let imageUrl = userModel?.entranceEffects?.defaultEffect?.image, let url = URL(string: imageUrl) {
                            profileCell.badgeStackView.addArrangedSubview(imageView)
                            imageView.snp.makeConstraints { (maker) in
                                maker.height.equalTo(32)
                                maker.width.equalTo(96)
                            }
                            imageView.af.setImage(withURL: url)
                        }
                    }
                    break
                case 2:
                    if let badges = userModel?.badges?.active {
                        profileCell.nextButton.isHidden = false
                        profileCell.badgeStackView.superview?.isHidden = false
                        for badge in badges {
                            let imageView = UIImageView()
                            if let imageUrl = badge.url, let url = URL(string: imageUrl) {
                                profileCell.badgeStackView.addArrangedSubview(imageView)
                                imageView.snp.makeConstraints { (maker) in
                                    maker.height.width.equalTo(36)
                                }
                                imageView.af.setImage(withURL: url)
                            }
                        }
                    }
                    break
                case 3:
                    if userModel?.chatBubbles?.defaultChatBubble != nil {
                        profileCell.nextButton.isHidden = false
                        let imageView = UIImageView()
                        if let imageUrl = userModel?.chatBubbles?.defaultChatBubble?.mainImage, let url = URL(string: imageUrl) {
                            profileCell.badgeStackView.addArrangedSubview(imageView)
                            profileCell.badgeStackView.superview?.isHidden = false
                            imageView.snp.makeConstraints { (maker) in
                                maker.width.height.equalTo(38)
                                //                maker.width.equalTo(58)
                            }
                            imageView.af.setImage(withURL: url)
                        }
                    }
                    break
                case 4:
                    profileCell.profileCellType = .Fans
                    profileCell.subtitleLabel.text = profileMenus[indexPath.row]
                    
                    var listSpender = ""
                    var idx = 0
                    for data in topSpender ?? [TopSpenderModel]() {
                        listSpender = listSpender + (data.fullname ?? "")
                        idx += 1
                        if idx >= 3 && topSpender?.count ?? 0 > 3 {
                            break
                        }
                        listSpender = listSpender + ", "
                    }
                    profileCell.rightLabel.text = listSpender
                    
                    break
                case 5:
                    break
                default:
                    break
                }
            } else {
                profileCell.profileCellType = .Detail
                profileCell.topLabel.text = viewMenus[indexPath.row]
                switch indexPath.row {
                case 0:
                    profileCell.subtitleLabel.text = userModel?.bio
                    break
                case 1:
                    profileCell.subtitleLabel.text = userModel?.birthday
                    break
                default:
                    break
                }
            }
        case 2:
            let topSpenderData = topSpender?[indexPath.row]
            profileCell.profileCellType = .Profile
            profileCell.subtitleLabel.text = topSpenderData?.fullname
            profileCell.profileImageView.image = UIImage(named: "ic_default_profile")
            
            if let imageUrl = topSpenderData?.coverPicture, let url = URL(string: imageUrl) {
                profileCell.profileImageView.af.setImage(withURL: url)
            }
            
        default:
            break
        }
        return profileCell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 1 && profileType == .Current) || (section == 2 && profileType == .Other) {
            return createBasicTableHeader(title: profileType == .Current ? "PROFILE DETAILS" : "TOP SPENDER")
        } else {
            return UIView()
        }
    }
}

extension ProfileVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 1 && profileType == .Current) || (section == 2 && profileType == .Other) ? 40 : 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 1:
                if profileType == .Current {
                    self.routeToTableActionVC(type: .SocialMedia)
                } else {
                    getUserSocMedOthers()
                }
                break
            default:
                break
            }
            break
        case 1:
            if profileType == .Current {
                if indexPath.row < 4 {
                    if let currentUser = userModel {
                        routeToProfileManagerVC(type: ProfileManagerType.init(rawValue: indexPath.row) ?? .Default, userModel: currentUser)
                    }
                } else if indexPath.row == 5 {
                    routeToTableVC(type: .Wallet)
                }
            }
            break
        case 2:
            let topSpenderData = topSpender?[indexPath.row]
            routeToProfileVC(userID: topSpenderData?.userId ?? "")
            break
        default:
            break
        }
    }
}

extension ProfileVC: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        SlideOverPresentationController(presentedViewController: presented, presenting: presenting)
    }
}
