//
//  RegisterVC.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit

enum RegisterType {
  case Host
  case Phone
  case Registered
  case ForgotPassword
  case NewPassword
  
  func title() -> String {
    switch self {
    case .Host:
      return "Join as host"
    case .Phone:
      return "Login with phone number"
    case .Registered:
      return "You're almost there !"
    case .ForgotPassword:
      return "Forgot Password"
    default:
      return ""
    }
  }
  
  func actionTitle() -> String {
    switch self {
    case .Host:
      return "Sign Up"
    case .Phone:
      return "Login"
    case .Registered:
      return "Contact admin"
    case .ForgotPassword:
      return "Login"
    case .NewPassword:
      return "Change"
    }
  }
  
  func cancelTitle() -> String {
    switch self {
    case .Registered:
      return "Skip"
    default:
      return "Cancel"
    }
  }
}

class RegisterVC: BaseStackVC {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subtitleLabel: UILabel!
  @IBOutlet weak var errorLabel: UILabel!
  @IBOutlet weak var termLabel: UILabel!
  @IBOutlet weak var actionButton: UIButton!
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var forgotPasswordButton: UIButton!
  
  var registerType: RegisterType = .Host
  private let authManager = AuthManager.shared
  
  lazy var nameField: RegisterFieldView = {
    let nameField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    nameField.textField.placeholder = "Name"
    nameField.textField.keyboardInputType = .Alphabet
    nameField.textField.minLength = 4
    nameField.rightIcon.superview?.isHidden = true
    nameField.leftIcon.image = UIImage(named: "ic_user_black")
    nameField.containerView.layer.cornerRadius = 9
    nameField.containerView.layer.masksToBounds = true
    nameField.textField.font = .defaultRegularFont(size:12)
    nameField.textField.textFieldHandler = {(sender) in
      switch sender {
      case .Min:
        self.showErrorMessage(message: "Min. name \(nameField.textField.minLength ?? 0) chars")
        break
      case .Valid:
        self.showErrorMessage(message: "")
        break
      default:
        break
      }
    }
    return nameField
  }()
  lazy var phoneField: RegisterFieldView = {
    let phoneField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    phoneField.textField.placeholder = "Phone number"
    phoneField.textField.keyboardInputType = .Phone
    phoneField.textField.containString = ["08", "628"]
    phoneField.textField.minLength = 10
    phoneField.textField.maxLength = 14
    phoneField.rightIcon.superview?.isHidden = true
    phoneField.leftIcon.image = UIImage(named: "ic_phone_black")
    phoneField.containerView.layer.cornerRadius = 9
    phoneField.containerView.layer.masksToBounds = true
    phoneField.textField.font = .defaultRegularFont(size:12)
    phoneField.textField.textFieldHandler = {(sender) in
      switch sender {
      case .Min:
        self.showErrorMessage(message: "Min. phone \(phoneField.textField.minLength ?? 0) chars")
        break
      case .Prefix:
        self.showErrorMessage(message: "Prefix phone must \(phoneField.textField.containString?.joined(separator: ", ") ?? "")")
        break
      case .Valid:
        self.showErrorMessage(message: "")
        break
      default:
        break
      }
    }
    return phoneField
  }()
  lazy var passwordField: RegisterFieldView = {
    let passwordField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    passwordField.textField.placeholder = "Password"
    passwordField.textField.isSecureTextEntry = true
    passwordField.textField.minLength = 8
    passwordField.textField.maxLength = 15
    passwordField.leftIcon.image = UIImage(named: "ic_lock_black")
    passwordField.rightIcon.image = UIImage(named: "ic_eye_black")
    passwordField.containerView.layer.cornerRadius = 9
    passwordField.containerView.layer.masksToBounds = true
    passwordField.textField.font = .defaultRegularFont(size:12)
    passwordField.rightTapped = {(sender) in
      passwordField.textField.isSecureTextEntry = !passwordField.textField.isSecureTextEntry
    }
    passwordField.textField.textFieldHandler = {(sender) in
      switch sender {
      case .Min:
        self.showErrorMessage(message: "Min. password \(passwordField.textField.minLength ?? 0) chars")
        break
      case .End:
        self.confirmField.textField.containString = [passwordField.textField.text ?? ""]
        break
      case .Valid:
        self.showErrorMessage(message: "")
        break
      default:
        break
      }
    }
    return passwordField
  }()
  lazy var confirmField: RegisterFieldView = {
    let confirmField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    confirmField.textField.placeholder = "Confirm password"
    confirmField.textField.isSecureTextEntry = true
    confirmField.textField.minLength = 8
    confirmField.textField.maxLength = 15
    confirmField.leftIcon.image = UIImage(named: "ic_lock_black")
    confirmField.rightIcon.image = UIImage(named: "ic_eye_black")
    confirmField.containerView.layer.cornerRadius = 9
    confirmField.containerView.layer.masksToBounds = true
    confirmField.textField.font = .defaultRegularFont(size:12)
    confirmField.rightTapped = {(sender) in
      confirmField.textField.isSecureTextEntry = !confirmField.textField.isSecureTextEntry
    }
    confirmField.textField.textFieldHandler = {(sender) in
      switch sender {
      case .Min:
        self.showErrorMessage(message: "Min. password \(confirmField.textField.minLength ?? 0) chars")
        break
      case .Prefix:
        self.showErrorMessage(message: "Confirm password not equal")
        break
      case .Valid:
        self.showErrorMessage(message: "")
        break
      default:
        break
      }
    }
    return confirmField
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func setupLayout() {
    titleLabel.font = .defaultBoldFont(size: 16)
    titleLabel.textColor = .PRIMARY_2
    titleLabel.text = registerType.title()
    
    subtitleLabel.font = .defaultRegularFont(size: 12)
    subtitleLabel.textColor = .init(hex: "6D6D6D")
    
    errorLabel.font = .defaultRegularFont(size: 12)
    errorLabel.textColor = .NEGATIVE_TEXT
    
    var checkAttachment = NSTextAttachment()
    if #available(iOS 13.0, *) {
      checkAttachment = NSTextAttachment(image: UIImage(named: "ic_check_green")!)
    } else {
      checkAttachment.image = UIImage(named: "ic_check_green")
    }
    let termAttstr = NSMutableAttributedString(attachment: checkAttachment)
    let termTextAttstr = NSMutableAttributedString(attributedString: "  By joining you agree to our Terms of Use".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 10)]))
    termAttstr.append(termTextAttstr)
    termLabel.attributedText = termAttstr
    termLabel.isUserInteractionEnabled = true
    let termGesture = UITapGestureRecognizer(target: self, action: #selector(termAction(_:)))
    termLabel.addGestureRecognizer(termGesture)
    termLabel.superview?.isHidden = ![RegisterType.Host, RegisterType.Phone].contains(registerType)
    
    forgotPasswordButton.setAttributedTitle("forgot password ?".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 10),
                                                                                          NSAttributedString.Key.foregroundColor: UIColor.black]),
                                            for: .normal)
    forgotPasswordButton.superview?.isHidden = ![RegisterType.Phone].contains(registerType)
    
    actionButton.layer.cornerRadius = 9
    actionButton.layer.masksToBounds = true
    actionButton.setAttributedTitle(registerType.actionTitle().toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14),
                                                                                         NSAttributedString.Key.foregroundColor: UIColor.white]),
                                    for: .normal)
    actionButton.setBackgroundImage(UIImage(color: UIColor.PRIMARY_1.withAlphaComponent(0.3)), for: .disabled)
    actionButton.setBackgroundImage(UIImage(color: UIColor.PRIMARY_1), for: .normal)
    actionButton.isEnabled = registerType == .Registered
    
    cancelButton.backgroundColor = .clear
    cancelButton.setAttributedTitle(registerType.cancelTitle().toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14),
                                                                                         NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]),
                                    for: .normal)
    
    let height = (Constant.screenSize.width - 70)*(50/329)
    switch registerType {
    case .Host:
      mainStackView.insertArrangedSubview(nameField, at: 1)
      mainStackView.insertArrangedSubview(phoneField, at: 2)
      mainStackView.insertArrangedSubview(passwordField, at: 3)
      mainStackView.insertArrangedSubview(confirmField, at: 4)
      nameField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      phoneField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      passwordField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      confirmField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      break
    case .Phone:
      mainStackView.insertArrangedSubview(phoneField, at: 1)
      mainStackView.insertArrangedSubview(passwordField, at: 2)
      phoneField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      passwordField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      break
    case .ForgotPassword:
      subtitleLabel.superview?.isHidden = false
      subtitleLabel.text = "We will send OTP code to your phone number to reset your password."
      mainStackView.insertArrangedSubview(phoneField, at: 2)
      phoneField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      break
    case .NewPassword:
      mainStackView.insertArrangedSubview(passwordField, at: 1)
      mainStackView.insertArrangedSubview(confirmField, at: 2)
      passwordField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      confirmField.snp.makeConstraints { (maker) in
        maker.height.equalTo(height)
      }
      break
    default:
      break
    }
    
    if registerType == .Registered {
      let errAttstr = NSMutableAttributedString(attributedString: "Please contact our admin to confirm your trial as a host.".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]))
      errorLabel.attributedText = errAttstr
    } else {
      errorLabel.font = .defaultRegularFont(size: 12)
      errorLabel.textColor = .NEGATIVE_TEXT
      errorLabel.text = ""
    }
    
    containerView.layer.cornerRadius = 20
    containerView.layer.masksToBounds = true
  }
  
//  MARK: Service
  private func doRegister() {
    let json: [String: Any] = [
      "phone": phoneField.textField.text ?? "",
      "fullname": nameField.textField.text ?? "",
      "password": confirmField.textField.text ?? "",
      "deviceToken": authManager.deviceToken,
      "firebaseToken": authManager.FCMToken,
      "device": Constant.currentDevice.type.rawValue,
      "operatingSystem": "\(Constant.currentDevice.systemName) \(Constant.currentDevice.systemVersion)",
      "preferredNetwork": getConnectionType()
    ]
    guard let requestModel = RegisterRequestModel.init(JSON: json) else { return }
    
    var registerModel = RegisterModel(request: requestModel, response: nil)
    
    provider.request(.Register(request: requestModel), successModel: RegisterResponseModel.self) { (result) in
        UserDefaults.standard.set(true, forKey: "phoneLogin")
      registerModel.response = result
      self.authManager.registerModel = registerModel
      self.dismissAllModal {
//        self.routeToRegister(registerType: .Registered)
        self.routeToOTP(otpType: .Register)
      }
    } failure: { (error) in
      self.showErrorMessage(message: error.messages.joined(separator: "\n"))
    }
  }
  
  private func doLoginPhone() {
    let json: [String: Any] = [
      "phone": phoneField.textField.text ?? "",
      "password": passwordField.textField.text ?? "",
      "deviceToken": authManager.deviceToken,
      "firebaseToken": authManager.FCMToken,
      "device": Constant.currentDevice.type.rawValue,
      "operatingSystem": "\(Constant.currentDevice.systemName) \(Constant.currentDevice.systemVersion)",
      "preferredNetwork": getConnectionType()
    ]
    
    guard let requestModel = RegisterRequestModel.init(JSON: json) else { return }
    
    var registerModel = RegisterModel(request: requestModel, response: nil)
    
    provider.request(.PhoneLogin(request: requestModel), successModel: RegisterResponseModel.self) { (result) in
      registerModel.response = result
      registerModel.response?.attributes?.cacheToken()
        UserDefaults.standard.set(true, forKey: "phoneLogin")
        UserDefaults.standard.set(self.passwordField.textField.text ?? "", forKey: "phoneLoginPass")
      self.dismissAllModal {
        self.getMyProfile {}
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
      }
    } failure: { (error) in
      self.showErrorMessage(message: error.messages.joined(separator: "\n"))
      if error.status == 400 && error.detail is String && error.detail as! String == "You need to verify your phone number" {
        let response = RegisterResponseModel()
        response.id = error.code
        registerModel.response = response
        self.authManager.registerModel = registerModel
        self.dismissAllModal {
          self.routeToOTP(otpType: .Register)
        }
      }
    }
  }
  
  private func doChangePassword() {
    if let request = ForgotPasswordRequestModel(JSON: ["phone": authManager.registerModel?.request?.phone ?? "", "code": authManager.otpRequestModel?.code ?? "", "password": confirmField.textField.text ?? ""]) {
      provider.request(.ForgotPassword(request: request), successModel: SuccessResponseModel.self) { (result) in
        self.routeToWarningModal(message: "Success change password") {
          self.dismissAllModal {
            self.routeToHome()
          }
        }
      } failure: { (error) in
        self.showErrorMessage(message: error.messages.joined(separator: "\n"))
      }
    }
  }
  
  private func doContactAdmin() {
    let urlWA = "whatsapp://send?phone=\(Constant.AdminPhoneNumber)"
    openURL(string: urlWA)
  }
  
//  MARK: Action
  @IBAction func authAction(_ sender: Any) {
    if !doValidate() { return }
    switch registerType {
    case .Host:
      doRegister()
      break
    case .Phone:
      doLoginPhone()
      break
    case .ForgotPassword:
      doForgotPassword()
      dismissAllModal {
        self.routeToOTP(otpType: .Forgot)
      }
      break
    case .NewPassword:
      doChangePassword()
      break
    default:
      doContactAdmin()
      break
    }
  }
  
  @IBAction func cancelAction(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func termAction(_ sender: Any) {
    
  }
  
  @IBAction func forgotAction(_ sender: Any) {
    self.routeToRegister(registerType: .ForgotPassword)
  }
  
//  MARK: Custom
  func showErrorMessage(message: String) {
    self.errorLabel.text = message
    self.actionButton.isEnabled = self.doValidate()
  }
  
  func doValidate() -> Bool {
    switch registerType {
    case .Host:
      return ((nameField.textField.text?.count ?? 0) > 0 && (phoneField.textField.text?.count ?? 0) > 0 && ((passwordField.textField.text?.count ?? 0) >> 0) > 0 && (confirmField.textField.text?.count ?? 0) > 0 && (errorLabel.text?.count ?? 0) == 0)
    case .Phone:
      return ((phoneField.textField.text?.count ?? 0) > 0 && ((passwordField.textField.text?.count ?? 0) >> 0) > 0 && (errorLabel.text?.count ?? 0) == 0)
    default:
      return true
    }
  }
  
  func doForgotPassword() {
    let json: [String: Any] = [
      "phone": phoneField.textField.text ?? "",
      "password": "",
      "deviceToken": authManager.deviceToken,
      "firebaseToken": authManager.FCMToken,
      "device": Constant.currentDevice.type.rawValue,
      "operatingSystem": "\(Constant.currentDevice.systemName) \(Constant.currentDevice.systemVersion)",
      "preferredNetwork": getConnectionType()
    ]
    
    guard let requestModel = RegisterRequestModel.init(JSON: json) else { return }
    let registerModel = RegisterModel(request: requestModel, response: nil)
    authManager.registerModel = registerModel
  }
}
