//
//  ReportVC.swift
//  sugarlive
//
//  Created by cleanmac on 21/11/21.
//

import UIKit

class ReportVC: BaseVC {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var reasonContainerView: UIView!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var reasonButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    // MARK: - Variables
    private let reasonList: [String] = [
        "Nudity/sexual content",
        "Hate speech",
        "Promote racialism",
        "Provocative",
        "Unacceptable behaviour",
    ]
    
    private var reason: String = "" {
        didSet {
            reasonLabel.text = reason
            lock = false
        }
    }
    private var lock: Bool = true {
        didSet {
            setSubmitButtonState(enabled: !lock)
        }
    }
    
    var userID: String?
    var liveID: String?

    // MARK: - Overriden Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        getOtherProfile()
    }
    
    override func setupNav() {
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_back_white"), style: .plain, target: self, action: #selector(dismissAction))
        let navTitle = UIBarButtonItem(customView: navLabel(title: "Report user"))
        navigationItem.leftBarButtonItems = [backButton, navTitle]
        navigationController?.navigationBar.barTintColor = .PRIMARY_1
        navigationController?.navigationBar.tintColor = .white
    }
    
    override func setupLayout() {
        reasonContainerView.layer.cornerRadius = 10
        reasonContainerView.layer.borderWidth = 1
        reasonContainerView.layer.borderColor = UIColor(hex: "#D8D8D8").cgColor
        
        reasonButton.setTitle("", for: .normal)
        reasonButton.setTitle("", for: .highlighted)
        
        setSubmitButtonState(enabled: !lock)
    }
    
    // MARK: - UI Setups
    private func setSubmitButtonState(enabled: Bool) {
        if enabled {
            submitButton.backgroundColor = UIColor(hex: "#2C004D")
            submitButton.setAttributedTitle("Submit report".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        } else {
            submitButton.backgroundColor = UIColor(hex: "#E9E9E9")
            submitButton.setAttributedTitle("Submit report".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14), NSAttributedString.Key.foregroundColor: UIColor(hex: "#6D6D6D")]), for: .normal)
        }
    }
    
    // MARK: - Actions
    @objc private func dismissAction() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reasonAction(_ sender: Any) {
        let vc = ModalSheetVC(title: "Choose main reason", delegate: self)
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if !lock {
            submitReport()
        }
    }
    
    // MARK: - API Calls
    private func getOtherProfile() {
        if let userID = userID {
            provider.request(.UserProfile(identifier: userID), successModel: UserResponseModel.self) { [weak self] (result) in
                guard let self = self else { return }
                self.liveID = result.attributes?.liveId
            } failure: { (error) in
                print(error.messages.joined(separator: "\n"))
            }
        }
    }
    
    private func submitReport() {
        guard let request = LiveReportRequestModel(JSON: ["liveId": userID ?? "", "reason": reason, ]) else { return }
        provider.request(.ReportLive(request: request), successModel: SuccessResponseModel.self) { (result) in
            self.routeToWarningModal(message: "Your report has been successfully sent to Sugarlive")
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }

}

// MARK: - Modal Sheet delegate
extension ReportVC: ModalSheetDelegate {
    func modalSheet(_ modalSheet: ModalSheetVC, itemForRowAt indexPath: IndexPath) -> String {
        reasonList[indexPath.row]
    }
    
    func modalSheet(_ modalSheet: ModalSheetVC, didSelectRowAt indexPath: IndexPath) {
        reason = reasonList[indexPath.row]
    }
    
    func numberOfItems(in modalSheet: ModalSheetVC) -> Int {
        reasonList.count
    }
    
}
