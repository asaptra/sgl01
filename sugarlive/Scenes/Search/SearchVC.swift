//
//  SearchVC.swift
//  sugarlive
//
//  Created by dodets on 01/10/21.
//

import UIKit
import ObjectMapper

class SearchVC: BaseTableVC {
    
    private var searchBarView: SearchBarView!
    
    private var searchMemberResponseModel: SearchMemberResponseModel?
    
    var searchKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupNav() {
        navigationItem.leftItemsSupplementBackButton = true
        searchBarView = SearchBarView(frame: CGRect(x: 0, y: 0, width: navigationController?.navigationBar.frame.size.width ?? 0.0, height: 26))
        searchBarView.delegate = self
        navigationItem.titleView = searchBarView
    }
    
    override func setupLayout() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        cells = ["SearchListCell"]
    }
    
    //  MARK: Service
    private func searchMember() {
        guard let request = SearchRequestModel(JSON: ["keyword": searchKey]) else { return }
        
        provider.request(.SearchMember(request: request), successModel: SearchMemberResponseModel.self) { (result) in
            self.searchMemberResponseModel = result
            self.tableView.reloadData()
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    //  MARK: Action
    @IBAction func searchUser(_ sender: Any) {
        searchMember()
    }
}

extension SearchVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchMemberResponseModel?.list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchListCell", for: indexPath) as! SearchListCell
        let data = searchMemberResponseModel?.list?[indexPath.row]
        cell.nameLabel.text = data?.fullname ?? ""
        cell.addressLbl.text = data?.hometown
        
        cell.profileImageView.image = UIImage.init(named: "ic_default_profile")
        if let imageUrl = data?.coverPicture, let url = URL(string: imageUrl) {
            cell.profileImageView?.af.setImage(withURL: url)
        }
        cell.levelLbl.text = "Lv. \(data?.userLevel?.level ?? 0)"
        cell.levelView.backgroundColor = UIColor.init(hex: data?.userLevel?.color ?? "6964D8")
        cell.levelLbl.textColor = UIColor.init(hex: data?.userLevel?.colorText ?? "ffffff")
        return cell
    }
}

extension SearchVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let data = searchMemberResponseModel?.list?[indexPath.row]
        let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
        vc.profileType = .Other
        vc.userId = data?.userId ?? ""
        vc.navigationItem.titleView = navLabel(title: "Profile Details")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SearchVC: SearchBarViewDelegate {
    func textFieldDidChanged(_ textField: UITextField) {
        searchKey = textField.text ?? ""
        if searchKey.count >= 3 {
            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchUser(_:)), userInfo: nil, repeats: false)
        }
    }
}

