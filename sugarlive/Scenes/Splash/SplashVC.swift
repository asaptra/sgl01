//
//  SplashVC.swift
//  sugarlive
//
//  Created by msi on 27/09/21.
//

import UIKit

class SplashVC: BaseVC {
  
  @IBOutlet weak var backgroundImage: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    getBackgroundSplash()
    if Constant.isLogin {
      self.getMyProfile {}
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
//  MARK: Service
  private func getBackgroundSplash() {
    provider.request(.Splash, successModel: SplashScreenResponseModel.self) { (result) in
      if let urlString = result.attributes?.current?.imageUrl, let url = URL(string: urlString) {
        self.backgroundImage.af.setImage(withURL: url)
      } else {
        self.backgroundImage.image = UIImage(named: "bg_splash")
      }
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
}
