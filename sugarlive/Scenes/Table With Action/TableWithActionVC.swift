//
//  TableWithActionVC.swift
//  sugarlive
//
//  Created by msi on 15/09/21.
//

import UIKit
import Fusuma

class TableWithActionVC: BaseTableVC {
  
  enum UploadPictureType {
    case UserProfile
    case UserCover
    case Feedback
  }
  
  var tableType: TableType = .Default
  var selectedIndexes = [IndexPath]()
  
  lazy var enterPasswordField: RegisterFieldView = {
    let passwordField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    passwordField.textField.placeholder = "Enter Password"
    passwordField.textField.isSecureTextEntry = true
    passwordField.textField.minLength = 8
    passwordField.textField.maxLength = 15
    passwordField.leftIcon.image = UIImage(named: "ic_lock_black")
    passwordField.rightIcon.image = UIImage(named: "ic_eye_black")
    passwordField.containerView.layer.cornerRadius = 9
    passwordField.containerView.layer.masksToBounds = true
    passwordField.textField.font = .defaultRegularFont(size:12)
    passwordField.rightTapped = {(sender) in
      passwordField.textField.isSecureTextEntry = !passwordField.textField.isSecureTextEntry
    }
    return passwordField
  }()
  
  lazy var oldPasswordField: RegisterFieldView = {
    let passwordField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    passwordField.textField.placeholder = "Old Password"
    passwordField.textField.isSecureTextEntry = true
    passwordField.textField.minLength = 8
    passwordField.textField.maxLength = 15
    passwordField.leftIcon.image = UIImage(named: "ic_lock_black")
    passwordField.rightIcon.image = UIImage(named: "ic_eye_black")
    passwordField.containerView.layer.cornerRadius = 9
    passwordField.containerView.layer.masksToBounds = true
    passwordField.textField.font = .defaultRegularFont(size:12)
    passwordField.rightTapped = {(sender) in
      passwordField.textField.isSecureTextEntry = !passwordField.textField.isSecureTextEntry
    }
    return passwordField
  }()
  lazy var passwordField: RegisterFieldView = {
    let passwordField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    passwordField.textField.placeholder = "New Password"
    passwordField.textField.isSecureTextEntry = true
    passwordField.textField.minLength = 8
    passwordField.textField.maxLength = 15
    passwordField.leftIcon.image = UIImage(named: "ic_lock_black")
    passwordField.rightIcon.image = UIImage(named: "ic_eye_black")
    passwordField.containerView.layer.cornerRadius = 9
    passwordField.containerView.layer.masksToBounds = true
    passwordField.textField.font = .defaultRegularFont(size:12)
    passwordField.rightTapped = {(sender) in
      passwordField.textField.isSecureTextEntry = !passwordField.textField.isSecureTextEntry
    }
    return passwordField
  }()
  lazy var confirmPasswordField: RegisterFieldView = {
    let passwordField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    passwordField.textField.placeholder = "Confirm Password"
    passwordField.textField.isSecureTextEntry = true
    passwordField.textField.minLength = 8
    passwordField.textField.maxLength = 15
    passwordField.leftIcon.image = UIImage(named: "ic_lock_black")
    passwordField.rightIcon.image = UIImage(named: "ic_eye_black")
    passwordField.containerView.layer.cornerRadius = 9
    passwordField.containerView.layer.masksToBounds = true
    passwordField.textField.font = .defaultRegularFont(size:12)
    passwordField.rightTapped = {(sender) in
      passwordField.textField.isSecureTextEntry = !passwordField.textField.isSecureTextEntry
    }
    return passwordField
  }()
  lazy var phoneField: RegisterFieldView = {
    let phoneField = RegisterFieldView.nib(withType: RegisterFieldView.self)
    phoneField.textField.placeholder = "Phone number"
    phoneField.textField.keyboardInputType = .Phone
    phoneField.textField.containString = ["08", "628"]
    phoneField.textField.minLength = 10
    phoneField.textField.maxLength = 12
    phoneField.rightIcon.superview?.isHidden = true
    phoneField.leftIcon.image = UIImage(named: "ic_phone_black")
    phoneField.containerView.layer.cornerRadius = 9
    phoneField.containerView.layer.masksToBounds = true
    return phoneField
  }()
  
  var currentUser: UserModel?
  private var feedbackRequestModel = FeedbackRequestModel()
  
  private var selectedTextField: UITextField?
  private var uploadType: UploadPictureType = .UserProfile
  private var selectedImages = [UIImage]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    switch tableType {
    case .EditProfile:
      currentUser = AuthManager.shared.currentUser
      photoPickerVC.delegate = self
      tableView.reloadData()
      break
    case .Feedback:
      photoPickerVC.delegate = self
      break
    case .SocialMedia:
      currentUser = AuthManager.shared.currentUser
      getUserSocMed()
      break
    case .ChangePhone:
      currentUser = AuthManager.shared.currentUser
      break
    case .EnterPassword:
      currentUser = AuthManager.shared.currentUser
    default:
      break
    }
  }
  
  override func setupNav() {
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: tableType.title()))
    self.navigationItem.leftItemsSupplementBackButton = true
  }
  
  override func setupLayout() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.tableFooterView = UIView()
    cells = ["SocialMediaCell", "CollectionTableCell", "BackgroundPreviewCell", "InputFieldCell", "InputViewCell"]
    
    doneButton.backgroundColor = .PRIMARY_1
    doneButton.layer.cornerRadius = 10
    doneButton.layer.masksToBounds = true
    doneButton.setAttributedTitle(tableType.actionButton().toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 16),
                                                                                     NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
    
    if tableType == .ChangePassword || tableType == .ForgotPassword || tableType == .EnterPassword {
      oldPasswordField.textField.textFieldHandler = {(sender) in
        switch sender {
        case .Min:
          print("Min. password \(self.oldPasswordField.textField.minLength ?? 0) chars")
          break
        case .Valid:
          print("Valid")
          break
        default:
          break
        }
      }
      
      passwordField.textField.textFieldHandler = {(sender) in
        switch sender {
        case .Min:
          print("Min. password \(self.passwordField.textField.minLength ?? 0) chars")
          break
        case .End:
          self.confirmPasswordField.textField.containString = [self.passwordField.textField.text ?? ""]
          break
        case .Valid:
          print("Valid")
          break
        default:
          break
        }
      }
      
      confirmPasswordField.textField.textFieldHandler = {(sender) in
        switch sender {
        case .Min:
          print("Min. password \(self.confirmPasswordField.textField.minLength ?? 0) chars")
          break
        case .Prefix:
          print("Confirm password not equal")
          break
        case .Valid:
          print("Valid")
          break
        default:
          break
        }
      }
      
      enterPasswordField.textField.textFieldHandler = {(sender) in
        switch sender {
        case .Min:
          print("Min. password \(self.enterPasswordField.textField.minLength ?? 0) chars")
          break
        case .Valid:
          print("Valid")
          break
        default:
          break
        }
      }
    } else if tableType == .ChangePhone {
      phoneField.textField.placeholder = "New phone number"
      phoneField.textField.textFieldHandler = {(sender) in
        switch sender {
        case .Min:
          print("Min. phone \(self.phoneField.textField.minLength ?? 0) chars")
          break
        case .Prefix:
          print("Prefix phone must \(self.phoneField.textField.containString?.joined(separator: ", ") ?? "")")
          break
        case .Valid:
          print("Valid")
          break
        default:
          break
        }
      }
    }
  }
  
  func configSocMedCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SocialMediaCell", for: indexPath) as! SocialMediaCell
    cell.separatorInset = .init(top: 0, left: 26, bottom: 0, right: 26)
    cell.selectionStyle = .none
    let name = tableType.rows(indexPath.section)[indexPath.row]
    cell.iconImageView.image = UIImage(named: "ic_\(name)")
    let i = currentUser?.socialMedias.firstIndex{ $0.socialMedia.rawValue == name }
    if let i = i {
      if let socmed = currentUser?.socialMedias[i] {
        var newSocmed = socmed
        cell.setActive(newSocmed.showOnLive)
        cell.inputField.text = socmed.userName
        cell.textFieldDidEndHandler = {(sender) in
          if let tf = sender as? UITextField {
            newSocmed.userName = tf.text
            self.currentUser?.socialMedias[i] = newSocmed
          }
        }
        cell.showTapped = {(sender) in
          if let showButton = sender as? UIButton {
            self.currentUser?.hideAllSocialMedia()
            newSocmed.showOnLive = showButton.isSelected
            self.currentUser?.socialMedias[i] = newSocmed
            tableView.reloadData()
          }
        }
        cell.showButton.isSelected = socmed.showOnLive
      }
    }
    return cell
    
  }
  
  func configEditProfileCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.section {
    case 0:
      let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionTableCell", for: indexPath) as! CollectionTableCell
      cell.collectionView.register(UINib.init(nibName: "PostImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PostImageCollectionCell")
      cell.collectionView.contentInset = .init(top: 0, left: 20, bottom: 0, right: 20)
      cell.collectionView.dataSource = self
      cell.collectionView.delegate = self
      cell.collectionView.reloadData()//
      cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
      return cell
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: "BackgroundPreviewCell", for: indexPath) as! BackgroundPreviewCell
      cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
      cell.selectionStyle = .none
      if let user = currentUser {
        cell.bind(user: user)
      }
      return cell
    case 2:
      let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldCell", for: indexPath) as! InputFieldCell
      let str = tableType.rows(indexPath.section)[indexPath.row]
      cell.inputTitleLabel.text = str
      cell.inputField.placeholder = "\(indexPath.row != 1 ? "Input" : "Select") \(str.capitalized)"
      cell.inputField.isUserInteractionEnabled = true
      cell.inputField.inputAccessoryView = nil
      cell.inputField.inputView = nil
      cell.inputField.delegate = self
      cell.inputField.tag = indexPath.row
      var value = ""
      switch indexPath.row {
      case 0:
        value = currentUser?.fullname ?? ""
        break
      case 1:
        value = currentUser?.gender ?? "Female"
        cell.inputField.isUserInteractionEnabled = false
        break
      case 2:
        value = currentUser?.birthday ?? ""
        cell.inputField.inputAccessoryView = inputToolbar
        cell.inputField.inputView = datePicker
        break
      case 3:
        value = currentUser?.hometown ?? ""
        break
      case 4:
        value = currentUser?.bio ?? ""
        break
      default:
        break
      }
      cell.inputField.text = value
      cell.selectorButton.isHidden = indexPath.row != 1
      cell.separatorInset = .zero
      cell.selectionStyle = .none
      return cell
    default:
      return UITableViewCell(style: .default, reuseIdentifier: "cell")
    }
  }
  
  func configChangePasswordCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    var registerFieldView: RegisterFieldView?
    switch indexPath.row {
    case 0:
      registerFieldView = oldPasswordField
      break
    case 1:
      registerFieldView = passwordField
      break
    case 2:
      registerFieldView = confirmPasswordField
      break
    default:
      break
    }
    registerFieldView?.translatesAutoresizingMaskIntoConstraints = false
    cell.contentView.addSubview(registerFieldView!)
    registerFieldView?.snp.makeConstraints({ (maker) in
      maker.top.equalTo(cell.contentView).offset(8)
      maker.left.equalTo(cell.contentView).offset(20)
      maker.right.equalTo(cell.contentView).inset(20)
      maker.bottom.equalTo(cell.contentView).inset(8)
    })
    return cell
  }
  
  func configForgotPasswordCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    var registerFieldView: RegisterFieldView?
    switch indexPath.row {
    case 0:
      registerFieldView = passwordField
      break
    case 1:
      registerFieldView = confirmPasswordField
      break
    default:
      break
    }
    registerFieldView?.translatesAutoresizingMaskIntoConstraints = false
    cell.contentView.addSubview(registerFieldView!)
    registerFieldView?.snp.makeConstraints({ (maker) in
      maker.top.equalTo(cell.contentView).offset(8)
      maker.left.equalTo(cell.contentView).offset(20)
      maker.right.equalTo(cell.contentView).inset(20)
      maker.bottom.equalTo(cell.contentView).inset(8)
    })
    return cell
  }
  
  func configEnterPasswordCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    var registerFieldView: RegisterFieldView?
    switch indexPath.row {
    case 0:
      registerFieldView = enterPasswordField
      break
    default:
      break
    }
    registerFieldView?.translatesAutoresizingMaskIntoConstraints = false
    cell.contentView.addSubview(registerFieldView!)
    registerFieldView?.snp.makeConstraints({ (maker) in
      maker.top.equalTo(cell.contentView).offset(8)
      maker.left.equalTo(cell.contentView).offset(20)
      maker.right.equalTo(cell.contentView).inset(20)
      maker.bottom.equalTo(cell.contentView).inset(8)
    })
    return cell
  }
  
  func configChangePhoneCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    phoneField.translatesAutoresizingMaskIntoConstraints = false
    cell.contentView.addSubview(phoneField)
    phoneField.snp.makeConstraints({ (maker) in
      maker.top.equalTo(cell.contentView).offset(8)
      maker.left.equalTo(cell.contentView).offset(20)
      maker.right.equalTo(cell.contentView).inset(20)
      maker.bottom.equalTo(cell.contentView).inset(8)
    })
    return cell
  }
  
  func configFeedbackCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let str = tableType.rows(indexPath.section)[indexPath.row]
    switch indexPath.row {
    case 0:
      let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldCell", for: indexPath) as! InputFieldCell
      cell.inputTitleLabel.text = str
      cell.inputField.placeholder = "Input \(str.capitalized)"
      cell.inputField.text = feedbackRequestModel.email
      cell.inputField.delegate = self
      cell.inputField.tag = indexPath.row
      cell.inputField.keyboardType = .emailAddress
      cell.selectorButton.isHidden = true
      cell.separatorInset = .zero
      return cell
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: "InputViewCell", for: indexPath) as! InputViewCell
      cell.inputTitleLabel.text = str
      cell.inputAreaView.placeholder = "Input \(str.capitalized)"
      cell.inputAreaView.textStr = feedbackRequestModel.message
      cell.inputAreaView.textViewDidEndHandler = {(sender) in
        if let textview = sender as? UITextView, textview.text != cell.inputAreaView.placeholder {
          self.feedbackRequestModel.message = textview.text
        }
      }
      cell.separatorInset = .zero
      return cell
    case 2:
      let cell = tableView.dequeueReusableCell(withIdentifier: "BackgroundPreviewCell", for: indexPath) as! BackgroundPreviewCell
      cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
      cell.topLabel.superview?.isHidden = false
      cell.topLabel.text = str
      cell.infoLabel.isHidden = true
      if let image = feedbackRequestModel.images?.first {
        cell.previewImageView.contentMode = .scaleAspectFit
        cell.previewImageView.image = image
        cell.placeholderImageView.isHidden = true
      } else {
        cell.previewImageView.image = UIImage(color: .init(hex: "F6F6F6"))
        cell.placeholderImageView.isHidden = false
      }
      return cell
    default:
      return UITableViewCell()
    }
  }
  
//  MARK: Service
  func postUpdateProfile() {
    if !validateUpdateProfile() {
      routeToWarningModal(message: "Name is too short (minimum is 2 characters)")
      return
    }
    guard let request = UpdateProfileRequestModel(JSON: ["name": currentUser?.fullname ?? "",
                                                         "gender": currentUser?.gender == "Male" ? true : false,
                                                         "birthDate": currentUser?.birthday ?? "",
                                                         "homeTown": currentUser?.hometown ?? "",
                                                         "bio":currentUser?.bio ?? ""]) else { return }
    provider.request(.UpdateProfile(request: request), successModel: UserResponseModel.self) { (result) in
      self.currentUser = result.attributes
      self.getMyProfile {}
      self.routeToWarningModal(message: "Profile change successful") {
        
      }
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: self.currentUser)
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  func postUploadCover() {
    let request = UploadRequestModel()
    request.imagesData = selectedImages.map({ $0.compress(to: 500) })
    provider.request(.UploadCover(request: request), successModel: UserResponseModel.self) { (result) in
      self.currentUser = result.attributes
      self.tableView.reloadData()
      self.getMyProfile {}
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: self.currentUser)
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  func postUploadProfile() {
    let request = UploadRequestModel()
    request.imagesData = selectedImages.map({ $0.compress(to: 500) })
    provider.request(.UploadPicture(request: request), successModel: UserResponseModel.self) { [weak self] (result) in
      guard let self = self else { return }
      self.currentUser = result.attributes
      self.tableView.reloadData()
      self.getMyProfile {
        if let id = self.currentUser?.userPictures.last?.id {
          self.setImageProfile(id: id, afterUploadingNewPicture: true)
        }
      }
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: self.currentUser)
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  func postChangePassword() {
    guard let request = UpdatePasswordRequestModel(JSON: ["currentPassword": oldPasswordField.textField.text ?? "",
                                                          "newPassword": passwordField.textField.text ?? "",
                                                          "confirmPassword": confirmPasswordField.textField.text ?? ""]) else { return }
    
    provider.request(.UpdatePassword(request: request), successModel: SuccessResponseModel.self) { (result) in
      self.routeToInformationModal(type: .Success, message: "Password successfully changed") {
        self.navigationController?.popViewController(animated: true)
      }
    } failure: { (error) in
      self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
    }
  }
  
  private func doResetPassword() {
    if let request = ForgotPasswordRequestModel(JSON: ["phone": AuthManager.shared.registerModel?.request?.phone ?? "", "code": AuthManager.shared.otpRequestModel?.code ?? "", "password": confirmPasswordField.textField.text ?? ""]) {
      provider.request(.ForgotPassword(request: request), successModel: SuccessResponseModel.self) { (result) in
        self.routeToInformationModal(type: .Success, message: "Password successfully changed") {
          self.navigationController?.popToRootViewController(animated: true)
        }
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
  func deleteImageProfile(id: String, completion: @escaping () -> Void = {}) {
    provider.request(.DeletePicture(identifier: id), successModel: UserResponseModel.self) { (result) in
      self.currentUser = result.attributes
      self.getMyProfile {
        self.tableView.reloadData()
      }
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: self.currentUser)
      completion()
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  func setImageProfile(id: String, afterUploadingNewPicture: Bool = false) {
    provider.request(.SetPicture(identifier: id), successModel: UserResponseModel.self) { [weak self] (result) in
      guard let self = self else { return }
      if !afterUploadingNewPicture {
        self.currentUser = result.attributes
        self.tableView.reloadData()
        self.getMyProfile {
          self.routeToInformationModal(type: .Success, message: "Profile picture set!")
        }
      }
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: self.currentUser)
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  func postFeedback() {
    provider.request(.Feedback(request: feedbackRequestModel), successModel: SuccessResponseModel.self) { (result) in
      self.feedbackRequestModel = FeedbackRequestModel()
      self.routeToWarningModal(message: "Feedback Sent.")
      self.navigationController?.popViewController(animated: true)
    } failure: { (error) in
      self.routeToWarningModal(message: error.messages.joined(separator: "\n"))
    }
  }
  
  func getUserSocMed() {
    if let userId = currentUser?.userId {
      provider.request(.SocMedAccount(identifier: userId), successModel: SocialMediaResponseModel.self) { (result) in
        self.currentUser?.socialMedias = result.attributes ?? []
        self.tableView.reloadData()
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
  func setUserSocMed() {
    if let request = currentUser?.socialMediaAsRequest {
      provider.request(.UpdateSocMed(request: request), successModel: SuccessResponseModel.self) { (result) in
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
        self.routeToWarningModal(message: "Social media updated.") {
          self.tableView.reloadData()
        }
      } failure: { (error) in
        print(error.messages.joined(separator: "\n"))
      }
    }
  }
  
//  MARK: Route
  func routeToGenderActionSheet() {
    popupSheetVC.showTitle = false
    let male = SocMedView.nib(withType: SocMedView.self)
    male.iconImageView.superview?.isHidden = true
    male.titleLabel.text = "Male"
    male.titleLabel.textAlignment = .center
    male.actionTapped = {(sender) in
      self.currentUser?.gender = "Male"
      self.popupSheetVC.dismiss(animated: true) {
        self.tableView.reloadRows(at: [IndexPath(item: 1, section: 2)], with: .automatic)
      }
    }
    let female = SocMedView.nib(withType: SocMedView.self)
    female.iconImageView.superview?.isHidden = true
    female.titleLabel.text = "Female"
    female.titleLabel.textAlignment = .center
    female.actionTapped = {(sender) in
      self.currentUser?.gender = "Female"
      self.popupSheetVC.dismiss(animated: true) {
        self.tableView.reloadRows(at: [IndexPath(item: 1, section: 2)], with: .automatic)
      }
    }
    popupSheetVC.additionalViews = [male, female]
    popupSheetVC.actionTapped = {(sender) in
      self.popupSheetVC.dismiss(animated: true) {
        
      }
    }
    self.present(popupSheetVC, animated: true, completion: nil)
  }
  
  func routeToOTP() {
    _ = ForgotPasswordRequestModel(JSON: [ "phone": AuthManager.shared.registerModel?.request?.phone ?? "",
                                                     "code":"2010",
                                                     "password":"inipassword"])
    let destinationVC = OTPVC(nibName: "OTPVC", bundle: nil)
    destinationVC.otpType = .Forgot
    self.navigationController?.pushViewController(destinationVC, animated: true)
  }
  
  func routeToChangeProfilePictureModal(id: String) {
    let alert = PopupAlertVC(nibName: "PopupAlertVC", bundle: nil)
    alert.modalTransitionStyle = .crossDissolve
    alert.modalPresentationStyle = .overFullScreen
    alert.alertType = .Alert
    alert.cancelStr = "No"
    alert.actionStr = "Yes"
    alert.messageStr = "Set this picture as your profile picture?"
    alert.additionalViews = []
    alert.cancelTapped = { (sender) in
        alert.dismiss(animated: true)
    }
    alert.actionTapped = { [unowned self] (sender) in
        alert.dismiss(animated: true) {
            self.setImageProfile(id: id)
        }
    }
    present(alert, animated: true, completion: nil)
  }
  
//  MARK: Action
  override func doneInputAction(_ sender: Any) {
    currentUser?.birthday = datePicker.date.toString(with: "yyyy-MM-dd")
    selectedTextField?.text = currentUser?.birthday
    self.view.endEditing(true)
  }
  
  override func cancelInputAction(_ sender: Any) {
    self.view.endEditing(true)
  }
  
  @IBAction func saveAction(_ sender: Any) {
    self.view.endEditing(true)
    switch tableType {
    case .EditProfile:
      postUpdateProfile()
      break
    case .Feedback:
      postFeedback()
      break
    case .SocialMedia:
      setUserSocMed()
      break
    case .ChangePassword:
      postChangePassword()
      break
    case .ForgotPassword:
      doResetPassword()
      break
    case .ChangePhone:
      let authManager = AuthManager.shared
      let responseModel = RegisterResponseModel()
      responseModel.id = currentUser?.userId ?? ""
      let registerModel = RegisterModel(request: RegisterRequestModel(), response: responseModel)
      authManager.registerModel = registerModel
      if let otpModel = OTPRequestModel(JSON: ["phone": phoneField.textField.text ?? ""]) {
        authManager.otpRequestModel = otpModel
      }
      routeToOTP(otpType: .ChangePhone)
    case .EnterPassword:
      let loginPass = UserDefaults.standard.string(forKey: "phoneLoginPass")
      if enterPasswordField.textField.text?.count ?? 0 >= enterPasswordField.textField.minLength ?? 0 {
        if enterPasswordField.textField.text ?? "" == loginPass {
          routeToTableActionVC(type: .ChangePhone)
        } else {
          self.routeToWarningModal(message: "Invalid Password")
        }
      } else {
        self.routeToWarningModal(message: "Min. password \(self.enterPasswordField.textField.minLength ?? 0) chars")
      }
      
    default:
      break
    }
  }
  
//  MARK: Custom
  func validateUpdateProfile() -> Bool {
    return self.currentUser?.fullname?.count ?? 0 > 1
  }
}

extension TableWithActionVC: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return tableType.numOfSections()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return tableType.numOfRow(section: section)
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch tableType {
    case .SocialMedia:
      return configSocMedCell(tableView, cellForRowAt: indexPath)
    case .EditProfile:
      return configEditProfileCell(tableView, cellForRowAt: indexPath)
    case .ChangePassword:
      return configChangePasswordCell(tableView, cellForRowAt: indexPath)
    case .ForgotPassword:
      return configForgotPasswordCell(tableView, cellForRowAt: indexPath)
    case .ChangePhone:
      return configChangePhoneCell(tableView, cellForRowAt: indexPath)
    case .EnterPassword:
      return configEnterPasswordCell(tableView, cellForRowAt: indexPath)
    case .Feedback:
      return configFeedbackCell(tableView, cellForRowAt: indexPath)
    default:
      return UITableViewCell(style: .default, reuseIdentifier: "cell")
    }
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return createBasicTableHeader(title: tableType.sections()[section])
  }
}

extension TableWithActionVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if tableType == .EditProfile && indexPath.section == 0 {
      return tableView.frame.width * (111/401)
    }
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if tableType == .EditProfile {
      return 40
    } else {
      return 0.001
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    switch tableType {
    case .EditProfile:
      switch indexPath {
      case IndexPath(row: 0, section: 1):
        uploadType = .UserCover
        routeToPictureActionSheet(galleryHandler: nil, cameraHandler: nil)
        break
      case IndexPath(row: 1, section: 2):
        routeToGenderActionSheet()
        break
      default:
        break
      }
      break
    case .Feedback:
      uploadType = .Feedback
      if indexPath.row == 2 {
        routeToPictureActionSheet(galleryHandler: nil, cameraHandler: nil)
      }
      break
    default:
      selectedIndexes.removeAll()
      selectedIndexes.append(indexPath)
      tableView.reloadData()
      break
    }
  }
}

extension TableWithActionVC: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 3
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostImageCollectionCell", for: indexPath) as! PostImageCollectionCell
    cell.imageView.image = UIImage(color: .init(hex: "DFDFDF"))
    cell.deleteButton.isHidden = true
    cell.isEmpty = true
    if indexPath.row == 0 {
      cell.imageView.image = UIImage(named: "ic_default_profile")
    } else {
      let index = (indexPath.row - 1)
      if index < currentUser?.userPictures.count ?? 0 {
        if let userProfile = currentUser?.userPictures[index], let urlString = userProfile.picture_url, let url = URL(string: urlString), let id = userProfile.id {
          cell.bind(url: url)
          cell.deleteButton.isHidden = false
          cell.deleteTapped = {(sender) in
            self.deleteImageProfile(id: id) { [unowned cell] in
              cell.isEmpty = true
            }
          }
        }
      }
    }
    return cell
  }
}

extension TableWithActionVC: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    collectionView.deselectItem(at: indexPath, animated: true)
    if let cell = collectionView.cellForItem(at: indexPath) as? PostImageCollectionCell {
      if indexPath.row > 0 {
        if cell.isEmpty {
          uploadType = .UserProfile
          routeToPictureActionSheet(galleryHandler: nil, cameraHandler: nil)
        } else {
          let index = (indexPath.row - 1)
          if (index >= 0 && currentUser?.userPictures.count ?? 0 > index) {
              routeToChangeProfilePictureModal(id: currentUser?.userPictures[index].id ?? "")
          }
//          if (currentUser?.userPictures.count ?? 0 > 0) && (index <= currentUser?.userPictures.count ?? 0) {
//            if let userProfile = currentUser?.userPictures[index], let id = userProfile.id, let currentProfilePicture = currentUser?.userPicture {
//              if currentProfilePicture.id != id {
//                setImageProfile(id: id)
//              }
//            }
//          }
          
        }
      }
    }
  }
}

extension TableWithActionVC: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return .init(width: (view.frame.width - 64)/3, height: collectionView.frame.height)
  }
}

extension TableWithActionVC: UITextFieldDelegate {
  func textFieldDidBeginEditing(_ textField: UITextField) {
    selectedTextField = textField
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    switch tableType {
    case .EditProfile:
      switch textField.tag {
      case 0:
        self.currentUser?.fullname = textField.text
        break
      case 3:
        self.currentUser?.hometown = textField.text
        break
      case 4:
        self.currentUser?.bio = textField.text
        break
      default:
        break
      }
      break
    case .Feedback:
      switch textField.tag {
      case 0:
        self.feedbackRequestModel.email = textField.text
        break
      default:
        break
      }
      break
    default:
      break
    }
  }
}

extension TableWithActionVC: UITextViewDelegate {
  func textViewDidEndEditing(_ textView: UITextView) {
    switch tableType {
    case .Feedback:
      switch textView.tag {
      case 1:
        self.feedbackRequestModel.message = textView.text
        break
      default:
        break
      }
      break
    default:
      break
    }
  }
}

extension TableWithActionVC: FusumaDelegate {
  func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
    print("Image selected")
    selectedImages.removeAll()
    selectedImages.append(image)
    switch uploadType {
    case .UserProfile:
      postUploadProfile()
      break
    case .UserCover:
      postUploadCover()
      break
    case .Feedback:
      feedbackRequestModel.images = selectedImages
      tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
    }
  }
  
  func fusumaVideoCompleted(withFileURL fileURL: URL) {
    print("Called just after a video has been selected.")
  }
  
  func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
    
  }
  
  func fusumaCameraRollUnauthorized() {
    print("Camera roll unauthorized")
  }
  
  func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {
    
  }
}
