//
//  SettingVC.swift
//  sugarlive
//
//  Created by msi on 13/09/21.
//

import UIKit

class TableWithoutAction: BaseTableVC {
  
  var tableType: TableType = .Default
  var userModel: UserModel?
  
  private var selectedIndexes = [IndexPath]()
  private var pageModel = PagingRequestModel()
  private var users = [UserModel]()
  private var listTopupHistory = [TopupHistoryModel]()
  
  private var stopPaging: Bool = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    switch tableType {
    case .Blocked:
      getBlockedUser()
      break
    case .Following:
      getFollowing()
      break
    case .Follower:
      getFollower()
      break
    case .Store:
      userModel = AuthManager.shared.currentUser
      break
    case .Wallet:
      userModel = AuthManager.shared.currentUser
      break
    case .Topup:
      getTopupHistory()
      break
    default:
      break
    }
  }
  
  override func setupNav() {
    if ([TableType.Default, TableType.HowToTopup].contains(tableType)) {
      self.navigationItem.titleView = navLabel(title: tableType.title())
    } else {
      self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navLabel(title: tableType.title()))
      self.navigationItem.leftItemsSupplementBackButton = true
    }
  }
  
  override func setupLayout() {
    setupTable()
  }
  
  private func setupTable() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .none
    cells = ["ProfileCell", "WalletCell", "TopupHistoryCell", "ProfileDetailCell", "StoreCell"]
  }
  
  func configSettingCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
    cell.separatorInset = .init(top: 0, left: 20, bottom: 0, right: 20)
    cell.profileCellType = .Setting
    let menu = menuModel[indexPath.section].settingMenu[indexPath.row]
    cell.profileImageView.image = UIImage(named: menu.icon)
    cell.subtitleLabel.text = menu.title
    return cell
  }
  
  func configWalletCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    self.tableView.separatorColor = .clear
    self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    switch indexPath.section {
    case 0:
      let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
      let totalCoin = "\(Int(userModel?.coin ?? 0))"
      cell.topLabel.text = totalCoin.rupiahFormatter()
      cell.bottomLabel.text = "Candies in possession"
      cell.actionContainerView.isHidden = false
      cell.leftButton.addTarget(self, action: #selector(topupAction(_:)), for: .touchUpInside)
      cell.rightButton.addTarget(self, action: #selector(topupHistoryAction(_:)), for: .touchUpInside)
      cell.iconImageView.image = UIImage(named: "ic_candies_wallet")
      return cell
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
      let totalCarrot = "\(Int(userModel?.totalCarrot ?? 0))"
      cell.topLabel.text = totalCarrot.rupiahFormatter()
      cell.bottomLabel.text = "Carrots in possession"
      cell.actionContainerView.isHidden = true
      cell.iconImageView.image = UIImage(named: "ic_carrots_wallet")
      return cell
    case 2:
      let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
      cell.selectionStyle = .none
      let attStr = NSMutableAttributedString(attributedString: "What is Carrot?".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14), NSAttributedString.Key.foregroundColor: UIColor.PRIMARY_1]))
      attStr.append("\n1. Carrots can be obtain by recieving Gift from viewers.\n2. Carrots can be redeem to Rewards.".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]))
      
      attStr.append("\n\nWhat is Candy?".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14), NSAttributedString.Key.foregroundColor: UIColor.PRIMARY_1]))
      attStr.append("\n1. Candy can be obtain by Top Up / Recharge.\n2. Candy needed for sending Gift to Host.\n3. You will get more bonuses if you bought a large number of Candy.\n4. You will get a lot of cashback Candy in some Events.".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]))
      
      cell.textLabel?.attributedText = attStr
      cell.textLabel?.numberOfLines = 0
      return cell
    default:
      return UITableViewCell(style: .default, reuseIdentifier: "cell")
    }
  }
  
  func configTopupCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "TopupHistoryCell", for: indexPath) as! TopupHistoryCell
    let data = listTopupHistory[indexPath.row]
    cell.dateLabel.text = data.datetime?.toDate(with: "yyyy-MM-dd HH:mm:ss").toString(with: "dd MMM yy  HH:mm")
    cell.nominalLabel.text = "IDR \("\(data.amount ?? 0)".rupiahFormatter())"
    cell.numberLabel.text = data.reference
    cell.statusLabel.text = "Success"
    cell.statusLabel.textColor = .GREEN_SUCCESS
    cell.typeLabel.text = "Topup candy"
    if ((data.reference?.contains("AG")) != nil) {
      cell.descLabel.text = "Partner Candy"
    } else if ((data.reference?.contains("SG")) != nil) {
      cell.descLabel.text = "Store"
    } else if ((data.reference?.contains("GPA")) != nil) {
      cell.descLabel.text = "Google Play"
    } else {
      cell.descLabel.text = ""
    }
      
    cell.separatorInset = .zero
    return cell
  }
  
  func configBlockedCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let profileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
    profileCell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    profileCell.selectionStyle = .none
    profileCell.profileCellType = .Blocked
    profileCell.subtitleLabel.font = .defaultBoldFont(size: 14)
    profileCell.profileImageView.layer.cornerRadius = 10
    
    let user = users[indexPath.row]
    profileCell.subtitleLabel.text = user.fullname
    
    if let urlString = user.coverPicture, let url = URL(string: urlString) {
      profileCell.profileImageView.af.setImage(withURL: url)
    } else {
      profileCell.profileImageView.image = UIImage(named: "ic_default_profile")
    }
    
    let unblockButton = UIButton()
    unblockButton.tag = indexPath.row
    var selected = ""
    var unselected = ""
    switch tableType {
    case .Blocked:
      selected = "Unblock"
      unselected = "Block"
      unblockButton.isSelected = true
      break
    case .Following:
      selected = "Follow"
      unselected = "Unfollow"
      unblockButton.isSelected = !user.isFollow
      break
    case .Follower:
      selected = "Follow"
      unselected = "Followed"
      unblockButton.isSelected = !user.isFollow
      break
    default:
      break
    }
    unblockButton.setAttributedTitle(selected.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 10),
                                                                        NSAttributedString.Key.foregroundColor: UIColor.white]),
                                     for: .selected)
    unblockButton.setAttributedTitle(unselected.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 10),
                                                                          NSAttributedString.Key.foregroundColor: UIColor.init(hex: "727272")]),
                                     for: .normal)
    unblockButton.contentEdgeInsets = .init(top: 8, left: 14, bottom: 8, right: 14)
    unblockButton.setBackgroundImage(UIImage(color: .PRIMARY_1), for: .selected)
    unblockButton.setBackgroundImage(UIImage(color: .init(hex: "E9E9E9")), for: .normal)
    unblockButton.layer.cornerRadius = 5
    unblockButton.layer.masksToBounds = true
    unblockButton.setContentHuggingPriority(UILayoutPriority.init(rawValue: 255), for: .horizontal)
    unblockButton.addTarget(self, action: #selector(unblockAction(_:)), for: .touchUpInside)
    profileCell.badgeStackView.addArrangedSubview(unblockButton)
    return profileCell
  }
  
  func configStoreCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let storeCell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath) as! StoreCell
    storeCell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    storeCell.selectionStyle = .none
    storeCell.itemLabel.font = .defaultRegularFont(size: 15)
    let itemName = tableType.rows(indexPath.section)[indexPath.row]
    storeCell.itemLabel.text = itemName
    storeCell.containerView.layer.borderWidth = 1
    storeCell.containerView.layer.borderColor = UIColor.init(hex: "C9C9C9").cgColor
    storeCell.containerView.layer.cornerRadius = 10
    storeCell.containerView.layer.masksToBounds = true
    let imageName = "ic_\(itemName.replacingOccurrences(of: " ", with: "_").lowercased())_store"
    storeCell.previewImage.image = UIImage(named: imageName)
    return storeCell
  }
  
  func configHelpCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
    cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    cell.profileCellType = indexPath.row == 0 ? .Info : .InfoDetail
    let menu = helpModel[indexPath.section]
    if indexPath.row == 0 {
      cell.subtitleLabel.text = menu.settingGroup
      cell.nextButton.isSelected = selectedIndexes.contains(indexPath)
    } else {
      cell.subtitleLabel.attributedText = String(format: headerHTML, menu.settingMenu[indexPath.row-1].title).htmlToAttributedString()
      cell.selectionStyle = .none
    }
    return cell
  }
  
  func configRuleCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
    cell.separatorInset = .init(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    cell.profileCellType = indexPath.row == 0 ? .Info : .InfoDetail
    let menu = ruleModel[indexPath.section]
    if indexPath.row == 0 {
      cell.subtitleLabel.text = menu.settingGroup
      cell.nextButton.isSelected = selectedIndexes.contains(indexPath)
    } else {
      cell.subtitleLabel.attributedText = String(format: headerHTML, menu.settingMenu[indexPath.row-1].title).htmlToAttributedString()
      cell.selectionStyle = .none
    }
    return cell
  }
  
  func configHowToTopupCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    self.tableView.separatorColor = .clear
    self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)

    let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
    cell.selectionStyle = .none
    cell.textLabel?.font = .defaultRegularFont(size: 12)
    cell.textLabel?.textColor = .MEDIUM_TEXT
    cell.textLabel?.text = "\n1. Anda dapat melakukan isi ulang dengan cara memilih salah satu channel pembayaran yang sudah tersedia di halaman depan.\n\n2. Setelah itu pilihlah jumlah Candy yang ingin Anda beli. Setiap channel memiliki harga dan jumlah candy yg berbeda, Pilihlah Channel pembayaran yg terbaik menurut Anda.\n\n3. Ikuti setiap langkah dan proses Pembayaran yang harus Anda lakukan sesuai kebijakan setiap channel yang telah dipilih.\n\n4. Candy akan otomatis masuk ke Akun Sugarlive Anda jika transaksi telah berhasil."
    cell.textLabel?.numberOfLines = 0
    return cell
  }
  
//  MARK: Service
  private func getBlockedUser() {
    provider.request(.BlockedUser(request: pageModel), successModel: UserLiveResponseModel.self) { (result) in
      self.users = result.attributes
      self.tableView.reloadData()
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func getFollowing() {
    if !stopPaging {
      if let id = userModel?.userId {
        provider.request(.GetFollowing(identifier: id, request: pageModel), successModel: UserLiveResponseModel.self, withProgressHud: false) { (result) in
          if result.attributes.count == 0 {
            self.stopPaging = true
          } else {
            self.pageModel.page += 1
            self.users.append(contentsOf: result.attributes)
            self.tableView.reloadData()
          }
        } failure: { (error) in
          print(error.messages.joined(separator: "\n"))
        }
      }
    }
  }
  
  private func getFollower() {
    if !stopPaging {
      if let id = userModel?.userId {
        provider.request(.GetFollower(identifier: id, request: pageModel), successModel: UserLiveResponseModel.self, withProgressHud: false) { (result) in
          if result.attributes.count == 0 {
            self.stopPaging = true
          } else {
            self.pageModel.page += 1
            self.users.append(contentsOf: result.attributes)
            self.tableView.reloadData()
          }
        } failure: { (error) in
          print(error.messages.joined(separator: "\n"))
        }
      }
    }
  }
  
  private func getTopupHistory() {
    provider.request(.TopupHistory, successModel: TopupHistoryResponseModel.self) { (result) in
      self.listTopupHistory = result.attributes ?? [TopupHistoryModel]()
      self.tableView.reloadData()
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func doFollow(user: UserModel) {
    guard let id = user.userId else { return }
    guard let request = UserRequestModel(JSON: ["userID": id]) else { return }
    provider.request(.Follow(request: request), successModel: SuccessResponseModel.self) { (result) in
      
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func doUnfollow(user: UserModel) {
    guard let id = user.userId else { return }
    guard let request = UserRequestModel(JSON: ["userID": id]) else { return }
    provider.request(.Unfollow(request: request), successModel: SuccessResponseModel.self) { (result) in
      
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func doBlock(user: UserModel) {
    guard let id = user.userId else { return }
    guard let request = UserRequestModel(JSON: ["userID": id]) else { return }
    provider.request(.BlockUser(request: request), successModel: SuccessResponseModel.self) { (result) in
      
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func doUnblock(user: UserModel) {
    guard let id = user.userId else { return }
    guard let request = UserRequestModel(JSON: ["userID": id]) else { return }
    provider.request(.UnblockUser(request: request), successModel: SuccessResponseModel.self) { (result) in
      
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
//  MARK: Route
  func routeToLiveHistory() {
    let destinationVC = LiveHistoryVC(nibName: "LiveHistoryVC", bundle: nil)
    self.navigationController?.pushViewController(destinationVC, animated: true)
  }
  
  func routeToLevel() {
    let destinationVC = LevelVC(nibName: "LevelVC", bundle: nil)
    self.navigationController?.pushViewController(destinationVC, animated: true)
  }
  
//  MARK: Action
  @IBAction func topupAction(_ sender: Any) {
    routeToTopupVC(type: .Topup, paymentMethod: .Default)
  }
  
  @IBAction func topupHistoryAction(_ sender: Any) {
    routeToTableVC(type: .Topup)
  }
  
  @IBAction func unblockAction(_ sender: Any) {
    if let button = sender as? UIButton {
      let user = users[button.tag]
      switch tableType {
      case .Blocked:
        button.isSelected ? doUnblock(user: user) : doBlock(user: user)
        break
      case .Following:
        button.isSelected ? doFollow(user: user) : doUnfollow(user: user)
        break
      case .Follower:
        button.isSelected ? doFollow(user: user) : doUnfollow(user: user)
        break
      default:
        break
      }
      button.isSelected = !button.isSelected
    }
  }
}

// MARK: - Table View data source
extension TableWithoutAction: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return tableType.numOfSections()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch tableType {
    case .Topup:
      return listTopupHistory.count
    case .Store, .Rule, .HowToTopup:
      return tableType.numOfRow(section: section)
    case .Blocked, .Follower, .Following:
      return users.count
    default:
      return tableType.numOfRow(section: section) + (tableType == .Help && selectedIndexes.map{$0.section}.contains(section) ? 1 : 0)
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch tableType {
    case .Setting:
      return configSettingCell(tableView, cellForRowAt: indexPath)
    case .Wallet:
      return configWalletCell(tableView, cellForRowAt: indexPath)
    case .Topup:
      return configTopupCell(tableView, cellForRowAt: indexPath)
    case .Blocked, .Follower, .Following:
      return configBlockedCell(tableView, cellForRowAt: indexPath)
    case .Store:
      return configStoreCell(tableView, cellForRowAt: indexPath)
    case .Help:
      return configHelpCell(tableView, cellForRowAt: indexPath)
    case .Rule:
      return configRuleCell(tableView, cellForRowAt: indexPath)
    case .HowToTopup:
      return configHowToTopupCell(tableView, cellForRowAt: indexPath)
    default:
      return UITableViewCell(style: .default, reuseIdentifier: "cell")
    }
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    switch tableType {
    case .Setting:
      return createBasicTableHeader(title: menuModel[section].settingGroup )
    case .Store:
      return createBasicTableHeader(title: tableType.sections()[section])
    case .HowToTopup:
      return createBasicTableHeader(title: tableType.sections()[section])
    default:
      return nil
    }
  }
}

// MARK: - Table View delegate
extension TableWithoutAction: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if [TableType.Help, TableType.Rule].contains(tableType) && indexPath.row != 0 {
      return selectedIndexes.map{ $0.section }.contains(indexPath.section) ? UITableView.automaticDimension : 0.0001
    }
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return [TableType.Setting, TableType.Store].contains(tableType) ? 40 : 0.001
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if tableType == .Setting {
      let menu = menuModel[indexPath.section].settingMenu[indexPath.row]
      switch menu.title {
      case "Edit profile":
        routeToTableActionVC(type: .EditProfile)
        break
      case "Level":
        routeToLevel()
        break
      case "Live history":
        routeToLiveHistory()
        break
      case "Store":
        routeToTableVC(type: .Store)
        break
      case "Change password":
        routeToTableActionVC(type: .ChangePassword)
        break
      case "Change phone number":
        routeToTableActionVC(type: .EnterPassword)
        //routeToTableActionVC(type: .ChangePhone)
        break
      case "About Us":
        routeToWebVC(type: .AboutUs)
        break
      case "Feedback":
        routeToTableActionVC(type: .Feedback)
        break
      case "Help Center":
        routeToWebVC(type: .HelpCenter)
        break
      case "Deactivate account":
        routeToDeactivateConfirmationModal()
        break
      case "Blocked user":
        routeToTableVC(type: .Blocked)
        break
      case "Logout":
        routeToLogoutModal()
        break
      default:
        break
      }
    } else if [TableType.Help, TableType.Rule].contains(tableType) {
      if indexPath.row == 0 {
        if selectedIndexes.contains(indexPath) {
          selectedIndexes.removeAll{ $0 == indexPath}
        } else {
          selectedIndexes.append(indexPath)
        }
        tableView.reloadData()
      }
    } else if tableType == .Store {
      if indexPath.row == 0 {
        routeToTopupVC(type: .Topup, paymentMethod: .Default)
      } else {
        if let currentUser = userModel {
          routeToProfileManagerVC(type: ProfileManagerType.init(rawValue: indexPath.row + 3) ?? .Default, userModel: currentUser)
        }
      }
    } else if tableType == .Following || tableType == .Follower {
      let user = users[indexPath.row]
      let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
      vc.profileType = .Other
      vc.userId = user.userId ?? ""
      vc.navigationItem.titleView = navLabel(title: "Profile Details")
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if tableType == .Follower || tableType == .Following {
      let lastElement = self.users.count - 1
      if indexPath.row == lastElement {
        tableType == .Following ? getFollowing() : getFollower()
      }
    }
  }
}
