//
//  TopSpenderVC.swift
//  sugarlive
//
//  Created by cleanmac-ada on 06/03/22.
//

import UIKit

class TopSpenderVC: BaseVC {
    
    // MARK: - IBOutlets and UI Components
    @IBOutlet weak var todayStackView: UIStackView!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var todayIndicatorView: UIView!
    
    @IBOutlet weak var monthlyStackView: UIStackView!
    @IBOutlet weak var monthlyLabel: UILabel!
    @IBOutlet weak var monthlyIndicatorView: UIView!
    
    @IBOutlet weak var totalStackView: UIStackView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalIndicatorView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variable
    private unowned var userModel: UserModel!
    
    private var dailyList: [TopSpenderModel] = []
    private var monthlyList: [TopSpenderModel] = []
    private var totalList: [TopSpenderModel] = []
    private var containerList: [TopSpenderModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    private var type: TopSpenderType! {
        didSet {
            setTypeBar()
        }
    }
    
    // MARK: - Overriden Functions
    init(userModel: UserModel) {
        super.init(nibName: "TopSpenderVC", bundle: nil)
        self.userModel = userModel
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func setupNav() {
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_back_white"), style: .plain, target: self, action: #selector(dismissAction))
        let navTitle = UIBarButtonItem(customView: navLabel(title: "\(userModel.fullname ?? "") top fans"))
        navigationItem.leftBarButtonItems = [backButton, navTitle]
        navigationController?.navigationBar.barTintColor = .PRIMARY_1
        navigationController?.navigationBar.tintColor = .white
    }
    
    // MARK: - UI Setups
    private func setupUI() {
        let todayAction = UITapGestureRecognizer(target: self, action: #selector(topBarAction(_:)))
        todayStackView.addGestureRecognizer(todayAction)
        
        let monthlyAction = UITapGestureRecognizer(target: self, action: #selector(topBarAction(_:)))
        monthlyStackView.addGestureRecognizer(monthlyAction)
        
        let totalAction = UITapGestureRecognizer(target: self, action: #selector(topBarAction(_:)))
        totalStackView.addGestureRecognizer(totalAction)
        
        type = .Daily
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "TopSpenderCell", bundle: nil), forCellReuseIdentifier: "TopSpenderCell")
        tableView.register(UINib(nibName: "TopSpenderHeaderCell", bundle: nil), forCellReuseIdentifier: "TopSpenderHeaderCell")
        
        getTopSpender()
    }
    
    // MARK: - Custom Functions
    private func setTypeBar() {
        todayIndicatorView.backgroundColor = type == .Daily ? .PRIMARY_1 : .white
        monthlyIndicatorView.backgroundColor = type == .Monthly ? .PRIMARY_1 : .white
        totalIndicatorView.backgroundColor = type == .Total ? .PRIMARY_1 : .white
        tableView.reloadData()
    }
    
    // MARK: - Actions
    @objc private func dismissAction() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc private func topBarAction(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        if view == todayStackView {
            type = .Daily
            containerList = dailyList
        } else if view == monthlyStackView {
            type = .Monthly
            containerList = monthlyList
        } else if view == totalStackView {
            type = .Total
            containerList = totalList
        }
    }
    
    // MARK: - API Call
    private func getTopSpender() {
        if let userId = userModel.userId {
            provider.request(.TopSpender(identifier: userId), successModel: TopSpenderResponseModel.self) { [weak self] (result) in
                guard let self = self else { return }
                self.dailyList = result.daily
                self.monthlyList = result.monthly
                self.totalList = result.total
                self.containerList = result.daily
                self.tableView.reloadData()
            } failure: { (error) in
                print(error.messages.joined(separator: "\n"))
            }
        }
    }
}

// MARK: - Table View delegate and data source
extension TopSpenderVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return containerList.isEmpty ? 0 : 1
        } else {
            return containerList.count - 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopSpenderHeaderCell", for: indexPath) as! TopSpenderHeaderCell
            cell.setupContents(model: containerList[indexPath.row])
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopSpenderCell", for: indexPath) as! TopSpenderCell
            cell.setupContents(model: containerList[indexPath.row + 1])
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        indexPath.section == 0 ? 180 : 60
    }

}
