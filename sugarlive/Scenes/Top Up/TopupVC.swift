//
//  TopupVC.swift
//  sugarlive
//
//  Created by msi on 13/10/21.
//

import UIKit
import SwiftKeychainWrapper
import StoreKit

class TopupVC: BaseCollectionVC {
    
    enum TopupType {
        case Payment
        case Topup
    }
    
    enum PaymentMethod: String, CaseIterable {
        case Telkomsel
        case Indomaret
        case XL
        case BankTransfer
        case Three
        case OVO
        case DANA
        case DOKU
        case LinkAja
        case TrueMoney
        case Default
        
        func title() -> String {
            switch self {
            case .XL, .OVO, .DANA, .DOKU:
                return self.rawValue.uppercased()
            case .BankTransfer:
                return "Bank Transfer"
            case .LinkAja:
                return "LinkAja"
            case .TrueMoney:
                return "True Money"
            case .Default:
                return "Choose amount"
            default:
                return self.rawValue
            }
        }
    }
    
    @IBOutlet weak var candyImage: UIImageView!
    @IBOutlet weak var candyLabel: UILabel!
    @IBOutlet weak var howTopupButton: UIButton!
    @IBOutlet weak var candyIcon: UIImageView!
    
    private let userModel = AuthManager.shared.currentUser
    
    var topupType: TopupType = .Topup
    var paymentMethod: PaymentMethod?
    var productsArray: [SKProduct]?
    var productFromApi = [String]()
    
    var isPresentedAsModal = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getIAPProductFromServer()
    }
    
    override func setupNav() {
        self.navigationItem.titleView = navLabel(title: "Topup")
        
        if isPresentedAsModal {
            let dismissButton = UIBarButtonItem(image: UIImage(named: "ic_back_white"), style: .plain, target: self, action: #selector(dismissNavigationControllerAction))
            navigationItem.leftBarButtonItem = dismissButton
        }
    }
    
    override func setupLayout() {
        collectionView.dataSource = self
        collectionView.delegate = self
        cells = ["CandyCollectionCell", "PostImageCollectionCell"]
        headers = ["StreamerCollectionHeaderView"]
        
        howTopupButton.backgroundColor = .init(hex: "0FA85C")
        howTopupButton.setAttributedTitle("How to topup".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 10),
                                                                                   NSAttributedString.Key.foregroundColor: UIColor.white]),
                                          for: .normal)
        howTopupButton.layer.cornerRadius = 4
        howTopupButton.layer.masksToBounds = true
        howTopupButton.isHidden = false
        
        switch topupType {
        case .Payment:
            candyIcon.isHidden = true
            
            candyLabel.text = "Payment"
            candyLabel.font = .defaultBoldFont(size: 14)
            candyLabel.textColor = .init(hex: "2C004D")
            
            doneButton.superview?.isHidden = true
            break
        case .Topup:
            candyIcon.isHidden = true
            
            //      let attStr = NSMutableAttributedString(attributedString: "You have \(userModel?.coin.roundedWithAbbreviations() ?? "0") Candy".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),NSAttributedString.Key.foregroundColor: UIColor.init(hex: "404040")]))
            //      attStr.addAttributes([NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12)], range: .init(location: 0, length: 9))
            candyLabel.text = "Choose amount"
            candyLabel.font = .defaultBoldFont(size: 14)
            
            howTopupButton.isHidden = false
            doneButton.isHidden = true
            break
        }
    }
    
    //  MARK: Service
    
    private func updateCandy(receipt: String, productID: String) {
        let currentUser = AuthManager.shared.currentUser
        let userId = currentUser?.userId
        
        if let request = PurchaseCandyRequestModel(JSON: [
            "user_id": userId ?? "",
            "product": productID,
            "provider": "ITUNES",
            "token": receipt,
            "sgid": currentUser?.sugarId ?? ""]) {
            provider.request(.PurchaseCandy(request: request), successModel: SuccessResponseModel.self) { (result) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
                self.routeToWarningModal(message: "Purchase Successful") {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            } failure: { (error) in
                print(error.messages.joined(separator: "\n"))
            }
        }
    }
    
    func getIAPProductFromServer() {
        provider.request(.IAP, successModel: InAppPurchaseResponseModel.self) { (result) in
            for data in result.attributes ?? [InAppPurchaseModel]() {
                self.productFromApi.append(data.itemID ?? "")
            }
            
            IAPHandler.shared.setProductIds(ids: self.productFromApi)
            self.getIAPProduct()
            
        } failure: { (error) in
            print(error.messages.joined(separator: "\n"))
        }
    }
    
    private func getIAPProduct() {
        IAPHandler.shared.fetchAvailableProducts { [weak self](products)   in
            guard let sSelf = self else {return}
            DispatchQueue.main.async {
                sSelf.productsArray = products.sorted {
                    let a = $0.localizedTitle
                    let b = $1.localizedTitle
                    return Int(a) ?? 0 < Int(b) ?? 0
                }
                sSelf.collectionView.reloadData()
            }
        }
    }
    
    //  MARK: Action
    @IBAction func howTopupAction() {
        routeToTableVC(type: .HowToTopup)
    }
    
    @objc private func dismissNavigationControllerAction() {
        navigationController?.dismiss(animated: true)
    }
}

extension TopupVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topupType == .Payment ? PaymentMethod.allCases.count : (productsArray?.count ?? 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch topupType {
        case .Payment:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostImageCollectionCell", for: indexPath) as! PostImageCollectionCell
            if let image = UIImage(named: "ic_\(PaymentMethod.allCases[indexPath.row].rawValue.lowercased())") {
                cell.bind(image: image)
                cell.imageView.contentMode = .center
            }
            cell.contentView.layer.borderColor = UIColor.init(hex: "E9E9E9").cgColor
            cell.contentView.layer.borderWidth = 1
            cell.contentView.layer.cornerRadius = 8
            cell.contentView.layer.masksToBounds = true
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CandyCollectionCell", for: indexPath) as! CandyCollectionCell
            //    cell.bind()
            if productsArray?.count ?? 0 > 0 {
                let item = productsArray?[indexPath.row]
                let price = IAPHandler.shared.getPriceFormatted(for: item ?? SKProduct())
                let itemNameArr = item?.localizedDescription.components(separatedBy: ["-"])
                cell.topLabel.text = itemNameArr?.first
                cell.priceLabel.text = price
                cell.countLabel.text = item?.localizedTitle.rupiahFormatter()
                cell.candyImage.image = UIImage.init(named: "ic_candy_\(itemNameArr?.last ?? "s")")
            }
            return cell
        }
    }
    
    //  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    //    switch topupType {
    //    case .Topup:
    //      let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "StreamerCollectionHeaderView", for: indexPath) as! StreamerCollectionHeaderView
    //      if kind == UICollectionView.elementKindSectionHeader {
    //        headerView.titleLabel.font = .defaultBoldFont(size: 14)
    //        headerView.titleLabel.text = paymentMethod?.title()
    //      }
    //      return headerView
    //    default:
    //      return UICollectionReusableView()
    //    }
    //  }
    
    func validateReceipt(productID: String) {
        if let receipt =  Bundle.main.appStoreReceiptURL {
            if let data = NSData(contentsOf: receipt) {
                //                let requestContents:[String:String] = ["receipt-data":data.base64EncodedString(options: []), "password": "c6dbb5aee3534d6dbd59d4ef853d57c0"]
                self.updateCandy(receipt: data.base64EncodedString(options: []), productID: productID)
                
            }
        }
    }
    
}

extension TopupVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        switch topupType {
        case .Payment:
            routeToTopupVC(type: .Topup, paymentMethod: PaymentMethod.allCases[indexPath.row])
            break
            
        case .Topup:
            if IAPHandler.shared.canMakePurchases() {
                IAPHandler.shared.purchase(product: self.productsArray?[indexPath.row] ?? SKProduct()) { (alert, product, transaction) in
                    self.validateReceipt(productID: product?.productIdentifier ?? "")
                }
            } else {
                self.routeToWarningModal(message: "In-App Purchases are not allowed in this device.")
            }
            break
        }
    }
}

extension TopupVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (Constant.screenSize.width - 48) / 2
        let height = (90/143) * width
        return .init(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return topupType == .Payment ? .zero : .init(width: collectionView.frame.width, height: 10/*60*/)
    }
}
