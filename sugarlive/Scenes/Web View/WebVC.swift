//
//  WebVC.swift
//  sugarlive
//
//  Created by msi on 01/10/21.
//

import UIKit
import WebKit

class WebVC: BaseVC {
  
  enum PageType {
    case AboutUs
    case FAQ
    case TermOfUse
    case Custom(name: String, url: String)
    case HTML(name: String, string: String)
    case HelpCenter
    case Default
    
    var fullUrl: String {
      switch self {
      case .AboutUs:
        return "https://sugarlive.co.id/about-us"
      case .FAQ:
        return "https://sugarlive.co.id/faq"
      case .TermOfUse:
        return "https://sugarlive.co.id/terms-of-use"
      case .Custom(_, let url):
        return url
      case .HTML(_, let string):
        return string
      case .HelpCenter:
        return "https://sugarlive.co.id/faq"
      default:
        return "https://sugarlive.co.id"
      }
    }
    
    var title: String {
      switch self {
      case .AboutUs:
        return "About Us"
      case . FAQ:
        return "FAQ"
      case .TermOfUse:
        return "Term Of Use"
      case .Custom(let name, _):
        return name
      case .HTML(let name, _):
        return name
      case .HelpCenter:
        return "Help Center"
      default:
        return ""
      }
    }
  }
  
  @IBOutlet weak var webkView: WKWebView!
  @IBOutlet weak var webview: UIWebView!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  var pageType: PageType = .Default
  private var isAppleRedirect = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func setupLayout() {
    webview.delegate =  self
    switch pageType {
    case .HTML:
      if let url = URL(string: PageType.Default.fullUrl) {
        webview.loadHTMLString(pageType.fullUrl, baseURL: url)
        webkView.load(URLRequest(url: url))
      }
      break
    default:
      if let url = URL(string: pageType.fullUrl) {
        spinner.startAnimating()
        webview.loadRequest(URLRequest(url: url))
        webkView.load(URLRequest(url: url))
      }
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.setNavLabel(title: pageType.title))
    navigationItem.leftItemsSupplementBackButton = true
    
    switch pageType {
    case .Custom:
      self.navigationController?.navigationBar.barTintColor = .white
      self.navigationController?.navigationBar.tintColor = .white
      break
    default:
      self.navigationController?.navigationBar.barTintColor = .PRIMARY_1
      self.navigationController?.navigationBar.tintColor = .white
      break
    }
  }
  
  func setNavLabel(title: String) -> UILabel {
    let label = UILabel(frame: .zero)
    label.numberOfLines = 0
    label.font = .defaultBoldFont(size: 14)
    switch pageType {
    case .Custom:
      label.textColor = .white
      break
    default:
      label.textColor = .white
      break
    }
    
    label.text = title
    return label
  }
  
//  MARK: Service
  private func doAppleSignIn(token: String) {
    let json: [String: Any] = [
      "token": token,
      "deviceToken": AuthManager.shared.deviceToken,
      "firebaseToken": AuthManager.shared.FCMToken,
      "device": Constant.currentDevice.type.rawValue,
      "operatingSystem": "\(Constant.currentDevice.systemName) \(Constant.currentDevice.systemVersion)",
      "preferredNetwork": getConnectionType()
    ]
    
    guard let requestModel = RegisterRequestModel.init(JSON: json) else { return }
    
    var registerModel = RegisterModel(request: requestModel, response: nil)
    
    provider.request(.AppleLogin(request: requestModel), successModel: RegisterResponseModel.self) { (result) in
      registerModel.response = result
      registerModel.response?.attributes?.cacheToken()
      self.getMyProfile {}
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateProfileResponse), object: nil)
      self.navigationController?.popViewController(animated: true)
    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
  
  private func getAppleToken(token: String) {
    guard let request = AppleTokenRequestModel(JSON: ["grant_type": "authorization_code",
                                                      "code": token,
                                                      "redirect_uri": Constant.kRedirectUri,
                                                      "client_id": Constant.kServiceID,
                                                      "client_secret": JWTManager.getClientSecret(fileName: "AuthKey_52TFTR7K45")]) else { return }
    provider.request(.AppleToken(request: request), successModel: SuccessResponseModel.self) { (result) in

    } failure: { (error) in
      print(error.messages.joined(separator: "\n"))
    }
  }
}

extension WebVC: UIWebViewDelegate {
  func webViewDidFinishLoad(_ webView: UIWebView) {
    spinner.stopAnimating()
    
    if isAppleRedirect {
      if let str = String(data: (webview.request?.httpBody!)!, encoding: .utf8) {
        let arrStr = str.components(separatedBy: "&")
        var dic = [String: Any]()
        for keyValue in arrStr {
          let splittedKeyValue = keyValue.components(separatedBy: "=")
          dic[splittedKeyValue[0]] = splittedKeyValue[1]
        }
        
        if let token = dic["id_token"] as? String {
          doAppleSignIn(token: token)
        }
      }
    }
  }
  
  func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
    if request.url?.absoluteString == Constant.kRedirectUri {
      isAppleRedirect = true
    }
    return true
  }
}
