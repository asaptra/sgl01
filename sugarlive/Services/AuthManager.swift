//
//  AuthManager.swift
//  sugarlive
//
//  Created by msi on 23/09/21.
//

import Foundation
import GoogleSignIn
import Firebase
import FBSDKLoginKit
import AuthenticationServices

class AuthManager: NSObject {
  static let shared = AuthManager()
  
  let signInConfig = GIDConfiguration.init(clientID: Constant.GoogleClientID)
  let FCMToken: String = Messaging.messaging().fcmToken ?? ""
  let deviceToken: String = Messaging.messaging().apnsToken?.hexString ?? ""
  
//  var appleHandler: ((ASAuthorizationAppleIDCredential?, Error?) -> Void) = {_,_ in }
  var appleHandler: ((Any?, Error?) -> Void) = {_,_ in }
  
  var currentUser: UserModel?
  var registerModel: RegisterModel?
  var otpRequestModel: OTPRequestModel?
  
  private override init() {}
  
  func signWithGoogle(vc: UIViewController, handler: @escaping ((GIDGoogleUser?, Error?)->Void)) {
    let loginManager = GIDSignIn.sharedInstance
    loginManager.signOut()
    loginManager.signIn(with: signInConfig, presenting: vc) { (user, error) in
      handler(user, error)
    }
  }
  
  func googleAuthentication(user: GIDGoogleUser, handler: @escaping ((GIDAuthentication?, Error?) -> Void)) {
    user.authentication.do { (authentication, error) in
      handler(authentication, error)
    }
  }
  
  func signWithFB(vc: UIViewController, handler: @escaping ((LoginResult?)->Void)) {
    let loginManager = LoginManager()
    loginManager.logOut()
    loginManager.logIn(permissions: [.publicProfile, .email], viewController: vc) { (result) in
      handler(result)
    }
  }
}

@available(iOS 13.0, *)
extension AuthManager {
  var authorizationController: ASAuthorizationController {
    let appleIDProvider = ASAuthorizationAppleIDProvider()
    let request = appleIDProvider.createRequest()
    request.requestedScopes = [.fullName, .email]
    let controller = ASAuthorizationController(authorizationRequests: [request])
    return controller
  }
  
  func signWithApple() {
    authorizationController.delegate = self
    authorizationController.performRequests()
  }
}

@available(iOS 13.0, *)
extension AuthManager: ASAuthorizationControllerDelegate {
  func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    appleHandler(nil, error)
  }
  
  func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
    if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
      appleHandler(appleIDCredential, nil)
    }

  }
}

extension AuthManager {
  public static func startSigninWithAppleJS() -> String {
    let nonce = randomNonceString(length: 32)
    let urlString = "https://appleid.apple.com/auth/authorize?client_id=\(Constant.kServiceID)&response_type=\("id_token%20code")&response_mode=\("form_post")&redirect_uri=\(Constant.kRedirectUri)&scope=\("name%20email")&state=\(nonce)"
    return urlString
  }
  
  public static func randomNonceString(length: Int = 32) -> String {
    precondition(length > 0)
    let charset: Array<Character> =
      Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
    var result = ""
    var remainingLength = length
    
    while remainingLength > 0 {
      let randoms: [UInt8] = (0 ..< 16).map { _ in
        var random: UInt8 = 0
        let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
        if errorCode != errSecSuccess {
          fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
        }
        return random
      }
      
      randoms.forEach { random in
        if remainingLength == 0 {
          return
        }
        
        if random < charset.count {
          result.append(charset[Int(random)])
          remainingLength -= 1
        }
      }
    }
    
    return result
  }
}
