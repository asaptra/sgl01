//
//  JWTManager.swift
//  sugarlive
//
//  Created by msi on 08/10/21.
//

import Foundation
import CupertinoJWT

class JWTManager: NSObject {
  public static func getClientSecret(fileName: String, fileExtension: String = "p8") -> String {
    let privateKeyPath = URL(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: fileExtension)!)
//    let privateKey = try! Data(contentsOf: privateKeyPath, options: .alwaysMapped)
    let privateString = try! String(contentsOf: privateKeyPath, encoding: .utf8)
//    let jwt = JWT(keyID: Constant.kID, teamID: Constant.kTeamID, issueDate: Date(), expireDuration: 86400*180)
//    let signedJWT = try! jwt.sign(with: privateString)
    let signedJWT = JWT.sign(keyID: Constant.kID, teamID: Constant.kTeamID, issueDate: Date(), expireDuration: 86400*180, auditor: "https://appleid.apple.com", p8: privateString)
    print("client secret: \(signedJWT)")
    
    return signedJWT
  }
}

extension JWT {
  struct NewHeader: Codable {
    /// alg
    let algorithm: String = "ES256"
    
    /// kid
    let keyID: String
    
    enum CodingKeys: String, CodingKey {
      case algorithm = "alg"
      case keyID = "kid"
    }
  }
  
  struct NewPayload: Codable {
    /// iss
    public let teamID: String
    
    /// iat
    public let issueDate: Int
    
    /// exp
    public let expireDate: Int
    
    /// aud
    public let auditor: String
    
    enum CodingKeys: String, CodingKey {
      case teamID = "iss"
      case issueDate = "iat"
      case expireDate = "exp"
      case auditor = "aud"
    }
  }
  
  public static func sign(keyID: String, teamID: String, issueDate: Date, expireDuration: TimeInterval, auditor: String, p8: P8) -> String  {
    let newHeader = NewHeader(keyID: keyID)
    
    let iat = Int(issueDate.timeIntervalSince1970.rounded())
    let exp = iat + Int(expireDuration)
    
    let newPayload = NewPayload(teamID: teamID, issueDate: iat, expireDate: exp, auditor: auditor)
    
    let headerString = try! JSONEncoder().encode(newHeader).base64EncodedURLString()
    let payloadString = try! JSONEncoder().encode(newPayload).base64EncodedURLString()
    let digest = "\(headerString).\(payloadString)"
    
    let signature = try! p8.toASN1()
      .toECKeyData()
      .toPrivateKey()
      .es256Sign(digest: digest)
    
    let token = "\(digest).\(signature)"
    return token
  }
}
