//
//  NetworkPath.swift
//  sugarlive
//
//  Created by msi on 09/09/21.
//

import Foundation

class NetworkPath {
    
    class func createPath(_ service: NetworkService) -> String {
        switch service {
        case .Register:
            return "/auth/register"
        case .PhoneLogin:
            return "/auth/signIn/phone"
        case .GoogleLogin:
            return "/auth/signIn/google"
        case .FBLogin:
            return "/auth/signIn/fb"
        case .AppleLogin:
            return "/auth/signIn/apple"
        case .PusherLogin:
            return "/members/auth/pusher"
        case .RequestOTP(let identifier), .ResendOTP(let identifier):
            return "/auth/otp/\(identifier)"
        case .VerifyOTP:
            return "/auth/otp/verify"
        case .RefreshFirebaseToken:
            return "/members/firebase/refreshToken"
        case .Revoke:
            return "/auth/revoke"
        case .ForgotPassword:
            return "/auth/forgot"
        case .SocMedAccount(let identifier):
            return "/members/socialMedia/\(identifier)"
        case .UpdateSocMed:
            return "/members/socialMedia"
        case .Banner:
            return "/apps/banners"
        case .EntranceEffects:
            return "/apps/entranceEffects"
        case .PopupBanner:
            return "/apps/popupBanners"
        case .Splash:
            return "/apps/loadingScreen"
        case .ChatBubbles:
            return "/apps/chatBubbles"
        case .LiveUser, .ChangeLiveMode:
            return "/live"
        case .NewMember:
            return "/members/new"
        case .TopMember:
            return "/members/top"
        case .RecomendedMember:
            return "/members/recommended"
        case .Event:
            return "/apps/events"
        case .SearchMember:
            return "/members/search"
        case .OnLiveFollowing(let identifier):
            return "/members/following/\(identifier)/live"
        case .OfflineFollowing(let identifier):
            return "/members/following/\(identifier)/offline"
        case .LevelProgress:
            return "/members/levelProgress"
        case .BlockedUser:
            return "/members/blocked"
        case .BlockUser:
            return "/members/block"
        case .UnblockUser:
            return "/members/unblock"
        case .Moderation:
            return "/members/moderation"
        case .UserProfile(let identifier):
            return "/members/profile/\(identifier)"
        case .MyProfile:
            return "/members/profile/me"
        case .DeactivateProfile:
            return "/members/deactivate"
        case .UpdateProfile:
            return "/members/profile"
        case .UploadCover:
            return "/members/upload/cover"
        case .UploadPicture:
            return "/members/upload/picture"
        case .DeletePicture(let identifier):
            return "/members/profile/picture/\(identifier)"
        case .SetPicture(let identifier):
            return "/members/profile/picture/default/\(identifier)"
        case .ChangePhone:
            return "/members/profile/phone"
        case .GetFollower(let identifier, _):
            return "/members/follower/\(identifier)"
        case .GetFollowing(let identifier, _):
            return "/members/following/\(identifier)"
        case .GiftHistory:
            return "/members/history/gift"
        case .LiveHistory(let identifier, _):
            return "/members/history/live/\(identifier)"
        case .TopSpender(let identifier):
            return "/members/topSpender/\(identifier)"
        case .TopupHistory:
            return "/members/history/topup"
        case .UserBalance(let identifier):
            return "/members/balance/\(identifier)"
        case .Follow:
            return "/members/follow"
        case .Unfollow:
            return "/members/unfollow"
        case .UpdatePassword:
            return "/members/profile/password"
        case .SetFrame:
            return "/members/profile/avatarFrame/default"
        case .SetEntrance:
            return "/members/profile/entranceEffect/default"
        case .SetBadge:
            return "/members/profile/badge"
        case .SetChatBubble:
            return "/members/profile/chatBubble/default"
        case .GetBattleOpponents:
            return "/members/battleOpponents"
        case .Feeds, .PostFeed:
            return "/feeds"
        case .ReportPostFeed:
            return "/feeds/report/post"
        case .PostFeedMedia:
            return "/feeds/media"
        case .FeedComments(let identifier, _):
            return "/feeds/\(identifier)/comments"
        case .PostComment(let identifier, _):
            return "/feeds/\(identifier)/comments"
        case .LikeFeed(let identifier, _):
            return "/feeds/\(identifier)/likes"
        case .DeleteFeed(let identifier), .UpdateFeed(let identifier, _):
            return "/feeds/\(identifier)"
        case .GetCurrentFeed(let identifier):
            return "/feeds/\(identifier)/single"
        case .BanCategory:
            return "/apps/ban/categories"
        case .BanReason(let identifier):
            return "/apps/ban/\(identifier)/reasons"
        case .BanDuration:
            return "/apps/ban/durations"
        case .Feedback:
            return "/apps/feedback"
        case .Notifications:
            return "/notification"
        case .UnreadNotifications:
            return "/notification/unread"
        case .ReadNotification(let identifier):
            return "/notification/read/\(identifier)"
        case .UnreadMessages:
            return "/chat/totalUnread"
        case .PrivateChat:
            return "/chat"
        case .PrivateChatConversation(let identifier, _):
            return "/chat/\(identifier)"
        case .InitChat:
            return "/chat/init"
        case .SendChat:
            return "/chat/send"
        case .Gift:
            return "/apps/gifts"
        case .LiveCategory:
            return "/apps/categories"
        case .Version:
            return "/apps/version"
        case .LevelCategory:
            return "/apps/level"
        case .ProductStore:
            return "/store/products"
        case .Purchase:
            return "/store/purchase"
        case .PurchaseCandy:
            return "/purchase"
        case .IAP:
            return "/apps/iap/itunes"
        case .WatchLive:
            return "/live/watch"
        case .StopWatchLive:
            return "/live/leave"
        case .StartLive:
            return "/live/start"
        case .StopLive:
            return "/live/stop"
        case .DetailLive(let identifier):
            return "/live/\(identifier)"
        case .LiveSummary(let identifier):
            return "/live/\(identifier)/last"
        case .SetLivePrivate:
            return "/live/private"
        case .LiveValidate:
            return "/live/watch/validate"
        case .ChatLive:
            return "/live/chat"
        case .ReportLive:
            return "/live/report"
        case .InviteMultiHost:
            return "/live/invitation"
        case .JoinMultiHost:
            return "/live/multi/join"
        case .RequestJoinMultiHost:
            return "/live/multi/join"
        case .MultihostJoinRequests:
            return "/live/multi/request"
        case .MultihostJoinRequestsViewer(let identifier):
            return "/live/multi/request/\(identifier)"
        case .ApproveJoinMultiHost:
            return "/live/multi/request/accept"
        case .CancelJoinMultiHost:
            return "/live/multi/request/cancel"
        case .StartBattle:
            return "/live/multi/startBattle"
        case .StopBattle:
            return "/live/multi/stopBattle"
        case .KickGuest:
            return "/live/room/admin"
        case .SetRoomAdmin, .UnsetRoomAdmin:
            return "/live/room/admin"
        case .KickViewer:
            return "/live/kick"
        case .KickMultiguest:
            return "/live/multi/kick"
        case .LiveTurn:
            return "/live/turn"
        case .AskJoinMultihost:
            return "/live/multi/request"
        case .StickerList:
            return "/live/sticker/list"
        case .SendSticker:
            return "/live/sticker"
        case .SendGift:
            return "/live/gift"
        case .LiveShareLink(let identifier):
            return "/live/link/\(identifier)"
        case .BidResponded:
            return "/live/bid/responded"
        case .BidPlaced:
            return "/live/bid/placed"
        case .BidAction:
            return "/live/bid/action"
        case .BidAnswer:
            return "/live/suit/answer"
        case .BidClose:
            return "/live/suit/close"
        default:
            return ""
        }
    }
}
