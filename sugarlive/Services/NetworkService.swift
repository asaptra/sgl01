//
//  NetworkService.swift
//  sugarlive
//
//  Created by msi on 09/09/21.
//

import Foundation
import Moya
import ObjectMapper
import SwiftKeychainWrapper
import SystemConfiguration
import CoreTelephony
import ACProgressHUD_Swift

let provider = MoyaProvider<NetworkService>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
let multiProvider = MoyaProvider<MultiTarget>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])

enum NetworkService {
  case Register(request: RegisterRequestModel)
  case PhoneLogin(request: RegisterRequestModel)
  case GoogleLogin(request: RegisterRequestModel)
  case FBLogin(request: RegisterRequestModel)
  case AppleLogin(request: RegisterRequestModel)
  case PusherLogin(request: PusherRequestModel)
  case RequestOTP(identifier: String)
  case ResendOTP(identifier: String)
  case VerifyOTP(request: OTPRequestModel)
  case RefreshFirebaseToken(request: RefreshTokenRequestModel)
  case Revoke
  case ForgotPassword(request: ForgotPasswordRequestModel)
  case UpdateSocMed(request: [String: Any])
  case SocMedAccount(identifier: String)
  
  case Banner
  case EntranceEffects
  case PopupBanner
  case Splash
  case ChatBubbles
  
  case LiveUser
  case NewMember(request: PagingRequestModel)
  case TopMember
  case RecomendedMember
  case Event(request: PagingRequestModel)
  case SearchMember(request: SearchRequestModel)
  case OnLiveFollowing(identifier: String)
  case OfflineFollowing(identifier: String)
  case LevelProgress
  case BlockedUser(request: PagingRequestModel)
  case BlockUser(request: UserRequestModel)
  case UnblockUser(request: UserRequestModel)
  case Moderation(request: ModerationRequestModel)
  case UserProfile(identifier: String)
  case MyProfile
  case DeactivateProfile
  case UpdateProfile(request: UpdateProfileRequestModel)
  case UploadCover(request: UploadRequestModel)
  case UploadPicture(request: UploadRequestModel)
  case DeletePicture(identifier: String)
  case SetPicture(identifier: String)
  case ChangePhone(request: ChangePhoneRequestModel)
  case GetFollower(identifier: String, request: PagingRequestModel)
  case GetFollowing(identifier: String, request: PagingRequestModel)
  case GiftHistory
  case LiveHistory(identifier: String, request: PagingRequestModel)
  case TopSpender(identifier: String)
  case TopupHistory
  case UserBalance(identifier: String)
  case Follow(request: UserRequestModel)
  case Unfollow(request: UserRequestModel)
  case UpdatePassword(request: UpdatePasswordRequestModel)
  case SetFrame(request: ProfileDefaultRequestModel)
  case SetEntrance(request: ProfileDefaultRequestModel)
  case SetBadge(request: ProfileDefaultRequestModel)
  case SetChatBubble(request: ProfileDefaultRequestModel)
  case GetBattleOpponents(request: BattleOpponentsRequestModel)
  case PrivateChat
  case PrivateChatConversation(identifier: String, request: PagingRequestModel)
  case InitChat(request: InitChatRequestModel)
  case SendChat(request: SendChatRequestModel)
  case Feeds(request: PagingRequestModel)
  case FeedComments(identifier: String, request: PagingRequestModel)
  case PostFeed(request: PostFeedRequestModel)
  case ReportPostFeed(request: ReportPostFeedRequestModel)
  case PostFeedMedia(request: PostFeedMediaRequestModel)
  case PostComment(identifier: String, request: PostFeedRequestModel)
  case LikeFeed(identifier: String, request: LikeFeedRequestModel)
  case DeleteFeed(identifier: String)
  case GetCurrentFeed(identifier: String)
  case UpdateFeed(identifier: String, request: PostFeedRequestModel)
  
  case BanCategory
  case BanReason(identifier: String)
  case BanDuration
  case Feedback(request: FeedbackRequestModel)
  
  case Notifications(request: PagingRequestModel)
  case UnreadNotifications
  case ReadNotification(identifier: String)
  
  case UnreadMessages
  
  case Gift
  case LiveCategory
  case Version
  case LevelCategory
  
  case ProductStore(request: PagingRequestModel)
  case Purchase(request: PurchaseRequestModel)
  case PurchaseCandy(request: PurchaseCandyRequestModel)
  case IAP
  
  case WatchLive(request: WatchAndStopWatchLiveRequestModel)
  case StopWatchLive(request: WatchAndStopWatchLiveRequestModel)
  case StartLive(request: StartLiveRequestModel)
  case StopLive(request: StopLiveRequestModel)
  case DetailLive(identifier: String)
  case LiveSummary(identifier: String)
  case SetLivePrivate(request: SetLivePrivateRequestModel)
  case LiveValidate(request: LiveValidateRequestModel)
  case ChatLive(request: LiveChatRequestModel)
  case ReportLive(request: LiveReportRequestModel)
  case ChangeLiveMode(request: ChangeLiveModeRequestModel)
  
  case InviteMultiHost(request: InviteMultiHostRequestModel)
  case JoinMultiHost(request: JoinMultiHostRequestModel)
  case RequestJoinMultiHost(request: RequestJoinMultiHostRequestModel)
  case MultihostJoinRequests
  case MultihostJoinRequestsViewer(identifier: String)
  case ApproveJoinMultiHost(request: ApproveCancelJoinMultiHostRequestModel)
  case CancelJoinMultiHost(request: ApproveCancelJoinMultiHostRequestModel)
  case StartBattle(request: StartBattleRequestModel)
  case StopBattle(request: StopBattleRequestModel)
  case KickGuest(request: UserIdRequestModel)
  
  case SetRoomAdmin(request: UserIdRequestModel)
  case UnsetRoomAdmin
  case KickViewer(request: UserIdRequestModel)
  case KickMultiguest(request: UserIdRequestModel)
  
  case LiveTurn(request: LiveTurnRequestModel)
  
  case AskJoinMultihost(request: AskJoinMultihostRequestModel)
  
  case StickerList
  case SendSticker(request: SendStickerRequestModel)
  case SendGift(request: SendGiftRequestModel)
  
  case LiveShareLink(identifier: String)
  
  case BidResponded(request: BidRespondedRequestModel)
  case BidPlaced(request: BidPlacedRequestModel)
  case BidAction(request: BidActionRequestModel)
  case BidAnswer(request: BidAnswerRequestModel)
  case BidClose(request: BidCloseRequestModel)
  
  case AppleToken(request: AppleTokenRequestModel)
}

extension NetworkService: TargetType {
  
  public var baseURL: URL {
    switch self {
    case .AppleToken:
      return URL(string: "https://appleid.apple.com/auth/token")!
    case .PurchaseCandy:
      return URL(string: "https://devpg.sugarlive.co.id")!
      //      return URL(string: "https://hostdev.sugarlive.co.id/v2/api/purchase")!
      
    default:
      return URL(string: Constant.baseUrl)!
    }
  }
  
  public var path: String {
    return NetworkPath.createPath(self)
  }
  
  public var method: Moya.Method {
    switch self {
    case .Register, .PhoneLogin, .GoogleLogin, .FBLogin, .AppleLogin, .PusherLogin, .VerifyOTP, .Revoke, .ForgotPassword, .Feedback, .SearchMember, .BlockUser, .UnblockUser, .Moderation, .Follow, .Unfollow, .PostFeed, .PostFeedMedia, .PostComment, .LikeFeed, .UploadCover, .UploadPicture, .AppleToken, .Purchase, .PurchaseCandy, .WatchLive, .StopWatchLive, .StartLive, .StopLive, .LiveValidate, .ChatLive, .ReportLive, .InviteMultiHost, .JoinMultiHost, .ApproveJoinMultiHost, .CancelJoinMultiHost, .StartBattle, .StopBattle, .KickGuest, .KickMultiguest, .SetRoomAdmin, .KickViewer, .LiveTurn, .AskJoinMultihost, .SendSticker, .SendGift, .BidResponded, .BidPlaced, .BidAction, .BidAnswer, .BidClose, .InitChat, .SendChat, .ReportPostFeed:
      return .post
    case .RefreshFirebaseToken, .UpdateProfile, .ReadNotification, .UpdateSocMed, .UpdateFeed, .ChangeLiveMode:
      return .put
    case .DeactivateProfile, .SetPicture, .ChangePhone, .SetFrame, .SetEntrance, .SetBadge, .UpdatePassword, .SetChatBubble, .SetLivePrivate:
      return .patch
    case .DeletePicture, .DeleteFeed, .UnsetRoomAdmin:
      return .delete
    default:
      return .get
    }
  }
  
  public var task: Task {
    switch self.method {
    case .post:
      return NetworkTask.createParams(self)
    case .get:
      switch self {
      case .NewMember, .BlockedUser, .GetFollower, .GetFollowing, .GetBattleOpponents, .LiveHistory, .Feeds, .FeedComments, .Notifications, .Event, .PrivateChatConversation, .ProductStore:
        return NetworkTask.createParams(self)
      default:
        return .requestPlain
      }
    case .put:
      switch self {
      case .UpdateProfile, .UpdateSocMed, .UpdateFeed, .ChangeLiveMode:
        return NetworkTask.createParams(self)
      default:
        return .requestPlain
      }
    case .patch:
      switch self {
      case .ChangePhone, .SetFrame, .SetEntrance, .SetBadge, .UpdatePassword, .SetChatBubble, .SetLivePrivate:
        return NetworkTask.createParams(self)
      default:
        return .requestPlain
      }
    default:
      return .requestPlain
    }
  }
  
  public var headers: [String : String]? {
    var header = ["Accept" : "application/vnd.api+json",
                  "Content-Type" : "application/vnd.api+json"]
    switch self {
    case .PhoneLogin, .Feedback, .RecomendedMember, .MyProfile, .UserProfile, .UpdateProfile, .Feeds, .FeedComments, .LikeFeed, .UploadCover, .UploadPicture, .SetPicture, .DeletePicture, .PostFeed, .PostFeedMedia, .PostComment, .LiveHistory, .BlockedUser, .UpdateSocMed, .ChatBubbles, .SocMedAccount, .SetFrame, .SetEntrance, .SetBadge, .Notifications, .UnreadNotifications, .UnreadMessages, .ReadNotification, .OnLiveFollowing, .OfflineFollowing, .UpdatePassword, .ChangePhone, .SetChatBubble, .GetBattleOpponents, .DeleteFeed, .GetCurrentFeed, .UpdateFeed, .GetFollowing, .GetFollower, .Follow, .Unfollow,. BlockUser, .UnblockUser, .TopSpender, .DeactivateProfile, .ProductStore, .Purchase, .TopupHistory, .PurchaseCandy, .WatchLive, .StopWatchLive, .StartLive, .StopLive, .LiveSummary, .SetLivePrivate, .LiveValidate, .ChatLive, .ReportLive, .InviteMultiHost, .JoinMultiHost, .MultihostJoinRequests, .MultihostJoinRequestsViewer, .ApproveJoinMultiHost, .CancelJoinMultiHost, .StartBattle, .StopBattle, .KickGuest, .PrivateChat, .SetRoomAdmin, .KickViewer, .KickMultiguest, .ChangeLiveMode, .LiveTurn, .AskJoinMultihost, .StickerList, .SendSticker, .SendGift, .LiveShareLink, .PrivateChatConversation, .InitChat, .SendChat, .BidResponded, .BidPlaced, .BidAction, .BidAnswer, .BidClose, .Moderation, .LevelProgress, .ReportPostFeed, .SearchMember:
      
      if let accessToken = KeychainWrapper.standard.string(forKey: kAccessToken) {
        header["x-access-token"] = accessToken
      }
      header["x-api-key"] = Constant.apiKey
      break
    case .AppleToken:
      return ["Content-Type" : "application/x-www-form-urlencoded; charset=UTF-8"]
    default:
      break
    }
    return header
  }
}

extension MoyaProvider {
//  func request<T:Mappable, E:Mappable>(_ target: Target, successModel: T.Type, errorModel: E.Type, success: @escaping (T) -> Void, failure: @escaping (E) -> Void) {
  func request<T:Mappable>(_ target: Target, successModel: T.Type, withProgressHud: Bool = true, success: @escaping (T) -> Void, failure: @escaping (ErrorResponseModel) -> Void) {
    
    if withProgressHud {
      ACProgressHUD.shared.showHUD()
    }
    request(target) { (result) in
      if withProgressHud {
        ACProgressHUD.shared.hideHUD()
      }
      switch result {
      case let .success(response):
        do{
          let json = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String : Any]
          switch response.statusCode {
          case 200:
            if let data = json?["data"] as? [String : Any] {
              success(T.init(JSON: data)!)
            }
            break
          case 400, 401, 403, 404:
            if response.statusCode == 401 {
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: kAnauthorizedResponse), object: nil)
            }
            failure(ErrorResponseModel.init(JSON: json!)!)
            break
          default:
            break
          }
        }catch{
          switch response.statusCode {
          case 204:
            success(SuccessResponseModel.init(JSON: ["id": "", "type": ""]) as! T)
            break
          default:
            var e = ErrorResponseModel.init(JSON: [:])
            e?.title = "Error"
            e?.detail = ["Casting": ["Failure Casting"]]
            failure(e!)
            break
          }
        }
        break
      case let .failure(error):
        ACProgressHUD.shared.hideHUD()
        var e = ErrorResponseModel.init(JSON: [:])
        e?.title = "Failed"
        e?.detail = ["": ["Failure Casting"]]
        failure(e!)
        print(error.localizedDescription)
        break
      }
    }
  }
}

func getConnectionType() -> String {
  guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
    return "NO INTERNET"
  }
  
  var flags = SCNetworkReachabilityFlags()
  SCNetworkReachabilityGetFlags(reachability, &flags)
  
  let isReachable = flags.contains(.reachable)
  let isWWAN = flags.contains(.isWWAN)
  
  if isReachable {
    if isWWAN {
      let networkInfo = CTTelephonyNetworkInfo()
      var carrierTypeName = ""
      if #available(iOS 12.0, *) {
        let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
        guard let carrier = carrierType?.first?.value else {
          return "UNKNOWN"
        }
        carrierTypeName = carrier
      } else {
        if let carrier = networkInfo.currentRadioAccessTechnology {
          carrierTypeName = carrier
        } else {
          return "UNKNOWN"
        }
      }
      
      switch carrierTypeName {
      case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
        return "2G"
      case CTRadioAccessTechnologyLTE:
        return "4G"
      default:
        return "3G"
      }
    } else {
      return "WIFI"
    }
  } else {
    return "NO INTERNET"
  }
}
