//
//  NetworkTask.swift
//  sugarlive
//
//  Created by msi on 09/09/21.
//

import Foundation
import Moya

class NetworkTask {
    
    class func createParams(_ service: NetworkService) -> Task {
        switch service {
        case .Register(let request), .PhoneLogin(let request), .GoogleLogin(let request), .FBLogin(let request), .AppleLogin(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .PusherLogin(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .RefreshFirebaseToken(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .ForgotPassword(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .VerifyOTP(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .UpdatePassword(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .Feedback(let request):
            return .uploadMultipart(request.feedbackFormData)
        case .NewMember(let request), .BlockedUser(let request), .GetFollower(_, let request), .GetFollowing(_, let request), .LiveHistory(_, let request), .Feeds(let request), .FeedComments(_, let request), .Notifications(let request), .Event(let request), .PrivateChatConversation(_, let request), .ProductStore(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: URLEncoding.queryString)
        case .GetBattleOpponents(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: URLEncoding.queryString)
        case .UpdateSocMed(let request):
            return .requestParameters(parameters: request, encoding: JSONEncoding.default)
        case .SearchMember(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .BlockUser(let request), .UnblockUser(let request), .Follow(let request), .Unfollow(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .Moderation(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .UpdateProfile(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .UploadCover(let request), .UploadPicture(let request):
            return .uploadMultipart(request.formData)
        case .ChangePhone(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .SetFrame(let request), .SetEntrance(let request), .SetBadge(let request), .SetChatBubble(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .PostFeedMedia(let request):
            return .uploadMultipart(request.feedFormData)
        case .PostFeed(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .ReportPostFeed(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .InitChat(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .SendChat(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .UpdateFeed(_, let request):
//            return .uploadMultipart(request.feedFormData)
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .PostComment(_, let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .LikeFeed(_, let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .Purchase(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .PurchaseCandy(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .AppleToken(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .WatchLive(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .StopWatchLive(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .StartLive(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .StopLive(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .SetLivePrivate(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .LiveValidate(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .ChatLive(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .ReportLive(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .InviteMultiHost(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .JoinMultiHost(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .RequestJoinMultiHost(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .ApproveJoinMultiHost(let request), .CancelJoinMultiHost(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .StartBattle(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .StopBattle(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .KickGuest(let request), .SetRoomAdmin(let request), .KickViewer(let request), .KickMultiguest(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .ChangeLiveMode(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .LiveTurn(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .AskJoinMultihost(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case.SendSticker(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case.SendGift(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .BidResponded(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .BidPlaced(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .BidAction(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .BidAnswer(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        case .BidClose(let request):
            return .requestParameters(parameters: request.toJSON(), encoding: JSONEncoding.default)
        default :
            return .requestParameters(parameters: [:],
                                      encoding: JSONEncoding.default)
        }
    }
}
