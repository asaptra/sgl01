//
//  ChatBubbleProvider.swift
//  sugarlive
//
//  Created by cleanmac-ada on 03/06/22.
//

import Foundation

class ChatBubbleProvider {
    static let shared = ChatBubbleProvider()
    
    private(set) var list: [ChatBubbleModel] = []
    
    private init() {}
    
    func storeChatBubbles(_ list: [ChatBubbleModel]) {
        self.list.removeAll()
        self.list = list
    }
    
    func getBubble(from id: String) -> ChatBubbleModel? {
        if let bubble = list.first(where: { $0.chatBubbleId == id }) {
            return bubble
        } else {
            return nil
        }
    }
}
