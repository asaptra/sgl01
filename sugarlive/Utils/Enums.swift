//
//  Enums.swift
//  sugarlive
//
//  Created by cleanmac-ada on 19/02/22.
//

import UIKit

enum MultiHostType: Int {
    case Solo = 2
    case FourMultiHost = 4
    case SixMultiHost = 6
    case GroupCall = 9
}

enum LiveType {
    case Host
    case Admin
    case Viewer
}

enum AudioVideoState: Int {
    case Mute = 0
    case Unmute = 1
    
    mutating func toggle() {
        switch self {
        case .Mute:
            self = .Unmute
        case .Unmute:
            self = .Mute
        }
    }
    
    func getOppositeValue() -> AudioVideoState {
        switch self {
        case .Mute:
            return .Unmute
        case .Unmute:
            return .Mute
        }
    }
}

enum MultiguestStreamingType: Int {
    case Publishing = 0
    case Streaming = 1
}

enum StreamUpdateType {
    case Add
    case Delete
}

enum LiveEndType {
    case Default
    case HostKicked
    case AdminKicked
    case BannedSession
}

enum ModalActionType {
    case Normal
    case Warning
    
    func getActionColor() -> UIColor {
        switch self {
        case .Normal:
            return UIColor(hex: "#414141")
        case .Warning:
            return UIColor.red
        }
    }
}

enum TopSpenderType {
    case Daily
    case Monthly
    case Total
}

enum PrivateRoomType: String {
    case Password = "password"
    case PayPerSession = "pay_per_session"
}

enum BidAmount: Equatable {
    case TenThousand
    case HundredThousand
    case Other(Int)
    
    var value: Int {
        switch self {
        case .TenThousand:
            return 10000
        case .HundredThousand:
            return 100000
        case .Other(let int):
            return int
        }
    }
}

enum BidAction: String {
    case Accept = "accept"
    case Reject = "reject"
}

enum SuitAnswer: String {
    case Jempol = "jempol"
    case Telunjuk = "telunjuk"
    case Kelingking = "kelingking"
}


enum GiftCategory: String {
    case Combo = "Combo"
    case LoAnimation = "Lo-Animation"
    case HiAnimation = "Hi-Animation"
    case GlobalAnimation = "Global Animation"
}
