//
//  PusherRequestBuilder.swift
//  sugarlive
//
//  Created by cleanmac on 23/12/21.
//

import Foundation
import PusherSwift
import SwiftKeychainWrapper

class PusherRequestBuilder: AuthRequestBuilderProtocol {
    private var userId: String = ""
    
    init(userId: String) {
        self.userId = userId
    }
    
    func requestFor(socketID: String, channelName: String) -> URLRequest? {
        var request = URLRequest(url: URL(string: Constant.baseUrl + "/members/auth/pusher")!)
        request.httpMethod = "POST"
        let params: [String: String] = [
            "user_id": userId,
            "socket_id": socketID,
            "channel_name": channelName
        ]
        request.httpBody = params.percentEncoded()
//        request.addValue(KeychainWrapper.standard.string(forKey: kAccessToken)!, forHTTPHeaderField: "x-access-token")
//        request.addValue("application/vnd.api+json", forHTTPHeaderField: "Accept")
//        request.addValue("application/vnd.api+json", forHTTPHeaderField: "Content-Type")
        return request
    }
}
