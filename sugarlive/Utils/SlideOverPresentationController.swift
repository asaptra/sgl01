//
//  SlideOverPresentationController.swift
//  sugarlive
//
//  Created by cleanmac on 24/11/21.
//

import UIKit

class SlideOverPresentationController: UIPresentationController {

    private let backgroundView: UIView!
    //private var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()
    
    override var frameOfPresentedViewInContainerView: CGRect {
        CGRect(origin: CGPoint(x: 0, y: 0),
               size: CGSize(width: containerView!.frame.width, height: containerView!.frame.height))
    }
    
    override init(presentedViewController: UIViewController, presenting presentingVc: UIViewController?) {
        backgroundView = UIView()
        backgroundView.backgroundColor = .black
        
        super.init(presentedViewController: presentedViewController, presenting: presentingVc)
        //tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
        backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //backgroundView.isUserInteractionEnabled = true
        //backgroundView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func presentationTransitionWillBegin() {
        backgroundView.alpha = 0
        containerView?.addSubview(backgroundView)
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.backgroundView.alpha = 0.7
        }, completion: { _ in
            
        })
    }
    
    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.backgroundView.alpha = 0
        }, completion: { _ in
            
        })
    }
    
    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()
        presentedView?.frame = frameOfPresentedViewInContainerView
        backgroundView.frame = containerView!.bounds
    }
    
    @objc private func dismissAction() {
        presentedViewController.dismiss(animated: true, completion: nil)
    }
}
