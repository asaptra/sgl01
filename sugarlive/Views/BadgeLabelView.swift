//
//  BadgeLabelView.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit

class BadgeLabelView: UIView {
  
  @IBOutlet weak var badgeImageView: UIImageView!
  @IBOutlet weak var categoryButton: UIButton!
  @IBOutlet weak var titleLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    categoryButton.layer.cornerRadius = categoryButton.frame.height/2
    categoryButton.layer.masksToBounds = true
  }
}
