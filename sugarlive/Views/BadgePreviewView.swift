//
//  BadgePreviewView.swift
//  sugarlive
//
//  Created by msi on 13/09/21.
//

import UIKit

class BadgePreviewView: UIView {
  
    @IBOutlet weak var leftBadgeView: UIView!
    @IBOutlet weak var rightBadgeView: UIView!
    @IBOutlet weak var centerBadgeView: UIView!
    @IBOutlet weak var leftBadgeImage: UIImageView!
    @IBOutlet weak var centerBadgeImage: UIImageView!
    @IBOutlet weak var rightBadgeImage: UIImageView!
    @IBOutlet weak var levelButton: UIButton!
    @IBOutlet weak var bubbleImage: UIImageView!
    @IBOutlet weak var chatLabel: UILabel!
    @IBOutlet weak var badgeContainer: UIStackView!
    
    //bubble
    @IBOutlet weak var bubbleChatContainer: UIView!
    @IBOutlet weak var topLeftBubbleImage: UIImageView!
    @IBOutlet weak var topMidBubbleImage: UIImageView!
    @IBOutlet weak var topRightBubbleImage: UIImageView!
    @IBOutlet weak var midLeftBubbleImage: UIImageView!
    @IBOutlet weak var midMidBubbleImage: UIImageView!
    @IBOutlet weak var midRightBubbleImage: UIImageView!
    @IBOutlet weak var bottomLeftBubbleImage: UIImageView!
    @IBOutlet weak var bottomMidBubbleImage: UIImageView!
    @IBOutlet weak var bottomRightBubbleImage: UIImageView!
    
    @IBOutlet weak var sampleChatLbl: UILabel!
    @IBOutlet weak var badgeLevelView: UIView!
    @IBOutlet weak var badgeNumberLbl: UILabel!
    
    var previewType: ProfileManagerType = .Badge {
    didSet {
      setupLayout()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  func setupLayout() {
//    let radius = (Constant.screenSize.width * 0.07) / 2
    
      leftBadgeView.layer.cornerRadius = leftBadgeView.layer.bounds.width / 2
      leftBadgeView.clipsToBounds = true
      rightBadgeView.cornerRadius = rightBadgeView.layer.bounds.width / 2
      rightBadgeView.clipsToBounds = true
      centerBadgeView.cornerRadius = centerBadgeView.layer.bounds.width / 2
      centerBadgeView.clipsToBounds = true

      levelButton.setAttributedTitle("15".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultSemiBoldFont(size: 10)]), for: .normal)
      levelButton.layer.cornerRadius = 10
      levelButton.layer.masksToBounds = true
      badgeLevelView.layer.cornerRadius = 10
      badgeLevelView.layer.masksToBounds = true
      badgeNumberLbl.font = .defaultSemiBoldFont(size:10)
    
    if previewType == .Badge {
      let attStr = NSMutableAttributedString(attributedString: "sugarlive : chat goes here".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 14),
                                                                          NSAttributedString.Key.foregroundColor: UIColor.black]))
      attStr.addAttributes([NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12)], range: .init(location: 0, length: 10))
      chatLabel.attributedText = attStr
        
    } else {
      let attStr = NSMutableAttributedString(attributedString: "sugarlive : your awesome text goes here".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 14),
                                                                                                                      NSAttributedString.Key.foregroundColor: UIColor.white]))
      attStr.addAttributes([NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12)], range: .init(location: 0, length: 10))
      chatLabel.attributedText = attStr
        sampleChatLbl.attributedText = attStr
    }
  }
}
