//
//  CategoryLiveView.swift
//  sugarlive
//
//  Created by msi on 07/10/21.
//

import UIKit

@IBDesignable class CategoryLiveView: UIView {
    @IBOutlet weak var carrotContainerView: UIView!
    @IBOutlet weak var carrotIcon: UIImageView!
    @IBOutlet weak var carrotLabel: UILabel!
    @IBOutlet weak var closeCarrotButton: UIButton!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    private var hashtags: [String] = []
    private var category: CategoryModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = CategoryLiveView.nib(withType: CategoryLiveView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupLayout()
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = CategoryLiveView.nib(withType: CategoryLiveView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    private func setupLayout() {
        carrotContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        carrotContainerView.layer.cornerRadius = carrotContainerView.frame.height / 2
        carrotContainerView.layer.masksToBounds = true
        
        categoryCollectionView.backgroundColor = .clear
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        categoryCollectionView.register(UINib(nibName: "LiveCategoryCollectionCell", bundle: nil), forCellWithReuseIdentifier: "LiveCategoryCollectionCell")
    }
    
    func setupCollectionViewData(category: CategoryModel, hashtags: [String]) {
        self.category = category
        self.hashtags = hashtags.filter{ $0 != "" }
        categoryCollectionView.reloadData()
    }
    
    func setCarrotAmount(_ amount: Double) {
        carrotLabel.text = "\(Int(amount))".thousandFormatter()
    }
}

extension CategoryLiveView: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        section == 0 ? 1 : hashtags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveCategoryCollectionCell", for: indexPath) as! LiveCategoryCollectionCell
        indexPath.section == 0 ? cell.setTitle(categoryName: category?.title ?? "", categoryColor: category?.color ?? "#000000") : cell.setTitle(hashtag: hashtags[indexPath.row])
        return cell
    }
}

extension CategoryLiveView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

extension CategoryLiveView: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = indexPath.section == 0 ? (category?.title ?? "").size(withAttributes: nil) : (hashtags[indexPath.row]).size(withAttributes: nil)
        let height = collectionView.frame.height
        return .init(width: (size.width + 25), height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        }
        return .zero
    }
}
