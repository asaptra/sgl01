//
//  ChatLiveView.swift
//  sugarlive
//
//  Created by msi on 07/10/21.
//

import UIKit

@IBDesignable class ChatLiveView: UIView {
    
    @IBOutlet weak var chatContainerView: UIView!
    @IBOutlet weak var chatTextField: UITextField!
    @IBOutlet weak var chatIcon: UIImageView!
    @IBOutlet weak var lockButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var versusButton: UIButton!
    @IBOutlet weak var rotateButton: UIButton!
    @IBOutlet weak var giftButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = ChatLiveView.nib(withType: ChatLiveView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupLayout()
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = ChatLiveView.nib(withType: ChatLiveView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    func setupLayout() {
        chatContainerView.layer.cornerRadius = chatContainerView.frame.size.height/2
        chatContainerView.layer.masksToBounds = true
        chatContainerView.backgroundColor = .init(hex: "2B2B2B")
        
        chatTextField.returnKeyType = .send
        chatTextField.attributedPlaceholder = "Chat here".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.white])
        
        lockButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        shareButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        versusButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        rotateButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        giftButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        likeButton.backgroundColor = UIColor.white
        
        lockButton.layer.cornerRadius = lockButton.frame.size.height / 2
        shareButton.layer.cornerRadius = shareButton.frame.size.height / 2
        versusButton.layer.cornerRadius = versusButton.frame.size.height / 2
        rotateButton.layer.cornerRadius = rotateButton.frame.size.height / 2
        giftButton.layer.cornerRadius = giftButton.frame.size.height / 2
        likeButton.layer.cornerRadius = likeButton.frame.size.height / 2
    }
    
    func setupButtons(_ type: LiveType, _ multihostType: MultiHostType? = nil) {
        if type == .Host {
            giftButton.isHidden = true
            likeButton.isHidden = true
            if multihostType != .Solo {
                versusButton.isHidden = true
            }
            
            if multihostType == .GroupCall {
                rotateButton.isHidden = true
            }
        } else {
            rotateButton.isHidden = true
            versusButton.isHidden = true
            lockButton.isHidden = true
        }
    }
    
    func setChatFieldDelegate(_ delegate: UITextFieldDelegate) {
        chatTextField.delegate = delegate
    }
}
