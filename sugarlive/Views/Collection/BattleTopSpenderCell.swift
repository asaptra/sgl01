//
//  BattleTopSpenderCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 29/06/22.
//

import UIKit
import AlamofireImage

class BattleTopSpenderCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topSpenderImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        topSpenderImage.layer.cornerRadius = topSpenderImage.frame.height / 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        topSpenderImage.image = UIImage(named: "ic_default_profile")
    }
    
    func setContents(_ model: BattleTopSpenderModel) {
        if let url = URL(string: model.coverPicture ?? "") {
            topSpenderImage.af.setImage(withURL: url)
        } else {
            topSpenderImage.image = UIImage(named: "ic_default_profile")
        }
    }

}
