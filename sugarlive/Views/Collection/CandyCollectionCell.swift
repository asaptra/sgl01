//
//  CandyCollectionCell.swift
//  sugarlive
//
//  Created by msi on 13/10/21.
//

import UIKit

class CandyCollectionCell: UICollectionViewCell {
  
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var topLabel: UILabel!
  @IBOutlet weak var bottomView: UIView!
  @IBOutlet weak var candyImage: UIImageView!
  @IBOutlet weak var countLabel: UILabel!
  @IBOutlet weak var candyLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
    func setupLayout() {
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true

        topView.backgroundColor = .init(hex: "FFEE43")
        topLabel.font = .defaultBoldItalicFont(size: 12)
        topLabel.textColor = .init(hex: "42215A")

        bottomView.backgroundColor = .init(hex: "42215A")

        countLabel.font = .defaultBoldFont(size: 16)
        countLabel.textColor = .white

        candyLabel.font = .defaultRegularFont(size: 11)
        candyLabel.textColor = .init(hex: "F294CE")
        candyLabel.text = "Candy"

        priceLabel.textColor = .init(hex: "29B2FF")
        priceLabel.font = .defaultRegularFont(size: 11)
    }
  
  func bind() {
    topLabel.text = "Rp. 300.000"
    countLabel.text = "1.200.000"
  }
}
