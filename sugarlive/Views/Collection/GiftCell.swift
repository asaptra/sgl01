//
//  GiftCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 09/05/22.
//

import UIKit
import AlamofireImage

class GiftCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var giftImage: UIImageView!
    @IBOutlet weak var giftNameLabel: UILabel!
    @IBOutlet weak var giftPriceLabel: UILabel!
    
    override var isSelected: Bool {
        willSet {
            super.isSelected = newValue
            if newValue {
                containerView.layer.borderColor = UIColor.systemBlue.cgColor
            } else {
                containerView.layer.borderColor = UIColor.clear.cgColor
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.borderWidth = 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func setContents(_ model: GiftModel) {
        if let url = URL(string: model.thumbnail ?? "") {
            let filter = AspectScaledToFitSizeFilter(size: giftImage.frame.size)
            giftImage.af.setImage(withURL: url, filter: filter)
        }
        
        giftNameLabel.text = model.gift ?? ""
        giftPriceLabel.text = "\(model.price)"
    }

}
