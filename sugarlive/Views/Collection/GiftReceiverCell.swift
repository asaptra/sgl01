//
//  GiftReceiverCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 12/06/22.
//

import UIKit
import AlamofireImage

class GiftReceiverCell: UICollectionViewCell {

    @IBOutlet weak var receiverImage: UIImageView!
    @IBOutlet weak var positionContainerView: UIView!
    @IBOutlet weak var positionLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            receiverImage.layer.borderColor = isSelected ? UIColor.white.cgColor : UIColor.lightGray.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        receiverImage.layer.borderWidth = 2
        receiverImage.layer.borderColor = UIColor.lightGray.cgColor
        
        positionContainerView.layer.borderWidth = 1
        positionContainerView.layer.borderColor = UIColor.white.cgColor
    }
    
    func setContents(imageURL: String, position: Int) {
        if let url = URL(string: imageURL) {
            receiverImage.af.setImage(withURL: url)
        } else {
            receiverImage.image = UIImage(named: "ic_default_profile")
        }
        
        positionLabel.text = position == 0 ? "Host" : "\(position + 1)"
    }
    
}
