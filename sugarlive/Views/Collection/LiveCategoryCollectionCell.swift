//
//  LiveCategoryCollectionCell.swift
//  sugarlive
//
//  Created by cleanmac on 13/11/21.
//

import UIKit

class LiveCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var labelContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        labelContainerView.layer.cornerRadius = labelContainerView.frame.size.height / 2
    }
    
    func setTitle(categoryName: String, categoryColor: String) {
        titleLabel.text = categoryName
        labelContainerView.backgroundColor = UIColor(hex: categoryColor).withAlphaComponent(0.5)
    }
    
    func setTitle(hashtag: String) {
        titleLabel.text = "#" + hashtag
        labelContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func setTitle(title: String, color: String = "#000000") {
        titleLabel.text = title
        labelContainerView.backgroundColor = UIColor(hex: color).withAlphaComponent(0.5)
    }
    
    func setTitle(title: String, color: UIColor = UIColor.black.withAlphaComponent(0.5), titleColor: UIColor) {
        titleLabel.text = title
        titleLabel.textColor = titleColor
        labelContainerView.backgroundColor = color
    }

}
