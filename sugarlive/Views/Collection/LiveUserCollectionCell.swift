//
//  LiveUserCollectionCell.swift
//  sugarlive
//
//  Created by cleanmac on 13/11/21.
//

import UIKit
import AlamofireImage

class LiveUserCollectionCell: UICollectionViewCell {

    @IBOutlet weak var avatarFrameImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarFrameImage.layer.cornerRadius = avatarFrameImage.frame.height / 2
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarFrameImage.isHidden = true
    }
    
    func setImage(frame: String, profile: String) {
        if let frameUrl = URL(string: frame) {
            avatarFrameImage.isHidden = false
            avatarFrameImage.af.setImage(withURL: frameUrl)
        } else {
            avatarFrameImage.isHidden = true
        }
        
        if let profileUrl = URL(string: profile) {
            profileImage.af.setImage(withURL: profileUrl)
        } else {
            profileImage.image = UIImage(named: "ic_default_profile")
        }
    }

}
