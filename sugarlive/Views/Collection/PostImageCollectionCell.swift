//
//  PostImageCollectionCell.swift
//  sugarlive
//
//  Created by msi on 11/09/21.
//

import UIKit

class PostImageCollectionCell: UICollectionViewCell {
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var deleteButton: UIButton!
  @IBOutlet weak var addImageView: UIImageView!
  
  var isEmpty = true
  
  var deleteTapped: (_ sender: Any) -> Void = {_ in }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    imageView.layer.cornerRadius = 10
    imageView.layer.masksToBounds = true
  }
  
  func bind(url: URL) {
    imageView.af.setImage(withURL: url)
    isEmpty = false
  }
  
  func bind(image: UIImage) {
    imageView.image = image
    isEmpty = false
  }
  
  @IBAction func deleteAction(_ sender: Any) {
    deleteTapped(sender)
  }
}
