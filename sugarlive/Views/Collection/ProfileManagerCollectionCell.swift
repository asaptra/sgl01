//
//  ProfileManagerCollectionCell.swift
//  sugarlive
//
//  Created by msi on 13/09/21.
//

import UIKit

class ProfileManagerCollectionCell: UICollectionViewCell {
  
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var selectedButton: UIButton!
  
  @IBOutlet weak var priceContainerView: UIView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var ownedButton: UIButton!
  @IBOutlet weak var ownedView: UIView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    containerView.layer.cornerRadius = 10
    containerView.layer.masksToBounds = true
    containerView.backgroundColor = .init(hex: "F2F2F2")
    
    nameLabel.font = .defaultBoldFont(size: 12)
    priceLabel.font = .defaultRegularFont(size: 10)
    
    ownedButton.setAttributedTitle("Owned".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 9),
                                                                     NSAttributedString.Key.foregroundColor: UIColor.white]),
                                   for: .normal)
    ownedButton.backgroundColor = .init(hex: "EC2020")
    ownedButton.layer.cornerRadius = 8
    ownedButton.layer.masksToBounds = true
  }
  
  func bind(product: ProductModel) {
    priceContainerView.isHidden = false
    nameLabel.text = product.name
    let price = Int(product.price - product.priceDiscount)
    priceLabel.text = "\(price.roundedWithAbbreviations()) Candy"
  }
}
