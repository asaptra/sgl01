//
//  StickerListCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 03/05/22.
//

import UIKit
import AlamofireImage

class StickerCell: UICollectionViewCell {
    
    @IBOutlet weak var stickerImage: UIImageView!
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                stickerImage.alpha = 1
            } else {
                stickerImage.alpha = 0.5
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isSelected = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        isSelected = false
        stickerImage.image = nil
    }
    
    func setImage(with imgUrl: String) {
        if let url = URL(string: imgUrl) {
            let filter = AspectScaledToFitSizeFilter(size: stickerImage.frame.size)
            stickerImage.af.setImage(withURL: url, filter: filter)
        }
    }

}
