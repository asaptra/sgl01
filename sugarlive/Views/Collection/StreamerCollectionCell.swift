//
//  StreamerCollectionCell.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit

class StreamerCollectionCell: UICollectionViewCell {
  
  @IBOutlet weak var profileContainerView: UIView!
  @IBOutlet weak var profileImageView: UIImageView!
  @IBOutlet weak var viewerContainerView: UIView!
  @IBOutlet weak var viewerLabel: UILabel!
  @IBOutlet weak var bottomStack: UIStackView!
  
  lazy var categoryView: BadgeLabelView = {
    let categoryView = BadgeLabelView.nib(withType: BadgeLabelView.self)
    categoryView.badgeImageView.superview?.isHidden = true
    categoryView.titleLabel.font = .defaultRegularFont(size: 12)
    categoryView.titleLabel.textColor = .white
    return categoryView
  }()
  
  lazy var nameView: BadgeLabelView = {
    let nameView = BadgeLabelView.nib(withType: BadgeLabelView.self)
    nameView.categoryButton.superview?.isHidden = true
    nameView.titleLabel.font = .defaultSemiBoldFont(size: 12)
    nameView.titleLabel.textColor = .white
    return nameView
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    profileContainerView.layer.cornerRadius = 10
    profileContainerView.layer.masksToBounds = true
    
    viewerContainerView.layer.cornerRadius = viewerContainerView.frame.height/2
    viewerContainerView.layer.masksToBounds = true
    viewerLabel.font = .defaultSemiBoldFont(size: 10)
    viewerLabel.textColor = .white
    
    categoryView.categoryButton.backgroundColor = .init(hex: "44A194")
    nameView.categoryButton.backgroundColor = .init(hex: "44A194")
    bottomStack.addArrangedSubview(categoryView)
    bottomStack.addArrangedSubview(nameView)
  }
  
  func bind(user: UserModel,_ isTop: Bool = true) {
    if let urlString = user.coverPicture, let url = URL(string: urlString) {
      profileImageView.af.setImage(withURL: url)
    } else {
      profileImageView.image = UIImage(named: "ic_default_profile")
    }
    viewerLabel.text = "\(Int(user.view))"
    if isTop {
      categoryView.categoryButton.backgroundColor = .init(hex: user.liveDetail?.categoryColor ?? "ffffff")
      categoryView.categoryButton.setAttributedTitle(user.liveDetail?.category?.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultSemiBoldFont(size: 10),
                                                                                                          NSAttributedString.Key.foregroundColor: UIColor.white]),
                                                     for: .normal)
      categoryView.titleLabel.text = ""
    } else {
      categoryView.categoryButton.backgroundColor = .init(hex: user.level?.color ?? "ffffff")
      categoryView.categoryButton.setAttributedTitle("\(user.level?.level ?? 0)".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultSemiBoldFont(size: 10),
                                                                                                           NSAttributedString.Key.foregroundColor: UIColor.white]),
                                                     for: .normal)
      categoryView.titleLabel.text = user.bio
    }
    nameView.titleLabel.text = user.fullname
    if user.verified {
      nameView.badgeImageView.image = UIImage(named: "ic_badge_purple")
    }
  }
}
