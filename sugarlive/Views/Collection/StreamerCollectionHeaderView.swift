//
//  StreamerCollectionHeaderView.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit

class StreamerCollectionHeaderView: UICollectionReusableView {
  @IBOutlet weak var titleLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    titleLabel.font = .defaultBoldFont(size: 14)
  }
}
