//
//  ComboBannerView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 07/06/22.
//

import UIKit
import SDWebImage

class ComboBannerView: UIView {

    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var giftNameLabel: UILabel!
    
    @IBOutlet weak var giftThumbnailImage: SDAnimatedImageView!
    @IBOutlet weak var giftMultiplierLabel: UILabel!
    
    static let DEFAULT_HEIGHT: CGFloat = 50
    static let DEFAULT_WIDTH: CGFloat = 350
    
    private let strokeTextAttributes: [NSAttributedString.Key : Any] = [
        .strokeColor : UIColor.white,
        .foregroundColor : UIColor(hex: "#8F3AFF"),
        .strokeWidth : -2.0,
    ]
    
    private var multiplier: Int = 1 {
        didSet {
            giftMultiplierLabel.attributedText = NSAttributedString(string: "\(multiplier)", attributes: strokeTextAttributes)
        }
    }
    
    private let multiplierLabelScale: CGAffineTransform = CGAffineTransform(scaleX: 1.5, y: 1.5)
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = ComboBannerView.nib(withType: ComboBannerView.self)
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if subviews.count == 0 {
            let view = ComboBannerView.nib(withType: ComboBannerView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = constraints
            removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    func setContents(_ model: GiftDetailEventModel) {
        senderNameLabel.text = model.sender?.fullname ?? ""
        giftNameLabel.text = "Send gift \"\(model.gift?.name ?? "")\""
        
        if let url = URL(string: model.gift?.thumbnail ?? "") {
            giftThumbnailImage.sd_setImage(with: url, placeholderImage: nil, options: [.retryFailed], context: [.imageThumbnailPixelSize: CGSize(width: giftThumbnailImage.bounds.width, height: giftThumbnailImage.bounds.height)])
        }
        
        multiplier = model.gift?.count ?? 1
    }
    
    func resetContents() {
        senderNameLabel.text = ""
        giftNameLabel.text = ""
        giftThumbnailImage.image = nil
        multiplier = 1
    }
    
    func increaseMultiplier(count: Int) {
        multiplier += count
        animateMultiplier()
    }
    
    private func animateMultiplier() {
        UIView.animate(withDuration: 0.1, delay: 0, animations: {
            self.giftMultiplierLabel.transform = self.multiplierLabelScale
            self.layoutIfNeeded()
        }, completion: { _ in
            UIView.animate(withDuration: 0.1, delay: 0, animations: {
                self.giftMultiplierLabel.transform = .identity
                self.layoutIfNeeded()
            })
        })
    }
}
