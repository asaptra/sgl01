//
//  CommentInputView.swift
//  sugarlive
//
//  Created by msi on 11/09/21.
//

import UIKit

class CommentInputView: UIView {
  
  @IBOutlet weak var inputContainerView: UIView!
  @IBOutlet weak var inputField: BaseTextView!
  @IBOutlet weak var sendButton: UIButton!
  
  var sendTapped: (_ sender: Any) -> Void = {_ in }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    inputContainerView.backgroundColor = .init(hex: "E9E9E9")
    
    inputField.font = .defaultSemiBoldFont(size: 14)
    inputField.textColor = .MEDIUM_TEXT
    
    sendButton.backgroundColor = .PRIMARY_2
  }
  
  @IBAction func sendAction(_ sender: Any) {
    sendTapped(sender)
  }
}
