//
//  CommentLiveView.swift
//  sugarlive
//
//  Created by msi on 07/10/21.
//

import UIKit

@IBDesignable class CommentLiveView: UIView {
  
  @IBOutlet weak var levelButton: UIButton!
  @IBOutlet weak var commentContainerView: UIView!
  @IBOutlet weak var bubbleImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var separatorLabel: UILabel!
  @IBOutlet weak var commentLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    let view = CommentLiveView.nib(withType: CommentLiveView.self)
    view.frame = self.bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    self.addSubview(view)
    setupLayout()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
  
  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    setupLayout()
  }
  
  override func awakeAfter(using coder: NSCoder) -> Any? {
    if self.subviews.count == 0 {
      let view = CommentLiveView.nib(withType: CommentLiveView.self)
      view.translatesAutoresizingMaskIntoConstraints = false
      let constraints = self.constraints
      self.removeConstraints(constraints)
      view.addConstraints(constraints)
      return view
    }
    return self
  }
  
  func setupLayout() {
    levelButton.layer.cornerRadius = 10
    levelButton.layer.masksToBounds = true
    levelButton.titleLabel?.numberOfLines = 0
    
    let font = UIFont.defaultRegularFont(size: 14)
    nameLabel.textColor = .white
    nameLabel.font = font
    separatorLabel.textColor = .white
    separatorLabel.font = font
    commentLabel.textColor = .white
    commentLabel.font = font
  }
}
