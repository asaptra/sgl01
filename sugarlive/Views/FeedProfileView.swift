//
//  FeedProfileView.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit

@IBDesignable class FeedProfileView: UIView {
  
  
  @IBOutlet weak var profileImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var actionButton: UIButton!
  
  var profileTapped: (_ sender: Any) -> Void = {_ in }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    let view = FeedProfileView.nib(withType: FeedProfileView.self)
    view.frame = self.bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    self.addSubview(view)
    setupLayout()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
  
  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    setupLayout()
  }
  
  override func awakeAfter(using coder: NSCoder) -> Any? {
    if self.subviews.count == 0 {
      let view = FeedProfileView.nib(withType: FeedProfileView.self)
      view.translatesAutoresizingMaskIntoConstraints = false
      let constraints = self.constraints
      self.removeConstraints(constraints)
      view.addConstraints(constraints)
      return view
    }
    return self
  }
  
  func setupLayout() {
    profileImageView.layer.cornerRadius = profileImageView.frame.width/2
    profileImageView.layer.masksToBounds = true
    nameLabel.font = .defaultBoldFont(size: 12)
    timeLabel.font = .defaultRegularFont(size: 10)
  }
  
  @IBAction func profileAction(_ sender: Any) {
    profileTapped(sender)
  }
}
