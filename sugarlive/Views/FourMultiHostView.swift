//
//  FourMultiHostView.swift
//  sugarlive
//
//  Created by cleanmac on 07/12/21.
//

import UIKit
import SnapKit

@IBDesignable class FourMultiHostView: UIView {

    @IBOutlet weak var firstHostContainerView: UIView!
    @IBOutlet weak var secondHostContainerView: UIView!
    @IBOutlet weak var thirdHostContainerView: UIView!
    @IBOutlet weak var fourthHostContainerView: UIView!
    
    private weak var hostLiveContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var secondGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 1
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var thirdGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var fourthGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 3
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = FourMultiHostView.nib(withType: FourMultiHostView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = FourMultiHostView.nib(withType: FourMultiHostView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    private func setupUI() {
        firstHostContainerView.addSubview(hostLiveContainerView)
        hostLiveContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }

        secondHostContainerView.addSubview(secondGuestContainerView)
        secondGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        thirdHostContainerView.addSubview(thirdGuestContainerView)
        thirdGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        fourthHostContainerView.addSubview(fourthGuestContainerView)
        fourthGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
    }
    
    func getHostViewByPosition(_ position: Int) -> LiveContainerView {
        switch position {
        case 0:
            return hostLiveContainerView
        case 1:
            return secondGuestContainerView
        case 2:
            return thirdGuestContainerView
        case 3:
            return fourthGuestContainerView
        default:
            return hostLiveContainerView
        }
    }
    
    func setLiveType(_ type: LiveType) {
        hostLiveContainerView.setAddGuestImage(type)
        secondGuestContainerView.setAddGuestImage(type)
        thirdGuestContainerView.setAddGuestImage(type)
        fourthGuestContainerView.setAddGuestImage(type)
    }
    
    func setMultihostType() {
        hostLiveContainerView.multihostType = .FourMultiHost
        secondGuestContainerView.multihostType = .FourMultiHost
        thirdGuestContainerView.multihostType = .FourMultiHost
        fourthGuestContainerView.multihostType = .FourMultiHost
    }
    
    func removeSubviews() {
        hostLiveContainerView.removeFromSuperview()
        secondGuestContainerView.removeFromSuperview()
        thirdGuestContainerView.removeFromSuperview()
        fourthGuestContainerView.removeFromSuperview()
    }
}
