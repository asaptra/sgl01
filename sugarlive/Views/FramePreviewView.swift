//
//  FramePreviewView.swift
//  sugarlive
//
//  Created by msi on 13/09/21.
//

import UIKit

class FramePreviewView: UIView {
  
  @IBOutlet weak var backgroundImage: UIImageView!
  @IBOutlet weak var profileImage: UIImageView!
  @IBOutlet weak var badgeImage: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    nameLabel.font = .defaultBoldFont(size: 14)
    nameLabel.textColor = .white
    profileImage.image = UIImage(named: "ic_default_profile")
    backgroundImage.image = UIImage(named: "bg_profile_picture")
    profileImage.layer.cornerRadius = ((Constant.screenSize.width*0.395)*0.7)/2
    profileImage.layer.masksToBounds = true
  }
}
