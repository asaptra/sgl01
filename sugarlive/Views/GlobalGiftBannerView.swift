//
//  GlobalGiftBannerView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 06/06/22.
//

import UIKit
import SDWebImage

class GlobalGiftBannerView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var giftThumbnailImage: SDAnimatedImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    static let DEFAULT_HEIGHT: CGFloat = 80
    static let DEFAULT_WIDTH: CGFloat = 350
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = GlobalGiftBannerView.nib(withType: GlobalGiftBannerView.self)
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if subviews.count == 0 {
            let view = GlobalGiftBannerView.nib(withType: GlobalGiftBannerView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = constraints
            removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    func setContents(_ model: GiftDetailEventModel) {
        containerView.layer.borderWidth = 5
        containerView.layer.borderColor = UIColor(hex: "#494857").cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = containerView.bounds
        gradientLayer.colors = [UIColor(hex: "#CE00FF").cgColor, UIColor(hex: "#FF5700").cgColor]
        containerView.layer.insertSublayer(gradientLayer, at: 0)
        
        if let url = URL(string: model.gift?.thumbnail ?? "") {
            giftThumbnailImage.sd_setImage(with: url, placeholderImage: nil, options: [.retryFailed], context: [.imageThumbnailPixelSize: CGSize(width: giftThumbnailImage.bounds.width, height: giftThumbnailImage.bounds.height)])
        }
        
        let att = NSMutableAttributedString(string: model.sender?.fullname ?? "", attributes: [.font: UIFont.defaultSemiBoldFont(size: 12), .foregroundColor: UIColor(hex: "#DFFF00")])
        att.append(NSAttributedString(string: " give ", attributes: [.font: UIFont.defaultSemiBoldFont(size: 12), .foregroundColor: UIColor.white]))
        att.append(NSAttributedString(string: model.gift?.name ?? "", attributes: [.font: UIFont.defaultSemiBoldFont(size: 12), .foregroundColor: UIColor(hex: "#00FFB9")]))
        att.append(NSAttributedString(string: " to \(model.receiver?.fullname ?? "")", attributes: [.font: UIFont.defaultSemiBoldFont(size: 12), .foregroundColor: UIColor.white]))
        descriptionLabel.attributedText = att
    }

}
