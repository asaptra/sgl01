//
//  GroupCallView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 19/02/22.
//

import UIKit

@IBDesignable class GroupCallView: UIView {

    @IBOutlet weak var firstHostContainerView: UIView!
    @IBOutlet weak var secondHostContainerView: UIView!
    @IBOutlet weak var thirdHostContainerView: UIView!
    @IBOutlet weak var fourthHostContainerView: UIView!
    @IBOutlet weak var fifthHostContainerView: UIView!
    @IBOutlet weak var sixthHostContainerView: UIView!
    @IBOutlet weak var seventhHostContainerView: UIView!
    @IBOutlet weak var eighthHostContainerView: UIView!
    @IBOutlet weak var ninthHostContainerView: UIView!
    
    private weak var hostLiveContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var secondGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 1
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var thirdGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var fourthGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 3
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var fifthGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 4
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var sixthGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var seventhGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 6
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var eighthGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 7
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private weak var ninthGuestContainerView: LiveContainerView! = {
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.position = 8
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = GroupCallView.nib(withType: GroupCallView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = GroupCallView.nib(withType: GroupCallView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    private func setupUI() {
        firstHostContainerView.addSubview(hostLiveContainerView)
        hostLiveContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }

        secondHostContainerView.addSubview(secondGuestContainerView)
        secondGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        thirdHostContainerView.addSubview(thirdGuestContainerView)
        thirdGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        fourthHostContainerView.addSubview(fourthGuestContainerView)
        fourthGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        fifthHostContainerView.addSubview(fifthGuestContainerView)
        fifthGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        sixthHostContainerView.addSubview(sixthGuestContainerView)
        sixthGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        seventhHostContainerView.addSubview(seventhGuestContainerView)
        seventhGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        eighthHostContainerView.addSubview(eighthGuestContainerView)
        eighthGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        ninthHostContainerView.addSubview(ninthGuestContainerView)
        ninthGuestContainerView.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview()
            constraint.trailing.equalToSuperview()
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
    }
    
    func getHostViewByPosition(_ position: Int) -> LiveContainerView {
        switch position {
        case 0:
            return hostLiveContainerView
        case 1:
            return secondGuestContainerView
        case 2:
            return thirdGuestContainerView
        case 3:
            return fourthGuestContainerView
        case 4:
            return fifthGuestContainerView
        case 5:
            return sixthGuestContainerView
        case 6:
            return seventhGuestContainerView
        case 7:
            return eighthGuestContainerView
        case 8:
            return ninthGuestContainerView
        default:
            return hostLiveContainerView
        }
    }
    
    func setLiveType(_ type: LiveType) {
        hostLiveContainerView.setAddGuestImage(type)
        secondGuestContainerView.setAddGuestImage(type)
        thirdGuestContainerView.setAddGuestImage(type)
        fourthGuestContainerView.setAddGuestImage(type)
        fifthGuestContainerView.setAddGuestImage(type)
        sixthGuestContainerView.setAddGuestImage(type)
        seventhGuestContainerView.setAddGuestImage(type)
        eighthGuestContainerView.setAddGuestImage(type)
        ninthGuestContainerView.setAddGuestImage(type)
    }
    
    func setMultihostType() {
        hostLiveContainerView.multihostType = .GroupCall
        secondGuestContainerView.multihostType = .GroupCall
        thirdGuestContainerView.multihostType = .GroupCall
        fourthGuestContainerView.multihostType = .GroupCall
        fifthGuestContainerView.multihostType = .GroupCall
        sixthGuestContainerView.multihostType = .GroupCall
        seventhGuestContainerView.multihostType = .GroupCall
        eighthGuestContainerView.multihostType = .GroupCall
        ninthGuestContainerView.multihostType = .GroupCall
    }
    
    func removeSubviews() {
        hostLiveContainerView.removeFromSuperview()
        secondGuestContainerView.removeFromSuperview()
        thirdGuestContainerView.removeFromSuperview()
        fourthGuestContainerView.removeFromSuperview()
        fifthGuestContainerView.removeFromSuperview()
        sixthGuestContainerView.removeFromSuperview()
        seventhGuestContainerView.removeFromSuperview()
        eighthGuestContainerView.removeFromSuperview()
        ninthGuestContainerView.removeFromSuperview()
    }
}
