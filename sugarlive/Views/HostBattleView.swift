//
//  HostBattleView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 08/06/22.
//

import UIKit
import ZegoExpressEngine

let kStartBattleAction = "StartBattleAction"

class HostBattleView: UIView {
    
    enum BattleViewType {
        case Host
        case Guest
        case Viewer
    }

    // MARK: - IBOutlets and UIComponents
    @IBOutlet weak var mainStreamView: UIView!
    @IBOutlet weak var mainResultImage: UIImageView!
    @IBOutlet weak var mainGifterCollectionView: UICollectionView!
    
    @IBOutlet weak var guestStreamView: UIView!
    @IBOutlet weak var guestResultImage: UIImageView!
    @IBOutlet weak var guestGifterCollectionView: UICollectionView!
    
    @IBOutlet weak var timerContainerView: UIView!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var progressContainerStackView: UIStackView!
    @IBOutlet weak var progressContainerView: UIView!
    @IBOutlet weak var progressMainHostView: UIView!
    @IBOutlet weak var progressMainHostWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mainStreakLabel: UILabel!
    @IBOutlet weak var guestStreakLabel: UILabel!
    
    @IBOutlet weak var titleContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var startCountdownLabel: UILabel!
    
    @IBOutlet weak var startBattleButton: UIButton!
    
    // MARK: Variables
    private let resultImageScaleTransaform = CGAffineTransform(scaleX: 130, y: 130)
    private let strokeTextAttributes: [NSAttributedString.Key : Any] = [
        .strokeColor : UIColor.white,
        //.foregroundColor : UIColor(hex: "#8F3AFF"),
        .strokeWidth : -2.0,
    ]
    
    private var hostModel: UserModel? {
        didSet {
            isHidden = (hostModel == nil) && (guestModel == nil)
        }
    }
    private var guestModel: UserModel? {
        didSet {
            isHidden = (hostModel == nil) && (guestModel == nil)
        }
    }
    
    private var battleInfo: BattleInfoEventModel? {
        didSet {
            progressContainerStackView.isHidden = !(battleInfo != nil)
            mainGifterCollectionView.isHidden = battleInfo == nil
            guestGifterCollectionView.isHidden = battleInfo == nil
            if battleInfo != nil {
                startBattleButton.isHidden = true
            } else {
                startBattleButton.isHidden = !(type == .Host)
            }
        }
    }
    private var startCountdownTimer: Timer? {
        didSet {
            startCountdownLabel.isHidden = (startCountdownTimer == nil)
        }
    }
    private var battleTimer: Timer?
    private var countdownDuration: Int = 5
    private var battleDuration: Int?
    
    private var type: BattleViewType = .Viewer {
        didSet {
            startBattleButton.isHidden = !(type == .Host)
        }
    }
    
    // MARK: - Overriden Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = HostBattleView.nib(withType: HostBattleView.self)
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if subviews.count == 0 {
            let view = HostBattleView.nib(withType: HostBattleView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = constraints
            removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    // MARK: - UI Setups
    func setupUI() {
        progressContainerView.layer.borderWidth = 2
        progressContainerView.layer.borderColor = UIColor.white.withAlphaComponent(0.5).cgColor
        
        timerContainerView.roundCorners(corners: [.topLeft, .topRight], radius: timerContainerView.frame.height)
        let timerGradientLayer = CAGradientLayer()
        timerGradientLayer.frame = timerContainerView.bounds
        timerGradientLayer.colors = [UIColor(hex: "#101020").cgColor, UIColor(hex: "#474792").cgColor]
        timerContainerView.layer.insertSublayer(timerGradientLayer, at: 0)
        
        titleContainerView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: titleContainerView.frame.height)
        let titleGradientLayer = CAGradientLayer()
        titleGradientLayer.frame = titleContainerView.bounds
        titleGradientLayer.colors = [UIColor(hex: "#101020").cgColor, UIColor(hex: "#474792").cgColor]
        titleContainerView.layer.insertSublayer(titleGradientLayer, at: 0)
        
        startBattleButton.layer.borderWidth = 2
        startBattleButton.layer.borderColor = UIColor.white.cgColor
        
        let startBattleGradientLayer = CAGradientLayer()
        startBattleGradientLayer.frame = startBattleButton.bounds
        startBattleGradientLayer.colors = [UIColor(hex: "#2C004D").cgColor, UIColor(hex: "#7C01CE").cgColor]
        startBattleButton.layer.insertSublayer(startBattleGradientLayer, at: 0)
        
        startCountdownLabel.attributedText = NSAttributedString(string: "\(countdownDuration)", attributes: strokeTextAttributes)
        
        mainGifterCollectionView.delegate = self
        mainGifterCollectionView.dataSource = self
        mainGifterCollectionView.register(UINib(nibName: "BattleTopSpenderCell", bundle: nil), forCellWithReuseIdentifier: "BattleTopSpenderCell")
        
        guestGifterCollectionView.delegate = self
        guestGifterCollectionView.dataSource = self
        guestGifterCollectionView.semanticContentAttribute = .forceRightToLeft
        guestGifterCollectionView.register(UINib(nibName: "BattleTopSpenderCell", bundle: nil), forCellWithReuseIdentifier: "BattleTopSpenderCell")
    }
    
    func setBattleTitle(_ title: String) {
        titleLabel.text = title
    }
    
    // MARK: - Custom Functions
    func setupModels(host: UserModel, guest: UserModel, type: HostBattleView.BattleViewType) {
        self.type = type
        hostModel = host
        guestModel = guest
        startPlayingStreams()
    }
    
    func removeModels() {
        hostModel = nil
        guestModel = nil
    }
    
    func startCountdown(completion: @escaping () -> Void) {
        startBattleButton.isHidden = true
        countdownDuration = 5
        startCountdownLabel.text = "\(countdownDuration)"
        startCountdownTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            if self.countdownDuration == 0 {
                self.startCountdownTimer?.invalidate()
                self.startCountdownTimer = nil
                completion()
            } else {
                self.countdownDuration -= 1
                self.startCountdownLabel.attributedText = NSAttributedString(string: "\(self.countdownDuration)", attributes: self.strokeTextAttributes)
            }
        }
    }
    
    private func startPlayingStreams() {
        if type != .Viewer {
            let mainCanvas = ZegoCanvas(view: (type == .Host) ? guestStreamView : mainStreamView)
            mainCanvas.viewMode = .aspectFill
            let playerConfig = ZegoPlayerConfig()
            playerConfig.resourceMode = .default
            ZegoExpressEngine.shared().startPlayingStream(((type == .Host) ? guestModel?.userLiveId ?? "" : hostModel?.userLiveId ?? ""), canvas: mainCanvas, config: playerConfig)
            
            let canvas = ZegoCanvas(view: (type == .Host) ? mainStreamView : guestStreamView)
            canvas.viewMode = .aspectFill
            ZegoExpressEngine.shared().startPreview(canvas)
        } else {
            let mainCanvas = ZegoCanvas(view: mainStreamView)
            mainCanvas.viewMode = .aspectFill
            let playerConfig = ZegoPlayerConfig()
            playerConfig.resourceMode = .default
            ZegoExpressEngine.shared().startPlayingStream(hostModel?.userLiveId ?? "", canvas: mainCanvas, config: playerConfig)
            
            let guestCanvas = ZegoCanvas(view: guestStreamView)
            guestCanvas.viewMode = .aspectFill
            let guestPlayerConfig = ZegoPlayerConfig()
            guestPlayerConfig.resourceMode = .default
            ZegoExpressEngine.shared().startPlayingStream(guestModel?.userLiveId ?? "", canvas: guestCanvas, config: guestPlayerConfig)
        }
    }
    
    private func stopPlayingStreams() {
        if type != .Viewer {
            ZegoExpressEngine.shared().stopPlayingStream(((type == .Host) ? guestModel?.userLiveId ?? "" : hostModel?.userLiveId ?? ""))
            ZegoExpressEngine.shared().stopPreview()
        } else {
            ZegoExpressEngine.shared().stopPlayingStream(hostModel?.userLiveId ?? "")
            ZegoExpressEngine.shared().stopPlayingStream(guestModel?.userLiveId ?? "")
        }
    }
    
    func updateBattleInfo(_ model: BattleInfoEventModel) {
        battleInfo = model
        updateProgress()
        if battleDuration == nil {
            battleDuration = type == .Viewer ? (battleInfo?.remaining ?? 0) : (battleInfo?.duration ?? 0)
            startTimer()
        }
        mainGifterCollectionView.reloadData()
        guestGifterCollectionView.reloadData()
    }
    
    func updateProgress() {
        if let battleInfo = battleInfo {
            if let hostModel = battleInfo.result?.first(where: { $0.position == 0 }) {
                progressMainHostWidthConstraint.constant = (progressContainerView.frame.width * CGFloat(((hostModel.progress ?? 0) / 100)))
                UIView.animate(withDuration: 0.2) {
                    self.layoutIfNeeded()
                }
                mainStreakLabel.text = "\(hostModel.earnedGifts ?? "")".thousandFormatter()
            }
            if let guestModel = battleInfo.result?.first(where: { $0.position == 1 }) {
                guestStreakLabel.text = "\(guestModel.earnedGifts ?? "")".thousandFormatter()
            }
        }
    }
    
    private func startTimer() {
        battleTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            if self.battleDuration == 0 {
                self.timerLabel.text = "00:00"
                self.battleTimer?.invalidate()
                self.setResultImage()
                self.resetProgress()
            } else {
                self.battleDuration! -= 1
                self.timerLabel.text = (String(self.battleDuration! / 60).count == 1 ? "0\(self.battleDuration! / 60)" : "\(self.battleDuration! / 60)") + ":" + (String(self.battleDuration! % 60).count == 1 ? "0\(self.battleDuration! % 60)" : "\(self.battleDuration! % 60)")
            }
        }
    }
    
    private func resetProgress() {
        timerLabel.text = "00:00"
        battleDuration = nil
        mainStreakLabel.text = "0"
        guestStreakLabel.text = "0"
        battleInfo = nil
    }
    
    func endBattle() {
        battleTimer?.invalidate()
        hostModel = nil
        guestModel = nil
        battleInfo = nil
        battleDuration = nil
        type = .Viewer
        stopPlayingStreams()
    }
    
    private func setResultImage() {
        let hostEarnedResult = Int(battleInfo?.result?.first(where: { $0.position == 0 })?.earnedGifts ?? "0")
        let guestEarnedResult = Int(battleInfo?.result?.first(where: { $0.position == 1 })?.earnedGifts ?? "0")
        
        if hostEarnedResult! > guestEarnedResult! {
            mainResultImage.image = UIImage(named: "ic_battle_winner")
            guestResultImage.image = UIImage(named: "ic_battle_loser")
            animateImage()
        } else if hostEarnedResult! < guestEarnedResult! {
            mainResultImage.image = UIImage(named: "ic_battle_loser")
            guestResultImage.image = UIImage(named: "ic_battle_winner")
            animateImage()
        }
    }
    
    private func animateImage() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.3, animations: {
            self.mainResultImage.transform = self.resultImageScaleTransaform
            self.guestResultImage.transform = self.resultImageScaleTransaform
            self.layoutIfNeeded()
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, delay: 2, animations: {
                self.mainResultImage.transform = .identity
                self.guestResultImage.transform = .identity
                self.layoutIfNeeded()
            })
        })
    }
    
    // MARK: - Action
    @IBAction func startBattleAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(kStartBattleAction), object: nil)
    }
    
}

// MARK: - Collection view delegate and data source
extension HostBattleView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mainGifterCollectionView {
            if let hostTopSpenders = battleInfo?.result?.first?.topSpender {
                return hostTopSpenders.count
            } else {
                return 0
            }
        } else {
            if let guestTopSpenders = battleInfo?.result?.last?.topSpender {
                return guestTopSpenders.count
            } else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BattleTopSpenderCell", for: indexPath) as! BattleTopSpenderCell
        if collectionView == mainGifterCollectionView {
            if let model = battleInfo?.result?.first?.topSpender?[indexPath.row] {
                cell.setContents(model)
            }
        } else {
            if let model = battleInfo?.result?.last?.topSpender?[indexPath.row] {
                cell.setContents(model)
            }
        }
        return cell
    }
    
}
