//
//  JackpotBannerView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 05/06/22.
//

import UIKit
import AlamofireImage

class JackpotBannerView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userPictureImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    static let DEFAULT_HEIGHT: CGFloat = 80
    static let DEFAULT_WIDTH: CGFloat = 350
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = JackpotBannerView.nib(withType: JackpotBannerView.self)
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if subviews.count == 0 {
            let view = JackpotBannerView.nib(withType: JackpotBannerView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = constraints
            removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    func setContents(_ model: JackpotEventModel) {
        containerView.layer.borderWidth = 5
        containerView.layer.borderColor = UIColor(hex: "#494857").cgColor
        
        if let pictureUrl = URL(string: model.picture ?? "") {
            userPictureImage.af.setImage(withURL: pictureUrl)
        }
        
        let att = NSMutableAttributedString(string: "\(model.fullname ?? "") win ", attributes: [.font: UIFont.defaultSemiBoldFont(size: 14), .foregroundColor: UIColor.white])
        att.append(NSAttributedString(string: "\(model.prize ?? 0)".thousandFormatter(), attributes: [.font: UIFont.defaultSemiBoldFont(size: 14), .foregroundColor: UIColor(hex: "#FFBD09")]))
        att.append(NSAttributedString(string: " candy", attributes: [.font: UIFont.defaultSemiBoldFont(size: 14), .foregroundColor: UIColor.white]))
        descriptionLabel.attributedText = att
    }
    
}
