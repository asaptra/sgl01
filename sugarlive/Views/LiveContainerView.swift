//
//  LiveContainerView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 19/02/22.
//

import UIKit
import AlamofireImage

@IBDesignable class LiveContainerView: UIView {

    @IBOutlet weak var addGuestImage: UIImageView!
    
    @IBOutlet weak var liveCanvasContainerView: UIView!
    @IBOutlet weak var liveCanvasView: UIView!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var guestNameLabel: UILabel!
    @IBOutlet weak var guestCoverImage: UIImageView!
    
    @IBOutlet weak var videoStateImage: UIImageView!
    @IBOutlet weak var audioStateImage: UIImageView!
    
    @IBOutlet weak var hostIndicatorView: UIView!
    
    var position: Int? {
        didSet {
            positionLabel.text = "\((position ?? 0) + 1)"
            hostIndicatorView.isHidden = !(position ?? 0 == 0)
        }
    }
    
    var guestModel: UserModel? {
        didSet {
            liveCanvasContainerView.isHidden = (guestModel == nil)
            if guestModel != nil {
                guestNameLabel.text = guestModel?.fullname
                setGuestImage()
            }
        }
    }
    
    var streamingType: MultiguestStreamingType?
    
    var multihostType: MultiHostType? {
        didSet {
            videoStateImage.isHidden = multihostType == .GroupCall
        }
    }
    
    var videoState: AudioVideoState = .Mute {
        didSet {
            guestCoverImage.isHidden = videoState == .Unmute
            liveCanvasView.isHidden = videoState == .Mute
            videoStateImage.image = videoState == .Mute ? UIImage(named: "ic_video_mute") : UIImage(named: "ic_video_unmute")
        }
    }
    
    var audioState: AudioVideoState = .Unmute {
        didSet {
            audioStateImage.image = audioState == .Mute ? UIImage(named: "ic_audio_mute") : UIImage(named: "ic_audio_unmute")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = LiveContainerView.nib(withType: LiveContainerView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupUI()
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = LiveContainerView.nib(withType: LiveContainerView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    private func setupUI() {
        positionLabel.layer.masksToBounds = true
        positionLabel.layer.cornerRadius = 10
        liveCanvasView.layer.masksToBounds = true
    }
    
    private func setGuestImage() {
        if let url = URL(string: guestModel?.userPictureUrl ?? "") {
            guestCoverImage.af.setImage(withURL: url)
        } else {
            guestCoverImage.image = UIImage(named: "ic_default_profile")
        }
    }
    
    func setAddGuestImage(_ type: LiveType) {
        if position != 0 {
            addGuestImage.image = type == .Host ? UIImage(named: "ic_plus_outline") : UIImage(named: "ic_default_profile")
        }
        
        if type == .Viewer {
            makeSecure()
        }
    }
}
