//
//  LiveSocMedView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 28/03/22.
//

import UIKit

class LiveSocMedView: UIView {

    @IBOutlet weak var containerImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = LiveSocMedView.nib(withType: LiveSocMedView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = LiveSocMedView.nib(withType: LiveSocMedView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    func setupContents(_ model: SocialMediaModel) {
        containerImage.image = UIImage(named: "ic_\(model.socialMedia.rawValue.lowercased())_account")
        usernameLabel.text = "@" + (model.userName ?? "")
    }
}
