//
//  MultihostRequestView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 09/03/22.
//

import UIKit

@IBDesignable class MultihostRequestView: UIView {
    
    @IBOutlet weak var waitingCountLabel: UILabel!
    @IBOutlet weak var joinView: UIView!
    
    var isHost: Bool = false {
        didSet {
            joinView.isHidden = isHost
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        joinView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = MultihostRequestView.nib(withType: MultihostRequestView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = MultihostRequestView.nib(withType: MultihostRequestView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    func setWaitingListCount(_ count: Int) {
        waitingCountLabel.text = "\(count)"
    }
}
