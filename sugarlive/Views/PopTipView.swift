//
//  PopTipView.swift
//  sugarlive
//
//  Created by msi on 11/10/21.
//

import UIKit

class PopTipView: UIView {
  
  @IBOutlet weak var editButton: UIButton!
  @IBOutlet weak var destructionButton: UIButton!
  
  var editString: String = "Edit"{
    didSet {
      editButton.setAttributedTitle(editString.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12),
                                                                         NSAttributedString.Key.foregroundColor: UIColor.init(hex: "414141")]),
                                    for: .normal)
    }
  }
  var destructionString: String = "Delete" {
    didSet {
      destructionButton.setAttributedTitle(destructionString.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12),
                                                                                       NSAttributedString.Key.foregroundColor: UIColor.init(hex: "F85454")]),
                                           for: .normal)
    }
  }
  
  var editTapped: (_ sender: Any) -> Void = {_ in }
  var deleteTapped: (_ sender: Any) -> Void = {_ in }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    editButton.contentHorizontalAlignment = .left
    destructionButton.contentHorizontalAlignment = .left
  }
  
//  MARK: Action
  @IBAction func editAction(_ sender: Any) {
    editTapped(sender)
  }
  
  @IBAction func deleteAction(_ sender: Any) {
    deleteTapped(sender)
  }
}
