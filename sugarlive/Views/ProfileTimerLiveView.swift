//
//  ProfileTimerLiveView.swift
//  sugarlive
//
//  Created by msi on 07/10/21.
//

import UIKit
import AlamofireImage

@IBDesignable class ProfileTimerLiveView: UIView {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var avatarFrameImageView: UIImageView!
    @IBOutlet weak var profileTimerContainerView: UIView!
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var viewerIcon: UIImageView!
    @IBOutlet weak var viewerLabel: UILabel!
    @IBOutlet weak var timerIcon: UIImageView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var userCollectionView: UICollectionView!
    @IBOutlet weak var closeButton: UIButton!
    
    var users: [UserModel] = []
    
    var userSelectedHandler: (UserModel) -> Void = { _ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = ProfileTimerLiveView.nib(withType: ProfileTimerLiveView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupLayout()
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = ProfileTimerLiveView.nib(withType: ProfileTimerLiveView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    private func setupLayout() {
        userCollectionView.backgroundColor = .clear
        userCollectionView.delegate = self
        userCollectionView.dataSource = self
        userCollectionView.register(UINib(nibName: "LiveUserCollectionCell", bundle: nil), forCellWithReuseIdentifier: "LiveUserCollectionCell")
        
        profileImageView.layer.cornerRadius = 16
        profileImageView.layer.masksToBounds = true
        
        profileTimerContainerView.layer.cornerRadius = profileTimerContainerView.frame.height / 2
        
        nameLabel.textColor = .white
        nameLabel.font = .defaultSemiBoldFont(size: 10)
        
        viewerLabel.textColor = .white
        viewerLabel.font = .defaultRegularFont(size: 8)
        
        timerLabel.textColor = .white
        timerLabel.font = .defaultRegularFont(size: 8)
        
        profileTimerContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func setupContents(userModel: UserModel, detailModel: WatchLiveDetailModel?) {
        if let imageUrl = userModel.userPictureUrl, let url = URL(string: imageUrl) {
            profileImageView.af.setImage(withURL: url)
        }
        
        if let frameUrl = URL(string: userModel.avatarFrames?.defaultFrame?.avatarIcon ?? "") {
            avatarFrameImageView.isHidden = false
            avatarFrameImageView.af.setImage(withURL: frameUrl)
        }
        
        nameLabel.text = userModel.fullname
        viewerLabel.text = "\(detailModel?.viewerCount ?? 0)"
    }
    
    func setViewerCount(_ count: Int32) {
        viewerLabel.text = "\(count)"
    }
    
    func setupCollectionViewData(with list: [UserModel]) {
        users = list
        viewerLabel.text = "\(users.count)"
        userCollectionView.reloadData()
    }
    
    func addUserToList(model: UserModel) {
        users.append(model)
        viewerLabel.text = "\(users.count)"
        userCollectionView.reloadData()
    }
    
    func removeUserFromList(model: UserModel) {
        users.removeAll(where: { $0.userId == model.userId })
        viewerLabel.text = "\(users.count)"
        userCollectionView.reloadData()
    }
}

extension ProfileTimerLiveView: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveUserCollectionCell", for: indexPath) as! LiveUserCollectionCell
        cell.avatarFrameImage.layer.cornerRadius = collectionView.frame.height / 2
        cell.profileImage.layer.cornerRadius = (collectionView.frame.height - 4) / 2
        cell.setImage(frame: users[indexPath.row].avatarFrames?.defaultFrame?.avatarIcon ?? "", profile: users[indexPath.row].userPictureUrl ?? "")
        return cell
    }
}

extension ProfileTimerLiveView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        userSelectedHandler(users[indexPath.row])
    }
}

extension ProfileTimerLiveView: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        return .init(width: height, height: height)
    }
}
