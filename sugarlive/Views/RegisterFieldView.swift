//
//  RegisterFieldView.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit

class RegisterFieldView: UIView {
  
  @IBOutlet weak var leftIcon: UIImageView!
  @IBOutlet weak var textField: BaseTextField!
  @IBOutlet weak var rightIcon: UIImageView!
  @IBOutlet weak var containerView: UIView!
  
  var leftTapped: (_ sender: Any) -> Void = {_ in }
  var rightTapped: (_ sender: Any) -> Void = {_ in }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
//  MARK: Action
  @IBAction func leftAction(_ sender: Any) {
    leftTapped(sender)
  }
  
  @IBAction func rightAction(_ sender: Any) {
    rightTapped(sender)
  }
}
