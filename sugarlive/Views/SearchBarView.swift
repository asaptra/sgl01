//
//  SearchBarView.swift
//  sugarlive
//
//  Created by cleanmac on 17/11/21.
//

import UIKit
import SnapKit

protocol SearchBarViewDelegate {
    func textFieldDidChanged(_ textField: UITextField)
}

class SearchBarView: UIView {
    
    private var containerView: UIView!
    private var searchField: UITextField!
    private var eraseButton: UIButton!
    
    var delegate: SearchBarViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupLayout() {
        containerView = UIView()
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.backgroundColor = UIColor(hex: "#4A097A")
        containerView.layer.cornerRadius = self.frame.size.height / 2
        self.addSubview(containerView)
        
        searchField = UITextField()
        searchField.translatesAutoresizingMaskIntoConstraints = false
        searchField.attributedPlaceholder = NSAttributedString(string: "Search here", attributes: [.foregroundColor: UIColor.white, .font: UIFont.defaultItalicFont(size: 14)])
        searchField.textColor = .white
        searchField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
        containerView.addSubview(searchField)
        searchField.snp.makeConstraints { constraint in
            constraint.leading.equalToSuperview().offset(16)
            constraint.trailing.equalToSuperview().offset(-16)
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
        eraseButton = UIButton()
        eraseButton.translatesAutoresizingMaskIntoConstraints = false
        eraseButton.setTitle("", for: .normal)
        eraseButton.setImage(UIImage(named: "ic_close_gray"), for: .normal)
        eraseButton.addTarget(self, action: #selector(eraseField), for: .touchUpInside)
        eraseButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        containerView.addSubview(eraseButton)
        eraseButton.snp.makeConstraints { constraint in
            constraint.width.equalTo(30)
            constraint.trailing.equalToSuperview().offset(-10)
            constraint.top.equalToSuperview()
            constraint.bottom.equalToSuperview()
        }
        
    }
    
    @objc private func eraseField() {
        searchField.text = nil
    }
    
    @objc private func textFieldChanged(_ sender: UITextField) {
        delegate?.textFieldDidChanged(sender)
    }
    
}
