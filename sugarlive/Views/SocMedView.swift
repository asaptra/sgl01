//
//  SocMedView.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class SocMedView: UIView {
  
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  
  var actionTapped: (_ sender: Any) -> Void = {_ in }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    titleLabel.font = .defaultRegularFont(size: 14)
  }
  
  @IBAction func doAction(_ sender: Any) {
    actionTapped(sender)
  }
}
