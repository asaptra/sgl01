//
//  SuitAnswerView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 22/05/22.
//

import UIKit

class SuitAnswerView: UIView {

    @IBOutlet weak var answerTextContainerView: UIView!
    @IBOutlet weak var answerJempolImage: UIImageView!
    @IBOutlet weak var answerTelunjukImage: UIImageView!
    @IBOutlet weak var answerKelingkingImage: UIImageView!
    
    var answerHandler: (SuitAnswer) -> Void = { _ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = SuitAnswerView.nib(withType: SuitAnswerView.self)
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if subviews.count == 0 {
            let view = SuitAnswerView.nib(withType: SuitAnswerView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = constraints
            removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    private func setupUI() {
        answerTextContainerView.layer.borderWidth = 1
        answerTextContainerView.layer.borderColor = UIColor(hex: "#EFEFEF").cgColor
        
        answerJempolImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(_:))))
        answerTelunjukImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(_:))))
        answerKelingkingImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(_:))))
    }
    
    @objc private func answer(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        if view == answerJempolImage {
            answerHandler(.Jempol)
        } else if view == answerTelunjukImage {
            answerHandler(.Telunjuk)
        } else {
            answerHandler(.Kelingking)
        }
    }
    
}
