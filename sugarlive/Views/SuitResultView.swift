//
//  SuitResultView.swift
//  sugarlive
//
//  Created by cleanmac-ada on 23/05/22.
//

import UIKit

class SuitResultView: UIView {
    
    @IBOutlet weak var hostResultImage: UIImageView!
    @IBOutlet weak var viewerResultImage: UIImageView!
    
    @IBOutlet weak var hostNameLabel: UILabel!
    @IBOutlet weak var viewerNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = SuitResultView.nib(withType: SuitResultView.self)
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if subviews.count == 0 {
            let view = SuitResultView.nib(withType: SuitResultView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = constraints
            removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    func setNames(host: String, viewer: String) {
        hostNameLabel.text = host
        viewerNameLabel.text = viewer
    }
    
    func setImages(host: String, viewer: String) {
        hostResultImage.image = UIImage(named: "ic_result_host_\(host)")
        viewerResultImage.image = UIImage(named: "ic_result_viewer_\(viewer)")
    }
}
