//
//  BackgroundPreviewCell.swift
//  sugarlive
//
//  Created by msi on 15/09/21.
//

import UIKit

class BackgroundPreviewCell: UITableViewCell {
  
  @IBOutlet weak var topLabel: UILabel!
  @IBOutlet weak var previewImageView: UIImageView!
  @IBOutlet weak var placeholderImageView: UIImageView!
  @IBOutlet weak var infoLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  private func setupLayout() {
    topLabel.font = .defaultSemiBoldFont(size: 12)
    topLabel.textColor = .DARK_TEXT
    
    previewImageView.layer.cornerRadius = 10
    previewImageView.layer.masksToBounds = true
    
    infoLabel.font = .defaultRegularFont(size: 12)
    infoLabel.textColor = .white
  }
  
  func bind(user: UserModel) {
    if let urlString = user.coverPicture, let url = URL(string: urlString) {
      previewImageView.af.setImage(withURL: url)
    } else {
      previewImageView.image = UIImage(named: "bg_profile_picture")
    }
  }
}
