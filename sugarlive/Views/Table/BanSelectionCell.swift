//
//  BanSelectionCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 27/04/22.
//

import UIKit

class BanSelectionCell: UITableViewCell {

    @IBOutlet weak var selectionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionLabel.font = .defaultRegularFont()
    }
    
    func setTitle(_ title: String) {
        selectionLabel.text = title
    }
    
}
