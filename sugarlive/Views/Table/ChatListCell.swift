//
//  ChatListCell.swift
//  sugarlive
//
//  Created by msi on 18/09/21.
//

import UIKit

class ChatListCell: UITableViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var chatLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var notifButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }

    func setupLayout() {
        profileImageView.layer.cornerRadius = 10
        profileImageView.layer.masksToBounds = true

        nameLabel.font = .defaultBoldFont(size: 14)
        nameLabel.textColor = .DARK_TEXT
        chatLabel.font = .defaultRegularFont(size: 12)
        chatLabel.textColor = .LIGHT_TEXT
        timeLabel.font = .defaultRegularFont(size: 12)
        timeLabel.textColor = .LIGHT_TEXT

        notifButton.backgroundColor = .PRIMARY_2
        notifButton.setAttributedTitle("2".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        notifButton.layer.cornerRadius = 13
        notifButton.layer.masksToBounds = true
        notifButton.isHidden = true
    }
}
