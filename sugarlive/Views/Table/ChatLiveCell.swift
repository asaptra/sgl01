//
//  ChatLiveCell.swift
//  sugarlive
//
//  Created by cleanmac on 09/11/21.
//

import UIKit
import AlamofireImage

class ChatLiveCell: UITableViewCell {
    
    enum BattleChatType {
        case Host
        case Guest
        case None
    }
    
    @IBOutlet weak var bubbleContainerView: UIView!
    @IBOutlet weak var topLeftBubbleImage: UIImageView!
    @IBOutlet weak var topMidBubbleImage: UIImageView!
    @IBOutlet weak var topRightBubbleImage: UIImageView!
    @IBOutlet weak var midLeftBubbleImage: UIImageView!
    @IBOutlet weak var midMidBubbleImage: UIImageView!
    @IBOutlet weak var midRightBubbleImage: UIImageView!
    @IBOutlet weak var bottomLeftBubbleImage: UIImageView!
    @IBOutlet weak var bottomMidBubbleImage: UIImageView!
    @IBOutlet weak var bottomRightBubbleImage: UIImageView!
    
    @IBOutlet weak var levelContainerView: UIView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var stickerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        stickerImage.isHidden = true
        stickerImage.image = nil
        bubbleContainerView.isHidden = true
    }

    func setContents(model: LiveChatResponseModel, battleType: ChatLiveCell.BattleChatType = .None) {
        stickerImage.isHidden = true
        levelContainerView.backgroundColor = UIColor(hex: model.levelColor ?? "")
        levelLabel.text = "Lvl.\(model.level ?? 0)"
        
        let attStr = NSMutableAttributedString(string: "\(model.fullName ?? "") : ", attributes: [.font: UIFont.defaultSemiBoldFont(size: 11)])
        attStr.append(NSAttributedString(string: model.message ?? "", attributes: [.font: UIFont.defaultRegularFont(size: 11)]))
        
        messageLbl.attributedText = attStr
        
        if battleType != .None {
            messageLbl.textColor = battleType == .Host ? UIColor(hex: "#F14BFF") : UIColor(hex: "#EF8122")
        }
        
        if let chatBubbleId = model.senderChatBubble {
            if let bubbleModel = ChatBubbleProvider.shared.getBubble(from: chatBubbleId) {
                setBubbleImage(bubbleModel)
            }
        }
    }
    
    func setContents(sticker: LiveStickerResponseModel) {
        stickerImage.isHidden = false
        levelContainerView.backgroundColor = UIColor(hex: sticker.level?.color ?? "")
        levelLabel.text = "Lvl.\(sticker.level?.level ?? 0)"
        
        let attStr = NSMutableAttributedString(string: "\(sticker.fullName ?? "") : ", attributes: [.font: UIFont.defaultSemiBoldFont(size: 11)])
        messageLbl.attributedText = attStr
        
        if let url = URL(string: sticker.sticker?.stickerUrl ?? "") {
            let filter = AspectScaledToFitSizeFilter(size: stickerImage.frame.size)
            stickerImage.af.setImage(withURL: url, filter: filter)
        }
    }
    
    private func setBubbleImage(_ model: ChatBubbleModel) {
        guard
            let topLeft = URL(string: model.topLeftImage ?? ""),
            let topMid = URL(string: model.topMidImage ?? ""),
            let topRight = URL(string: model.topRightImage ?? ""),
            let midLeft = URL(string: model.midLeftImage ?? ""),
            let midMid = URL(string: model.midMidImage ?? ""),
            let midRight = URL(string: model.midRightImage ?? ""),
            let bottomLeft = URL(string: model.bottomLeftImage ?? ""),
            let bottomMid = URL(string: model.bottomMidImage ?? ""),
            let bottomRight = URL(string: model.bottomRightImage ?? "")
        else { return }
        
        bubbleContainerView.isHidden = false
        
        topLeftBubbleImage.af.setImage(withURL: topLeft)
        topMidBubbleImage.af.setImage(withURL: topMid)
        topRightBubbleImage.af.setImage(withURL: topRight)
        midLeftBubbleImage.af.setImage(withURL: midLeft)
        midMidBubbleImage.af.setImage(withURL: midMid)
        midRightBubbleImage.af.setImage(withURL: midRight)
        bottomLeftBubbleImage.af.setImage(withURL: bottomLeft)
        bottomMidBubbleImage.af.setImage(withURL: bottomMid)
        bottomRightBubbleImage.af.setImage(withURL: bottomRight)
    }
    
}
