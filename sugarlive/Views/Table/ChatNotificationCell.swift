//
//  ChatNotificationCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 28/03/22.
//

import UIKit

class ChatNotificationCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setContents(_ model: ViewerLiveJoinNotificationModel) {
        containerView.backgroundColor = .darkGray
        
        let attStr = NSMutableAttributedString(string: "\(model.name) ", attributes: [.font: UIFont.defaultBoldFont(size: 11)])
        attStr.append(NSAttributedString(string: "Joined the room", attributes: [.font: UIFont.defaultRegularFont(size: 11)]))
        infoLabel.attributedText = attStr
    }
    
    func setContents(_ model: LiveNotificationModel) {
        containerView.backgroundColor = .darkGray
        
        let attStr = NSMutableAttributedString(string: model.message, attributes: [.font: UIFont.defaultBoldFont(size: 11)])
        infoLabel.attributedText = attStr
    }
    
}
