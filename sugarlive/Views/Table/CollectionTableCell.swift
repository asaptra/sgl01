//
//  CollectionTableCell.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class CollectionTableCell: UITableViewCell {
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
}
