//
//  ComboBannerCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 14/07/22.
//

import UIKit
import SDWebImage

class ComboBannerCell: UITableViewCell {
    
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var giftNameLabel: UILabel!
    
    @IBOutlet weak var giftThumbnailImage: SDAnimatedImageView!
    @IBOutlet weak var giftMultiplierLabel: UILabel!
    
    static let DEFAULT_HEIGHT: CGFloat = 58
    
    private let strokeTextAttributes: [NSAttributedString.Key : Any] = [
        .strokeColor : UIColor.white,
        .foregroundColor : UIColor(hex: "#8F3AFF"),
        .strokeWidth : -2.0,
    ]
    
    private var multiplier: Int = 1 {
        didSet {
            giftMultiplierLabel.attributedText = NSAttributedString(string: "\(multiplier)", attributes: strokeTextAttributes)
        }
    }
    
    private let multiplierLabelScale: CGAffineTransform = CGAffineTransform(scaleX: 1.5, y: 1.5)

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        senderNameLabel.text = ""
        giftNameLabel.text = ""
        giftThumbnailImage.image = nil
        multiplier = 1
    }
    
    func setContents(_ model: ComboGiftModel) {
        senderNameLabel.text = model.detailModel.sender?.fullname ?? ""
        giftNameLabel.text = "Send gift \"\(model.detailModel.gift?.name ?? "")\""
        
        if let url = URL(string: model.detailModel.gift?.thumbnail ?? "") {
            giftThumbnailImage.sd_setImage(with: url, placeholderImage: nil, options: [.retryFailed], context: [.imageThumbnailPixelSize: CGSize(width: giftThumbnailImage.bounds.width, height: giftThumbnailImage.bounds.height)])
        }
        
        multiplier = model.multiplier
    }
    
    func resetContents() {
        senderNameLabel.text = ""
        giftNameLabel.text = ""
        giftThumbnailImage.image = nil
        multiplier = 1
    }
    
    func increaseMultiplier(count: Int) {
        multiplier = count
        animateMultiplier()
    }
    
    private func animateMultiplier() {
        UIView.animate(withDuration: 0.1, delay: 0, animations: {
            self.giftMultiplierLabel.transform = self.multiplierLabelScale
            self.layoutIfNeeded()
        }, completion: { _ in
            UIView.animate(withDuration: 0.1, delay: 0, animations: {
                self.giftMultiplierLabel.transform = .identity
                self.layoutIfNeeded()
            })
        })
    }
    
}
