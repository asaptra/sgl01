//
//  ConversationCell.swift
//  sugarlive
//
//  Created by dodets on 11/05/22.
//

import UIKit

class ConversationCell: UITableViewCell {

    @IBOutlet weak var profileView: UIImageView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var chatLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileView.cornerRadius = 10
        chatView.cornerRadius = 10
        chatLbl.font = .defaultRegularFont(size: 10)
        chatLbl.textColor = .PRIMARY_1
        timeLbl.font = .defaultBoldFont(size: 10)
        timeLbl.textColor = .PRIMARY_1
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
