//
//  FeedCommentCell.swift
//  sugarlive
//
//  Created by msi on 11/09/21.
//

import UIKit

class FeedCommentCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var feedLabel: UILabel!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var feedContainerView: UIView!
    @IBOutlet weak var actionContainerView: UIView!
    
    var isHideAction = true {
        didSet {
            actionContainerView.isHidden = isHideAction
        }
    }
    var likeTapped: (_ sender: Any) -> Void = {_ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    func setupLayout() {
        profileImageView.layer.cornerRadius = (Constant.screenSize.width*0.09)/2
        profileImageView.layer.masksToBounds = true
        
        feedContainerView.backgroundColor = .init(hex: "F2F2F2")
        feedContainerView.layer.cornerRadius = 10
        
        nameLabel.font = .defaultBoldFont(size: 14)
        nameLabel.textColor = .DARK_TEXT
        
        feedLabel.font = .defaultRegularFont(size: 14)
        feedLabel.textColor = .DARK_TEXT
        
        timeButton.isUserInteractionEnabled = false
        timeButton.setAttributedTitle(attributeTitle("Just now"), for: .normal)
        likeButton.setAttributedTitle(attributeTitle(" Like"), for: .normal)
        replyButton.setAttributedTitle(attributeTitle("Reply"), for: .normal)
        replyButton.isHidden = true
    }
    
    private func attributeTitle(_ title: String) -> NSAttributedString {
        return title.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14),
                                               NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT])
    }
    
    func bind(comment: FeedCommentModel) {
        if let urlString = comment.coverPicture, let url = URL(string: urlString) {
            profileImageView.af.setImage(withURL: url)
        } else {
            profileImageView.image = UIImage(named: "ic_default_profile")
        }
        nameLabel.text = comment.fullname
        feedLabel.attributedText = commentTag(comment: comment)
        likeButton.setImage(UIImage(named: comment.isLiked ? "ic_heart_red" : "ic_heart_gray"), for: .normal)
        likeButton.setAttributedTitle(attributeTitle(" Like\(comment.totalLike == 0 ? "" : " (\(comment.totalLike))")"), for: .normal)
        
        timeButton.setAttributedTitle(attributeTitle(comment.createdAt?.toDate(with: "yyyy-MM-dd HH:mm:ss").timeAgoDisplay() ?? "Just now"), for: .normal)
    }
    
    func bind(likeCount: Int, isLiked: Bool) {
        likeButton.setImage(UIImage(named: isLiked ? "ic_heart_red" : "ic_heart_gray"), for: .normal)
        likeButton.setAttributedTitle(attributeTitle(" Like\(likeCount == 0 ? "" : " (\(likeCount))")"), for: .normal)
    }
    
    private func commentTag(comment: FeedCommentModel) -> NSMutableAttributedString {
        let attStr = NSMutableAttributedString(attributedString: (comment.content?.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 14),
                                                                                                             NSAttributedString.Key.foregroundColor: UIColor.DARK_TEXT]))!)
        for user in comment.taggedUser {
            if let name = user.fullname, let string: NSString = comment.content as NSString? {
                let range = string.range(of: name)
                attStr.addAttributes([NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 14)], range: range)
            }
        }
        
        return attStr
    }
    
    @IBAction func likeAction(_ sender: Any) {
        if let button = sender as? UIButton {
            button.isSelected = !button.isSelected
        }
        likeTapped(sender)
    }
}
