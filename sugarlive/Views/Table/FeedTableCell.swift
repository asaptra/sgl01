//
//  FeedTableCell.swift
//  sugarlive
//
//  Created by msi on 10/09/21.
//

import UIKit
import Popover

class FeedTableCell: UITableViewCell {
  
  @IBOutlet weak var feedProfileView: FeedProfileView!
  @IBOutlet weak var feedLabel: UILabel!
  @IBOutlet weak var feedListCommentLabel: UILabel!
  @IBOutlet weak var feedDetailCommentLabel: UILabel!
  @IBOutlet weak var feedDetailLikeButton: UIButton!
  @IBOutlet weak var likeButton: UIButton!
  @IBOutlet weak var commentButton: UIButton!
  @IBOutlet weak var profileContainerView: UIView!
  @IBOutlet weak var profileSeparatorContainerView: UIView!
  @IBOutlet weak var likeCommentContainerView: UIView!
  @IBOutlet weak var commentLikeContainerView: UIView!
  @IBOutlet weak var actionContainerView: UIView!
  @IBOutlet weak var contentStack: UIStackView!
  @IBOutlet weak var imageStack: UIStackView!
  
  private var feed: FeedModel?
  let popTip: Popover = {
    let options = [
      .type(.down),
      .cornerRadius(4),
      .animationIn(0.3),
      .blackOverlayColor(UIColor.clear),
      .arrowSize(CGSize.zero),
      .borderColor(UIColor.lightGray)
    ] as [PopoverOption]
    return Popover(options: options, showHandler: nil, dismissHandler: nil)
  }()
  lazy var popTipView = PopTipView.nib(withType: PopTipView.self)
  
  var isHideAction = true {
    didSet {
      profileContainerView.isHidden = isHideAction
      profileSeparatorContainerView.isHidden = isHideAction
      likeCommentContainerView.isHidden = isHideAction
      commentLikeContainerView.isHidden = !isHideAction
      actionContainerView.isHidden = isHideAction
    }
  }
  var likeTapped: (_ sender: Any) -> Void = {_ in }
  var commentTapped: (_ sender: Any) -> Void = {_ in }
  var profileTapped: (_ sender: Any) -> Void = {_ in }
  
  private let buttonAttributes = [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
    likeButton.setAttributedTitle("  Like".toAttributedString(with: buttonAttributes), for: .normal)
    commentButton.setAttributedTitle("  Comment".toAttributedString(with: buttonAttributes), for: .normal)
    feedDetailLikeButton.setAttributedTitle(" Like (\(0))".toAttributedString(with: buttonAttributes), for: .normal)
    feedDetailCommentLabel.attributedText = "Comments (\(0))".toAttributedString(with: buttonAttributes)
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    feedProfileView.profileImageView.layer.cornerRadius = ((Constant.screenSize.width-40)*0.1)/2
  }
  
  func setupLayout() {
//    feedProfileView = FeedProfileView.nib(withType: FeedProfileView.self)
//    feedProfileView.prepareForInterfaceBuilder()
    feedLabel.font = .defaultRegularFont(size: 14)
    feedLabel.textColor = .DARK_TEXT
    feedListCommentLabel.font = .defaultRegularFont(size: 10)
    feedListCommentLabel.textColor = .DARK_TEXT
    feedDetailCommentLabel.font = .defaultBoldFont(size: 10)
    feedDetailCommentLabel.textColor = .MEDIUM_TEXT
    
    feedProfileView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapAction(_:))))
  }
  
  func bind(feed: FeedModel) {
    self.feed = feed
    if let urlString = feed.coverPicture, let url = URL(string: urlString) {
      feedProfileView.profileImageView.af.setImage(withURL: url)
    } else {
      feedProfileView.profileImageView.image = UIImage(named: "ic_default_profile")
    }
    feedProfileView.nameLabel.text = feed.fullname
    feedProfileView.timeLabel.text = feed.createdAt?.toDate(with: "yyyy-MM-dd HH:mm:ss").timeAgoDisplay()
    feedProfileView.actionButton.addTarget(self, action: #selector(showPopTip(_:)), for: .touchUpInside)
    feedLabel.text = feed.content
    feedListCommentLabel.text = "\(feed.totalLike) Likes, \(feed.totalComment) Comment"
    feedDetailLikeButton.setAttributedTitle(" Like (\(feed.totalLike))".toAttributedString(with: buttonAttributes), for: .normal)
    feedDetailCommentLabel.attributedText = "Comments (\(feed.totalComment))".toAttributedString(with: buttonAttributes)
    likeButton.isSelected = feed.isLiked
    feedDetailLikeButton.isSelected = feed.isLiked
    
    generateImage(feed: feed)
  }
  
  func bind(likeCount: Int, commentCount: Int) {
    feedListCommentLabel.text = "\(likeCount) Likes, \(commentCount) Comment"
    feedDetailLikeButton.setAttributedTitle(" Like (\(likeCount))".toAttributedString(with: buttonAttributes), for: .normal)
    feedDetailCommentLabel.attributedText = "Comments (\(commentCount))".toAttributedString(with: buttonAttributes)
  }
  
//  MARK: Action
  @IBAction func likeAction(_ sender: Any) {
    if let button = sender as? UIButton {
      button.isSelected = !button.isSelected
    }
    likeTapped(sender)
  }
  
  @IBAction func commentAction(_ sender: Any) {
    commentTapped(sender)
    
  }
  
  @IBAction func showPopTip(_ sender: Any) {
    if self.feed?.sugarId == AuthManager.shared.currentUser?.sugarId {
      popTipView.editButton.isHidden = false
      popTipView.editString = "Edit"
      popTipView.destructionString = "Delete"
    } else {
      popTipView.editButton.isHidden = true
      popTipView.destructionString = "Report"
    }
    popTipView.frame = .init(x: 0, y: 0, width: 150, height: popTipView.editButton.isHidden ? 40 : 80)
    if let button = sender as? UIButton {
      popTip.show(popTipView, fromView: button)
    }
    
    popTipView.deleteTapped = {(sender) in
      self.popTip.dismiss()
      
      if self.feed?.sugarId == AuthManager.shared.currentUser?.sugarId {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteFeed"), object: self.feed)
      } else {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reportFeed"), object: self.feed)
      }
      
      //show report reason
    }
    
  }
  
  @objc func profileTapAction(_ sender: Any) {
    profileTapped(sender)
  }
  
//  MARK: Custom
  func generateImage(feed: FeedModel) {
    imageStack.superview?.isHidden = true
    _ = imageStack.arrangedSubviews.map{ $0.removeFromSuperview() }
    
    let stackTop = UIStackView()
    stackTop.axis = .horizontal
    stackTop.spacing = 12
    stackTop.distribution = .fillEqually
    stackTop.translatesAutoresizingMaskIntoConstraints = false
    
    if let firstImage = feed.image, let url = URL(string: firstImage) {
      let imageView = UIImageView()
      imageView.clipsToBounds = true
      imageView.contentMode = .scaleAspectFill
      imageView.af.setImage(withURL: url)
      stackTop.addArrangedSubview(imageView)
    }
    
    if let secondImage = feed.image2, let url = URL(string: secondImage) {
      let imageView = UIImageView()
      imageView.clipsToBounds = true
      imageView.contentMode = .scaleAspectFill
      imageView.af.setImage(withURL: url)
      stackTop.addArrangedSubview(imageView)
    }
    
    if stackTop.arrangedSubviews.count > 0 {
      imageStack.superview?.isHidden = false
      imageStack.addArrangedSubview(stackTop)
    }
    
    let stackBottom = UIStackView()
    stackBottom.axis = .horizontal
    stackBottom.spacing = 12
    stackBottom.distribution = .fillEqually
    stackBottom.translatesAutoresizingMaskIntoConstraints = false
    
    if let thirdImage = feed.image3, let url = URL(string: thirdImage) {
      let imageView = UIImageView()
      imageView.clipsToBounds = true
      imageView.contentMode = .scaleAspectFill
      imageView.af.setImage(withURL: url)
      stackBottom.addArrangedSubview(imageView)
    }
    
    if let fourthImage = feed.image4, let url = URL(string: fourthImage) {
      let imageView = UIImageView()
      imageView.clipsToBounds = true
      imageView.contentMode = .scaleAspectFill
      imageView.af.setImage(withURL: url)
      stackBottom.addArrangedSubview(imageView)
    }
    
    if stackBottom.arrangedSubviews.count > 0 {
      imageStack.addArrangedSubview(stackBottom)
    }
    
    if stackTop.arrangedSubviews.count == 1 {
      stackTop.snp.makeConstraints { (maker) in
        maker.height.equalTo(Constant.screenSize.width * (365 / 357))
      }
    } else if stackTop.arrangedSubviews.count == 2 {
      stackTop.snp.makeConstraints { (maker) in
        maker.height.equalTo(Constant.screenSize.width * (174 / 357))
      }
    }
    if stackBottom.arrangedSubviews.count > 0 {
      stackBottom.snp.makeConstraints { (maker) in
        maker.height.equalTo(Constant.screenSize.width * (174 / 357))
      }
    }
  }
}
