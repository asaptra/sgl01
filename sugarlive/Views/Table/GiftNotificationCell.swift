//
//  GiftNotificationCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 12/06/22.
//

import UIKit
import SDWebImage

class GiftNotificationCell: UITableViewCell {
    
    @IBOutlet weak var levelContainerView: UIView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var thumbnailImage: SDAnimatedImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailImage.image = nil
    }
    
    func setContents(_ model: GiftDetailEventModel) {
        levelContainerView.backgroundColor = UIColor(hex: model.level?.color ?? "")
        levelLabel.text = "Lvl.\(model.level?.level ?? 0)"
        
        descriptionLabel.text = (model.sender?.fullname ?? "") + " " + (model.chat ?? "")
        
        if let url = URL(string: model.gift?.thumbnail ?? "") {
            thumbnailImage.sd_setImage(with: url, placeholderImage: nil, options: [.retryFailed], context: [.imageThumbnailPixelSize: CGSize(width: thumbnailImage.bounds.width, height: thumbnailImage.bounds.height)])
        }
    }
    
}
