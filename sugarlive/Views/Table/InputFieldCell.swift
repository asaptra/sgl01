//
//  InputFieldCell.swift
//  sugarlive
//
//  Created by msi on 15/09/21.
//

import UIKit

class InputFieldCell: UITableViewCell {
  
  @IBOutlet weak var inputTitleLabel: UILabel!
  @IBOutlet weak var inputField: UITextField!
  @IBOutlet weak var selectorButton: UIButton!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  private func setupLayout() {
    inputTitleLabel.font = .defaultSemiBoldFont(size: 12)
    inputTitleLabel.textColor = .DARK_TEXT
    inputField.font = .defaultRegularFont(size: 14)
    inputField.textColor = .black
  }
}
