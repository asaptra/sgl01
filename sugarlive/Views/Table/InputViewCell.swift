//
//  InputViewCell.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class InputViewCell: UITableViewCell {
  
  @IBOutlet weak var inputTitleLabel: UILabel!
  @IBOutlet weak var inputAreaView: BaseTextView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  private func setupLayout() {
    inputTitleLabel.font = .defaultSemiBoldFont(size: 12)
    inputTitleLabel.textColor = .DARK_TEXT
    inputAreaView.font = .defaultRegularFont(size: 16)
    inputAreaView.textColor = .black
  }
}
