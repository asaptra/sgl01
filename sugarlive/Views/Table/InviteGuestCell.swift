//
//  InviteGuestCell.swift
//  sugarlive
//
//  Created by cleanmac on 11/12/21.
//

import UIKit
import AlamofireImage

class InviteGuestCell: UITableViewCell {

    @IBOutlet weak var guestImage: UIImageView!
    @IBOutlet weak var guestNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setContents(userModel: UserModel) {
        if let imageUrl = userModel.coverPicture, let url = URL(string: imageUrl) {
            guestImage.af.setImage(withURL: url)
        } else {
            guestImage.image = UIImage(named: "ic_default_profile")
        }
        guestNameLabel.text = userModel.fullname
    }
    
}
