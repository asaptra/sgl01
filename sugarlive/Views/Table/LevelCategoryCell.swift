//
//  LevelCategoryCell.swift
//  sugarlive
//
//  Created by dodets on 26/10/21.
//

import UIKit

class LevelCategoryCell: UITableViewCell {

    @IBOutlet weak var levelTitleLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var whatLevelLabel: UILabel!
    @IBOutlet weak var upgradeLevelLabel: UILabel!
    @IBOutlet weak var userLevelLabel: UILabel!
    @IBOutlet weak var userLevelBadgeLabel: UILabel!
    @IBOutlet weak var bgLevelView: UIImageView!
    @IBOutlet weak var infoLevelView: UIView!
    
    func setupUI() {
      let attStr = NSMutableAttributedString(attributedString: "What is Level".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.PRIMARY_2]))
      attStr.append("\n1. Everytime you upgrade your level could get a more valuable label.\n2. Higher Level will help you to get more attention from broadcasters and viewers.".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]))
      
      let attStr2 = NSMutableAttributedString(attributedString: "How to upgrade your level".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.PRIMARY_2]))
      attStr2.append("\n1. You can get Exp by starting a live broadcasting now.\n2. Keep stay online and interact with broadcasters to get more Exp.\n3. Sharing Live will get more Exp.\n4. The best way to get Exp is to send more gifts. ".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12), NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]))
      
      levelTitleLabel.font = .defaultRegularFont(size: 12)
      levelTitleLabel.textColor = .init(hex: "929292")
      levelTitleLabel.text = "Level"
      levelLabel.font = .defaultBoldFont(size: 40)
      levelLabel.textColor = .init(hex: "444444")
      
      pointLabel.font = .defaultBoldFont(size: 12)
      pointLabel.textColor = .init(hex: "6964D8")
      
      whatLevelLabel.attributedText = attStr
      upgradeLevelLabel.attributedText = attStr2
      
      let font = UIFont.defaultBoldFont(size: 10)
      userLevelLabel.font = font
      userLevelLabel.textColor = .white
      userLevelLabel.text = "User Level"
      userLevelBadgeLabel.font = font
      userLevelBadgeLabel.textColor = .white
      userLevelBadgeLabel.text = "User Level Badge"
        
        bgLevelView.cornerRadius = bgLevelView.frame.width / 2
        bgLevelView.backgroundColor = .PRIMARY_2
        infoLevelView.cornerRadius = infoLevelView.frame.width / 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
