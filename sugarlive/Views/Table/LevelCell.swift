//
//  LevelCell.swift
//  sugarlive
//
//  Created by dodets on 26/10/21.
//

import UIKit

class LevelCell: UITableViewCell {

    @IBOutlet weak var userLevel: UILabel!
    @IBOutlet weak var levelBadge: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let font = UIFont.defaultBoldFont(size: 10)
        let badgeFont = UIFont.defaultBoldFont(size: 8)
        //setup
        userLevel.font = font
        userLevel.textColor = .DARK_TEXT
        userLevel.text = "User Level"
        levelBadge.titleLabel?.font = font
        levelBadge.setTitleColor(.white, for: .normal)
        
        userLevel.font = font
        levelBadge.setAttributedTitle("Level".toAttributedString(with: [NSAttributedString.Key.font: badgeFont,
                                                                      NSAttributedString.Key.foregroundColor: UIColor.white]),
                                  for: .normal)
        levelBadge.layer.cornerRadius = 10
        levelBadge.layer.masksToBounds = true
        levelBadge.setBackgroundImage(UIImage(color: .init(hex: "999999")), for: .normal)
        levelBadge.setBackgroundImage(UIImage(color: .init(hex: "999999")), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
