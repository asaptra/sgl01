//
//  LiveActivityCell.swift
//  sugarlive
//
//  Created by cleanmac on 09/11/21.
//

import UIKit

class LiveActivityCell: UITableViewCell {

    @IBOutlet weak var pillContainerView: UIView!
    @IBOutlet weak var activityLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
