//
//  LiveDetailCell.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class LiveDetailCell: UITableViewCell {
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var regLabel: UILabel!
  @IBOutlet weak var regTimeLabel: UILabel!
  @IBOutlet weak var multiguestLabel: UILabel!
  @IBOutlet weak var multiguestTimeLabel: UILabel!
  @IBOutlet weak var carrotLabel: UILabel!
  
  private let regularFont = UIFont.defaultRegularFont(size: 12)
  private let regularColor = UIColor.DARK_TEXT
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    dateLabel.font = regularFont
    dateLabel.textColor = regularColor
    
    regLabel.font = regularFont
    regLabel.textColor = regularColor
    
    regTimeLabel.font = regularFont
    regTimeLabel.textColor = regularColor
    
    multiguestLabel.font = regularFont
    multiguestLabel.textColor = regularColor
    
    multiguestTimeLabel.font = regularFont
    multiguestTimeLabel.textColor = regularColor
    
    carrotLabel.font = regularFont
    carrotLabel.textColor = regularColor
    
  }
}
