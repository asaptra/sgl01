//
//  LiveHeaderCell.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class LiveHeaderCell: UITableViewCell {
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var durationLabel: UILabel!
  @IBOutlet weak var durationDetailLabel: UILabel!
  @IBOutlet weak var carrotLabel: UILabel!
  @IBOutlet weak var carrotDetailLabel: UILabel!
  @IBOutlet weak var indicatorButton: UIButton!
  @IBOutlet weak var bottomLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    indicatorButton.superview?.isHidden = false
    bottomLabel.superview?.isHidden = true
    setupLayout()
  }
  
  func setupLayout() {
    dateLabel.font = .defaultBoldFont(size: 12)
    dateLabel.textColor = .PRIMARY_2
    
    durationLabel.font = .defaultRegularFont(size: 10)
    durationLabel.textColor = .MEDIUM_TEXT
    
    durationDetailLabel.font = .defaultBoldFont(size: 12)
    durationDetailLabel.textColor = .DARK_TEXT
    
    carrotLabel.font = .defaultRegularFont(size: 10)
    carrotLabel.textColor = .MEDIUM_TEXT
    
    carrotDetailLabel.font = .defaultBoldFont(size: 12)
    carrotDetailLabel.textColor = .DARK_TEXT
    
    bottomLabel.font = .defaultRegularFont(size: 12)
    bottomLabel.textColor = .DARK_TEXT
  }
}
