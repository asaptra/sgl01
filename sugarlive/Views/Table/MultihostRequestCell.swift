//
//  MultihostRequestCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 09/03/22.
//

import UIKit
import AlamofireImage

class MultihostRequestCell: UITableViewCell {
    
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var approveView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupContents(order: Int, model: UserMultihostJoinModel, isHost: Bool) {
        orderLabel.text = "\(order)"
        if let url = URL(string: model.coverPicture ?? "") {
            userImage.af.setImage(withURL: url)
        }
        usernameLabel.text = model.fullname
        approveView.isHidden = !isHost
    }
    
}
