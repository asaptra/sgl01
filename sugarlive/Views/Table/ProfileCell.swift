//
//  ProfileCell.swift
//  sugarlive
//
//  Created by msi on 12/09/21.
//

import UIKit

class ProfileCell: UITableViewCell {
  
  enum ProfileCellType {
    case Profile
    case NoBadge
    case Badge
    case Fans
    case Detail
    case SocialMedia
    case Setting
    case Blocked
    case Notification
    case Info
    case InfoDetail
    case Default
  }
  
  @IBOutlet weak var profileImageView: UIImageView!
  @IBOutlet weak var topLabel: UILabel!
  @IBOutlet weak var subtitleLabel: UILabel!
  @IBOutlet weak var badgeStackView: UIStackView!
  @IBOutlet weak var rightLabel: UILabel!
  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var separatorView: UIView!
    
  var profileCellType: ProfileCellType = .Default {
    didSet {
      setupLayout()
    }
  }
  
  private let grayColor = UIColor.DARK_TEXT
  private let boldTitleFont = UIFont.defaultBoldFont(size: 12)
  private let regularTitleFont = UIFont.defaultRegularFont(size: 12)
  
  override func awakeFromNib() {
    super.awakeFromNib()
//    setupLayout()
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    profileCellType = .Default
  }
  
  func setupLayout() {
    resetContent()
    
    profileImageView.layer.cornerRadius = profileImageView.frame.width/2
    profileImageView.layer.masksToBounds = true
    
    switch profileCellType {
    case .Profile, .Blocked:
      topLabel.superview?.isHidden = true
      badgeStackView.superview?.isHidden = profileCellType == .Profile
      rightLabel.superview?.isHidden = true
      nextButton.superview?.isHidden = true
      subtitleLabel.font = .defaultRegularFont(size: 12)
      break
    case .NoBadge, .Badge, .Fans:
      profileImageView.superview?.isHidden = true
      topLabel.superview?.isHidden = true
      badgeStackView.superview?.isHidden = [ProfileCellType.NoBadge, ProfileCellType.Fans].contains(profileCellType)
      rightLabel.superview?.isHidden = profileCellType != .Fans
      nextButton.superview?.isHidden = profileCellType == .Fans
      subtitleLabel.font = .defaultRegularFont(size: 12)
      subtitleLabel.textColor = grayColor
      break
    case .Detail:
      profileImageView.superview?.isHidden = true
      badgeStackView.superview?.isHidden = true
      rightLabel.superview?.isHidden = true
      nextButton.superview?.isHidden = true
      topLabel.font = boldTitleFont
      topLabel.textColor = grayColor
      subtitleLabel.font = regularTitleFont
      subtitleLabel.textColor = .init(hex: "434343")
      break
    case .SocialMedia:
      profileImageView.superview?.isHidden = true
      topLabel.superview?.isHidden = true
      rightLabel.superview?.isHidden = true
      subtitleLabel.font = boldTitleFont
      subtitleLabel.textColor = grayColor
      break
    case .Setting:
      topLabel.superview?.isHidden = true
      badgeStackView.superview?.isHidden = true
      rightLabel.superview?.isHidden = true
      nextButton.superview?.isHidden = true
      subtitleLabel.font = .defaultRegularFont(size: 12)
      subtitleLabel.textColor = grayColor
      break
    case .Notification:
      profileImageView.superview?.isHidden = false
      topLabel.superview?.isHidden = false
      subtitleLabel.superview?.isHidden = false
      badgeStackView.superview?.isHidden = true
      rightLabel.superview?.isHidden = true
      nextButton.superview?.isHidden = true
      subtitleLabel.font = .defaultSemiBoldItalicFont(size: 12)
      subtitleLabel.textColor = .MEDIUM_TEXT
      break
    case .Info, .InfoDetail:
      profileImageView.superview?.isHidden = true
      topLabel.superview?.isHidden = false
      subtitleLabel.superview?.isHidden = false
      badgeStackView.superview?.isHidden = true
      rightLabel.superview?.isHidden = true
      nextButton.superview?.isHidden = true
      if profileCellType == .Info {
        nextButton.superview?.isHidden = false
        nextButton.setImage(UIImage(named: "ic_bottom_arrow_gray"), for: .normal)
        nextButton.setImage(UIImage(named: "ic_top_arrow_gray"), for: .selected)
      }
      break
    default:
      profileImageView.superview?.isHidden = false
      topLabel.superview?.isHidden = false
      subtitleLabel.superview?.isHidden = false
      badgeStackView.superview?.isHidden = false
      rightLabel.superview?.isHidden = false
      nextButton.superview?.isHidden = false
      topLabel.textColor = .black
      subtitleLabel.textColor = .black
      rightLabel.textColor = .black
      break
    }
    
    rightLabel.font = regularTitleFont
    rightLabel.textColor = grayColor
      rightLabel.font = .defaultRegularFont(size: 10)
    
  }
  
  private func resetContent() {
    profileImageView.image = nil
    topLabel.text = ""
    subtitleLabel.text = ""
    _ = badgeStackView.arrangedSubviews.map{ $0.removeFromSuperview() }
    rightLabel.text = ""
    nextButton.setImage(UIImage(named: "ic_right_arrow_gray"), for: .normal)
  }
  
//  MARK: Custom
  func setTitleNotification(boldStr: String, regularStr: String) {
    let attStr = NSMutableAttributedString(attributedString: boldStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                                               NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]))
    attStr.append(regularStr.toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 12),
                                                       NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT]))
    topLabel.attributedText = attStr
  }
  
  func bind(notification: NotificationModel) {
    if let urlString = notification.additionalData?.coverPicture, let url = URL(string: urlString) {
      profileImageView.af.setImage(withURL: url)
    } else {
      profileImageView.image = UIImage(named: "ic_default_profile")
    }
    setTitleNotification(boldStr: "", regularStr: notification.content ?? "")
    subtitleLabel.text = notification.createdAt
    backgroundColor = notification.read ? .white : .init(hex: "F1F2FF")
  }
  
  func bind(user: UserModel) {
    subtitleLabel.text = user.fullname
    if let urlString = user.userPictureUrl, let url = URL(string: urlString) {
      profileImageView.af.setImage(withURL: url)
    } else {
      profileImageView.image = UIImage(named: "ic_default_profile")
    }
  }
}
