//
//  ProfileDetailCell.swift
//  sugarlive
//
//  Created by msi on 12/09/21.
//

import UIKit

class ProfileDetailCell: UITableViewCell {
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileBorderImageView: UIImageView!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var levelButton: UIButton!
    @IBOutlet weak var verifiedBadgeImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var followerCountLabel: UILabel!
    @IBOutlet weak var followerLabel: UILabel!
    @IBOutlet weak var carrotCountLabel: UILabel!
    @IBOutlet weak var carrotLabel: UILabel!
    @IBOutlet weak var candyCountLabel: UILabel!
    @IBOutlet weak var candyLabel: UILabel!
    @IBOutlet weak var badgeContainerView: UIView!
    @IBOutlet weak var badgeStackView: UIStackView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var candyContainer: UIView!
    
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var messageBtn: UIButton!
    @IBOutlet weak var followMessageContainer: UIView!
    
    private let counterFont = UIFont.defaultBoldFont(size: 12)
    private let counterColor = UIColor.PRIMARY_1
    private let labelFont = UIFont.defaultRegularFont(size: 10)
    private let labelColor = UIColor.init(hex: "434343")

    var editTapped: (_ sender: Any) -> Void = {_ in }
    var followingTapped: (_ sender: Any) -> Void = {_ in }
    var followerTapped: (_ sender: Any) -> Void = {_ in }
  
    var followUser: () -> Void = {}
    var messageUser: () -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
  
    func setupLayout() {
        profileImageView.layer.cornerRadius = profileImageView.frame.width/2
        profileImageView.layer.masksToBounds = true

        usernameLabel.font = .defaultBoldFont(size: 14)
        usernameLabel.textColor = .white
        idLabel.font = .defaultBoldFont(size: 11)
        idLabel.textColor = .white

        locationLabel.textColor = .white

        levelButton.layer.cornerRadius = 12
        levelButton.layer.masksToBounds = true

        followingCountLabel.font = counterFont
        followingCountLabel.textColor = counterColor
        followingLabel.font = labelFont
        followingLabel.textColor = labelColor
        followingLabel.text = "Following"

        followerCountLabel.font = counterFont
        followerCountLabel.textColor = counterColor
        followerLabel.font = labelFont
        followerLabel.textColor = labelColor
        followerLabel.text = "Followers"

        carrotCountLabel.font = counterFont
        carrotCountLabel.textColor = counterColor
        carrotLabel.font = labelFont
        carrotLabel.textColor = labelColor
        carrotLabel.text = "Carrots"

        candyCountLabel.font = counterFont
        candyCountLabel.textColor = counterColor
        candyLabel.font = labelFont
        candyLabel.textColor = labelColor
        candyLabel.text = "Candy"
        
        followBtn.titleLabel?.font = counterFont
        messageBtn.titleLabel?.font = counterFont

        bottomContainerView.layer.cornerRadius = 10
        bottomContainerView.layer.masksToBounds = true
        borderView.applyCardView(cornerRadius: 10, shadowOffsetWidth: 2, shadowOffsetHeight: 2, shadowColor: UIColor.gray, shadowOpacity: 0.5)
    }
  
    func bind(user: UserModel) {
        if let urlString = user.userPictureUrl, let url = URL(string: urlString) {
            profileImageView.af.setImage(withURL: url)
        } else {
            profileImageView.image = UIImage(named: "ic_default_profile")
        }
    
        if let urlString = user.avatarFrames?.defaultFrame?.avatarIcon, let url = URL(string: urlString) {
            profileBorderImageView.af.setImage(withURL: url)
        } else {
            profileBorderImageView.image = nil
        }

        if let urlString = user.coverPicture, let url = URL(string: urlString) {
            backgroundImageView.af.setImage(withURL: url)
        } else {
            backgroundImageView.image = UIImage(named: "bg_profile_picture")
        }
    
        levelButton.backgroundColor = .init(hex: user.level?.color ?? "ffffff")
        levelButton.setTitleColor(.init(hex: user.level?.colorText ?? "ffffff"), for: .normal)
        levelButton.setTitle("\(user.level?.level ?? 0)", for: .normal)
        levelButton.titleLabel?.font =  .defaultFont(size: 10)
        verifiedBadgeImageView.isHidden = !user.verified
        usernameLabel.text = user.fullname
        idLabel.text = "ID : \(user.sugarId ?? "-")"
      
        var locationAttachment = NSTextAttachment()
        if #available(iOS 13.0, *) {
            locationAttachment = NSTextAttachment(image: UIImage(named: "ic_map_pin_white")!)
        } else {
            locationAttachment.image = UIImage(named: "ic_map_pin_white")
        }
        let locationAttstr = NSMutableAttributedString(attachment: locationAttachment)
        locationAttstr.append("  \(user.hometown ?? "-")".toAttributedString(with: [NSAttributedString.Key.font:UIFont.defaultRegularFont(size: 11), NSAttributedString.Key.foregroundColor: UIColor.white]))
        locationLabel.attributedText = locationAttstr
        followingCountLabel.text = "\(user.followingCount.roundedWithAbbreviations())"
        followerCountLabel.text = "\(user.followerCount.roundedWithAbbreviations())"
        carrotCountLabel.text = "\(Int(user.totalCarrot).roundedWithAbbreviations())"
        candyCountLabel.text = "\(Int(user.coin).roundedWithAbbreviations())"
        
        _ = badgeStackView.arrangedSubviews.map{ $0.removeFromSuperview() }
        if let badges = user.badges?.active {
            badgeContainerView.isHidden = false
            for badge in badges {
                let imageView = UIImageView()
                let url = URL(string: badge.url ?? "") ?? URL(fileURLWithPath: "")
                imageView.snp.makeConstraints { (maker) in
                    maker.height.width.equalTo(25)
                }
                imageView.af.setImage(withURL: url)
                badgeStackView.addArrangedSubview(imageView)
            }
        } else {
            badgeContainerView.isHidden = true
        }
        
        followBtn.addTarget(self, action: #selector(followUserAction), for: .touchUpInside)
        messageBtn.addTarget(self, action: #selector(messageUserAction), for: .touchUpInside)

    }
  
//  MARK: Action
  @IBAction func editAction(_ sender: Any) {
    editTapped(sender)
  }
  
  @IBAction func followingAction(_ sender: Any) {
    followingTapped(sender)
  }
  
  @IBAction func followerAction(_ sender: Any) {
    followerTapped(sender)
  }
    
    @objc func followUserAction() {
      followUser()
    }
    
    @objc func messageUserAction() {
      messageUser()
    }
}
