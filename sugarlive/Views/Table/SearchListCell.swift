//
//  SearchListCell.swift
//  sugarlive
//
//  Created by dodets on 01/10/21.
//

import UIKit

class SearchListCell: UITableViewCell {

  @IBOutlet weak var profileImageView: UIImageView!
  @IBOutlet weak var profileView: UIView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var levelLbl: UILabel!
  @IBOutlet weak var levelView: UIView!
  @IBOutlet weak var addressLbl: UILabel!
  @IBOutlet weak var containerView: UIView!
    
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    profileView.layer.cornerRadius = 10
    profileView.layer.masksToBounds = true
    
    levelView.layer.cornerRadius = 10
    levelView.layer.masksToBounds = true
    
    containerView.layer.cornerRadius = 10
    containerView.layer.masksToBounds = true
    
    containerView.layer.borderWidth = 1
    containerView.layer.borderColor = UIColor.BORDER_LIST.cgColor
    
    nameLabel.font = .defaultRegularFont(size: 14)
    nameLabel.textColor = .DARK_TEXT
    
    addressLbl.font = .defaultRegularFont(size: 12)
    addressLbl.textColor = .DARK_TEXT
    
    levelLbl.font = .defaultRegularFont(size: 10)
    levelLbl.textColor = .white
  }
}
