//
//  SocialMediaCell.swift
//  sugarlive
//
//  Created by msi on 15/09/21.
//

import UIKit

class SocialMediaCell: UITableViewCell {
  
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var inputField: BaseTextField!
  @IBOutlet weak var showButton: UIButton!
  
  var textFieldDidEndHandler: ((_ sender: Any) -> Void)?
  var showTapped: (_ sender: Any) -> Void = {_ in }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  private func setupLayout() {
//    inputField.delegate = self
    inputField.font = .defaultRegularFont(size: 14)
    inputField.textColor = .MEDIUM_TEXT
    inputField.attributedPlaceholder = "@youraccount".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultRegularFont(size: 14),
                                                                                NSAttributedString.Key.foregroundColor: UIColor.MEDIUM_TEXT])
    inputField.textFieldHandler = {(sender) in
      switch sender {
      case .Valid:
        if let handler = self.textFieldDidEndHandler {
          handler(self.inputField)
        }
        break
      default:
        break
      }
    }
    
    showButton.layer.borderWidth = 1
    showButton.layer.borderColor = UIColor.init(hex: "DBDBDB").cgColor
    showButton.layer.cornerRadius = showButton.frame.height/2
    showButton.layer.masksToBounds = true
    setActive()
  }
  
  func setActive(_ value: Bool = false) {
    showButton.setAttributedTitle("Show on live".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                           NSAttributedString.Key.foregroundColor: value ? UIColor.white : UIColor.MEDIUM_TEXT]),
                                  for: .normal)
    showButton.backgroundColor = value ? .PRIMARY_2 : .white
    showButton.isSelected = value
  }
  
  @IBAction func showAction(_ sender: Any) {
    setActive(!showButton.isSelected)
    showTapped(sender)
  }
}

extension SocialMediaCell: UITextFieldDelegate {
  func textFieldDidEndEditing(_ textField: UITextField) {
    if let handler = textFieldDidEndHandler {
      handler(textField)
    }
  }
}
