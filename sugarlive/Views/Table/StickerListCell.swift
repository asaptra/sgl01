//
//  StickerListCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 03/05/22.
//

import UIKit

protocol StickerListCellDelegate: AnyObject {
    func didSelectItem(_ package: StickerPackageModel)
    func didSelectItem(_ sticker: StickerModel)
}

class StickerListCell: UITableViewCell {
    
    enum StickerType {
        case Package
        case Sticker
    }

    @IBOutlet weak var listCollectionView: UICollectionView!
    
    private var packages: [StickerPackageModel] = []
    private var stickers: [StickerModel] = []
    
    private var stickerType: StickerType!
    
    weak var delegate: StickerListCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        listCollectionView.register(UINib(nibName: "StickerCell", bundle: nil), forCellWithReuseIdentifier: "StickerCell")
        listCollectionView.delegate = self
        listCollectionView.dataSource = self
    }
    
    func setContents(packages: [StickerPackageModel]) {
        stickerType = .Package
        self.packages = packages
        listCollectionView.reloadData()
    }
    
    func setContents(stickers: [StickerModel]) {
        stickerType = .Sticker
        self.stickers = stickers
        listCollectionView.reloadData()
    }
    
}

extension StickerListCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        stickerType == .Package ? packages.count : stickers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath) as! StickerCell
        stickerType == .Package ? cell.setImage(with: packages[indexPath.row].icon ?? "") : cell.setImage(with: stickers[indexPath.row].sticker ?? "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .init(width: frame.height, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if stickerType == .Package {
            delegate?.didSelectItem(packages[indexPath.row])
        } else {
            delegate?.didSelectItem(stickers[indexPath.row])
        }
    }
}
