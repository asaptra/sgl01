//
//  StoreCell.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class StoreCell: UITableViewCell {
  
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var previewImage: UIImageView!
  @IBOutlet weak var itemLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    itemLabel.font = .defaultRegularFont(size: 14)
  }
}
