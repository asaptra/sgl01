//
//  TopSpenderCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 07/03/22.
//

import UIKit
import AlamofireImage

class TopSpenderCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelContainerView: UIView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var carrotLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupContents(model: TopSpenderModel) {
        containerView.applyCardView()
        
        if let url = URL(string: model.coverPicture ?? "") {
            profileImage.af.setImage(withURL: url)
        }
        
        nameLabel.text = model.fullname
        levelContainerView.layer.cornerRadius = 10
        levelContainerView.backgroundColor = UIColor(hex: model.levelColor ?? "")
        levelLabel.textColor = UIColor(hex: model.levelColorText ?? "")
        levelLabel.text = "\(model.level ?? 0)"
        
        carrotLabel.text = model.spend ?? "0"
    }
    
}
