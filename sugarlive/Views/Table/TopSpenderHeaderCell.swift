//
//  TopSpenderHeaderCell.swift
//  sugarlive
//
//  Created by cleanmac-ada on 02/06/22.
//

import UIKit

class TopSpenderHeaderCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var carrotLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupContents(model: TopSpenderModel) {
        containerView.applyCardView()
        
        profileImage.layer.borderColor = UIColor(hex: "#FD9801").cgColor
        profileImage.layer.borderWidth = 5
        
        if let url = URL(string: model.coverPicture ?? "") {
            profileImage.af.setImage(withURL: url)
        }
        
        nameLabel.text = model.fullname
        carrotLabel.text = model.spend ?? "0"
    }
    
}
