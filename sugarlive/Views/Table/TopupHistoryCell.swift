//
//  TopupHistoryCell.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class TopupHistoryCell: UITableViewCell {
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var typeLabel: UILabel!
  @IBOutlet weak var descLabel: UILabel!
  @IBOutlet weak var numberLabel: UILabel!
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var nominalLabel: UILabel!
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    dateLabel.font = .defaultRegularFont(size: 10)
    dateLabel.textColor = .MEDIUM_TEXT
    
    typeLabel.font = .defaultBoldFont(size: 12)
    typeLabel.textColor = .DARK_TEXT
    
    descLabel.font = .defaultRegularFont(size: 10)
    descLabel.textColor = .MEDIUM_TEXT
    
    numberLabel.font = .defaultRegularFont(size: 10)
    numberLabel.textColor = .MEDIUM_TEXT
    
    statusLabel.font = .defaultBoldFont(size: 12)
    
    nominalLabel.font = .defaultBoldFont(size: 12)
    nominalLabel.textColor = .DARK_TEXT
  }
}
