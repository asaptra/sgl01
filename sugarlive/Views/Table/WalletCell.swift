//
//  WalletCell.swift
//  sugarlive
//
//  Created by msi on 16/09/21.
//

import UIKit

class WalletCell: UITableViewCell {
  
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var topLabel: UILabel!
  @IBOutlet weak var bottomLabel: UILabel!
  @IBOutlet weak var rightButton: UIButton!
  @IBOutlet weak var leftButton: UIButton!
  @IBOutlet weak var actionContainerView: UIView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setupLayout()
  }
  
  func setupLayout() {
    containerView.layer.cornerRadius = 10
    containerView.layer.borderColor = UIColor.init(hex: "C9C9C9").cgColor
    containerView.layer.borderWidth = 1
    containerView.layer.masksToBounds = true
    
    iconImageView.layer.cornerRadius = iconImageView.frame.height/2
    iconImageView.layer.masksToBounds = true
    
    topLabel.font = .defaultBoldFont(size: 20)
    topLabel.textColor = .DARK_TEXT
    bottomLabel.font = .defaultRegularFont(size: 12)
    bottomLabel.textColor = .DARK_TEXT
    
    leftButton.backgroundColor = .init(hex: "0FA85C")
    leftButton.setAttributedTitle("Top Up".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                      NSAttributedString.Key.foregroundColor: UIColor.white]),
                                   for: .normal)
    leftButton.layer.cornerRadius = 5
    leftButton.layer.masksToBounds = true
    
    rightButton.setAttributedTitle("History".toAttributedString(with: [NSAttributedString.Key.font: UIFont.defaultBoldFont(size: 12),
                                                                      NSAttributedString.Key.foregroundColor: UIColor.PRIMARY_1]),
                                  for: .normal)
    
  }
}
