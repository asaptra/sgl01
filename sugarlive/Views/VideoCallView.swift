//
//  VideoCallView.swift
//  sugarlive
//
//  Created by cleanmac on 07/12/21.
//

import UIKit
import AlamofireImage
import ZegoExpressEngine

@IBDesignable class VideoCallView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var streamContainerView: UIView!
    @IBOutlet weak var streamCanvasView: UIView!
    @IBOutlet weak var guestPictureImage: UIImageView!
    @IBOutlet weak var guestNameLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var videoStateImage: UIImageView!
    @IBOutlet weak var audioStateImage: UIImageView!
    
    @IBOutlet weak var inviteIndicatorImage: UIImageView!
    
    @IBOutlet weak var pulseView: UIView!
    @IBOutlet weak var countdownLabel: UILabel!
    
    private var pulseLayers = [CAShapeLayer]()
    
    private var MAX_INVITE_DURATION: Int = 60
    private var countdownDuration: Int = 0
    private var countdownTimer: Timer?
    
    var guestModel: UserModel? {
        didSet {
            streamContainerView.isHidden = (guestModel == nil)
            if guestModel != nil {
                guestNameLabel.text = guestModel?.fullname
                setGuestImage()
                resetCountdownTimer()
                stopPulseAnimation()
            }
        }
    }
    
    var streamingType: MultiguestStreamingType?
    
    var videoState: AudioVideoState = .Mute {
        didSet {
            guestPictureImage.isHidden = videoState == .Unmute
            streamCanvasView.isHidden = videoState == .Mute
            videoStateImage.image = videoState == .Mute ? UIImage(named: "ic_video_mute") : UIImage(named: "ic_video_unmute")
        }
    }
    
    var audioState: AudioVideoState = .Unmute {
        didSet {
            audioStateImage.image = audioState == .Mute ? UIImage(named: "ic_audio_mute") : UIImage(named: "ic_audio_unmute")
        }
    }
    
    var isCloseButtonHidden: Bool = false {
        didSet {
            closeButton.isHidden = isCloseButtonHidden
        }
    }
    
    var closeTapped: (_: UserModel, _: MultiguestStreamingType) -> Void = { _, _ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        streamContainerView.layer.masksToBounds = true
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = VideoCallView.nib(withType: VideoCallView.self)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        streamContainerView.layer.masksToBounds = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        streamContainerView.layer.masksToBounds = true
    }
    
    override func awakeAfter(using coder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            let view = VideoCallView.nib(withType: VideoCallView.self)
            view.translatesAutoresizingMaskIntoConstraints = false
            let constraints = self.constraints
            self.removeConstraints(constraints)
            view.addConstraints(constraints)
            return view
        }
        return self
    }
    
    private func setGuestImage() {
        if let urlString = guestModel?.userPictureUrl {
            let url = URL(string: urlString)!
            guestPictureImage.af.setImage(withURL: url)
        } else if let urlString = guestModel?.coverPicture {
            let url = URL(string: urlString)!
            guestPictureImage.af.setImage(withURL: url)
        } else {
            guestPictureImage.image = UIImage(named: "ic_default_profile")
        }
    }
    
    func startInviteCountdown() {
        countdownDuration = MAX_INVITE_DURATION
        countdownLabel.text = "60"
        countdownLabel.isHidden = false
        inviteIndicatorImage.isHidden = true
        
        pulseView.isHidden = false
        createPulseLayers()
        
        countdownTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
            if self.countdownDuration == 0 {
                self.resetCountdownTimer()
                self.stopPulseAnimation()
            } else {
                self.countdownDuration -= 1
                self.countdownLabel.text = "\(self.countdownDuration)"
            }
        })
        isUserInteractionEnabled = false
    }
    
    func resetCountdownTimer() {
        countdownTimer?.invalidate()
        countdownTimer = nil
        countdownLabel.isHidden = true
        inviteIndicatorImage.isHidden = false
        isUserInteractionEnabled = true
    }
    
    func createPulseLayers() {
        for _ in 0...2 {
            let circularPath = UIBezierPath(arcCenter: .zero, radius: 50, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
            let pulseLayer = CAShapeLayer()
            pulseLayer.path = circularPath.cgPath
            pulseLayer.lineWidth = 2.0
            pulseLayer.fillColor = UIColor.clear.cgColor
            pulseLayer.lineCap = CAShapeLayerLineCap.round
            pulseLayer.position = CGPoint(x: pulseView.frame.size.width/2.0, y: pulseView.frame.size.width/2.0)
            pulseView.layer.addSublayer(pulseLayer)
            pulseLayers.append(pulseLayer)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.animatePulseLayer(index: 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.animatePulseLayer(index: 1)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.animatePulseLayer(index: 2)
                }
            }
        }
    }
    
    func animatePulseLayer(index: Int) {
        pulseLayers[index].fillColor = UIColor.PRIMARY_1.cgColor
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 2.0
        scaleAnimation.fromValue = 0.0
        scaleAnimation.toValue = 0.9
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        scaleAnimation.repeatCount = .greatestFiniteMagnitude
        pulseLayers[index].add(scaleAnimation, forKey: "scale")
        
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.duration = 2.0
        opacityAnimation.fromValue = 0.9
        opacityAnimation.toValue = 0.0
        opacityAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        opacityAnimation.repeatCount = .greatestFiniteMagnitude
        pulseLayers[index].add(opacityAnimation, forKey: "opacity")
    }
    
    func stopPulseAnimation() {
        pulseView.isHidden = true
        pulseView.layer.sublayers?.forEach({ $0.removeAllAnimations() })
        pulseView.layer.sublayers?.removeAll()
        pulseLayers.removeAll()
        layoutIfNeeded()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        closeTapped(guestModel!, streamingType!)
    }
    
}
